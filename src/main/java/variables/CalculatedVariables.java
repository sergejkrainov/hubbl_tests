package variables;

import org.junit.runner.Result;
import org.openqa.selenium.WebDriver;
import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * ����� ��� �������� ����������, � ������� �������� ��������, ������������ � �������� ���������� �����
 * @author sbt-kraynov-sa
 *
 */
public class CalculatedVariables {
	
	public static LinkedHashMap<String, String> actualValues = new LinkedHashMap<String, String>();
	public static LinkedHashMap<String, String> expectedValues = new LinkedHashMap<String, String>();
	public static HashMap<String, String> configValues = new HashMap<String, String>();
	public static HashMap<String, String> statusResultTest = new HashMap<String, String>();
	
	public static String xmlResponse = "";
	
	public static String xmlMessage = "";
	public static Document xmlDOMMessage;
	
	public static boolean isTestFailed = false;
	public static boolean isError = false;
	public static boolean isRunFromConsole = false;
	
	public static String startTime = "";
	
	public static String testOperation = "";
	public static String testCase = "";
	
	public static String testUUID = "";
	public static String webServiceMethodName = "";
	public static String pathToCopyResults = "";
	public static String realLogNameFromRemoteServer = "";
	public static String stringAreaForCheckLogs = "";
	public static String rqUID = "";
	public static String operUID = "";
	public static String operUIDgetCardHolderRq = "";	
	public static String rQTm = "";
	public static String mainApplRegNumber = "";
	public static String acctId = "";
	public static String extDt = "";
	public static String issueDate = "";
	
	public static String idSeries = "";
	public static String idNum = "";
	public static String lastName = "";
	public static String firstName = "";
	public static String middleName = "";
	
	public static String Id_client = "";
	
	public static String mailAccount = "";
	
	public static String cardNumber = "";

	public static String logName = "";
	
	public static String rowCounts = "";
	
	public static String moduleFileToLoadCOD = "/osbtmp/dpcappl/WS_test/";
	
	public static Result testResult = null;
	
	public static WebDriver webDriver;
	
	public static String PERSON_MAJOR = "";
	
	public static String PERSON_MINOR = "";
	
	public static String way4rqUID = "";
	
	public static String way4operUID = "";
	
	public static String way4rQTm = "";
	
	public static String testName;
	
	public static String runTestClassName;
	
	public static String testCase2;
	
	public static String testRqUID;
	
	public static String createProductPackageRs0;
	
	public static String createProductPackageRs99;
	
	public static String rqUIDgetCardHolderRq;
	
	public static String rQTmgetCardHolderRq;
	
	public static String testCaseRun;
	
	public static String TechnoUID;
	
	public static String operUID1;
	
	public static String operUID2;
	
	public static String operUID3;
	
	public static String operUID4;


}
