package variables;

import java.util.Properties;

public class Config {

    public static String runDirPath = System.getProperty("user.dir");//����������, �� ������� ����������� �������;

    public static String testOperation = "Card2payment";
	public static String testCase = "Card2paymentSmoke";
	
	public static String reportsPath = "";
	
	public static String resultsPath = "";
	public static String resultsDir = "";
	public static String resultsPathCommon = "";
	public static String configPath = "\\src\\test\\resources\\hubbl\\csv\\config\\";
	public static String logsPath = "";
	
	public static String xmlOutPath = "";
	public static String xmlInPath = "";
	
	public static final String XLS_PATH = "src\\test\\resources\\hubbl\\xls\\";
	
	public static String wsdlPathCOD = "DepoCOD.wsdl";
	public static String xsdPath = "";
	public static String hostToWebServicesCOD = "http://10.68.16.78:2501/";
	public static String testStandName = "st";
	public static String remoteServerLogsCOD = "/osbwork/dpcappl/WebServis/dbg1/";
	public static String remoteServerLoginCOD = "dpcappl";
	public static String remoteServerPasswordCOD = "dpc123";
	public static String remoteServerHostCOD = "10.68.16.78";
	
	public static String BDHostCOD = "10.116.90.175";
	public static String BDPortCOD = "1521";
	public static String BDServiceNameCOD = "CODXIM01";
	public static String BDLoginCOD = "dbaosb";
	public static String BDPasswordCOD = "dbaosb";
	
	public static String BDHostHUBBL = "10.116.92.156";
	public static String BDPortHUBBL = "1521";
	public static String BDServiceNameHUBBL = "pamtxs01";
	public static String BDLoginHUBBL = "dbaosb";
	public static String BDPasswordHUBBL = "dbaosb";
	
	public static String mqHost = "SBT-OTRP-002.ca.sbrf.ru";
	public static String mqPort	= "1500";
	public static String mqQueueManager	= "HBL.QM";
	public static String mqChannel = "CHNL.HBL";
	
	public static String smtpHost = "SMTP.SBRF.RU";
	public static String smtpPort = "25";
	public static String smtpLogin = "sbt-avtotest";
	public static String smtpPassword =	"avtotest5678";
	public static String mailTo	= "SBT-Kraynov-SA@mail.ca.sbrf.ru";
	public static String isSendResultsToMail = "true";
	
	public static String isIssueFpp	= "false";
	public static String addminHubblPath ="http://sbt-ot-125:9080/HubBLWeb/Controller";
	
	public static String addminHubblDomain = "ALPHA";
	public static String addminHubblLogin = "oper";
	public static String addminHubblPassword = "oper";
	
	public static String webServiceMethodCOD = "createForm190ForCharge";
	
	public static String remoteModulePathCOD = "";
	
	public static String logsHUBBL = "";
	
	public static Properties runTestSuiteProp = new Properties();
	public static Properties stendProp = new Properties();
	public static Properties businessProcessProp = new Properties();
	
	public static Properties allProp = new Properties();
	
	public static String  screenManagerPath = "http://10.116.178.237/";
	public static String  screenManagerScreenId = "5832da877a18d4418de952be";
	public static String  screenManagerPassword = "avto2345^&*(";
	
	public static int  ODay = 0;
	
	//businessProcessProps
	public static String  OperDay = "";
	public static String  OperatorInfoOperatorLogin = "";
	public static String  OperatorInfoOperatorCode = "";
	public static String  OperatorInfoOperatorName = "";
	public static String  IssueBankInfoBranchId = "";
	public static String  IssueBankInfoAgencyId = "";
	public static String  IssueBankInfoRegionId = "";
	public static String  IssueBankInfoRbTbBrchId = "";
	public static String  DeliveryBankInfoBranchId = "";
	public static String  DeliveryBankInfoAgencyId = "";
	public static String  DeliveryBankInfoRegionId = "";
	public static String  DeliveryBankInfoRbTbBrchId = "";
	public static String  OperatorInfoOperatorLogin2 = "";
	public static String  OperatorInfoOperatorCode2 = "";
	public static String  OperatorInfoOperatorName2 = "";
	
	public static String  isJenkinsRun = "";
	public static String  isRunFromCSV = "";

	public static String DeliveryBankInfoBranchId22;
	public static String DeliveryBankInfoAgencyId22;
	public static String DeliveryBankInfoRegionId22;
	public static String DeliveryBankInfoRbTbBrchId22;
	public static String OperDay22;
	public static String OperatorInfoOperatorLogin22;
	public static String OperatorInfoOperatorCode22;
	public static String OperatorInfoOperatorName22;
	
	public static final String  MasterCardStandart = "5469";

	

}
