package variables;

import libs.DataBaseLib;

import java.util.HashMap;

public class QuerryGetParams {
	
	/**
	 *
	 * @param testClassName - �������� ������ ��� ������������
	 * @param testCaseName - �������� ���������
	 * @param paramNames - ������ ����������� ���������� ��� �������
	 * @param rowNumber - ����� ������ � ���������� ������� 
	 * @return sQuerry
	 */
	public static String getSquerryGetParams(String testClassName, String testCaseName, String paramNames, int rowNumber, int idMega){
		
		String sQuerry = "";
		String additionalParams = "";
		String depositKind = "";
		switch(testClassName){
		
			case "Card2payment":
				switch(testCaseName){
					case "Card2paymentNotNominal":
						depositKind = " d.kind between 50 and 56 ";
					break;
					
				}
				sQuerry = "select " + paramNames + " from ( select rownum as rwnm, to_char(trunc(o.day), 'yyyy-mm-dd') as OperDate, d.branchno as Osb, d.office as Filial,\n"
						+ " �p.surname as SurName, p.firstname as Name, p.secondname as SecondName, d.id_major as DepositMajor, d.id_minor as DepositMinor, c.textembose as EmbossedText, �to_char(trunc(c.enddate),\n"
						+ " 'yyyy-mm-dd') as CardEndDate, c.numcard as CardNumber, d.balance as Balance � � � � � \n"
						+ "from client.person p, deposit.deposit d, operday.dpcoperday o, deposit.dcard c � � � � � \n"
						+ "where c.id_mega = 38  and (c.blockcode = 0 or c.blockcode is null)\n"
						+ " and c.enddate > sysdate and d.id_mega = 38 and c.deposit_major = d.id_major �and c.textembose is not null \n"
						+ "and c.deposit_minor = d.id_minor and " + depositKind + " and d.state = 0 \n"
						+ "and d.currency = 810 and d.balance > 100 and p.id_mega = 38 and d.person_major = p.id_major �\n"
						+ "and d.person_minor = p.id_minor and o.id_mega = 38 and o.state = 20 and rownum <= 5 and c.numcard is not null) \n"
						+ "where rwnm = (select (ROUND(DBMS_RANDOM.value(1,5))) from dual)";
				break;
			
			case "Acc2acc":
				additionalParams = getAdditionalParams(testCaseName, idMega);
				sQuerry = createQuerryGetAccount(paramNames, additionalParams, rowNumber, idMega);
		}
		
		return sQuerry;
		
	}
	
	/**
	 * ������� ����������� ������� �� ���������� ������ �� �������� �� ����� 
	 * @param paramNames - ������ ���������� ����������
	 * @param additionalParams - �������������� ������� �������
	 * @param rowNumber - ����� ���������� ������
	 * @param idMega - id ��������� �����
	 * @return
	 */
	public static String createQuerryGetAccount(String paramNames, String additionalParams, int rowNumber, int idMega){
		
		rowNumber++;
		String existsFromIrbis = "";
		/*if(!(additionalParams.contains("and d.kind = 61") || additionalParams.contains("and d.kind <> 98") || 
				additionalParams.contains("and d.kind = 93"))){
			existsFromIrbis = " and EXISTS (select 1 \n"
					+ "             from irbis.t4_qvb q, irbis.t4_qvb_pargr pq \n"
					+ "             where q.qdtn1 = d.kind \n"
					+ "             and q.qdtsub = d.subkind \n"
					+ "             and q.irbis_date_beg = (select * \n"
					+ "                                     from (select act.date_beg \n"
					+ "                                           from irbis.actualdata act \n"
					+ "                                           where upper(act.tablename) = upper('t4_qvb') \n"
					+ "                                           order by act.date_beg desc \n"
					+ "                                          ) \n"
					+ "                                     where rownum < 2  \n"    
					+ "                                    ) \n"
					+ "            and pq.pg_codgr = q.q_group \n"
					+ "            and pq.irbis_date_beg = (select * \n"
					+ "                                     from (select act.date_beg \n"
					+ "                                           from irbis.actualdata act \n"
					+ "                                           where upper(act.tablename) = upper('t4_qvb_pargr') \n"
					+ "                                           order by act.date_beg desc \n"
					+ "                                          ) \n"
					+ "                                     where rownum < 2  \n"   
					+ "                                    ) \n"
					+ "            ) \n";
		}*/
		String sQuerry = "select " + paramNames + " from \n"
				+ " (select d.printableno as Account, \n"
				+ " p.surname as SurName, p.firstname as Name, p.secondname as SecondName, d.branchno as Osb,d.office as Filial,to_char(trunc(p.birth), \n"
				+ " 'yyyy-mm-dd') as DateOfBirth,  p.sex as Sex, p.personcard_major as PersonCardIdMajor, p.personcard_minor as  PersonCardIdMinor, d.balance as Balance,\n"
				+ " d.id_minor as DepoMinor, d.id_major as DepoMajor, p.id_major as PersonIdMajor, p.id_minor as PersonIdMinor\n"
				+ " from deposit.deposit d, client.person p \n"
				+ " where 1=1 \n"
				+ " and d.id_mega = " + idMega + " \n"
				+   additionalParams + "\n"
				+ " and d.state = 0 \n"
				+ " and d.currency = 810 \n"
				+ " and p.id_mega = " + idMega + " \n"
				+ " and (p.id_major, p.id_minor) = ((d.person_major, d.person_minor)) \n"
				//+ " and p.secondname is not null and p.sex is not null \n"
				//+ " and p.personcard_major is not null and p.personcard_minor is not null \n"
				//+ " and not exists (select * from CLIENT.memento_mori c where c.person_minor = p.id_minor and c.person_major = p.id_major) \n"
				+ existsFromIrbis
				
				+ " and rownum < " + rowNumber + " order by rownum desc ) where rownum < 2"; 
		return sQuerry;
	}
	
	/**
	 * ������� ����������� ������� �� ���������� ������ �� �������� �� ����� 
	 * @param paramNames - ������ ���������� ����������
	 * @param additionalParams - �������������� ������� �������
	 * @param rowNumber - ����� ���������� ������
	 * @param idMega - id ��������� �����
	 * @return
	 */
	public static String createQuerryGetDepositCard(String paramNames, String personIdMajor, String personIdMinor){
		
		String sQuerry = "select " + paramNames + " from (select numcard as CardNumber, to_char(trunc(enddate),'yyyy-mm-dd') as CardEndDate from deposit.dcard  \n"
					   + "                          where id_mega = 38 and (person_major, person_minor) =  \n"
					   + "	     (select person_major, person_minor from client.personcard  where \n"
					   + "          id_mega = 38 and person_major = " + personIdMajor + " and person_minor = " + personIdMinor + " and rownum < 2) \n"
					   + " and rownum < 2) \n"; 
		return sQuerry;
	}
	
	/**
	 * ������� ���������� ������ �� ��������
	 * @param rowParams - ������ ���������� ��� ������
	 * @param rowNumber - ����� ������
	 * @param idMega - id  ��������� �����
	  * @return
	  * @throws Exception
	  */
	 public static HashMap<String, String> setClientAccount(HashMap<String, String> rowParams, int rowNumber, int idMega) throws Exception{

		 //��������� �� ��������
		 
		 	String paramNames = "Account,SurName,Name,SecondName,Osb,Filial,DateOfBirth,Sex,PersonCardIdMajor,PersonCardIdMinor,Balance,DepoMinor,DepoMajor,PersonIdMajor,PersonIdMinor";
			String sQuerryGetAccountParams = QuerryGetParams.getSquerryGetParams(CalculatedVariables.testOperation, CalculatedVariables.testCase, paramNames, rowNumber, idMega);

			HashMap<String,String> resultsFromDBAccount = new HashMap<String,String>();
			resultsFromDBAccount = DataBaseLib.getAnyRowAsHashMap(sQuerryGetAccountParams, paramNames.split(","),"COD");					

			rowParams = new HashMap<String, String>();


		 //��������� �� ����������, ��������� � ���������
			String sQuerry = "";																
			sQuerry = "select  pc.documentkind as \"Type\", pc.no as \"Number\", pc.series as Series, to_char(trunc(pc.issueday), 'yyyy-mm-dd') as \"When\" from client.personcard pc "
				           + "where pc.person_major = " + resultsFromDBAccount.get("PersonIdMajor") + " and pc.person_minor = " + resultsFromDBAccount.get("PersonIdMinor") + 
				           " and pc.isactual = 1";
			HashMap<String,String> resultsFromDBDocuments = new HashMap<String,String>();
			resultsFromDBDocuments = DataBaseLib.getAnyRowAsHashMap(sQuerry, "Type,Number,Series,When".split(","),"COD");

		 //��������� ������
			rowParams.putAll(resultsFromDBAccount);
			rowParams.putAll(resultsFromDBDocuments);
		
			
			rowParams.remove("DepoMinor");
			rowParams.remove("DepoMajor");
			
			System.out.println("selectedAccount = " + resultsFromDBAccount);
			
			return rowParams;
		 				 
	 }
	 
	 /**
	  * ������� ��������� ������ �� �������� �� ������ ��������
	  * @param accountNumber - ����� ��������
	  * @return ������ �� �������� � ���� ��������(personMajor,personMinor,idMinor,idMajor,balance)
	  * @throws Exception
	  */
	 public static HashMap<String, String> getAccountDataByNumber(String accountNumber) throws Exception{
		 String paramNames = "Account,SurName,Name,SecondName,Osb,Filial,DateOfBirth,Sex,PersonCardIdMajor,PersonCardIdMinor,"
		 		+ "Balance,DepoMinor,DepoMajor,PersonIdMajor,PersonIdMinor";
		 String sQuerryGetParams = "with nom as (select '" + accountNumber + "' as print from dual) \n"
				 						+ "select " + paramNames + " from \n"
				 						+ " (select d.printableno as Account, \n"
				 						+ " p.surname as SurName, p.firstname as Name, p.secondname as SecondName, d.branchno as Osb,d.office as Filial,to_char(trunc(p.birth), \n"
				 						+ " 'yyyy-mm-dd') as DateOfBirth,  p.sex as Sex, p.personcard_major as PersonCardIdMajor, p.personcard_minor as  PersonCardIdMinor, d.balance as Balance,\n"
				 						+ " d.id_minor as DepoMinor, d.id_major as DepoMajor, p.id_major as PersonIdMajor, p.id_minor as PersonIdMinor\n"
										+ "from deposit.depokey98 dk98 \n"
										+ "join deposit.deposit d on dk98.deposit_major=d.id_major and dk98.deposit_minor=d.id_minor and dk98.id_mega = d.id_mega \n"
										+ "join client.person p on (p.id_mega, p.id_major, p.id_minor) = ((d.id_mega, d.person_major, d.person_minor)), nom  \n"
										+ "where dk98.account =substr(print ,1,5) \n"
										+ "and dk98.currency = substr(print ,6,3) \n"
										+ "and dk98.key = substr(print ,9,1) \n"
										+ "and dk98.branch = substr(print ,10,4) \n"
										+ "and dk98.no =substr(print ,14,7) \n"
										
										+ ") ";
		 
		 HashMap<String,String> resultsFromDBAccount = new HashMap<String,String>();
		 resultsFromDBAccount = DataBaseLib.getAnyRowAsHashMap(sQuerryGetParams, paramNames.split(","),"COD");

		 HashMap<String, String> rowParams = new HashMap<String, String>();


		 //��������� �� ����������, ��������� � ���������		
		 String sQuerry = "";																
		 sQuerry = "select  pc.documentkind as \"Type\", pc.no as \"Number\", pc.series as Series, to_char(trunc(pc.issueday), 'yyyy-mm-dd') as \"When\" from client.personcard pc "
				           + "where pc.person_major = " + resultsFromDBAccount.get("PersonIdMajor") + " and pc.person_minor = " + resultsFromDBAccount.get("PersonIdMinor");
		 HashMap<String,String> resultsFromDBDocuments = new HashMap<String,String>();
		 resultsFromDBDocuments = DataBaseLib.getAnyRowAsHashMap(sQuerry, "Type,Number,Series,When".split(","),"COD");

		 //��������� ������			
		 rowParams.putAll(resultsFromDBAccount);
		 rowParams.putAll(resultsFromDBDocuments);
			
		 rowParams.remove("DepoMinor");
		 rowParams.remove("DepoMajor");
			
		 System.out.println("selectedAccount = " + resultsFromDBAccount);
			
		 return rowParams;

	 }
	 
	 /**
	  * ������� ���������� ������ �� �����������
	  * @param depoIdMajor - DEPOSIT.deposit_major
	  * @param depoIdMinor - DEPOSIT.deposit_minor
	  * @return
	  * @throws Exception
	  */
	 public static HashMap<String, String> getBeneficiaryData(String depoIdMajor, String depoIdMinor ) throws Exception{
		 String paramNames = "SurName,Name,SecondName,DateOfBirth,Sex,PersonIdMajor,PersonIdMinor";

		 String sQuerryGetParams = 	"select " + paramNames + " from \n"
				 						+ " (select p.surname as SurName, p.firstname as Name, p.secondname as SecondName,to_char(trunc(p.birth), \n"
				 						+ " 'yyyy-mm-dd') as DateOfBirth,  p.sex as Sex, p.id_major as PersonIdMajor, p.id_minor as PersonIdMinor\n"
										+ "from client.person p, DEPOSIT.depoInvestor di \n"
										+ " where di.id_mega = 38 and p.id_mega = 38 and p.id_major = di.person_major and p.id_minor = di.person_minor \n"
										+ " and di.deposit_minor = " + depoIdMinor + " and di.deposit_major = " + depoIdMajor
										+ " and rownum < 2"
										+ ") ";
		 
		 HashMap<String,String> resultsFromDBAccount = new HashMap<String,String>();
		 resultsFromDBAccount = DataBaseLib.getAnyRowAsHashMap(sQuerryGetParams, paramNames.split(","),"COD");

		 HashMap<String, String> rowParams = new HashMap<String, String>();


		 //��������� �� ����������, ��������� � ���������		
		 String sQuerry = "";																
		 sQuerry = "select  pc.documentkind as \"Type\", pc.no as \"Number\", pc.series as Series, to_char(trunc(pc.issueday), 'yyyy-mm-dd') as \"When\" from client.personcard pc "
				           + "where pc.person_major = " + resultsFromDBAccount.get("PersonIdMajor") + " and pc.person_minor = " + resultsFromDBAccount.get("PersonIdMinor");
		 HashMap<String,String> resultsFromDBDocuments = new HashMap<String,String>();
		 resultsFromDBDocuments = DataBaseLib.getAnyRowAsHashMap(sQuerry, "Type,Number,Series,When".split(","),"COD");

		 //��������� ������			
		 rowParams.putAll(resultsFromDBAccount);
		 rowParams.putAll(resultsFromDBDocuments);
			
		 System.out.println("selectedBineficiaryParams = " + rowParams);
			
		 return rowParams;

	 }
	 
	 /**
	  * ������� ������������ �������������� ������� � sql �������
	  * @param testCaseName - �������� �����
	  * @param idMega - megaId
	  * @return - �������������� ������� � sql �������
	  */
	 private static String getAdditionalParams(String testCaseName, int idMega){
		 String additionalParams = "";
		 String lossRights = "";
		 
		 if(testCaseName.contains("Acc2AccClose143Nominal")){
			 lossRights = "1";
		 }else{
			 lossRights = "0";
		 }
		 
		
		 if(testCaseName.contains("NotNominal") || testCaseName.equals("TopUpCredit2accNotNominal") ){
			 
			 additionalParams = " and d.kind = 61 and d.subkind = 1 " + " and d.balance > 100 ";
			 if(testCaseName.contains("Acc2AccClose143Nominal")){
				 
			 }
			
		 }else {
			 if (idMega == 38) {//��� id �����, ��������� �� 38, ������ ������, ��� ��� �� ��� ������ ����� ����
					additionalParams = " and d.kind = 98 and d.subkind = 2 " + "  and d.loss_rights = " + lossRights + " and d.balance > 100 \n";
					additionalParams += " and exists (select * from DEPOSIT.depoInvestor dpi, client.person pi  "
							+ " where dpi.id_mega = " + idMega + " and dpi.deposit_minor = d.id_minor and dpi.deposit_major = d.id_major \n"
							+ " and (dpi.id_mega,dpi.person_major,dpi.person_minor) = ((pi.id_mega,pi.id_major,pi.id_minor)) \n"
							//+ " and not exists (select * from CLIENT.memento_mori cmri where cmri.person_minor = pi.id_minor and cmri.person_major = pi.id_major) \n"
							+ " and (pi.life_state is null or pi.life_state = 0) \n"
							+ " and rownum < 2) \n";
					additionalParams += " and (p.life_state is null or p.life_state = 0) ";
			 }else{
				 additionalParams = " and d.kind = 98 and d.subkind = 2 ";//�������������� ������������
			 }
			
		 }
		 
		 if(testCaseName.equals("Acc2AccClose143NominalToAnotherNotNominal")){

			 //�������� �� ���������� ������������ �������
			 additionalParams += " and not exists (select * from deposit.f190 f where d.id_mega = f.id_mega and f.deleted = 0 "
			 		+ "and d.printableno = f.senderkey and f.recipientkeycode= 8 and f.paykindcode = 6) \n";
		 }
		 
		 
		 
		 return additionalParams;
	 }
	
	

}
