package libs;

import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

public class HabblLib {

	public static void sendRQtoMQ(String xmlMessage) throws Exception {


		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */

	public static void checkWay4issueCardRqRs() throws Exception {
		Document xmlDoc;
		String xmlMessage;
		//��������� �� ���� ���������� xml ��������� IssueCardRq
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "IssueCardRq", "Windows-1251", 35);	
	//	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRq.xml", "Windows-1251", "IssueCardRq");
		
		String actualResultFromDB44 = GetSQLway4Rq4hubbleLoop("ISSUE_CARD", 35);
		
		//"WHERE '" + CalculatedVariables.testRqUID + "' IN (CLAIMOPERUID, CLAIMRQUID, RQUID, OPERUID) and '' IN (Way4TYPE);"; 
		//CalculatedVariables.testRqUID
				
				
		xmlMessage = actualResultFromDB44;
		
			
		//xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRq.xml", "Windows-1251");
		/*//�������� xml ��������� �� xsd �����
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "IssueCardRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "IssueCardRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "IssueCardRq", "true");*/
		Document issueCardRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(issueCardRqDoc, "RqUID");
		CalculatedVariables.mainApplRegNumber = XMLLib.getElementValueFromDocument(issueCardRqDoc, "MainApplRegNumber");
		CalculatedVariables.acctId = XMLLib.getElementValueFromDocument(issueCardRqDoc, "AcctId");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ��������� �� ������������ ����� � WAY4 (�������� �����)", "Windows-1251", 35);

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ��������� �� ������������ ����� � WAY4 (�������� �����)");

		//��������� xml ��������� IssueCardRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "IssueCardRs");
		xmlDoc = XMLLib.redefineXMLParamsIssueCardRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "IssueCardRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\"
				+ "CreateNewCardOnlineConnectUDBO" + "\\" + "IssueCardRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml

		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
	//	System.out.println("Wait for 1 seconds.. in checkWay4issueCardRqRs");
	//	Thread.sleep(1000);

		//��������� �� ���� ���������� xml ��������� IssueCardRs
//		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRs.xml", "Windows-1251", "IssueCardRs");
//		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRs.xml", "Windows-1251");
	
		String xmlMessage2 =GetSQLway4Rs4hubbleLoop("ISSUE_CARD", 35);
				
		Document issueCardRsDoc = XMLLib.convertStringToDom(xmlMessage2, "UTF-8");
		
		String mainApplRegNumberIssueCardRs = XMLLib.getElementValueFromDocument(issueCardRsDoc, "MainApplRegNumber");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("mainApplRegNumberIssueCardRs", mainApplRegNumberIssueCardRs);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("mainApplRegNumberIssueCardRs", CalculatedVariables.mainApplRegNumber);	
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void checkWay4CustAddRqRs() throws Exception {
		Document xmlDoc;
		String xmlMessage;
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRq", "Windows-1251", 45);	
		//	System.out.println("���� 1 ������... in checkWay4CustAddRqRs");
	//	Thread.sleep(1000);
		//��������� �� ���� ���������� xml ��������� CustAddRq
	//	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRq.xml", "Windows-1251", "CustAddRq");

		
	
		//"WHERE '"+ CalculatedVariables.testRqUID +"' IN (CLAIMOPERUID, CLAIMRQUID, RQUID, OPERUID) and 'CUST_ADD' IN (Way4TYPE);"; 
		//CalculatedVariables.testRqUID

		//	System.out.println("���� 1 ������...in checkWay4CustAddRqRs");
	//	Thread.sleep(1000);
		
	//	xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRq.xml", "Windows-1251");
		String actualResultFromDB44 = GetSQLway4Rq4hubbleLoop("CUST_ADD", 35);
		xmlMessage = actualResultFromDB44;
		
		
		
		/*//�������� xml ��������� �� xsd �����
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "CustAddRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "CustAddRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "CustAddRq", "true");*/
		Document custAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqTm");

//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");

		//��������� xml ��������� CustAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CustAddRs");
		xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CustAddRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\"
				+ "CreateNewCardOnlineConnectUDBO" + "\\" + "CustAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml

		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void NotifyIssueCardResultNfRs() throws Exception {
		Document xmlDoc;
		String xmlMessage;
		//��������� xml ��������� NotifyIssueCardResultNfRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "NotifyIssueCardResultNfRs");
		xmlDoc = XMLLib.redefineXMLParamsNotifyIssueCardResultNfRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "NotifyIssueCardResultNfRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\"
				+ "CreateNewCardOnlineConnectUDBO" + "\\" + "NotifyIssueCardResultNfRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml

		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "NotifyIssueCardResultNf", "Windows-1251", 35);
		//��������� �� ���� ���������� xml ��������� NotifyIssueCardResultNf
	//	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251", "NotifyIssueCardResultNf");
	//	xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251");
		
		
		Document notifyIssueCardResultNfDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusCode");
		String severityNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "Severity");
		String statusDescNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusDesc");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeNotifyIssueCardResultNf", statusCodeNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("severityNotifyIssueCardResultNf", severityNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("statusDescNotifyIssueCardResultNf", statusDescNotifyIssueCardResultNf);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeNotifyIssueCardResultNf", "0");
		CalculatedVariables.expectedValues.put("severityNotifyIssueCardResultNf", "Posted");
		CalculatedVariables.expectedValues.put("statusDescNotifyIssueCardResultNf", "Successfully processed");
	}

	
	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws InterruptedException
	 */
	public static void checkWay4getCardHolderRqRs()
			throws Exception {
		Document xmlDoc;
		String xmlMessage;


		//������� 30 ������ ��������� ���������
		//   CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRq", "Windows-1251", 45);


		//��������� �� ���� ���������� xml ��������� GetCardHolderRq
	//	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251", "GetCardHolderRq");

		System.out.println("���� 1 ������... in checkWay4getCardHolderRqRs");
		Thread.sleep(1000);

		//������ ������ � �� AC ����� �� ��������� ������ �� �� ������ �� ������� RqUID
		
		String actualResultFromDB44 = GetSQLway4Rq4hubbleLoop("GET_CARD_HOLDER", 35);
		
		
		xmlMessage = actualResultFromDB44;
		//�������� xml ��������� �� xsd �����
		String isXMLValid;
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "GetCardHolderRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "GetCardHolderRq", isXMLValid);
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "GetCardHolderRq", "true");
		Document getCardHolderRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqUID");
		CalculatedVariables.operUIDgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "OperUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
		

//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log",
//				"Windows-1251",  idrecordWay4Queue + " GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log",
				"Windows-1251", "GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");


		//��������� xml ��������� GetCardHolderRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "GetCardHolderRs");
		xmlDoc = XMLLib.redefineXMLParamsGetCardHolderRs(xmlDoc, "0");
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "GetCardHolderRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\"
				+ "CreateNewCardOnlineConnectUDBO" + "\\" + "GetCardHolderRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml

		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("���� 1 ������...");
		Thread.sleep(1000);
	}

	
	
	
	/*
	 *  ���� ��������� 2 ������� ������ � ��������� ������.
	 *  
	 *  //��������� �� ���� ���������� xml ��������� IssueCardRq
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "IssueCardRq", "Windows-1251", 35);	
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRq.xml", "Windows-1251", "IssueCardRq");
		
		String actualResultFromDB44 = GetSQLway4Rq4hubble("ISSUE_CARD");
		*/ 
	
	/**
	 * @return 
	 * @throws Exception 
	 */
	public static String GetSQLway4Rq4hubbleLoop(String way4metod, int timeOut) throws Exception {
		//boolean isContains = false;

		//������� �����
		LocalDateTime currentStartTime = LocalDateTime.now();	

		String actualResultFromDB44 = "null"; 
		String NullLink = "null"; 
		
		if (actualResultFromDB44 == NullLink) { System.out.println("actualResultFromDB44 == NullLink"); }
		
		
		while(actualResultFromDB44 == NullLink) {
			//	������ ������ � �� ���� ���� ����� �� NULL �� ������� �� �����.
			String sQuerry44 = 	"WITH V AS\r\n" + 
				"(\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
					"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
					"we.WEONLINEINCOMMINGMESSAGE AS \"Online-����� (���������)\",\r\n" +
					"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.TCCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN CARDISSUERS ci\r\n" + 
				"    ON we.WECARDISSUERSID = ci.CARDISSUERSID\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = ci.CITRANSACTIONID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
					"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
					"we.WEONLINEINCOMMINGMESSAGE AS \"Online-����� (���������)\",\r\n" +
					"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.tcCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = we.WETCID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				")\r\n" + 
				"SELECT WAY4RQ FROM V\r\n" + 
				"WHERE '"+ CalculatedVariables.testRqUID +"' IN (CLAIMOPERUID, CLAIMRQUID, RQUID, OPERUID) and '" + way4metod + "' IN (Way4TYPE)"; 
		//CalculatedVariables.testRqUID

	    actualResultFromDB44 = DataBaseLib.getDataAsString(sQuerry44, "HUBBL");
	    
	    
		System.out.println("actualResultFromDB = " + actualResultFromDB44 );

			if (//���� ����� �������� ��������� �������, �� �������
				   Duration.between(currentStartTime, LocalDateTime.now())
				            .getSeconds() > timeOut
				            ){
				  // isContains = false; 
				   try {
					   CommonLib.WriteLog("FAIL", "����������� ������ � �� ����� " + way4metod + " � ������� " + timeOut + " ������",
									Config.resultsPath );
				   } catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
				   }
				   System.setProperty("isError", "true");
				   System.setProperty("isFail", "true");
				throw new Exception("����������� ������ " + way4metod + " � ������� " + timeOut + " ������");
				   /*try {
						CommonLib.postProcessingTest();
				   } catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				   }*/
			 }else{
				//���� ���� �������
				 Thread.sleep(500);
				 System.out.println("Wait for 0.5 seconds.. in GetSQLway4Rq4hubbleLoop");
				// continue;
			 }
		}
		
		if (actualResultFromDB44 != NullLink) { System.out.println("actualResultFromDB44 != NullLink"); }
		
		return actualResultFromDB44;
	}
	
	public static String GetSQLway4Rs4hubbleLoop(String way4metod, int timeOut) throws Exception {
		//boolean isContains = false;

		//������� �����
		LocalDateTime currentStartTime = LocalDateTime.now();	

		String actualResultFromDBRs = "null"; 
		String NullLink = "null"; 
		
		while(actualResultFromDBRs == NullLink) {
			//	������ ������ � �� ���� ���� ����� �� NULL �� ������� �� �����.
			String sQuerry44 = 	"WITH V AS\r\n" + 
				"(\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
					"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
				"we.WEONLINEINCOMMINGMESSAGE AS WAY4Rs,\r\n" +
					"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.TCCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN CARDISSUERS ci\r\n" + 
				"    ON we.WECARDISSUERSID = ci.CARDISSUERSID\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = ci.CITRANSACTIONID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
					"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
				"we.WEONLINEINCOMMINGMESSAGE AS WAY4Rs,\r\n" +
					"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.tcCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = we.WETCID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				")\r\n" + 
				"SELECT WAY4Rs FROM V\r\n" + 
				"WHERE '"+ CalculatedVariables.testRqUID +"' IN (CLAIMOPERUID, CLAIMRQUID, RQUID, OPERUID) and '" + way4metod + "' IN (Way4TYPE)"; 
		//CalculatedVariables.testRqUID

	    actualResultFromDBRs = DataBaseLib.getDataAsString(sQuerry44, "HUBBL");
	    
		System.out.println("actualResultFromDB = " + actualResultFromDBRs );

			if (//���� ����� �������� ��������� �������, �� �������
				   Duration.between(currentStartTime, LocalDateTime.now())
				            .getSeconds() > timeOut
				            ){
				   //isContains = false; 
				   try {
					   CommonLib.WriteLog("FAIL", "����������� ������ � �� ����� " + way4metod + " � ������� " + timeOut + " ������",
									Config.resultsPath );
				   } catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
				   }
				   System.setProperty("isError", "true");
				   System.setProperty("isFail", "true");
				throw new Exception("����������� ������ " + way4metod + " � ������� " + timeOut + " ������");
				   /*try {
						CommonLib.postProcessingTest();
				   } catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				   }*/
			 }else{
				//���� ���� �������
				 Thread.sleep(500);
				// continue;
			 }
		}
		
		return actualResultFromDBRs;
	}
	
	
	public static String GetSQLway4Rq4hubble(String way4metod) {
		
	String sQuerry44 = 	"WITH V AS\r\n" + 
				"(\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
			"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
			"we.WEONLINEINCOMMINGMESSAGE AS \"Online-����� (���������)\",\r\n" +
			"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.TCCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN CARDISSUERS ci\r\n" + 
				"    ON we.WECARDISSUERSID = ci.CARDISSUERSID\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = ci.CITRANSACTIONID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT we.WEMESSAGETYPE AS Way4TYPE, \r\n" + 
				"we.WEOUTGOINGMESSAGE AS \"WAY4RQ\",\r\n" +
			"ws.WSNAME || '(' || ws.WSID || ')' as \"������\", \r\n" +
			"we.WEONLINEINCOMMINGMESSAGE AS \"Online-����� (���������)\",\r\n" +
			"we.WEOFFLINEINCOMMINGMESSAGE AS \"Offline-����� (�������)\",\r\n" +
				"tc.TCOPERUID as OPERUID,\r\n" + 
				"tc.tcCLAIMOPERUID as CLAIMOPERUID,\r\n" + 
				"tc.TCRQUID AS RQUID,\r\n" + 
				"c.CLCLAIMRQUID as CLAIMRQUID\r\n" + 
				"FROM \r\n" + 
				"    WAY4MESSAGEEXCHANGE we\r\n" + 
				"INNER JOIN TRANSACTIONCONTEXT tc\r\n" + 
				"    ON tc.TCID = we.WETCID\r\n" + 
				"INNER JOIN PARAMETERS p\r\n" + 
				"    ON p.PRTCID = tc.TCID\r\n" + 
				"INNER JOIN CALLPARAMETERS cp\r\n" + 
				"    ON cp.PCCONTEXTID = tc.TCID\r\n" + 
				"INNER JOIN WAY4MESSAGESTATUS ws\r\n" + 
				"    ON ws.WSID = we.WESTATUSID\r\n" + 
				"LEFT JOIN WAY4RETRY wr\r\n" + 
				"    ON wr.WRWAY4EXCHANGEID = we.WEID AND wr.WRWAY4EXCHANGEDATE = we.WEDATE\r\n" + 
				"LEFT JOIN CLAIM c\r\n" + 
				"    ON c.CLCLAIMOPERID = tc.TCCLAIMOPERUID\r\n" + 
				")\r\n" + 
				"SELECT WAY4RQ FROM V\r\n" + 
				"WHERE '"+ CalculatedVariables.testRqUID +"' IN (CLAIMOPERUID, CLAIMRQUID, RQUID, OPERUID) and '" + way4metod + "' IN (Way4TYPE)"; 
		//CalculatedVariables.testRqUID

	    String actualResultFromDB44 = DataBaseLib.getDataAsString(sQuerry44, "HUBBL");
		System.out.println("actualResultFromDB = " + actualResultFromDB44 );
		
		return actualResultFromDB44;
	}

	public static String GetSQL4hubbleRSLoop(int timeOut) throws Exception {
		//boolean isContains = false;


		//������� �����
		LocalDateTime currentStartTime = LocalDateTime.now();	

		String createProductPackageRsStr = "null"; 
		String NullLink = "null";

		String way4metod = "����� �� RQuid"; 
		
		while(createProductPackageRsStr == NullLink) {
			//	������ ������ � �� ���� ���� ����� �� NULL �� ������� �� �����.
			String sQuerry44 = 	"select x.response \n" + 
					"FROM (\n" + 
					"    select c.CLCLAIMRQUID as RQUID,\n" + 
					"    cr.CRMESSAGE as RESPONSE,\n" + 
					"    rank() over (partition by CLCLAIMRQUID order by CRSENDTIME desc) as rnk\n" + 
					"    FROM CLAIM_RESPONSE cr\n" + 
					"    INNER JOIN CLAIM c\n" + 
					"        ON cr.CRCLAIMOPERUID = c.CLCLAIMOPERID\n" + 
					"    union all\n" + 
					"    select e.ESBRQUID as RQUID, e.ESBRESPONSE as RESPONSE, 1 as rnk from ESBPARAMETERS e\n" + 
					") x\n" + 
					"where \n" + 
					"x.rnk = 1\n" + 
					"and x.rquid = '" + CalculatedVariables.testRqUID + "' ";  
			
			createProductPackageRsStr = DataBaseLib.getDataAsString(sQuerry44, "HUBBL");
			System.out.println("createProductPackageRsStr = " + createProductPackageRsStr );

			if (//���� ����� �������� ��������� �������, �� �������
				   Duration.between(currentStartTime, LocalDateTime.now())
				            .getSeconds() > timeOut
				            ){
				   //isContains = false; 
				   try {
					   CommonLib.WriteLog("FAIL", "����������� ������ � �� ����� " + way4metod + " � ������� " + timeOut + " ������",
									Config.resultsPath );
				   } catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
				   }
				   System.setProperty("isError", "true");
				   System.setProperty("isFail", "true");
				throw new Exception("����������� ������ " + way4metod + " � ������� " + timeOut + " ������");
				   /*try {
						CommonLib.postProcessingTest();
				   } catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				   }*/
			 }else{
				//���� ���� �������
				 Thread.sleep(500);
				// continue;
			 }
		}
		
		return createProductPackageRsStr;
	}
	
	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void CheckJrnTotal60() throws Exception {
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 35);

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
		//XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "first");

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 60 � ���
		//String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		//System.out.println("jrnTotal60 = " + jrnTotal60Str);
		
		Document actualResultFromDB441 = XMLLib.convertStringToDom(CalculatedVariables.createProductPackageRs0, "UTF-8");
		String Transaction116 = XMLLib.getNextElementValueFromDocument(actualResultFromDB441, "Transaction", 4);

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 60 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Transaction116;
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		
		
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
		String subSystemCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "SubSystemCode");
		//String ukrBankCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "UKRBankCode");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal60", subSystemCodeJrnTotal60);
	//	CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", ukrBankCodeJrnTotal60);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal60", "1");
	//	CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", "IGN");
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static  void CheckJrnTotal68() throws Exception {
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 68 � ���", "Windows-1251", 35);

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		//XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "first");

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		//String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		//System.out.println("jrnTotal68 = " + jrnTotal68Str);
		
		Document actualResultFromDB441 = XMLLib.convertStringToDom(CalculatedVariables.createProductPackageRs0, "UTF-8");
		String Transaction1681 = XMLLib.getNextElementValueFromDocument(actualResultFromDB441, "Transaction", 3);
		
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Transaction1681.toString();
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str.toString(), "UTF-8");
		
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
	//	String ukrBankCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "UKRBankCode");
		String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
		CalculatedVariables.mailAccount = mailAccountJrnTotal68;

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);
		//CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", ukrBankCodeJrnTotal68);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		//CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", "IGN");
		
		String sQuerry = "select CODECARD from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal68 + "'";

		String codeCardJrnTotal68 = DataBaseLib.getDataAsString(sQuerry, "COD");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("codeCardJrnTotal68", codeCardJrnTotal68);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("codeCardJrnTotal68", "111703");
	}

	/**
	 * @throws Exception
	 */
	public static void CheckUDBO() throws Exception {
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���� (�������� �����)", "Windows-1251", 35);

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���� (�������� �����)");
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static  void CheckjrnTotal25() throws Exception {
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 25 � ���", "Windows-1251", 35);

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
		//XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "first");

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
		//String jrnTotal25StrFirst = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251");
		//System.out.println("jrnTotal25First = " + jrnTotal25StrFirst);
	
		Document actualResultFromDB441 = XMLLib.convertStringToDom(CalculatedVariables.createProductPackageRs0, "UTF-8");
		String Transaction11 = XMLLib.getNextElementValueFromDocument(actualResultFromDB441, "Transaction", 1);
		
		String jrnTotal25StrFirst0 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Transaction11.toString();
		System.out.println("jrnTotal25First = " + jrnTotal25StrFirst0);
		
		
		
		Document jrnTotal25First = XMLLib.convertStringToDom(jrnTotal25StrFirst0.toString(), "UTF-8");
		String typeOperCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "TypeOperCode");
		String subSystemCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "SubSystemCode");
		String ReceiverJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "Receiver");
		
		String sQuerry2 = "select state from client.edbo where id_mega = '38' and edbo_no = '" + ReceiverJrnTotal25First + "'";
		String stateJrnTotal25First = DataBaseLib.getDataAsString(sQuerry2, "COD");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25First", typeOperCodeJrnTotal25First);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25First", subSystemCodeJrnTotal25First);
		CalculatedVariables.actualValues.put("stateJrnTotal25First", stateJrnTotal25First);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25First", "25");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25First", "1");
		CalculatedVariables.expectedValues.put("stateJrnTotal25First", "0");


		//������� 30 ������ ��������� ���������
		//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ����� � ����", "Windows-1251", 35);
		//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ����� � ���� (�������� �����)");

		//������� 30 ������ ��������� ���������
		//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ �� ������ ����� � ���� (�������� �����)", "Windows-1251", 35);

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
		//	XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25Last.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "last");
		
		Document actualResultFromDB442 = XMLLib.convertStringToDom(CalculatedVariables.createProductPackageRs0, "UTF-8");
		String Transaction112 = XMLLib.getNextElementValueFromDocument(actualResultFromDB442, "Transaction", 2);
		
		String jrnTotal25StrFirst2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Transaction112.toString();
		System.out.println("jrnTotal25Last = " + jrnTotal25StrFirst2);

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
		//String jrnTotal25StrLast = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25Last.xml", "Windows-1251");
		//System.out.println("jrnTotal25Last = " + jrnTotal25StrLast);
		
		Document jrnTotal25Last = XMLLib.convertStringToDom(jrnTotal25StrFirst2, "UTF-8");
		String typeOperCodeJrnTotal25Last = XMLLib.getElementValueFromDocument(jrnTotal25Last, "TypeOperCode");
		String subSystemCodeJrnTotal25Last = XMLLib.getElementValueFromDocument(jrnTotal25Last, "SubSystemCode");
		String accountNumber = XMLLib.getElementValueFromDocumentNode(jrnTotal25Last, "Comment", "Way");
		String account = accountNumber.substring(0, 5);
		String currency = accountNumber.substring(5, 8);
		String key = accountNumber.substring(8, 9);
		String branch = accountNumber.substring(9, 13);
		String no = accountNumber.substring(13, 20);
		
		String sQuerry11 = "SELECT DEPOSIT.DEPOSIT.STATE " +
				  " FROM DEPOSIT.DEPOSIT, DEPOSIT.DEPOKEY98 " +
				  " WHERE DEPOKEY98.ACCOUNT=" + account +
				  "      AND DEPOKEY98.CURRENCY=" + currency +
				  "      AND DEPOKEY98.KEY=" + key +
				  "      AND DEPOKEY98.BRANCH=" + branch +
				  "      AND DEPOKEY98.NO=" + no +
				  "      AND DEPOKEY98.DEPOSIT_MAJOR=DEPOSIT.ID_MAJOR " +
				  "      and DEPOKEY98.DEPOSIT_MINOR=DEPOSIT.ID_MINOR ";

		String stateJrnTotal25Last = DataBaseLib.getDataAsString(sQuerry11, "COD");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25Last", typeOperCodeJrnTotal25Last);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25Last", subSystemCodeJrnTotal25Last);
		CalculatedVariables.actualValues.put("stateJrnTotal25Last", stateJrnTotal25Last);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25Last", "25");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25Last", "6");
		CalculatedVariables.expectedValues.put("stateJrnTotal25Last", "0");
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void CheckjrnTotal0() throws Exception {
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 35);

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
		//XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "first");

		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
		//String jrnTotal0Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0.xml", "Windows-1251");
		//System.out.println("jrnTotal0 = " + jrnTotal0Str);
		
		Document actualResultFromDB441 = XMLLib.convertStringToDom(CalculatedVariables.createProductPackageRs0, "UTF-8");
		String Transaction11 = XMLLib.getNextElementValueFromDocument(actualResultFromDB441, "Transaction", 0);
		
		String jrnTotal0Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Transaction11;
		System.out.println("jrnTotal0 = " + jrnTotal0Str);
				
		
		Document jrnTotal0 = XMLLib.convertStringToDom(jrnTotal0Str, "UTF-8");
		String typeOperCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "TypeOperCode");
		String subSystemCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "SubSystemCode");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0", typeOperCodeJrnTotal0);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0", subSystemCodeJrnTotal0);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0", "0");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0", "6");
	}

	/**
	 * @throws Exception
	 * @throws IOException
	 */
	public static void sendRQfor() throws Exception {
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CreateProductPackageConnectUDBORq");
		xmlDoc = XMLLib.redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
	
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageConnectUDBORq");

		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\"
				+ "CreateNewCardOnlineConnectUDBO" + "\\" + "CreateProductPackageConnectUDBORq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml


		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
	}

	public static void response99test() throws Exception {
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs

		//������ ������ � �� AC ����� �� ��������� ������ �� �� ������ �� ������� RqUID ������������� �� ������� �������.
		
		String createProductPackageRsStr = GetSQL4hubbleRSLoop(20);

		
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr );
		
		CalculatedVariables.createProductPackageRs99=createProductPackageRsStr;

		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);
				
	//	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");

		//��������� �������� ���� statusCode �� ������ createProductPackageRs
	//	String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
	//	System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);

		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "99");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Ok");
	}


	// ���� ������� �� �������� � CommonLib.waitForLogContainsString �� �� ������ �������� � �� �����
	// ��������� ���� �� ��� ������ �� �������� � ����� ��� ����� ��� ������ �������� ����� ������ � �� �������� "�����" �������� ��� way4
	
	/*
	public static boolean waitForBDContainsString(String uniqueParam, 

	}
	
	// ��������� ����������� ������ � ���� ������
		public static boolean waitForBDContainsResponseInner(String... args) {
		
		
		//������ ������ � �� AC ����� �� ��������� ������ �� �� ������ �� ������� RqUID
		
		String sQuerry33 = 	"select p.PRRESPONSE FROM HUBBL.TRANSACTIONCONTEXT tc\n" + 
				"INNER JOIN HUBBL.PARAMETERS p\n" + 
				"    ON p.PRTCID = tc.TCID\n" + 
				"LEFT JOIN HUBBL.CLAIM c\n" + 
				"    ON c.CLID = tc.TCCLAIMID\n" + 
				"where '" + CalculatedVariables.testRqUID + "' in (tc.TCCLAIMOPERUID, tc.TCOPERUID, tc.TCRQUID, c.CLCLAIMRQUID)  \n ";  
		
		String actualResultFromDB22 = DataBaseLib.getDataAsString(sQuerry33, "HUBBL");
		System.out.println("actualResultFromDB = " + actualResultFromDB22 );
		
		
	}
	
		
	*/
	
	public static void GetFinalResponseFromBDhubble() throws Exception  {


		//������ ������ � �� AC ����� �� ��������� ������ �� �� ������ �� ������� RqUID ������������� �� ������� �������.
	
	
		
		CalculatedVariables.createProductPackageRs0 = GetSQL4hubbleRSLoop(20);
			
		
			while (CalculatedVariables.createProductPackageRs99.equals(CalculatedVariables.createProductPackageRs0)) {
				
				CalculatedVariables.createProductPackageRs0 = GetSQL4hubbleRSLoop(20);
			}
				
		
			
	}			
	
}
