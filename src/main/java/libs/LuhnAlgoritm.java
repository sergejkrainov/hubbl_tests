package libs;

public class LuhnAlgoritm {

	public static String correctNumberByLuhn(String num){
		int controlSum = 0;
		int lastNum = 0;

		if (isValidNumber(num)) {
            System.out.println(num + " is valid");
        }
        else {
            System.out.println(num + " is NOT valid");
        	controlSum = Integer.parseInt(System.getProperty("LuhnControlSum"));
        	int mod = controlSum % 10;
        	lastNum = Integer.parseInt(num.substring(num.length() - 1));
        	if(lastNum + (10 - mod) > 9){
        		lastNum = lastNum - mod;
        	}else{
        		lastNum = lastNum + 10 - mod;
        	}
        	

        }
		StringBuffer buf = new StringBuffer(num);
		buf.setCharAt(buf.length() - 1, String.valueOf(lastNum).toCharArray()[0]);
		num = buf.toString();
		if (isValidNumber(num)) {
            System.out.println(num + " is valid");
        }
        else {
            System.out.println(num + " is NOT valid");
        }
		
		return num;
		
	}
	
    private static boolean isValidNumber(String s) {
        return doLuhn(s, false) % 10 == 0;
    }

    private static int doLuhn(String s, boolean evenPosition) {
        int sum = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            int n = Integer.parseInt(s.substring(i, i + 1));
            if (evenPosition) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            evenPosition = !evenPosition;
        }
        System.setProperty("LuhnControlSum", String.valueOf(sum));
        return sum;
    }


}
