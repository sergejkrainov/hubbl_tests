package libs;

import variables.CalculatedVariables;
import variables.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class DataBaseLib {
	
	private  static Connection getConnection (String projectName) throws Exception
    {
        Connection conn = null;
        Statement stmt  = null;
        String host = "", port = "", serviceName = "", login = "", password = "";
        switch(projectName){
        	case "COD":
        		host = Config.BDHostCOD;
        		port = Config.BDPortCOD;
        		serviceName = Config.BDServiceNameCOD;
        		login = Config.BDLoginCOD;
        		password = Config.BDPasswordCOD;
        		break;
        	case "HUBBL":
        		host = Config.BDHostHUBBL;
        		port = Config.BDPortHUBBL;
        		serviceName = Config.BDServiceNameHUBBL;
        		login = Config.BDLoginHUBBL;
        		password = Config.BDPasswordHUBBL;
        		break;
        }
        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            conn  = DriverManager.getConnection("jdbc:oracle:thin:@//" + host + ":" + port + "/" + serviceName, login, password);
            stmt = conn.createStatement();

            stmt.close();
        } catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to get connection to DataBase: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
			 throw new Exception();
		 }
        
        return conn;
    }

	public static void ExecuteQueryUpdate ( String sQuery, String projectName ) throws Exception
    {
        Connection conn = null;
        try
        {
            conn = getConnection (projectName);
            conn.setAutoCommit ( true );
            Statement st = conn.createStatement();
            System.out.println("Excec update query: " + sQuery);
            st.executeUpdate(sQuery);
            st.close();
            //conn.commit();
            conn.close();   
        }catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
			 throw new Exception();

		 }
    }
	
    public static String getDataAsString ( String sQuery, String projectName){
        String sResult = new String ();
        
        try
        {
            Connection conn; 
            conn = getConnection (projectName);
             
            Statement stmt = conn.createStatement();
          //  System.out.println("Exequte sQuery = " + sQuery);
            ResultSet r = stmt.executeQuery (sQuery);
            sResult = "null"; 
            while (r.next())  
            {   
              if ( r.getString(1) != null )  {
                   sResult = r.getString(1);
                   break;
                } else{
                	sResult = "null";
                	break;
                	
                }
            }
            r.close();
            stmt.close();
            conn.close();                                   
            
         }catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
			 
			 sResult = "null";
		 }
        return sResult; 
        
    }

     public static int  getDataAsInt ( String sQuery, String projectName){
        int iResult = -1;
        try
        {
            Connection conn;
            conn = getConnection (projectName);
            
            Statement stmt = conn.createStatement();
            
            System.out.println("Exequte sQuery = " + sQuery);
            
            ResultSet r = stmt.executeQuery (sQuery);
                            
            while (r.next())  
            {   
              if ( r.getInt(1) > 0 )  {
                   iResult = r.getInt(1);
                   break;
              }          
            }
            r.close();
            stmt.close();
            conn.close();           
        }catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
		 }
        return iResult;
    }
	
	 public static String[][] getDataAsTable (String sQuery, String projectName)
    {
        String[][] tableResult;
        Connection conn;
        int colCount, rowCount, k = 0;
        
        try {
            conn = getConnection(projectName);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                    ResultSet.CONCUR_READ_ONLY );
            System.out.println("Exequte querry = " + sQuery);
            ResultSet rs = stmt.executeQuery (sQuery);
            ResultSetMetaData rsmd = rs.getMetaData();

            rs.last(); // ��� � ��������� ������� ����������
            rowCount = rs.getRow(); // ���� ����� ��������� ������
            rs.beforeFirst(); // ��� � ������

            colCount = rsmd.getColumnCount(); // ����� �������� � ����������

            tableResult = new String[rowCount][colCount]; // ��������� ������-�������
            while (rs.next()) { // ���� �� ��������� �� ����� �������
               for (int j = 0; j < colCount; j++) {
                   tableResult[k][j] = rs.getString(j+1);
               }
               k++;
            }
            rs.close();
            stmt.close();
            conn.close();
            return tableResult;
        }catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
		 }
        
        return null;
    }

    public static List<String> getAnyRow (String sQuery, String projectName)
    {
        List<String> lstResult = new ArrayList<String>();
        Connection conn;
        int colCount, rowCount, i = 0, k = 1;
        Random rand = new Random ();
        
        try {
            conn = getConnection(projectName);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                    ResultSet.CONCUR_READ_ONLY );
            System.out.println("Exequte sQuery = " + sQuery);
            ResultSet rs = stmt.executeQuery (sQuery);
            ResultSetMetaData rsmd = rs.getMetaData();

            rs.last(); // ��� � ��������� ������� ����������
            rowCount = rs.getRow(); // ���� ����� ��������� ������

            if (rowCount > 1000) {
                rowCount = 1000;
            } // ����� �� ��������� ���� ������. ������ �� 100 ����������
            i = rand.nextInt(rowCount)+1; // i = [1;rowCount]
            rs.beforeFirst();

            colCount = rsmd.getColumnCount(); // ����� �������� � ����������

            while (rs.next()) { // ���� ���� ��������� � ���� �� ��������� �� ��������� ������
                if (k==i) {
                    for (int j = 0; j < colCount; j++) {
                        lstResult.add(rs.getString(j + 1)); // ��������� ������ ���� ���������� � ����.
                    }
                    break;
                }
                k++;
                //rs.next();
            }
            rs.close();
            stmt.close();
            conn.close();
            
        } catch(Exception e){
			 try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
		 }
        
        return lstResult;
    }
    
    public static HashMap<String,String> getAnyRowAsHashMap (String sQuery, String[] ParamNames, String projectName)
    {
    	System.out.println("Exequte sQuery = " + sQuery);
    	HashMap<String,String> resultRow = new HashMap<String,String>();
        Connection conn;
        int colCount, rowCount, i = 0, k = 1;
        Random rand = new Random ();
        
        try {
            conn = getConnection(projectName);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                    ResultSet.CONCUR_READ_ONLY );
            ResultSet rs = stmt.executeQuery (sQuery);
            ResultSetMetaData rsmd = rs.getMetaData();

            rs.last(); // ��� � ��������� ������� ����������
            rowCount = rs.getRow(); // ���� ����� ��������� ������

            if (rowCount > 1000) {
                rowCount = 1000;
            } // ����� �� ��������� ���� ������. ������ �� 100 ����������
            i = rand.nextInt(rowCount)+1; // i = [1;rowCount]
            rs.beforeFirst();

            colCount = rsmd.getColumnCount(); // ����� �������� � ����������

            while (rs.next()) { // ���� ���� ��������� � ���� �� ��������� �� ��������� ������
                if (k==i) {
                    for (int j = 0; j < colCount; j++) {
                        //lstResult.add(rs.getString(j+1)); // ��������� ������ ���� ���������� � ����.
                        resultRow.put(ParamNames[j], rs.getString(j+1));
                    }
                    break;
                }
                k++;
                //rs.next();
            }
            rs.close();
            stmt.close();
            conn.close();

        } catch (IllegalArgumentException e) {//���� ������ �� ������ �� ����� ������
            if (CalculatedVariables.testCase.equals("SearchClientCommonCardWrongFIO")) {//��� ������� �������� ������ �� ���������� ������, �� ������
                resultRow = null;
        	}else{
        		
        		 try {
     				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e.getMessage(), Config.resultsPath );
     			 } catch (Exception e1) {
     				// TODO Auto-generated catch block
     				e1.printStackTrace();
     			 }
        		 System.setProperty("isError", "true");
    			 System.setProperty("isFail", "true");
        	}

        } catch (Exception e3) {//���� ��� ������� � �� �������� ������
            try {
				CommonLib.WriteLog("FAIL", "Error due to exequting sql request: \n" + e3.getMessage(), Config.resultsPath );
			 } catch (Exception e4) {
				// TODO Auto-generated catch block
				e4.printStackTrace();
			 }
			 System.setProperty("isError", "true");
			 System.setProperty("isFail", "true");
		 }
         
        return resultRow;
    }

    // ������ ��������� ������������� ��� � ��� �� ������ ��
    public static String getOperationDaySQL(String id_mega) {
        return "select TO_CHAR(trunc(day),'yyyy-mm-dd') as \"OperDate\"  from\n" +
                "(   select a.* from operday.dpcoperday a\n" +
                "    where (a.id_mega = "+ id_mega +"\n" +
                "    and a.state in (20, 30))\n" +
                "    or exists (select 1 from  operday.dpcfinishtask dpc \n" +
                "    where dpc.id_mega = "+ id_mega +" \n" +
                "    and dpc.state = 0\n" +
                "    and dpc.finishkind = 'OCSTOPLF'\n" +
                "    and a.id_mega = dpc.id_mega\n" +
                "    and a.state = 40\n" +
                "    and trunc(a.day) = trunc(dpc.day)\n" +
                "      )\n" +
                "    order by trunc(a.day) desc)\n" +
                "where rownum < 2";
    }


}
