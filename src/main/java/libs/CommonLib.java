package libs;

import org.junit.runner.notification.Failure;
import variables.CalculatedVariables;
import variables.Config;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonLib {

	
	/**
	 * ������� ��������� ������ ������������� �������(���������� ������������� ������� ����������� ���������)
	 * @param str - ������� ������
	 * @param length - ����� ������
	 * @return
	 * @throws Exception
	 */
	public static String setLengthString(String str, int length) throws Exception {	
		/*StringBuffer buf = new StringBuffer();
		buf.append(str);
		buf.setLength(length);
		for(int i = str.length(); i < length; i++){
			buf.setCharAt(i, ' ');
		}
		
		return buf.toString();*/
		
		try(Formatter fr = new Formatter()){	
			return fr.format("%-" + length + "s", str).toString();
		}
	
	}
	
	/**
	 * ������� ��������� ��������� ������ ������������ �����
	 * @param countLetters ����� ������
	 * @return ������ ������������ �����
	 * @throws Exception
	 */
	public static String getRandomLetters(int countLetters) throws Exception {

		String letters[] = {"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
				"�", "�", "�", "�", "�", "�", "�", "�", "�", "�"};
		
		Random rnd = new Random();
		
		String lettersStr = "";
		
		for(int i = 0; i < countLetters; i++){
			lettersStr+= letters[rnd.nextInt(letters.length)].toUpperCase();
		}		
		
		return lettersStr;
	
	}
	
	/**
	 * ������� ��������� ��������� ������ �� ���� ������������ �����
	 * @param countLetters ����� ������
	 * @return ������ ������������ �����
	 * @throws Exception
	 */
	public static String getRandomNumericLetters(int countLetters) throws Exception {	
		
		String letters[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		
		Random rnd = new Random();
		
		String lettersStr = "";
		
		for(int i = 0; i < countLetters; i++){
			lettersStr+= letters[rnd.nextInt(letters.length)];
		}		
		
		return lettersStr;
	
	}
	
	/**
	 * ������� �������� ������� � ����� ���������� ���������
	 * @param pathFromLog - ���� � ����� �� �������
	 * @param pathToLog - ���� � �����
	 * @param uniqueParam - ���������� ��������, �� �������� ���� ����
	 * @param textToContains - ��������� ���������
	 * @param charset - ���������
	 * @param timeOut - ������� � ��������
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static boolean waitForLogContainsString(String pathFromLog, String pathToLog, String uniqueParam, 
			String textToContains, String charset, int timeOut) throws Exception {	
		boolean isContains = false;

		System.out.println("����� " + timeOut + " ������... ��������� � ����:" + textToContains);

		//������� �����
		LocalDateTime currentStartTime = LocalDateTime.now();
		
		while(true){
			//��������� ���� � ���������� ������� ��� ��������		
			FileLib.copyFiles(pathFromLog, pathToLog, charset);
			//��������� ����
			String fileContent = FileLib.readFromFile(pathToLog, charset);
			//���� � ���� ���� ��������� ���������� ��������, �� ��������� ��� �� ���������� ������
			if(fileContent.contains(uniqueParam)){
				//���������� ���� ����� ������� �����
				fileContent = fileContent.substring(fileContent.indexOf(uniqueParam), fileContent.length());
				//���� ���������� ���� ����� ������� ����� �������� � ���� ��������� �����, �� �������
				if(fileContent.contains(textToContains)){
					isContains = true;
					break;
				}else{
					if (//���� ����� �������� ��������� �������, �� �������
							   Duration.between(currentStartTime, LocalDateTime.now())
							            .getSeconds() > timeOut
							            ){
							   isContains = false; 
							   try {
								   CommonLib.WriteLog("FAIL", "� ���� " + pathToLog.replace("\\\\", "\\") + " ����������� ������ " + textToContains + " � ������� " + timeOut + " ������",
												Config.resultsPath );
							   } catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
							   }
							   System.setProperty("isError", "true");
							   System.setProperty("isFail", "true");
						throw new Exception("� ���� " + pathToLog.replace("\\\\", "\\") + " ����������� ������ " + textToContains + " � ������� " + timeOut + " ������");
							   /*try {
									CommonLib.postProcessingTest();
							   } catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
							   }*/
						 }else{
						//���� ���� �������
							 Thread.sleep(500);
							 continue;
						 }
				}
				
			}else{
				if (//���� ����� �������� ��������� �������, �� �������
					   Duration.between(currentStartTime, LocalDateTime.now())
					            .getSeconds() > timeOut
					            ){
					   isContains = false;
					   try {
						   CommonLib.WriteLog("FAIL", "� ���� " + pathToLog + " ����������� ���������� �������� " + uniqueParam,
										Config.resultsPath );
					   } catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
					   }
					   System.setProperty("isError", "true");
					   System.setProperty("isFail", "true");
					throw new Exception("� ���� " + pathToLog + " ����������� ���������� �������� " + uniqueParam);
					   /*try {
							CommonLib.postProcessingTest();
					   } catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
					   }*/
				 }else{
					//���� ���� �������
					 Thread.sleep(500);
					 continue;
				 }
			}
		}

		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
				
		return isContains;
	}
	
	/**
	 * ������� ��������� ����������� ��������������
	 * @return
	 * @throws Exception
	 */
	public static String generateUID() throws Exception {
		String newUUID = java.util.UUID.randomUUID().toString().replaceAll("-",""); 
		return newUUID;
	}
	
	/**
	 * ������� ������ ������ � ���
	 * 
	 */
	public static void StartLog(String fileName) throws Exception {
		
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<html>\n" + "<head>\n" + "<title>Report of automation testing Hubble.cards " +  "</title>\n"
				+ "<h1>Report of automation testing: " + CalculatedVariables.testCase +  "</h1>\n" 
				+ "</head>\n" + "<meta charset=\"Windows-1251\">\n" + "<BODY bgcolor=\"2EAADB\" class=\"raphael\">\n");
		writer.close();
	}
	
	/**
	 * ������� ������� �������� ����������
	 * @param commandLine - ������� ��� ������� ����������
	 * @param description - �������� ����������
	 * @throws IOException
     * @throws InterruptedException 
     */
    public static void execProcess(String commandLine, String description) throws IOException, InterruptedException {
 	   
 	   Runtime proc = Runtime.getRuntime();
 	   Process pr = proc.exec(commandLine);
 	   
 	   System.out.println("Wait for exec " + description + " ...");
 	  System.out.println("Command line is " + commandLine);
		while (pr.isAlive()) {//����, ���� ���������� ����������

			Thread.sleep(500);
 	   }
 	   System.out.println("Exec " + description + " complete!");	   
 	   
    }
    
	/**
	 * ������� ������ ��������� � ������ � ������� ����������� ���������
	 *
	 * @param str - ������, ��� ������������ ����� ���������
	 * @param regex - ���������� ���������, �� �������� �������������� �����
	 * 
	 */
	public static boolean isStrContainsSubStr(String str, String regex) throws Exception {

		String strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(strLocalLogs);
		return m.find();
	}

	/**
	 * ������� ��������� ������ � ���
	 * 
	 */
	public static void EndLog(String fileName) throws Exception {

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("</PRE></BODY>\n</HTML>\n");
		writer.close();
	}

	/**
	 * ������� ������ ������ � ������� ���� � ����� ����������� ������
	 *
	 * @Param index - ������
	 * 
	 */
	public static void StartTableCommonResults(String index, String fileName) throws Exception {
		
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<table width=\"100%\" bgcolor=\"86D8E3\" style=\"table-layout:fixed\" id=\"table" + index
				+ "\" onclick=\"myFun(event)\" border=\"1\">\n" + "<col width=20%>\n" + "<col width=20%>\n"
				+ "<col width=60%>\n" + "<tr><th>TestCaseName</th>" + "<th>Result</th>" + "<th>EndTime</th>" + "</tr>\n");

		writer.close();
	}
	/**
	 * ������� ������ ������ � ������� ����
	 *
	 * @Param index - ������
	 * 
	 */
	public static void StartTable(String index, String fileName) throws Exception {

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<table width=\"100%\" bgcolor=\"86D8E3\" style=\"table-layout:fixed\" id=\"table" + index
				+ "\" onclick=\"myFun(event)\" border=\"1\">\n" + "<col width=20%>\n" + "<col width=20%>\n"
				+ "<col width=60%>\n" + "<tr><th>Level</th>" + "<th>Time</th>" + "<th>Log Message</th>" + "</tr>\n");
		writer.append("<tr onmouseover=\"ChangeColor(this, true);\" onmouseout=\"ChangeColor(this, false);\">");
		writer.append("<td>");
		writer.append("<b>");
		writer.append("<center><font color = \"FFFFFF\">");
		writer.append("START");
		writer.append("</font></center>");
		writer.append("</b>");
		writer.append("</td>");
		writer.append("<td>");
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		// this.WriteLog("INFO", formattedDate);
		writer.append("<center>" + formattedDate + "</center>");
		writer.append("</td>");
		writer.append("<td>");
		writer.append("Script Started");
		writer.append('\n');
		writer.append("</td>");
		writer.append("</tr>\n");
		writer.close();
	}

	/**
	 * ������� ��������� ������ � ������� ���� c j,obvb htpekmnfnfvb ntcnf
	 * @param fileName - ���� � ����� � ������������ �����
	 * 
	 */
	public static void EndTableCommonResults(String fileName) throws Exception {
		
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<tr onmouseover=\"ChangeColor(this, true);\" onmouseout=\"ChangeColor(this, false);\">");
		writer.append("<td>");
		writer.append("<b>");
		writer.append("<center><font color = \"FFFFFF\">");
		writer.append("END");
		writer.append("</font></center>");
		writer.append("</b>");
		writer.append("</td>");
		writer.append("<td>");
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		// this.WriteLog("INFO", formattedDate);
		writer.append("<center>" + formattedDate + "</center>");
		writer.append("</td>");
		writer.append("<td>");
		writer.append("Tests Finished");
		writer.append('\n');
		writer.append("</td>");
		writer.append("</tr>\n");
		writer.append("</table>\n");
		writer.close();
	}
	
	
	/**
	 * ������� ��������� ������ � ������� ����
	 * 
	 * 
	 */
	public static void EndTable(String fileName) throws Exception {

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<tr onmouseover=\"ChangeColor(this, true);\" onmouseout=\"ChangeColor(this, false);\">");
		writer.append("<td>");
		writer.append("<b>");
		writer.append("<center><font color = \"FFFFFF\">");
		writer.append("END");
		writer.append("</font></center>");
		writer.append("</b>");
		writer.append("</td>");
		writer.append("<td>");
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		// this.WriteLog("INFO", formattedDate);
		writer.append("<center>" + formattedDate + "</center>");
		writer.append("</td>");
		writer.append("<td>");
		writer.append("Script Finished");
		writer.append('\n');
		writer.append("</td>");
		writer.append("</tr>\n");
		writer.append("</table>\n");
		writer.close();
	}
	
	/**
	 * ������� ��������� ������ � ���
	 * 
	 */
	public static void getRunTestSuiteProp() throws Exception {
		Config.runTestSuiteProp.load(new FileInputStream(Config.runDirPath + Config.configPath + "RunTestSettings.txt"));

	}

	public static void getRunTestSuiteProp2() throws Exception {

	System.out.println ( "Config.runDirPath = " +	Config.runDirPath);

		Config.runTestSuiteProp.load(new FileInputStream(Config.runDirPath + Config.configPath + "RunTestSettings.txt"));

	}

		/**
		 * ������� ������ � ��� ��������� � ����� ����������� ������
		 *
		 * @Param statusResult - ��������� ������� �����
		 * @Param endTime - ����� ���������� �����
		 *
         */
	public static void WriteLogCommonResults(String statusResult, String endTime, String fileName) throws Exception {

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<tr onmouseover=\"ChangeColor(this, true);\" onmouseout=\"ChangeColor(this, false);\">");
		
		writer.append("<td>");
		writer.append(CalculatedVariables.testCase);
		writer.append("</td>");
		
		writer.append("<td>");
		writer.append("<b>");
		if (statusResult.equals("OK")) {
			writer.append("<center><font color = \"0E8C1D\">");
			writer.append(statusResult);
			writer.append("</font></center>");
		} else if (statusResult.equals("FAIL")) {
			writer.append("<center><font color = \"FF0000\">");
			writer.append(statusResult);
			writer.append("</font></center>");
		} 

		writer.append("</b>");
		writer.append("</td>");
		writer.append("<td>");
		
		writer.append("<center>" + endTime + "</center>");
		writer.append("</td>");
		writer.append("</tr>\n");
		writer.close();
		
	}
	/**
	 * ������� ������ � ��� ���������
	 *
	 * @Param Type - ��� ���������
	 * @Param Text - ���������� ���������
	 * 
	 */
	public static void WriteLog(String Type, String Text, String fileName) throws Exception {

		System.out.println(Text);

		Text = Text.replace(
			"\r",
			"<br>").replace(
					"<", 
					"&lt;").replace(
							">", 
							"&gt;");

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, true), "Windows-1251"));
		writer.append("<tr onmouseover=\"ChangeColor(this, true);\" onmouseout=\"ChangeColor(this, false);\">");
		writer.append("<td>");
		writer.append("<b>");
		if (Type.equals("INFO")) {
			writer.append("<center><font color = \"0E8C1D\">");
			writer.append(Type);
			writer.append("</font></center>");
		} else if (Type.equals("WARN")) {
			writer.append("<center><font color = \"FFFF00\">");
			writer.append(Type);
			writer.append("</font></center>");
		} else if (Type.equals("FAIL")) {
			writer.append("<center><font color = \"FF0000\">");
			writer.append(Type);
			writer.append("</font></center>");
		} else if (Type.equals("MISC")) {
			writer.append("<center><font color = \"DE50DE\">");
			writer.append(Type);
			writer.append("</font></center>");
		}

		writer.append("</b>");
		writer.append("</td>");
		writer.append("<td>");
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		// this.WriteLog("INFO", formattedDate);
		writer.append("<center>" + formattedDate + "</center>");
		writer.append("</td>");
		writer.append("<td>");
		writer.append(Text);
		writer.append('\n');
		writer.append("</td>");
		writer.append("</tr>\n");
		// writer.append("\r\n");
		// writer.append("NewLine!!!");
		formattedDate = null;
		writer.close();
	}
	
	/**
	 * ������� ��������� ����������� �����
	 * @throws Exception
	 */
	public static void postProcessingTest() throws Exception {

		CommonLib.EndTable(Config.resultsPath);
		CommonLib.EndLog(Config.resultsPath);
		
		FileLib.copyFiles(Config.resultsPath, CalculatedVariables.pathToCopyResults + "\\testResult.html", "Windows-1251");
		
		File dir = new File(Config.logsPath);	    	   
	 	File[] files = dir.listFiles();
		//�������� ��� ����� � ������ � ���������� � ������������ ��� �����
	 	if(files.length > 0){
	 		   for(int i = 0; i < files.length; i++){
	 			   String fileName = files[i].getName();
	 			   FileLib.copyFiles(Config.logsPath + "\\" + fileName, CalculatedVariables.pathToCopyResults + "\\" + fileName, "Windows-1251");
	 			   
	 		   }
	 	}

		//�������� ������ ������� ������
		getStatusResultTest();

		//���� ������ �� �� ������� � �������� �������� ������ �� �����
		if(!CalculatedVariables.isRunFromConsole && Config.isSendResultsToMail.equals("true")){
			CommonLib.sendResultsByEmail();
		}

		//���� ������ �� �� �������
		if(!CalculatedVariables.isRunFromConsole){
			//������� ���������� � ������������ ������
			FileLib.clearFolder(Config.resultsDir);
			//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		}
		
		clearAllObjects();
		
		
	}
	
	/**
	 * ������� ������������� ������� ������
	 * @throws Exception
	 */
	public static void afterRunningTest() throws Exception{
		System.out.println("Total number of tests:" + CalculatedVariables.testResult.getRunCount());
		System.out.println("Total number of tests faild:" + CalculatedVariables.testResult.getFailureCount());
		if(CalculatedVariables.testResult.getFailureCount() > 0){
			System.setProperty("isFail", "true");
			for(Failure fails : CalculatedVariables.testResult.getFailures()){
				CommonLib.WriteLog("FAIL", "Error: \n" + fails.getTrace(), Config.resultsPath );
			}
		}

	}
	
	/**
	 * ������� ������ �������� �����
	 * @throws Exception
	 */
	public static void resetAllsettings(){
		//���������� �������, ��� ������ ���
		System.setProperty("isFail", "false");
		System.setProperty("isError", "false");

	}
	
	/**
	 * ������� ��������� ������ ������
	 * @throws Exceptionf
	 */
	public static ArrayList<String> getRunClasses(){
		String testsStr = FileLib.readFromFile(System.getProperty("user.dir") + Config.configPath + "\\RunClasses.txt", "Windows-1251");
		return new ArrayList<String>(Arrays.asList(testsStr.split("\r\n")));
	}
	
	/**
	 * ������� ��������� ������ ���������� ������������ ������
	 * @throws Exception
	 */
	public static void getStatusResultTest() throws Exception {
		
		DateFormat formatForResult = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT);
		Calendar calendar = Calendar.getInstance();

		// ����� ���������� �����
		String endTime = formatForResult.format(calendar.getTime());

		String resultContent = FileLib.readFromFile(Config.resultsPath, "Windows-1251");

		String statusResult = "";

		if (resultContent.contains(">WARN")) {

			statusResult = "WARN";

		}
		if (resultContent.contains(">FAIL")) {

			statusResult = "FAIL";

		}
		if (!resultContent.contains(">FAIL") && !resultContent.contains(">WARN")) {

			statusResult = "OK";

		}

		CalculatedVariables.statusResultTest.put("testCase" , CalculatedVariables.testCase);
		CalculatedVariables.statusResultTest.put("testStatus" , statusResult);
		CalculatedVariables.statusResultTest.put("testEndTime" , endTime);
		
		CommonLib.WriteLogCommonResults(statusResult, endTime, Config.resultsPathCommon);

		
	}
	
	/**
	 * ������� �������� ����������� ������������ �� �����
	 * @throws Exception
	 */
	public static void sendResultsByEmail() {
		
		try {

			// ���� � ������ ��� ��������
			String pathToSendFileResult = Config.resultsPath;

			System.out.println("���������� ���� � ������� " + pathToSendFileResult);

			// ���� ������
			String subject = "Test results: " + CalculatedVariables.testCase + "_Stand_" + Config.allProp.get("testStandName") +  "_" + CalculatedVariables.startTime + 
					"_" + CalculatedVariables.statusResultTest.get("testStatus");


			// ���� ������
			String body = "Test " +  CalculatedVariables.testCase + " started at " + CalculatedVariables.startTime + 
					" .\n\n Ended at " + CalculatedVariables.statusResultTest.get("testEndTime") + " . \n\n " 
					 + "\n\n\n Results see in attachment file. ";

			Properties props = new Properties();

			props.put(
				"mail.smtp.host",
				Config.smtpHost);
			props.put(
					"mail.smtp.auth",
					"true");
			props.put(
				"mail.smtp.port",
				Config.smtpPort);
			props.put(
				"mail.from",
				"SBT-AVTOTEST@mail.ca.sbrf.ru");
			Session session = Session.getInstance(
				props,
				new javax.mail.Authenticator() {
		            protected PasswordAuthentication getPasswordAuthentication() {
		               return new PasswordAuthentication(
		                  Config.smtpLogin, Config.smtpPassword);
		            }
				}
					);
			System.out.println("mail.smtp.host:" + Config.smtpHost);
			System.out.println("mail.smtp.host:" + Config.smtpPort);
			System.out.println("mail.from:" + "SBT-AVTOTEST@mail.ca.sbrf.ru");
			System.out.println("mailTo:" + Config.mailTo);
			
			String mailTo = Config.mailTo;
			try {
				MimeMessage msg = new MimeMessage(session);
				MimeBodyPart mbp1 = new MimeBodyPart();
				MimeBodyPart mbp2 = new MimeBodyPart();

				String[] emails = mailTo.split(","); // ������ �����������

				InternetAddress[] dests = new InternetAddress[emails.length];
				for (int i = 0; i < emails.length; i++) {
					dests[i] = new InternetAddress(emails[i].trim().toLowerCase());
				}
				mbp1.setText(body);
				mbp2.attachFile(pathToSendFileResult);
				msg.setFrom();
				msg.setRecipients(
					Message.RecipientType.TO,
					dests);
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);
	
				msg.setContent(mp);
				Transport.send(msg);
				System.out.println(subject);
				System.out.println("���� � ������� ������� ���������");
			} catch (MessagingException mex) {
				
					System.err.println("send failed, exception: " + mex);
			}
			

		} catch (Exception ex) {

			System.err.println("�������� ������ � �������� �������� �����������: " + ex.getMessage());

		}
	
	}
	
	/**
	 * ������� ������� ������ ����� ���������� ������
	 */
	public static void clearAllObjects(){
		
		Config.allProp.clear();
		Config.businessProcessProp.clear();
		Config.stendProp.clear();
		Config.runTestSuiteProp.clear();
		Config.configPath = "\\src\\test\\resources\\hubbl\\csv\\config\\";
		
	}
}
