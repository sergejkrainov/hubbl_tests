package libs;

import variables.CalculatedVariables;
import variables.Config;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;


public class JunitMethods {
	
	/**
	 * ������� �������� �������� ���� �� ��������� true
	 * @param actualFieldName - �������� ������������ ����
	 * @param actualFieldValue - �������� ������������ ����
	 * @throws Exception 
	 */
	public static void assertTrue(String actualFieldName, boolean actualFieldValue) throws Exception{

		boolean isFailed = true;

		try{
			org.junit.Assert.assertTrue(actualFieldName, actualFieldValue);
			isFailed = false;
		}finally{
			if(isFailed){
				CommonLib.WriteLog("FAIL", actualFieldName + " is not ok!The value must be true, but it is " + actualFieldValue, Config.resultsPath);
				System.setProperty("isFail", "true");
			}else{
				CommonLib.WriteLog("INFO", actualFieldName + " ok!", Config.resultsPath);
			}
		}
		
	}
	
	
	public static void assertNull(String actualFieldName, boolean actualFieldValue) throws Exception{

		boolean isFailed = true;

		try{
			org.junit.Assert.assertNull(actualFieldName, actualFieldValue);
			isFailed = false;
		}finally{
			if(isFailed){
				CommonLib.WriteLog("FAIL", actualFieldName + " is not ok!The value must be NULL, but it is " + actualFieldValue, Config.resultsPath);
				System.setProperty("isFail", "true");
			}else{
				CommonLib.WriteLog("INFO", actualFieldName + " ok!", Config.resultsPath);
			}
		}
		
	}
	
	/**
	 * ������� �������� ���� �������� �� ���������
	 * @param actualFieldName - �������� ������������ ����
	 * @param actualFieldValue - �������� ������������ ����
	 * @param expectedFieldValue - ��������� �������� ������������ ����
	 * @throws Exception 
	 */
	public static <T> void assertEqualsValues( T expectedFieldValue,  T actualFieldValue, String actualFieldName) throws Exception{
		
		boolean isFailed = true;

		try{
			if(!expectedFieldValue.equals(actualFieldValue)){
				throw new Exception();
			}
			isFailed = false;
		}catch(Exception e){}finally{
			if(isFailed){
				CommonLib.WriteLog("FAIL", actualFieldName + " is not ok!The value must be " + expectedFieldValue + ", but it is " + actualFieldValue, Config.resultsPath);
				System.setProperty("isFail", "true");
			}else{
				CommonLib.WriteLog("INFO", actualFieldName + " ok!", Config.resultsPath);
			}
		}

	}
	
	/**
	 * ������� �������� �� ��������� �������� ��������� � ���� ��������
	 * @param expectedMap - ��������� ���������� ������
	 * @param actualMap - ������� ���������� ������
	 * @throws Exception 
	 */
	public static void checkHashMaps(HashMap<String, String> expectedMap, HashMap<String, String> actualMap) throws Exception{
		
		Set<Entry<String, String>> entrSet = expectedMap.entrySet();
		Iterator<Entry<String, String>> it = entrSet.iterator();
		Entry<String, String> entr;
		
		while(it.hasNext()){
			entr = it.next();

			JunitMethods.assertEqualsValues(entr.getValue(), actualMap.get(entr.getKey()), entr.getKey());
		}
	}
	
	/**
	 * ������� �������� ����� � ���������� �������
	 * @param uniqueParamSearch - ���������� ��������, �� �������� ���������� ����� ���������� ������ � ����
	 * @param checkStrings - ������� ����� � ����, ������� ���������� ���������
	 * @throws Exception
	 */
	public static void checkLogsFromRemoteServer(String uniqueParamSearch, String ... checkStrings) throws Exception{
	
		System.out.println("logsPath = " + Config.logsPath) ;
		File dir = new File(Config.logsPath);	    	   
	 	File[] files = dir.listFiles();
	 	System.out.println("uniqueParamSearch = " + uniqueParamSearch) ;
		//���� ��� � ���������� ���� � ����������� ��������� ��� �������
	 	if(files.length > 0){
	 		   for(int i = 0; i < files.length; i++){
	 			   String fileName = files[i].getName();
	 			   String fileContent = FileLib.readFromFile(Config.logsPath + fileName, "cp866");
	 			   if(fileContent.contains(uniqueParamSearch)){
	 				   CalculatedVariables.realLogNameFromRemoteServer = files[i].getName();
	 				   System.out.println("realLogNameFromRemoteServer = " + CalculatedVariables.realLogNameFromRemoteServer);
	 				   String strCheckedFor = fileContent.substring(fileContent.lastIndexOf(uniqueParamSearch), fileContent.length()); 				   
	 				   System.out.println("strCheckedFor = " + strCheckedFor);
	 				   CalculatedVariables.stringAreaForCheckLogs += strCheckedFor + "\t\n";
	 			   }else{
	 				   continue;
	 			   }
	 		   }
	 	}
 	   
	 	for(String checks : checkStrings){
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("Check contains remoteFilelog for " + checks, String.valueOf(CalculatedVariables.stringAreaForCheckLogs.contains(checks)));

			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("Check contains remoteFilelog for " + checks, "true");

	 	}
	}
	
	/**
	 * ������� �������� ����� � ���������� �������
	 * @param uniqueParamSearch - ���������� ��������, �� �������� ���������� ����� ���������� ������ � ����
	 * @param charset - ���������
	 * @param checkStrings - ������� ����� � ����, ������� ���������� ���������
	 * @throws Exception
	 */
	public static void checkLogs(String filePath, String charset, String ... checkStrings) throws Exception{
	
	 	String fileContent = FileLib.readFromFile(filePath, charset);

	 	for(String checks : checkStrings){
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("Check contains filelog for " + checks, String.valueOf(fileContent.contains(checks)));

			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("Check contains filelog for " + checks, "true");

	 	}
	}
	/**
	 * ������� �������� ����� � ���������� �������
	 * @param uniqueParamSearch - ���������� ��������, �� �������� ���������� ����� ���������� ������ � ����
	 * @param charset - ���������
	 * @param checkStrings - ������� ����� � ����, ������� ���������� ���������
	 * @throws Exception
	 */
	
	public static void checkLogsNOT(String filePath, String charset, String ... checkStrings) throws Exception{
	
	 	String fileContent = FileLib.readFromFile(filePath, charset);

	 	for(String checks : checkStrings){
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("Check NOT contains filelog for " + checks, String.valueOf(fileContent.contains(checks)));

			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("Check NOT contains filelog for " + checks, "false");

	 	}
	}

}
