package libs;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import variables.Config;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * ����� ��� ������ � ��������� ��������
 * 
 * @author sbt-kraynov-sa
 *
 */
public class RemoteServerMethods {

	/**
	 * ������� ���������� ����������� ����� ��� �������� �� ��������� ������
	 * 
	 * @param operationName
	 *            - �������� �������� �����
	 * @param testCaseName
	 *            - �������� ����� �����
	 * @param addParams
	 *            - �������������� ���������
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String prepareFileTextContent(String operationName, String testCaseName, String... addParams)
			throws Exception {

		String textContent = "";
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String paramNames = "";
		String sQuerry = "";
		HashMap<String, String> rowParams;
		String endDate = "";
		Date curDateCardBlockCode;
		String fcurDateCardBlockCode;
		String birthDay = "";
		String textEmbose = "";
		String fio = "";
		Random rnd = new Random();

		/*switch (operationName) {

		case "CardBlock":

			paramNames = "NUMCARD,ENDDATE,BRANCHNO,OFFICE";
			sQuerry = "select a.numcard, a.ENDDATE, a.BRANCHNO, a.OFFICE from deposit.dcard a, deposit.deposit b "
					+ " where a.deposit_minor=b.id_minor and a.deposit_major =b.id_major and  a.numcard > 0 "
					+ "and blockcode is null and a.id_mega = 38 and b.id_mega = a.id_mega "
					+ "and (select count(*) from deposit.dcard c where c.numcard = a.numcard) = 1 and rownum <2 ";
			rowParams = DataBaseLib.getAnyRowAsHashMap(sQuerry, paramNames.split(","));
			System.out.println("rowParams = " + rowParams);

			endDate = rowParams.get("ENDDATE").split("-")[0].substring(2) + rowParams.get("ENDDATE").split("-")[1];

			curDateCardBlockCode = date;

			curDateCardBlockCode.setMinutes(curDateCardBlockCode.getMinutes() - 20);

			fcurDateCardBlockCode = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(curDateCardBlockCode);
			CalculatedVariables.cardNumber = rowParams.get("NUMCARD");
			CalculatedVariables.endDateCard = endDate;
			CalculatedVariables.branchNumber = rowParams.get("BRANCHNO");
			CalculatedVariables.officeNumber = rowParams.get("OFFICE");
			textContent = rowParams.get("NUMCARD") + "\t" + endDate + "\t38/" + rowParams.get("BRANCHNO") + "/"
					+ rowParams.get("OFFICE") + "\t" + addParams[0] + "\tte\tcomment\tAANDRIENKO\t"
					+ fcurDateCardBlockCode;

			break;

		}*/

		return textContent;

	}

	/**
	 * ������� �������� ���������� ����� �� ��������� ������
	 * 
	 * @param fileContent
	 *            - ���������� �����
	 * @param fileName
	 *            - ��� �����
	 * @param pathToFileRemoteServer
	 *            - ���� � ����� �� ��������� �������
	 */
	public static void putFileToRemoteServer(String fileContent, String fileName, String pathToFileRemoteServer) {
		String user = Config.remoteServerLoginCOD;
		String password = Config.remoteServerPasswordCOD;
		String host = Config.remoteServerHostCOD;
		int port = 22;

		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			System.out.println("Establishing Connection...");
			session.connect();
			System.out.println("Connection established.");

			System.out.println("Crating SFTP Channel.");
			ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
			sftpChannel.connect();
			System.out.println("SFTP Channel created.");

			sftpChannel.put(new ByteArrayInputStream(fileContent.getBytes(Charset.forName("cp866"))),
					pathToFileRemoteServer + fileName, sftpChannel.OVERWRITE);
			System.out.println("File " + fileName + " was downloaded to " + pathToFileRemoteServer);

		} catch (Exception e) {
			System.err.print(e);
		}
	}

	/**
	 * ������� �������� ������ � ���������� ������� � ��������� ����������
	 * 
	 * @param fileFrom
	 * @param fileTo
	 */
	public static void downloadFileFromRemoteServer(String fileFrom, String fileTo) {
		String user = Config.runTestSuiteProp.getProperty("remoteServerLoginCOD");
		String password = Config.runTestSuiteProp.getProperty("remoteServerPasswordCOD");
		String host = Config.runTestSuiteProp.getProperty("remoteServerHostCOD");
		int port = 22;
		try {
			JSch jsch = new JSch();
			System.out.println("user:" + user);
			System.out.println("password:" + password);
			System.out.println("host:" + host);
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			System.out.println("Establishing Connection...");
			session.connect();
			System.out.println("Connection established.");

			System.out.println("Crating SFTP Channel.");
			ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
			sftpChannel.connect();
			System.out.println("SFTP Channel created.");

			InputStream out = null;
			System.out.println("Download file " + fileFrom + " ...");
			out = sftpChannel.get(fileFrom);
			StringBuffer sb = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(out));
			String line = "";
			while ((line = br.readLine()) != null){
				sb.append(line);
			}
		    br.close();
			String fileContext = sb.toString();
			FileLib.writeToFile(fileTo, fileContext, "UTF-8");

		} catch (Exception e) {
			System.err.print(e);
		}
	}
}
