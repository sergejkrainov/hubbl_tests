package libs;

import variables.Config;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FileLib {

	/**
	 *
     * ������� ������ ������ � ����
     *
     * @param fName - ���� � �����
     * @param data - ������, ������� ���������� �������� � ����
     * @param charset - ���������
     */
	    public static void writeToFile(String fName, String data, String charset) throws IOException {
	        try {
	        	System.gc();
	        	File f = new File(fName);
                if (f.exists()) {//������� ����, ���� �� ��� ����������
                    f.delete();
	        		
	        	}
	        	Thread.sleep(500);
	            try(PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fName, false), charset))){

		            BufferedWriter BWf = new BufferedWriter(pw);
                    BWf.write(data); // �������������� ������ � ����
                    BWf.close(); // �� ������� ������� ����
                    pw.close();
	            }

	        } catch (Exception e) {
	        	e.printStackTrace();
	            System.out.println("Cannot write to file: " + fName);
	        }
	    }
	    
	    /**
	    *
         * ������� ������ ������ �� �����
         *
         * @param fileName - ���� � �����
         * @param charset - ���������
         */
	       public static String readFromFile(String fileName, String charset) {
	    	   String fileContent = "";
	    	   
		       try(FileInputStream stream = new FileInputStream(new File(fileName))){
				   FileChannel fc = stream.getChannel();
				   MappedByteBuffer bufferBytes = fc.map(
				   			FileChannel.MapMode.READ_ONLY,
				   			0,
				   			fc.size());
				   
	
				   fileContent = Charset.forName(
						   charset).decode(
				   			bufferBytes).toString();
				   stream.close();
		       } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	           return fileContent;
	       }
	       
	       /**
	        *
            * @param fPath - ���� � ����� � �����������
            * @param mapParams - ������, ���� ��������� ���������
            * @throws IOException
	        */
	       public static void setParamsToHashMap(String fPath, HashMap<String, String> mapParams) throws IOException {
	    	   
	    	   FileInputStream fstream = null;
	           DataInputStream in      = null;
	           BufferedReader  br      = null;
	           String sDelimiter       = new String ("\t");
	    	   fstream = new FileInputStream(fPath);
	           in = new DataInputStream ( fstream );
	           br = new BufferedReader  ( new InputStreamReader(in) );
	           String sLine            = new String ();

	           while ( (sLine = br.readLine()) != null ){
	                if ( sLine.contains( sDelimiter ) ){	                	
	                    List<String> arr = Arrays.asList(sLine.split ( sDelimiter ));
	                    mapParams.put(arr.get(0), arr.get(1));
	                   
	                } 
	            }
	           br.close();
	           
	       }
	       
	       /**
            * ������� ����������� ���� ������
            * @param fileFrom - ����, ������ ����������
            * @param fileTo - ����, ���� ����������
            * @param charset - ���������
            * @throws IOException
	        */
	       public static void copyFiles(String fileFrom, String fileTo, String charset) throws IOException {
	    	   String buff = readFromFile(fileFrom, charset);
	    	   writeToFile(fileTo, buff, charset);
	    	   
	       }
	       
	       /**
            * ������� ���������� ���� ��� �������� ����� �� ������ ����
            * @param fileFrom - ���� � ����� � �����
            * @param fileTo - ���� � ����� � ����������� xml ����������
            * @param charset - ���������
            * @param uniquesearchParam - ���������� ��������, �� �������� ����� ������ xml ��������� � ����
            * @param countLeftBorderPosition - ���������� �� ����������� ��������� �� ������ ���� ����������� ����
            * @throws IOException
			 */
			public static void extractLocalLog(String fileFrom, String fileTo, String charset,String uniquesearchParam, String uniquesearchParamName, int countLeftBorderPosition) throws IOException{
                //������ ���
                String logContent = FileLib.readFromFile(fileFrom, charset);
                //����� ����, ���������� ������ ���������� �� �������� �����
                String logXMLContains = logContent.substring(logContent.indexOf("<" + uniquesearchParamName + ">" + uniquesearchParam) - countLeftBorderPosition, logContent.length());
		    	FileLib.writeToFile(fileTo, logXMLContains, "Windows-1251");
			}
	       
	       /**
            * ������� �������� ����� � ���������� �������
            * @param logPath - ���� � �����
            * @param logNameMask - ����� ����� ����� � �����
            * @throws IOException
	        * @throws InterruptedException 
	        */
	       public static void downLoadLogsFromRemoteServer(String logPath, String logNameMask) throws IOException, InterruptedException {
	    	   
	    	   Runtime proc = Runtime.getRuntime();
	    	   System.out.println("Execute " + "pscp -v -C -2 -l " + Config.remoteServerLoginCOD + " -pw " + Config.remoteServerPasswordCOD +  " " + 
	    			   	Config.remoteServerLoginCOD + "@" + Config.remoteServerHostCOD + ":" + logPath + logNameMask + " " + Config.logsPath);
	    	   Process pr = proc.exec("pscp -v -C -2 -l " + Config.remoteServerLoginCOD + " -pw " + Config.remoteServerPasswordCOD +  " " + 
	    			   	Config.remoteServerLoginCOD + "@" + Config.remoteServerHostCOD + ":" + logPath + logNameMask + " " + Config.logsPath);
	    	   
	    	   System.out.println("Wait for download files...");
               while (pr.isAlive()) {//����, ���� ���������� ��� �����

                   Thread.sleep(1000);
	    	   }
	    	   System.out.println("Download files complete.");	   
	    	   
	       }
	       
	       /**
            * ������� �������� ������ �� ����������
            * @param folderName - ��� ����������
            * @throws IOException
	        */
	       public static void clearFolder(String folderName) throws IOException {
	    	   System.out.println("Remove files from " + folderName);
	    	   System.gc();
	    	   File dir = new File(folderName);	    	   
	    	   File[] files = dir.listFiles();
	    	   
	    	   if(files.length > 0){
	    		   for(int i = 0; i < files.length; i++){
	    			   System.out.println("Remove file " + files[i].getName());
	    			   files[i].delete();
	    		   }
	    	   }
	       }
	
}
