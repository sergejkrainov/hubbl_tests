package libs;

import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.*;
import com.ibm.msg.client.wmq.WMQConstants;


import javax.jms.*;
import java.util.Enumeration;

/**
 * Created by sbt-sharikov-pi on 13.10.2017.
 */
public class MQLib22 {
	//private static final Logger LOG = Logger.getLogger(MQLib22.class);

	/**
	 * Функция коннекта к MQ серверу
	 *
	 * @String mqHost - хост MQ сервера
	 * @String mqPort - порт MQ сервера
	 * @String mqQueueManager - название менеджера очередей
	 * @String mqChannel - название канала
	 */
	public static MQQueueConnection connectToMQ(String mqHost, String mqPort, String mqQueueManager, String mqChannel) throws Exception {
		MQQueueConnection connect = null;
		try {
			MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
			cf.setHostName(mqHost);
			cf.setPort(Integer.parseInt(mqPort));
			//cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
			cf.setTransportType(WMQConstants.WMQ_CM_CLIENT);
			cf.setQueueManager(mqQueueManager);
			cf.setChannel(mqChannel);
			connect = (MQQueueConnection) cf.createConnection();
		} catch (Exception e) {
			System.out.println("Не удалось подключиться к MQ серверу");
		}
		return connect;
	}

	/**
	 * Функция отправки сообщения на MQ сервер
	 *
	 * @String message - тело сообщения
	 * @String queueName - название очереди
	 * @String mqHost - хост MQ сервера
	 * @String mqPort - порт MQ сервера
	 * @String mqQueueManager - название менеджера очередей
	 * @String mqChannel - название канала
	 */
	public static void sendMessageToMQ(String message, String queueName, String mqHost, String mqPort,
									   String mqQueueManager, String mqChannel) throws Exception {
		try {
			System.out.println("Start send message to queueName " + queueName);
			MQQueueConnection connection = connectToMQ(mqHost, mqPort, mqQueueManager, mqChannel);
			MQQueueSession session = (MQQueueSession) connection.createQueueSession(false, MQSession.AUTO_ACKNOWLEDGE);
			MQQueue queue = new MQQueue("queue:///" + queueName);
			MQQueueSender sender = (MQQueueSender) session.createSender(queue);
			JMSTextMessage JMSmessage = (JMSTextMessage) session.createTextMessage(message);

			connection.start();
			sender.send(JMSmessage);
			System.out.println("Sent message: " + JMSmessage);
			sender.close();
			session.close();
			connection.close();
		} catch (JMSException jmsex) {
			System.out.println("JMS error");
		} catch (Exception ex) {
			System.out.println("exception error");
		}
	}

	/**
	 * Функция получения сообщения от MQ сервера
	 *
	 * @String queueName - название очереди
	 * @String mqHost - хост MQ сервера
	 * @String mqPort - порт MQ сервера
	 * @String mqQueueManager - название менеджера очередей
	 * @String mqChannel - название канала
	 */
	public static String receiveMessageFromMQ(String queueName, String mqHost, String mqPort, String mqQueueManager, String mqChannel,
											  String selector) throws Exception {
		QueueConnection connectionMQ = null;
		try {
			connectionMQ = connectToMQ(mqHost, mqPort, mqQueueManager, mqChannel);
			QueueSession queueSession = connectionMQ.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue responseQueue = queueSession.createQueue("queue:///" + queueName);
			connectionMQ.start();
			String textMsg = "";
			Message message;
			MessageConsumer consumer = null;
			String sltr = selector;
			consumer
					= queueSession.createConsumer(responseQueue, sltr);
			message = consumer.receive(60000);
			if (message instanceof TextMessage) {
				textMsg = ((TextMessage) message).getText();
			} else {
				byte[] buf = new byte[(int) ((BytesMessage) message).getBodyLength()];
				((BytesMessage) message).readBytes(buf);
				textMsg = new String(buf, "UTF-8");
			}
			System.out.println(textMsg);
			consumer.close();
			queueSession.close();
			return textMsg;
		} catch (JMSException jmsex) {
			return "Error";
		} catch (Exception ex) {
			return "Error";
		}
	}

	/**
	 * Функция очистки очереди сообщений для увеличения шанса получения именно нашего сообщения из очереди
	 *
	 * @throws Exception
	 * @String queueName - название очереди
	 * @String mqHost - хост MQ сервера
	 * @String mqPort - порт MQ сервера
	 * @String mqQueueManager - название менеджера очередей
	 * @String mqChannel - название канала
	 */
	public static void clearQueue(String queueName, String mqHost, String mqPort, String mqQueueManager, String mqChannel) throws Exception {
		Queue responseQueue;
		Session session = null;
		QueueBrowser qb = null;
		MessageConsumer consumer;
		QueueConnection connectionMQ = null;
		try {
			connectionMQ = connectToMQ(mqHost, mqPort, mqQueueManager, mqChannel);
			responseQueue = new MQQueue(queueName);
			session = connectionMQ.createSession(true, -1);
			qb = session.createBrowser(responseQueue);
			Enumeration<?> enm = qb.getEnumeration();
			int qDeaph = 0;
			while (enm.hasMoreElements()) {
				Message msg = (Message) enm.nextElement();
				consumer = session.createConsumer(responseQueue);
				Message message = consumer.receive(20000);
				if (message != null) {
					qDeaph++;	}
			}
			if (qDeaph > 0) {
				System.out.println(("Из очереди " + queueName + " было очищено сообщений: " + qDeaph));
			}
		} catch (Exception e) {
			System.out.println("Error: " + e.getCause().getMessage());
		} finally {
			session.commit();
			qb.close();
			session.close();
			connectionMQ.close();
		}
	}

	public static void clearQueue2(String queueName, String mqHost, String mqPort, String mqQueueManager, String mqChannel) {
		Queue responseQueue;
		Session session = null;
		QueueBrowser qb = null;
		MessageConsumer consumer;
		QueueConnection connectionMQ = null;
		try {
			connectionMQ = connectToMQ(mqHost, mqPort, mqQueueManager, mqChannel);
			QueueSession queueSession = connectionMQ.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			responseQueue = queueSession.createQueue("queue:///" + queueName);
			connectionMQ.start();
			String textMsg = "";
			Message message;
			consumer = null;
			int qDeaph = 0;
			consumer = queueSession.createConsumer(responseQueue);
			while (consumer.receiveNoWait() != null) {
				message = consumer.receiveNoWait();
				if (message instanceof TextMessage) {
					textMsg = ((TextMessage) message).getText();
					if (textMsg.equals(null)) break;
				}
				if (textMsg != null) {
					qDeaph++;
				}
			}
			consumer.close();
			queueSession.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}