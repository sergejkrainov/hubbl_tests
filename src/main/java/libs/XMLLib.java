package libs;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import variables.CalculatedVariables;
import variables.Config;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;


public class XMLLib {

	/**
	 * ������� �������� xml �� xsd �����
	 * @param xml - ���������� xml
	 * @param xsdPath - ���� � xml
	 * @param xmlName - ��� xml �����
	 * @return
	 * @throws Exception 
	 */
		public static String validationXML(String xml, String xsdPath, String xmlName) throws Exception {
			String validationResult = "";
			// 1. ����� � �������� ���������� ������� ��� ����� XML Schema
			SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			// 2. ���������� �����
			// ����� ����������� � ������ ���� java.io.File, �� �� ����� ������ ������������
			// ������ java.net.URL � javax.xml.transform.Source
			File schemaLocation = new File(xsdPath);
	        Schema schema = factory.newSchema(schemaLocation);

			// 3. �������� ���������� ��� �����
			Validator validator = schema.newValidator();

			// 4. ������ ������������ ���������
			Reader reader = new StringReader(xml);
	
	        StreamSource source = new StreamSource(reader);

			// 5. ��������� ���������
			try {
	            validator.validate(source);
	            validationResult = "true";
	        }
	        catch (SAXException ex) {
	            validationResult = "xml " + xmlName + " is not valid because : \n" + ex.getMessage();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return validationResult;
	        
	    }
		
		/**
		 * ������� ���������� xml ��������� �� ���� � ������ � ����
		 * @param fileFrom - ���� � ����� � �����
		 * @param fileTo - ���� � ����� � ����������� xml ����������
		 * @param charset - ���������
		 * @param tagName - ��� ����, �� �������� ������ xml ���������
		 * @throws IOException
		 */
		public static void extractXMLFromLog(String fileFrom, String fileTo, String charset, String tagName) throws IOException {
	    	//    
			String logContent = FileLib.readFromFile(fileFrom, charset);
	    	// ,         
	    	String logXMLContains = logContent.substring(logContent.indexOf("<" + tagName), logContent.indexOf("</" + tagName + ">") + ("</" + tagName + ">").length() );
	    	
	    	
	    	
	    	FileLib.writeToFile(fileTo, logXMLContains, charset);
	    	   
	    }
		
		/**
		 * ������� ���������� ���������� xml ��������� �� ���� � ������ � ����
		 * @param fileFrom - ���� � ����� � �����
		 * @param fileTo - ���� � ����� � ����������� xml ����������
		 * @param charset - ���������
		 * @param tagName - ��� ����, �� �������� ������ xml ���������
		 * @throws IOException
		 */
		public static void extractLastXMLFromLog(String fileFrom, String fileTo, String charset, String tagName) throws IOException {
			//������ ���
			String logContent = FileLib.readFromFile(fileFrom, charset);
			//����� ����, ���������� ������ ���������� �� �������� �����
			String logXMLContains = logContent.substring(logContent.lastIndexOf("<" + tagName), logContent.lastIndexOf("</" + tagName + ">") + ("</" + tagName + ">").length() );
	    	   
	    	FileLib.writeToFile(fileTo, logXMLContains, charset);
	    	   
	    }
		
		/**
		 * ������� ���������� xml ��������� �� ���� � ������ � ���� �� ����� �������
		 * @param fileFrom - ���� � ����� � �����
		 * @param fileTo - ���� � ����� � ����������� xml ����������
		 * @param charset - ���������
		 * @param tagName - ��� ����, �� �������� ������ xml ���������
		 * @param leftBorderText - ����� � ����� ��������
		 * @param countAppearence - ���������� ����� ���������� ��������� � �����(first/last)
		 * @throws IOException
		 */
		public static void extractXMLFromLogByLeftBorder(String fileFrom, String fileTo, String charset, String tagName, String leftBorderText, String countAppearence) throws IOException {
			//������ ���
			String logContent = FileLib.readFromFile(fileFrom, charset);
			int leftBorderIndex = 0;
			switch(countAppearence){
				case "first":
					leftBorderIndex = logContent.indexOf(leftBorderText) + leftBorderText.length() +
							"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>".length() + 2;//2 ������� ��������
					break;
				case "last":
					leftBorderIndex = logContent.lastIndexOf(leftBorderText) + leftBorderText.length() +
							"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>".length() + 2;//2 ������� ��������
					break;
			}
			
			logContent = logContent.substring(leftBorderIndex);
			//����� ����, ���������� ������ ���������� �� �������� �����
			String logXMLContains = logContent.substring(0, logContent.indexOf("</" + tagName + ">") + ("</" + tagName + ">").length() );
	    	   
	    	FileLib.writeToFile(fileTo, logXMLContains, charset);
	    	   
	    }
		
		/**
		 * ������� ���������� xml ��������� �� ���� � ������ � ���� �� ����� �������
		 * @param fileFrom - ���� � ����� � �����
		 * @param charset - ���������
		 * @param tagName - ��� ����, �� �������� ������ xml ���������
		 * @param leftBorderText - ����� � ����� ��������
		 * @param countAppearence - ���������� ����� ���������� ��������� � �����(first/last)
		 * @throws IOException
		 */
/*		public static String extractValueFromLogByTagName(String fileFrom, String charset) {
			String xmlMessage = "";
			return xmlMessage;
		}
	*/	
		
	    /**
		 *
		 * ������� ������������ xml � �������
		 *
		 * @param xmlDoc - XML � Document �������������
		 * @param xsdNameValidation - �������� xsd ����� ��� ��������� xml
		 * @param xmlContentTemplateName - �������� ������� � XML
		 * @param xmlName - ��� xml �����
		 */
		    public static String createXMLMessage(Document xmlDoc, String xsdNameValidation, boolean isXmlValidation, String xmlContentTemplateName) {
		    	
		    	String xmlMessage = "";
		    	try{
	
		            CalculatedVariables.xmlDOMMessage = xmlDoc;
		            CalculatedVariables.xmlMessage = convertDomToString(xmlDoc);

					//����������� DOM Document � String
					xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + CalculatedVariables.xmlMessage;//��� �������� �������������� �������� �������� ���������

					// System.out.println("xmlMessage = " + xmlMessage);
		            
		            if(isXmlValidation){
						//�������� xml ��������� �� xsd �����
						String isXMLValid;
						isXMLValid = validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + xsdNameValidation + ".xsd", xmlContentTemplateName);				 			 	
						CalculatedVariables.actualValues.put("XMLRequestValid_for_" + xmlContentTemplateName, isXMLValid);
						//��������� ����������� �������� ����������
						CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + xmlContentTemplateName, "true");
		            }
		            
		    	}catch(Exception e){
		    		e.printStackTrace();
		    	}
	            
	            return xmlMessage;

		    	
		    }
		    
		    /**
			 *
			 * @param testOperationName - �������� ������ ��� ������������
			 * @param templateName - �������� ������� � xml ����������
			 * @param docXML - xml ��������
			 * @return docXML
			 */
			 public static Document redefineXMLParams(String testOperationName, String templateName, Document docXML) throws Exception {
				 Random rnd = new Random();	
				 String paramNames = "";
				 String sQuerryGetParams = "";
				 HashMap<String, String> rowParams = new HashMap<String, String>(); 
				 HashMap<String, String> mainParams = new HashMap<String, String>();
				 HashMap<String, String> resultRow = new HashMap<String, String>();
				 String[] firstRow;
				 String[] secondRow;
				 String[] selectedRow;
				 String sQuerry = "";
				 String[][] resultsFromDB;
				 List<String> rowList;
				 String newUUID = "";
				 String RqUID = "";
				 String OperUID = "";
				 String RqTm = "";
				 
				 LocalDate todayDate = LocalDate.now();
				 LocalDateTime todayDateTime = LocalDateTime.now();
				 DateTimeFormatter localDateFormatter;
				 
				 String currentOperDate = "";

				 
				 switch(testOperationName){
					
				 	case "CreateNewCard":
						switch(templateName){
							case "CreateProductPackageRq":
								
								currentOperDate = todayDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
								
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								RqUID = newUUID;
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								OperUID = newUUID;
								CalculatedVariables.testUUID = RqUID;
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("OperUID", OperUID);
								rowParams.put("RqTm", todayDateTime.toString());
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
							break;
							
							case "CreateProductPackageConnectUDBORq":
								
								
								currentOperDate = todayDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
								
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								RqUID = newUUID;
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								OperUID = newUUID;
								CalculatedVariables.testUUID = RqUID;

								String lastName = "�������-" + CommonLib.getRandomLetters(15);
								String firstName = CommonLib.getRandomLetters(15);
								String middleName = CommonLib.getRandomLetters(15);
								
								String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
								String idNum = CommonLib.getRandomNumericLetters(6);
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("OperUID", OperUID);
								rowParams.put("RqTm", todayDateTime.toString());
								rowParams.put("IdSeries", idSeries);
								rowParams.put("IdNum", idNum);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

								//������ ��� 
								rowParams.clear();
								rowParams.put("LastName", lastName);
								rowParams.put("FirstName", firstName);
								rowParams.put("MiddleName", middleName);
								
								docXML = setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
								
							break;
							
							case "GetCardHolderRs":
								
								RqUID = CalculatedVariables.rqUID;
								OperUID = CalculatedVariables.operUIDgetCardHolderRq;
								RqTm = CalculatedVariables.rQTm;
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("OperUID", OperUID);
								rowParams.put("RqTm", RqTm);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
							break;
							
							case "CustAddRs":
								
								RqUID = CalculatedVariables.rqUID;
								RqTm = CalculatedVariables.rQTm;
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("RqTm", RqTm);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
							break;
							
							case "IssueCardRs":
								
								RqUID = CalculatedVariables.rqUID;
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("MainApplRegNumber", CalculatedVariables.mainApplRegNumber);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
							break;
							
							case "NotifyIssueCardResultNfRs":
								
								RqUID = CalculatedVariables.rqUID;
								String cardNumNotifyIssueCardResultNf = "";

								//���������� 16-�� ������� ����� �����, ������� ����������� � ��
								do{
									cardNumNotifyIssueCardResultNf = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
									sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNumNotifyIssueCardResultNf + "'";
								}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
								System.out.println("GeneratedCardNum = " + cardNumNotifyIssueCardResultNf);
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("MainApplRegNumber", CalculatedVariables.mainApplRegNumber);
								rowParams.put("AcctId", CalculatedVariables.acctId);
								rowParams.put("CardNum", cardNumNotifyIssueCardResultNf);
											
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
							break;
							
						}
						break;
						
				 	case "IssueNewCard":
				 		switch(templateName){
				 			case "InitiateDeliveryCardRq":
				 				
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								RqUID = newUUID;
								newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
								OperUID = newUUID;
								CalculatedVariables.operUID = OperUID;
								System.out.println("OperUID = " + OperUID);
								
								rowParams.put("RqUID", RqUID);
								rowParams.put("OperUID", OperUID);
								rowParams.put("NumContrCard", CalculatedVariables.mailAccount);
								
								sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
								rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
								
								sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
								rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
								
								CalculatedVariables.extDt = rowParams.get("ExpDt");
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

								//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
				 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
				 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
				 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
				 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
				 				
				 				deleteTagInDocument(docXML, "Birthplace");
				 				deleteTagInDocument(docXML, "Citizenship");
				 				deleteTagInDocument(docXML, "TaxId");
				 				deleteTagInDocument(docXML, "ClientCategory");
				 				deleteTagInDocument(docXML, "ClientStatus");
				 				deleteTagInDocument(docXML, "Verified");
				 				deleteTagInDocument(docXML, "Signed");
				 				deleteTagInDocument(docXML, "ContactInfo");
				 				
				 				break;
				 				
				 			case "CardAcctDInqRs":
				 				
								rowParams.put("RqUID", CalculatedVariables.rqUID);
								rowParams.put("RqTm", CalculatedVariables.rQTm);

								//���� EndDt ,����� �� InitiateDeliveryCardRq + 1 ����
								LocalDate endDtDate = LocalDate.parse(CalculatedVariables.extDt, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
								
								endDtDate = endDtDate.plusDays(1);
								String endDtDateStr = endDtDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
								rowParams.put("EndDt", endDtDateStr);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
								break;
								
				 			case "CardStatusModASyncRs":
				 				
								rowParams.put("RqUID", CalculatedVariables.rqUID);
								rowParams.put("RqTm", CalculatedVariables.rQTm);
								rowParams.put("OperUID", CalculatedVariables.operUID);
								
								docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
								
								break;
								

				 		}
				 		break;
				
					
				}
				return docXML;
			 }
			 
			 /**
			  * ������ �������� ����� � xml �� HashMap � ������������ ����
			  * @param docXMLFrom - xml, ������ ���������� �������� ���
			  * @param docXMLTo - xml, ��� ���������� �������� ���
			  * @param tagName - �������� ����-�����, ������� ��������� ��������
			  * @param tagNameBefore - �������� ����-�����, ����� ������� ���������� �������� ������� ���-����
			  * 
			  * @return
			  */
			 public static Document replaceTagInDocument(Document docXMLFrom, Document docXMLTo, String tagName, String tagNameBefore){
				 
				 NodeList nodeListFrom = docXMLFrom.getElementsByTagName(tagName);
	 			 Element elListFrom = (Element) nodeListFrom.item(0);
	 			 NodeList nodeListTo = docXMLTo.getElementsByTagName(tagName);
	 			 Element elListTo = (Element) nodeListTo.item(0);
	 			 Element elListToParrent = (Element) elListTo.getParentNode();
	 			 elListToParrent.removeChild(elListTo);
	 			 docXMLTo.adoptNode(elListFrom);
	 			 elListToParrent.insertBefore(elListFrom, docXMLTo.getElementsByTagName(tagNameBefore).item(0));
				 
				 return docXMLTo;
			 }
			 
			 /**
			  * ������ �������� ����� � xml �� HashMap � ������������ ����
			  * @param docXML - xml, ��� ���������� ������� ���
			  * @param tagName - �������� ����-�����, ������� ��������� �������
			  * 
			  * @return
			  */
			 public static Document deleteTagInDocument(Document docXML, String tagName){
				 
				 try {
				 NodeList nodeList = docXML.getElementsByTagName(tagName);
	 			 Element elList = (Element) nodeList.item(0);
	 			 Element elListToParrent = (Element) elList.getParentNode();
	 			 elListToParrent.removeChild(elList);
				   } catch (NullPointerException e) {
					 System.out.println("���� ���� " + tagName + " � docXML");

				 }

				 
				 
				 return docXML;
			 }
			 
			 /**
			  * ������ �������� ����� � xml �� HashMap � ������������ ����
			  * @param rowParams - HashMap � ����������� � ���������� �����
			  * @param docXML - xml, ��� ���������� �������� ���������
			  * @param tagName - �������� ����-�����, ��� ��������� ��������� ���������
			  * @return
			  */
			 public static Document setXMLValuesIntoTagNameFromHashMap(HashMap<String, String> rowParams, Document docXML, String tagName){
				 
				 System.out.println("set params to xml in tagName " + tagName + " : " + rowParams );
				 	Set<Entry<String, String>> entrSet = rowParams.entrySet();
					Iterator<Entry<String, String>> it = entrSet.iterator();
					Entry<String, String> entr;
					while(it.hasNext()){
						entr = it.next();
						NodeList nodeListSearchIn = docXML.getElementsByTagName(tagName);
						Element elListBlock = (Element)nodeListSearchIn.item(0);

						NodeList nodeList = elListBlock.getElementsByTagName(entr.getKey());
						Element elList;
						switch(nodeList.getLength()){
							case 1:
								elList = (Element) nodeList.item(0);
								break;
							case 2: 
								elList = (Element) nodeList.item(1);
								break;
							default:
								elList = (Element) nodeList.item(0); 
						}

						elList.setTextContent(entr.getValue());
						
					}
					
					
					return docXML;
				 
			 }
			 
			 /**
			  * ������ �������� ����� � xml �� HashMap
			  * @param rowParams - HashMap � ����������� � ���������� �����
			  * @param docXML - xml, ��� ���������� �������� ���������
			  * @param isSetUniqueParam - ������������� �� ���������� �������� ���������
			  * @param uniqueParamName - ��� ���������, �������� ��������������� ���������� ��������
			  * 
			  * @throws Exception
			  */
			 public static Document setXMLValuesFromHashMap(HashMap<String, String> rowParams, Document docXML, 
					 boolean isSetUniqueParam, String uniqueParamName) {
				 try{
				 	Set<Entry<String, String>> entrSet = rowParams.entrySet();
					Iterator<Entry<String, String>> it = entrSet.iterator();
					Entry<String, String> entr;
					while(it.hasNext()){
						entr = it.next();
						NodeList nodeList = docXML.getElementsByTagName(entr.getKey());
						Element elList;
						switch(nodeList.getLength()){
							case 1:
								elList = (Element) nodeList.item(0);
								break;
							case 2: 
								elList = (Element) nodeList.item(1);
								break;
							default:
								elList = (Element) nodeList.item(0); 
						}
						elList.setTextContent(entr.getValue());
						
					}
					
					if(isSetUniqueParam){
						//������������� ���������� UUID
						String newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
						NodeList nodeList = docXML.getElementsByTagName(uniqueParamName);
						Element elList = (Element) nodeList.item(0);
						elList.setTextContent(newUUID);
						CalculatedVariables.testUUID = newUUID;
					}
				 }catch(Exception e){
					 e.printStackTrace();
				 }
					
					return docXML;
				 
			 }
			 
			 /**
			  * ������� ��������� �������� ���� � xml ���������
			  * @param rowParams - HashMap � ����������� � ���������� �����
			  */
			 public static Document setXMLTagValue(Document xmlDocument, String tagName, String tagValue) throws Exception {
				 
				 NodeList nodeList = xmlDocument.getElementsByTagName(tagName);
				 Element elList = (Element) nodeList.item(0);
				 elList.setTextContent(tagValue);
				 
				 return xmlDocument;
			 }
		    
		    /**
			 *
			 * ������� ������������ xml � �������
			 *
			 * @param xmlDocument - Document ������ ��� ����������� � String
			 */
			 public static String convertDomToString(Document xmlDocument) throws Exception {
			    	
			    	TransformerFactory tf = TransformerFactory.newInstance();
		            Transformer transformer = tf.newTransformer();
		            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		            StringWriter writer = new StringWriter();
		            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
		            String output = writer.getBuffer().toString();
		            
		            return output;
			    	
			  }
			 
			 /**
			  * ������� ����������� xml � ���� String � Document ������
			  * @param xmlContent - xml � ���� ������
			  * @param charset - ���������
			  * @return
			 * @throws ParserConfigurationException 
			  * @throws Exception
			  */
			 public static Document convertStringToDom(String xmlContent, String charset) throws ParserConfigurationException{

				 //����������� String xml ������ � DOM ��������
				 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		            DocumentBuilder db;
		            db = dbf.newDocumentBuilder();
		            Document doc = null;
		            try{
		            	doc =  db.parse(new ByteArrayInputStream(xmlContent.getBytes(charset)));
		            }catch(Exception e){
		            	System.err.println(e.getMessage());
		            }
					return doc;
				 
			 }
			 
			 /**
			  * ������� ��������� �������� ���� � xml Document
			  * @param docXML - Document ������������� xml
			  * @param fieldName - �������� ���� � xml
			  * @return
			  * @throws Exception
			  */
			 public static String getElementValueFromDocument(Document docXML, String fieldName){
				 
				 String result = "";
				 NodeList nodeList = docXML.getElementsByTagName(fieldName);
				 Element elList = (Element) nodeList.item(0);
				 result = elList.getTextContent();//�������� �������� ���� �� ������
				 return result;
				 
				 
			 }
			 
			 public static String getNextElementValueFromDocument(Document docXML, String fieldName,int NextN){
				 
				 String result = "";
				 NodeList nodeList = docXML.getElementsByTagName(fieldName);
				 Element elList = (Element) nodeList.item(NextN);
				 result = elList.getTextContent();//�������� �������� ���� �� ������
				 return result;
				 
				 
			 }
			 
			 
			 /**
			  * ������� ��������� �������� ���� � xml Document � ��������� �����
			  * @param docXML - Document ������������� xml
			  * @param fieldName - �������� ���� � xml
			  * @param nodeName - �������� ����� � xml, ��� ������ ����
			  * @return
			  * @throws Exception
			  */
			 public static String getElementValueFromDocumentNode(Document docXML, String fieldName, String nodeName) throws Exception {
				 
				 String result = "";
				 NodeList nodeListSearchIn = docXML.getElementsByTagName(nodeName);
				 Element elListBlock = (Element)nodeListSearchIn.item(0);
			
				 NodeList nodeList = elListBlock.getElementsByTagName(fieldName);
				 Element elList = (Element) nodeList.item(0);
				 result = elList.getTextContent();//�������� �������� ���� �� ������
				 
				 return result;
				 
				 
			 }
			 
			 /**
			  * ������� ��������� �������� ���� � xml Document � ��������� �����
			  * @param docXML - Document ������������� xml
			  * @param fieldName - �������� ���� � xml
			  * @param nodeName - �������� ����� � xml, ��� ������ ����
			  * @param index - ���������� ����� �����
			  * @return
			  * @throws Exception
			  */
			 public static String getElementValueFromDocumentNodeByIndex(Document docXML, String fieldName, String nodeName, int index) throws Exception {
				 
				 String result = "";
				 NodeList nodeListSearchIn = docXML.getElementsByTagName(nodeName);
				 Element elListBlock = (Element)nodeListSearchIn.item(index - 1);
			
				 NodeList nodeList = elListBlock.getElementsByTagName(fieldName);
				 Element elList = (Element) nodeList.item(0);
				 result = elList.getTextContent();//�������� �������� ���� �� ������
				 
				 return result;
				 
				 
			 }
			 
			 /**
			 *
			  * ������� ����������� xml � Base64
			 *
			  * @param xmlDocument - xml �������� ��� �����������
			  * @param charset - ���������
			 * 
			 */
			 public static String convertXMLToBase64(String xmlDocument, String charset) throws Exception {
				 String toBase64Str = "";
				 Base64.Encoder enc = Base64.getEncoder();
				 toBase64Str = enc.encodeToString(xmlDocument.getBytes(charset));
				 return toBase64Str;
				 
			 }
			 
			 /**
			 *
			  * ������� ����������� Base64 � xml
			 *
			  * @param xmlDocumentBase64 - xml �������� ��� �����������
			  * @param charset - ���������
			 */
			 public static String convertBase64ToXML(String xmlDocumentBase64, String charset) throws Exception {
				 String toXMLStr = "";
				 Base64.Decoder dec = Base64.getDecoder();
				 byte[] bt = dec.decode(xmlDocumentBase64.getBytes(charset));

				 String str = new String(bt);
				 toXMLStr = str;
				 return toXMLStr;
				 
			 }
			 
			 
			 /**
			 *
			  * ������� ������ ��� �������
			 *
			  * @param wsdl - ���� � wsdl
			  * @param requestEnvelope - ���� xml ���������
			  * @param charset - ���������
			 * 
			 * 
			 */
			 public static void soap(String wsdl, String requestEnvelope, String charset, String xmlName){
				 try{
				 	String isXMLValid;		 	
				 	String webServiceName = requestEnvelope.substring(requestEnvelope.indexOf("</dep:") + 6, requestEnvelope.indexOf("</soapenv:Body>") - 5);
				 
			        // Create SOAP connection
			        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			        //Send SOAP Message to SOAP Server
			        SOAPMessage soapMessage = soapConnection.call(createSOAPMsg(requestEnvelope), wsdl);
			        
			        String respBase64 = soapMessage.getSOAPBody().getTextContent();
			        System.out.println("ResponseBase64 is:" + respBase64);
			        String resp = convertBase64ToXML(respBase64, charset);
			        System.out.println("Response is:" + resp);
			        CalculatedVariables.xmlResponse = resp;

					 //�������� xml ��������� �� xsd �����
				 	isXMLValid = validationXML(resp, Config.allProp.getProperty("xsdPath"), xmlName);
				 	
				 	CalculatedVariables.actualValues.put("XMLResponseValid_for_" + webServiceName, isXMLValid);

					 //��������� ����������� �������� ����������
					CalculatedVariables.expectedValues.put("XMLResponseValid_for_" + webServiceName, "true");
				 }catch(Exception e){
					 try {
						CommonLib.WriteLog("FAIL", "Error due to sending xml request: \n" + e.getMessage(), Config.resultsPath );
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 System.setProperty("isError", "true");
					 System.setProperty("isFail", "true");
					 try {
						CommonLib.postProcessingTest();
					 } catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					 }	
				 }

			 }
			 
			 /**
			 *
			  * ������� �������� SOAPMessage ��� ������ ��� �������
			 *
			  * @param inputXML - xml ���������
			 * 
			 */
			 private static SOAPMessage createSOAPMsg(String inputXML) throws Exception{
			        byte[] buffer = inputXML.getBytes();

			        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);
			        StreamSource source = new StreamSource(stream);

			        MessageFactory messageFactory = MessageFactory.newInstance();
			        SOAPMessage soapMessage = messageFactory.createMessage();
			        SOAPPart soapPart = soapMessage.getSOAPPart();

			        soapPart.setContent(source);

			        soapMessage.saveChanges();

			        return soapMessage;
			  }
			 
			 /**
			  *
			  * @param testOperation - �������� �������� �������� 
			  * @param testCaseName - �������� ���������
			  * @param xmlContentTemplateName - �������� ������� � XML
			  * @return - ������ � XML � Document �������������
			  * @throws Exception
			  */
			 public static Document loadXMLTemplate(String testOperation, String testCaseName, String xmlContentTemplateName) throws Exception{
				 
				 String xmlTemplate = "";
				 //������ xml  ��������
					if(xmlContentTemplateName.contains("Rq")){
						xmlTemplate = FileLib.readFromFile(Config.xmlInPath + "request" + "\\" + testOperation + "\\" + testCaseName + "\\" 
								+ xmlContentTemplateName + ".xml", "UTF-8" );
					}else{
						xmlTemplate = FileLib.readFromFile(Config.xmlInPath + "response" + "\\" + testOperation + "\\" + testCaseName + "\\" 
								+ xmlContentTemplateName + ".xml", "UTF-8" );
					}


				 //����������� String xml ������ � DOM ��������
				 Document doc = convertStringToDom(xmlTemplate, "UTF-8");
			        
			        return doc;
			 }
			 
			 	/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateDeliveryCardMomentumRq(Document docXML, String... args) throws Exception{
					 HashMap<String, String> rowParams = new HashMap<String, String>(); 
					 String newUUID = "";
					 String RqUID = "";
					 String OperUID = "";
					 String sQuerry = "";

					 String cardNum = "";

					//���������� 16-�� ������� ����� �����, ������� ����������� � ��
					do{
						cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
						sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
					}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
						System.out.println("GeneratedCardNum = " + cardNum);
						CalculatedVariables.cardNumber = cardNum;
					 
					 LocalDateTime todayDateTime = LocalDateTime.now();
					 
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 RqUID = newUUID;
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 OperUID = newUUID;
					 CalculatedVariables.testUUID = RqUID;
					 CalculatedVariables.operUID = OperUID;
					 System.out.println("OperUID = " + OperUID);
					 CalculatedVariables.rqUID = RqUID;
					 System.out.println("RqUID = " + RqUID);

					String lastName = "�������-" + CommonLib.getRandomLetters(15);
					 CalculatedVariables.lastName = lastName;
					 String firstName = CommonLib.getRandomLetters(15);
					 CalculatedVariables.firstName = firstName;
					 String middleName = CommonLib.getRandomLetters(15);
					 CalculatedVariables.middleName = middleName;
						
					 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
					 CalculatedVariables.idSeries = idSeries;
					 String idNum = CommonLib.getRandomNumericLetters(6);
					 CalculatedVariables.idNum = idNum;

					//���������  � template.xml
					 //String cardCode = "111707";
					 //String productCode = "IRRDBCMF--";
					 //String eDBOContractFlag = "true";
					 //String contractProductCode = "IRRD--";
					 
						
					 rowParams.put("RqUID", RqUID);
					 rowParams.put("OperUID", OperUID);
					 rowParams.put("RqTm", todayDateTime.toString());
					 rowParams.put("IdSeries", idSeries);
					 rowParams.put("IdNum", idNum);
					 rowParams.put("CardNum", cardNum);


					//������ info 
					 rowParams.put("OperDate", Config.businessProcessProp.getProperty("OperDay"));
					 rowParams.put("UserName", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin"));
					 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode"));
					 rowParams.put("UserFIO", Config.businessProcessProp.getProperty("OperatorInfoOperatorName"));
							
					 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//������ ���
					 rowParams.clear();
					 rowParams.put("LastName", lastName);
					 rowParams.put("FirstName", firstName);
					 rowParams.put("MiddleName", middleName);
						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");

					//������ info
				 	 rowParams.clear();
					 rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
					 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
					 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
					 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info
					 rowParams.clear();
					 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId"));
					 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId"));
					 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId"));
					 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId"));
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

					return docXML;
				}

			 
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 *  
				 *  */

				public static Document redefineXMLParamsInitiateReplaceMomentumCardToCardRq(Document docXML, String... args) throws Exception {
					 HashMap<String, String> rowParams = new HashMap<String, String>(); 
					 String newUUID = "";
					 String RqUID = "";
					 String OperUID = "";
					 
					 LocalDateTime todayDateTime = LocalDateTime.now();
					 
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 RqUID = newUUID;
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 OperUID = newUUID;
					 CalculatedVariables.testUUID = RqUID;
					 CalculatedVariables.operUID = OperUID;
					 System.out.println("OperUID = " + OperUID);
					 CalculatedVariables.rqUID = RqUID;
					 System.out.println("RqUID = " + RqUID);


					//String lastName = "�������-" + CommonLib.getRandomLetters(15);
					 //String firstName = CommonLib.getRandomLetters(15);
					 //String middleName = CommonLib.getRandomLetters(15);
					//System.out.println("��� 3 ");
						
					 //String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
					 //String idNum = CommonLib.getRandomNumericLetters(6);
					//System.out.println("��� 4 ");

					//���������  � template.xml
					 //String cardCode = "111707";
					 //String productCode = "IRRDBCMF--";
					 //String eDBOContractFlag = "true";
					 //String contractProductCode = "IRRD--";
					 
					 						
					 rowParams.put("RqUID", RqUID);
					 rowParams.put("OperUID", OperUID);
					 rowParams.put("RqTm", todayDateTime.toString());
					 rowParams.put("IdSeries", CalculatedVariables.idSeries);
					 rowParams.put("IdNum", CalculatedVariables.idNum);


					//������ info 
					 rowParams.put("OperDay", Config.businessProcessProp.getProperty("OperDay"));
					 rowParams.put("OperatorLogin", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin"));
					 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode"));
					 rowParams.put("OperatorName", Config.businessProcessProp.getProperty("OperatorInfoOperatorName"));
					 
							
					 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);


					//������ ���
					 rowParams.clear();
					 rowParams.put("LastName", CalculatedVariables.lastName);
					 rowParams.put("FirstName", CalculatedVariables.firstName);
					 rowParams.put("MiddleName", CalculatedVariables.middleName);
					 
					
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");


					//������ info
				 	 rowParams.clear();
					 rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
					 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
					 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
					 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
					 
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");


					//������ info
					 rowParams.clear();
					 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId"));
					 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId"));
					 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId"));
					 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId"));
					 
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");


					//������ info �� ����� ��������

					 rowParams.clear();
					 rowParams.put("CardNum", CalculatedVariables.cardNumber);
					 rowParams.put("IssueDate", CalculatedVariables.issueDate);
					 rowParams.put("ExpDt", CalculatedVariables.extDt);

					 
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdFrom");
//					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardRecMomentum");

					 if(CalculatedVariables.testCase.equals("ChangeCardOnlineMomentumMomentum")){
						 String cardNum = "";
						 String sQuerry = "";

						 //���������� 16-�� ������� ����� �����, ������� ����������� � ��
							do{
								cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
								sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
							}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
								System.out.println("GeneratedCardNum = " + cardNum);

						 //������ info
						 	rowParams.clear();

							rowParams.put("CardNum", cardNum);
							
							docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdTo");
								 						
			 				}
				 

					return docXML;
				}

	 
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsCreateProductPackageConnectUDBORq(Document docXML, String... args) throws Exception{
					 HashMap<String, String> rowParams = new HashMap<String, String>(); 
					 
					 String newUUID = "";
					 String RqUID = "";
					 String OperUID = "";
					 
					 LocalDateTime todayDateTime = LocalDateTime.now();
				
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 RqUID = newUUID;
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 OperUID = newUUID;
					 CalculatedVariables.testUUID = RqUID;
					 CalculatedVariables.testRqUID = RqUID;

					String lastName = "�������-" + CommonLib.getRandomLetters(15);
					 String firstName = CommonLib.getRandomLetters(15);
					 String middleName = CommonLib.getRandomLetters(15);
						
					 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
					 String idNum = CommonLib.getRandomNumericLetters(6);
						
					 rowParams.put("RqUID", RqUID);
					 rowParams.put("OperUID", OperUID);
					 rowParams.put("RqTm", todayDateTime.toString());
					 rowParams.put("IdSeries", idSeries);
					 rowParams.put("IdNum", idNum);
					 rowParams.put("OperDate", Config.OperDay);
					 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					 System.out.println("rowParams = " + rowParams);	
					 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//������ ���
					 rowParams.clear();
					 rowParams.put("LastName", lastName);
					 rowParams.put("FirstName", firstName);
					 rowParams.put("MiddleName", middleName);
						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");

					//������ info 
				 	 rowParams.clear();
					 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info 
					 rowParams.clear();
					 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
					 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
					 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
					 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
		
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsCreateProductPackageRq(Document docXML) throws Exception{
					 HashMap<String, String> rowParams = new HashMap<String, String>(); 
					 String newUUID = "";
					 String RqUID = "";
					 String OperUID = "";
					 
					 LocalDateTime todayDateTime = LocalDateTime.now();
				
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 RqUID = newUUID;
					 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					 OperUID = newUUID;
					 CalculatedVariables.testUUID = RqUID;

					String lastName = "�������-" + CommonLib.getRandomLetters(15);
					 String firstName = CommonLib.getRandomLetters(15);
					 String middleName = CommonLib.getRandomLetters(15);
						
					 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
					 String idNum = CommonLib.getRandomNumericLetters(6);
						
					 rowParams.put("RqUID", RqUID);
					 rowParams.put("OperUID", OperUID);
					 rowParams.put("RqTm", todayDateTime.toString());
					 rowParams.put("IdSeries", idSeries);
					 rowParams.put("IdNum", idNum);
					 rowParams.put("OperDate", Config.OperDay);
					 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//������ ���
					 rowParams.clear();
					 rowParams.put("LastName", lastName);
					 rowParams.put("FirstName", firstName);
					 rowParams.put("MiddleName", middleName);
						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");

					//������ info 
				 	 rowParams.clear();
					 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info 
					 rowParams.clear();
					 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
					 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
					 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
					 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
		
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsGetCardHolderRs(Document docXML, String... args) throws Exception{
					 HashMap<String, String> rowParams = new HashMap<String, String>(); 
					 String RqUID = "";
					 String OperUID = "";
					 String RqTm = "";
					 
					 RqUID = CalculatedVariables.rqUID;
					 OperUID = CalculatedVariables.operUID;
					 RqTm = CalculatedVariables.rQTm;
						
					 rowParams.put("RqUID", RqUID);
					 rowParams.put("OperUID", OperUID);
					 rowParams.put("RqTm", RqTm);
					
					 if (args.length > 0) {rowParams.put("StatusCode", args[0]);}
					 else  rowParams.put("StatusCode", "0");
					 
					 /*
					  * ��� ������.
						������ �������� ��������:
						0 � �������; ������ ������ � ��� ������ ��������� � ��������;
						2200 - Client Not Found; (������ �� ������)
						1770 - Security values specified by Customer are not correct; (�� ������ ��� ���������� ������ ���)
						
					  */
					 
					 docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsCustAddRs(Document docXML, String... args) throws Exception{
					
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String RqTm = "";
					 
					RqUID = CalculatedVariables.rqUID;
					RqTm = CalculatedVariables.rQTm;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("RqTm", RqTm);
					
					
					if (args.length > 0) {rowParams.put("StatusCode", args[0]);}
									
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsIssueCardRs(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					
					RqUID = CalculatedVariables.rqUID;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("MainApplRegNumber", CalculatedVariables.mainApplRegNumber);
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsNotifyIssueCardResultNfRs(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String sQuerry = "";
					RqUID = CalculatedVariables.rqUID;
					String cardNumNotifyIssueCardResultNf = "";

					//���������� 16-�� ������� ����� �����, ������� ����������� � ��
					do{
						cardNumNotifyIssueCardResultNf = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
						sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNumNotifyIssueCardResultNf + "'";
					}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
					System.out.println("GeneratedCardNum = " + cardNumNotifyIssueCardResultNf);
					
					rowParams.put("RqUID",  CalculatedVariables.mainApplRegNumber);
					rowParams.put("MainApplRegNumber", CalculatedVariables.mainApplRegNumber);
					rowParams.put("AcctId", CalculatedVariables.acctId);
					rowParams.put("CardNum", cardNumNotifyIssueCardResultNf);
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateDeliveryCardRq(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String newUUID = "";
					 String OperUID = "";
					String sQuerry = "";
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					RqUID = newUUID;
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					OperUID = newUUID;
					CalculatedVariables.operUID = OperUID;
					System.out.println("OperUID = " + OperUID);
					CalculatedVariables.rqUID = RqUID;
					System.out.println("RqUID = " + RqUID);
					CalculatedVariables.testUUID = RqUID;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("OperUID", OperUID);
					rowParams.put("NumContrCard", CalculatedVariables.mailAccount);
					rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);

	 				if(CalculatedVariables.testCase.equals("IssueNewCardOnlineWithoutAccountOpenConnectUDBO")){
						//������ info
				 	//rowParams.clear();
					rowParams.put("EDBOContractFlag", "true");
						 						
					//docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "EDBOContract");
	 				}

					sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					CalculatedVariables.extDt = rowParams.get("ExpDt");
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
	 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
	 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
	 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
	 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
	 				
	 				deleteTagInDocument(docXML, "Birthplace");
	 				deleteTagInDocument(docXML, "Citizenship");
	 				deleteTagInDocument(docXML, "TaxId");
	 				deleteTagInDocument(docXML, "ClientCategory");
	 				deleteTagInDocument(docXML, "ClientStatus");
	 				deleteTagInDocument(docXML, "Verified");
	 				deleteTagInDocument(docXML, "Signed");
	 				deleteTagInDocument(docXML, "ContactInfo");
					
	 				//if(CalculatedVariables.testCase.equals("IssueNewCardOnlineWithoutAccountOpenConnectUDBO")){
					//������ info
				 	//rowParams.clear();
					//rowParams.put("EDBOContractFlag", "false");
						 						
					//docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "EDBOContract");
	 				//}

					//������ info 
				 	rowParams.clear();
					rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info 
					rowParams.clear();
					rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
					rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
					rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
						 						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
					 
					return docXML;
				}
				
				
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsCardAcctDInqRs(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					
					rowParams.put("RqUID", CalculatedVariables.rqUID);
					rowParams.put("RqTm", CalculatedVariables.rQTm);

					//���� EndDt ,����� �� InitiateDeliveryCardRq + 1 ����
					LocalDate endDtDate = LocalDate.parse(CalculatedVariables.extDt, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
					
					endDtDate = endDtDate.plusDays(1);
					String endDtDateStr = endDtDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
					rowParams.put("EndDt", endDtDateStr);
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsCardStatusModASyncRs(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					
					rowParams.put("RqUID", CalculatedVariables.rqUID);
					rowParams.put("RqTm", CalculatedVariables.rQTm);
					rowParams.put("OperUID", CalculatedVariables.operUID);
					
					if (args.length > 0) {rowParams.put("StatusCode", args[0]);}
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsReissueCardRs(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					
					rowParams.put("RqUID", CalculatedVariables.rqUID);
//					rowParams.put("RqTm", CalculatedVariables.rQTm);
					rowParams.put("OperUID", CalculatedVariables.operUID);
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateBlockCardRq(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String newUUID = "";
					 String OperUID = "";
					String sQuerry = "";
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					RqUID = newUUID;
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					OperUID = newUUID;
					CalculatedVariables.operUID = OperUID;
					System.out.println("OperUID = " + OperUID);
					CalculatedVariables.rqUID = RqUID;
					System.out.println("RqUID = " + RqUID);
					CalculatedVariables.testUUID = RqUID;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("OperUID", OperUID);
					rowParams.put("AcctId", CalculatedVariables.acctId);
					rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					
					sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					CalculatedVariables.extDt = rowParams.get("ExpDt");
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
	 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
	 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
	 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
	 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
	 				
	 				deleteTagInDocument(docXML, "Birthplace");
	 				deleteTagInDocument(docXML, "Citizenship");
	 				deleteTagInDocument(docXML, "TaxId");
	 				deleteTagInDocument(docXML, "ClientCategory");
	 				deleteTagInDocument(docXML, "ClientStatus");
	 				deleteTagInDocument(docXML, "Verified");
	 				deleteTagInDocument(docXML, "Signed");
	 				deleteTagInDocument(docXML, "ContactInfo");
	 				deleteTagInDocument(docXML, "Resident");
	 			//	deleteTagInDocument(docXML, "ControlWord");

					//������ info 
				 	 rowParams.clear();
					 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");
					 
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateReissueCardRq(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String newUUID = "";
					String OperUID = "";
					String sQuerry = "";
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					RqUID = newUUID;
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					OperUID = newUUID;
					CalculatedVariables.operUID = OperUID;
					System.out.println("OperUID = " + OperUID);
					CalculatedVariables.rqUID = RqUID;
					System.out.println("RqUID = " + RqUID);
					CalculatedVariables.testUUID = RqUID;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("OperUID", OperUID);
					rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					
				//	if (Channel_From != null) { rowParams.put("Channel", Channel_From); };
					
					sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					CalculatedVariables.extDt = rowParams.get("ExpDt");
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
	 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
	 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
	 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
	 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
	 				
	 				deleteTagInDocument(docXML, "Birthplace");
	 				deleteTagInDocument(docXML, "Citizenship");
	 				deleteTagInDocument(docXML, "TaxId");
	 				deleteTagInDocument(docXML, "ClientCategory");
	 				deleteTagInDocument(docXML, "ClientStatus");
	 				deleteTagInDocument(docXML, "Verified");
	 				deleteTagInDocument(docXML, "Signed");
	 				deleteTagInDocument(docXML, "ContactInfo");
	 				deleteTagInDocument(docXML, "Resident");
	 			//	deleteTagInDocument(docXML, "ControlWord");


					//������ info 
				 	rowParams.clear();
					rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info 
					rowParams.clear();
					rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
					rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
					rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
						 						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
	 				
					
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateChangeTariffCardRq(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String newUUID = "";
					 String OperUID = "";
					String sQuerry = "";
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					RqUID = newUUID;
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					OperUID = newUUID;
					CalculatedVariables.operUID = OperUID;
					System.out.println("OperUID = " + OperUID);
					CalculatedVariables.rqUID = RqUID;
					System.out.println("RqUID = " + RqUID);
					CalculatedVariables.testUUID = RqUID;
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("OperUID", OperUID);
					//rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					
					sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					CalculatedVariables.extDt = rowParams.get("ExpDt");
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
	 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
	 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
	 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
	 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
	 				
	 				deleteTagInDocument(docXML, "Birthplace");
	 				deleteTagInDocument(docXML, "Citizenship");
	 				deleteTagInDocument(docXML, "TaxId");
	 				deleteTagInDocument(docXML, "ClientCategory");
	 				deleteTagInDocument(docXML, "ClientStatus");
	 				deleteTagInDocument(docXML, "Verified");
	 				deleteTagInDocument(docXML, "Signed");
	 				deleteTagInDocument(docXML, "ContactInfo");
	 				deleteTagInDocument(docXML, "Resident");
	 			//	deleteTagInDocument(docXML, "ControlWord");

					//������ info 
				 	rowParams.clear();
					rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

					//������ info 
					rowParams.clear();
					rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
					rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
					rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
						 						
					return docXML;
				}
				
				/**
				 *
				 * @param docXML - ������ XML � Document �������������
				 * @return
				 * @throws Exception 
				 */
				public static Document redefineXMLParamsInitiateCloseCardDepositRq(Document docXML, String... args) throws Exception{
					HashMap<String, String> rowParams = new HashMap<String, String>(); 
					String RqUID = "";
					String newUUID = "";
					 String OperUID = "";
					String sQuerry = "";
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					RqUID = newUUID;
					newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
					OperUID = newUUID;
					CalculatedVariables.operUID = OperUID;
					System.out.println("OperUID = " + OperUID);
					CalculatedVariables.rqUID = RqUID;
					System.out.println("RqUID = " + RqUID);
					
					rowParams.put("RqUID", RqUID);
					rowParams.put("OperUID", OperUID);
					rowParams.put("AcctId", CalculatedVariables.acctId);
					rowParams.put("OperDate", Config.OperDay);
					rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
					//rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
					
					sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
					rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
					
					CalculatedVariables.extDt = rowParams.get("ExpDt");
					
					docXML = setXMLValuesFromHashMap(rowParams, docXML, false, null);

					//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
	 				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
	 						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
	 				Document createProductPackageRqDoc = convertStringToDom(createProductPackageRq, "UTF-8");
	 				replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
	 				
	 				deleteTagInDocument(docXML, "Birthplace");
	 				deleteTagInDocument(docXML, "Citizenship");
	 				deleteTagInDocument(docXML, "TaxId");
	 				deleteTagInDocument(docXML, "ClientCategory");
	 				deleteTagInDocument(docXML, "ClientStatus");
	 				deleteTagInDocument(docXML, "Verified");
	 				deleteTagInDocument(docXML, "Signed");
	 				deleteTagInDocument(docXML, "ContactInfo");
	 				deleteTagInDocument(docXML, "Resident");
	 		//		deleteTagInDocument(docXML, "ControlWord");

					//������ info 
				 	rowParams.clear();
					rowParams.put("BranchId", Config.IssueBankInfoBranchId);
					rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
					rowParams.put("RegionId", Config.IssueBankInfoRegionId);
					rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
						 						
					 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");
					 
					return docXML;
				}




	 

}
