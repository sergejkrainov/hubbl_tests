package libs;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import variables.CalculatedVariables;
import variables.Config;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class BrowserActions {
	
	/**
     * ������� ������ ������ ��������� IssueFpp ��� ������� HUBBL
     * @throws InterruptedException
	 * @author sbt-kraynov-sa
	 *
     * @param isIssueFpp - �������� �� ����� ��������� Fpp
     */
	public static void setIssueFppMode(String isIssueFpp) throws InterruptedException{
		
		WebDriver driver = CalculatedVariables.webDriver;
		try{
		
			loginToHubblAdmin();

            //��������� � ������ �����������������
            clickToElementByText(driver, "menu", "�����������������");
            Thread.sleep(100);

            //��������� � ������ �����
            clickToElementByText(driver, "menu", "�����");
            Thread.sleep(100);

            //��������� �� ��������� �������� � ������� �����
            driver.findElement(By.xpath("//input[@id='currentPage']")).clear();
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys("2");
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys(Keys.ENTER);
			//driver.findElement(By.xpath("//a[@id='toNextPaqe']")).click();
			Thread.sleep(100);

            //��������� �� �������������� �������� isIssueFpp
            driver.findElement(By.xpath("//span[text()='isIssueFpp']")).click();
			Thread.sleep(100);

            //������ �������� �������� isIssueFpp
            driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).clear();
			driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).sendKeys(isIssueFpp);
			Thread.sleep(100);

            //�������� ���������
            driver.findElement(By.xpath("//button[@id='saveButton']")).click();
			Thread.sleep(100);
			
			driver.close();
			driver.quit();

		}catch(Exception e){
			e.printStackTrace();
			driver.quit();
		}
		
	}
	
	/**
     * ������� ������ ������ ��������� DeliveryFpp ��� ������� HUBBL
     * @throws InterruptedException
	 * @author sbt-kraynov-sa
	 *
     * @param isDeliveryFpp - �������� �� ����� ��������� Delivery Fpp
     */
	public static void setDeliveryFppMode(String isDeliveryFpp) throws InterruptedException{
		WebDriver driver = CalculatedVariables.webDriver;
		try{
		
			loginToHubblAdmin();

            //��������� � ������ �����������������
            clickToElementByText(driver, "menu", "�����������������");
            Thread.sleep(100);

            //��������� � ������ �����
            clickToElementByText(driver, "menu", "�����");
            Thread.sleep(100);

            //��������� �� ��������� �������� � ������� �����
            driver.findElement(By.xpath("//input[@id='currentPage']")).clear();
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys("2");
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys(Keys.ENTER);
			//driver.findElement(By.xpath("//a[@id='toNextPaqe']")).click();
			Thread.sleep(100);

            //��������� �� �������������� �������� isIssueFpp
            driver.findElement(By.xpath("//span[text()='isDeliveryFpp']")).click();
			Thread.sleep(100);

            //������ �������� �������� isIssueFpp
            driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).clear();
			driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).sendKeys(isDeliveryFpp);
			Thread.sleep(100);

            //�������� ���������
            driver.findElement(By.xpath("//button[@id='saveButton']")).click();
			Thread.sleep(100);


		}catch(Exception e){
			e.printStackTrace();
			driver.quit();
		}
		
	}
	
	/**
     * ������� ������ ������ ��������� BlockListFpp ��� ������� HUBBL
     * @throws InterruptedException
	 * @author sbt-kraynov-sa
	 *
     * @param isBlockListFpp - �������� �� ����� ��������� BlockListFpp
     */
	public static void setBlockListFppMode(String isBlockListFpp) throws InterruptedException{
		WebDriver driver = CalculatedVariables.webDriver;
		try{
		
			loginToHubblAdmin();

            //��������� � ������ �����������������
            clickToElementByText(driver, "menu", "�����������������");
            Thread.sleep(100);

            //��������� � ������ �����
            clickToElementByText(driver, "menu", "�����");
            Thread.sleep(100);

            //��������� �� ��������� �������� � ������� �����
            driver.findElement(By.xpath("//input[@id='currentPage']")).clear();
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys("1");
			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys(Keys.ENTER);
			//driver.findElement(By.xpath("//a[@id='toNextPaqe']")).click();
			Thread.sleep(100);

            //��������� �� �������������� �������� isIssueFpp
            driver.findElement(By.xpath("//span[text()='isBlockListFpp']")).click();
			Thread.sleep(100);

            //������ �������� �������� isIssueFpp
            driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).clear();
			driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).sendKeys(isBlockListFpp);
			Thread.sleep(100);

            //�������� ���������
            driver.findElement(By.xpath("//button[@id='saveButton']")).click();
			Thread.sleep(100);


		}catch(Exception e){
			e.printStackTrace();
			driver.quit();
		}
		
	}
	
	/**
     * ������� ������ ������ ��������� BlockListFpp ��� ������� HUBBL
     * @throws InterruptedException
	 * @author sbt-kraynov-sa
	 *
     * @param isTurnOn - �������� �� ����� ���������
     * @param mode - ����� ���������
     * @param pageNumber - ����� �������� � �������, ��� ��������� ������ ��������
     *
	 */
	public static void setHUBBLMode(String isTurnOn, String mode, String pageNumber) throws InterruptedException{
		WebDriver driver = CalculatedVariables.webDriver;
		try{

			if (Integer.valueOf(pageNumber) > 1 ) {
                //��������� �� ��������� �������� � ������� �����
                driver.findElement(By.xpath("//input[@id='currentPage']")).clear();
	 			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys(pageNumber);
	 			driver.findElement(By.xpath("//input[@id='currentPage']")).sendKeys(Keys.ENTER);
	 			//driver.findElement(By.xpath("//a[@id='toNextPaqe']")).click();
				Thread.sleep(100);
				}

            //��������� �� �������������� �������� isIssueFpp
            driver.findElement(By.xpath("//span[text()='" + mode + "']")).click();
			Thread.sleep(100);

            //������ �������� �������� isIssueFpp
            driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).clear();
			driver.findElement(By.xpath("//input[@name='OPDEFAULTVALUE']")).sendKeys(isTurnOn);
			Thread.sleep(100);

            //�������� ���������
            driver.findElement(By.xpath("//button[@id='saveButton']")).click();
			Thread.sleep(100);


		}catch(Exception e){
			e.printStackTrace();
			driver.quit();
		}
		
	}
	
	/**
     * �������� �� ������� �� ���������� ����������� � �������� ������
     * @throws InterruptedException
	 * @author sbt-kraynov-sa
	 *
     * @param driver - ����������
     * @param className - �������� ������
     * @param text - ��������� ����������
     *
	 */
	public static void clickToElementByText(WebDriver driver, String className, String text) throws InterruptedException{
	List<WebElement> resultPhonesAll = driver.findElements(By.xpath("//span[@class='" + className + "']"));
		for(WebElement el: resultPhonesAll){
			if(el.getText().contains(text)){
				el.click();
				break;
			}
		}
	}
	
	/**
     * ������� ����������� � ���������������� ������� HUBBL
     * @throws InterruptedException
	 */
	public static void loginToHubblAdmin() {
try{		
		WebDriver driver = CalculatedVariables.webDriver;
		driver.get(Config.addminHubblPath);
		driver.manage().window().maximize();

    //������� ���� �����
    driver.findElement(By.xpath("//input[@id='domain']")).clear();
    //��������� ���� �����
    driver.findElement(By.xpath("//input[@id='domain']")).sendKeys(Config.addminHubblDomain);

    //������� ���� ��� ������������
    driver.findElement(By.xpath("//input[@id='j_username']")).clear();
    //��������� ���� ��� ������������
    driver.findElement(By.xpath("//input[@id='j_username']")).sendKeys(Config.addminHubblLogin);

    //��������� ���� ������
    driver.findElement(By.xpath("//input[@id='j_password']")).sendKeys(Config.addminHubblPassword);

    //�������� ������ �����������
    driver.findElement(By.xpath("//input[@name='Submit' and @value='�����������']")).click();

    //������� ����������� 1 �������
    Thread.sleep(200);


    //��������� � ������ �����������������
    clickToElementByText(driver, "menu", "�����������������");
    Thread.sleep(100);

    //��������� � ������ �����
    clickToElementByText(driver, "menu", "�����");
    Thread.sleep(100);
		
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	/**
     * ������� ������ �� ������� � ���������������� ������� HUBBL
     * @throws InterruptedException
	 * @throws AWTException 
	 */
	public static void logOutFromHubblAdmin() throws InterruptedException, AWTException{
		
		WebDriver driver = CalculatedVariables.webDriver;
        //�������� �����
        driver.findElement(By.xpath("//button[@id='exitButton']")).click();


        //������� ��������� ����������� ����
        Thread.sleep(100);
		
		Robot rbt = new Robot();
		
		rbt.keyPress(KeyEvent.VK_ENTER);
		rbt.keyRelease(KeyEvent.VK_ENTER);
		
		Thread.sleep(1000);
		
		driver.quit();
		
	}

}
