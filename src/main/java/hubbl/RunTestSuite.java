package hubbl;

import libs.CommonLib;
import org.junit.runner.JUnitCore;
import variables.CalculatedVariables;
import variables.Config;

import java.util.ArrayList;


public class RunTestSuite {
	public static void main(String args[]) throws Exception{
		
		runTests(args);
		
		postRunningTests();
	
	}
	

	public static void startInitTests(String args[], String operationName, String testCaseName){
		
		System.out.println("UserDir = " + System.getProperty("user.dir"));
		try{
			//�������������� ��������� ������� �����
			Init.initTest(args, operationName, testCaseName);
		}catch(Exception e){
			//���� ��������� ������ ��� ������������� �����
			System.setProperty("isError", "true");
			try {
				CommonLib.WriteLog("FAIL", "Error before running tests: \n" + e.toString(), Config.resultsPath );
				CommonLib.postProcessingTest();
				System.exit(-1);
			 } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 }
			e.printStackTrace();
		}

		System.out.println("ResultDir = " + Config.resultsPath);
	}
	
	public static void runTests(String args[]) throws Exception{
		ArrayList<String> testNames = new ArrayList<String>();
		CommonLib.getRunTestSuiteProp();
		CalculatedVariables.testOperation = Config.runTestSuiteProp.getProperty("testOperation");
		CalculatedVariables.testCase = Config.runTestSuiteProp.getProperty("testCase");
		//������ ��������� ������ ������
		if(Config.runTestSuiteProp.getProperty("isRunFromCSV").equals("true")){//�� ����� RunClasses.txt
			testNames = CommonLib.getRunClasses();
		}else{//����� �� RunTestSettings
			testNames.add(CalculatedVariables.testOperation + " " + CalculatedVariables.testCase);
		}
		if(Init.isRunFromConsole(args)){//�� �������(JAZZ), ������� ��� ���������
			testNames.clear();
			testNames.add(args[0] + " " + args[1]);
		}
		//�������� ������ �� ������ ���� ������
		for(String testName: testNames ){
			int counter = 0; // 1 �� ���������� ��������� 2 ����
			String testOperation = testName.split(" ")[0];
			String testCase = testName.split(" ")[1];
			CalculatedVariables.testOperation = testOperation;
			CalculatedVariables.testCase = testCase;
			do{ //���� ���� ��� ���������� ������� ����� � ������ ������
				CommonLib.resetAllsettings();
				CommonLib.clearAllObjects();
				startInitTests(args, testOperation, testCase);
				//�������� ������ � �������� ����� �� �����, � �� �� ����� csv
				
				System.out.println("Try to start test attemtion " + (counter + 1));
				CalculatedVariables.testCase = testCase;
				try{				
					//��������� �����
					System.out.println("testCase name: " + testCase);
					CalculatedVariables.testCaseRun = testCase;
					String runTestClassName = "hubbl." + testOperation.toLowerCase() + "." + testCase;
					System.out.println("Run test class : " + runTestClassName);
					CalculatedVariables.testResult = JUnitCore.runClasses(Class.forName(runTestClassName));
					CommonLib.afterRunningTest();
					
		
				}catch(Exception e){
					//���� ��������� ������ ��� ������ �����
					System.setProperty("isError", "true");
					try {
						CommonLib.WriteLog("FAIL", "Error during running tests: \n" + e.toString(), Config.resultsPath );
						CommonLib.postProcessingTest();
						System.exit(-1);
					 } catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					 }
					e.printStackTrace();
				}
				
				finally{
					//������������ ���������� ������
					try {
						//������������� ������ 

						CommonLib.postProcessingTest();

						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					
				counter++;
				}
				
				
			}while(System.getProperty("isFail").equals("true") && counter < 1);
			
		}
	}


	public static void postRunningTests(){
		if(System.getProperty("isFail").equals("true") || CalculatedVariables.testResult.getFailureCount() > 0){
			System.exit(-1);
		}else{
			System.exit(0);
		}
		
	}
}
