package hubbl;

import libs.CommonLib;
import libs.FileLib;
import libs.RemoteServerMethods;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import variables.CalculatedVariables;
import variables.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * ����� ��� ������������� ������
 * @author sbt-kraynov-sa
 *
 */
public class Init {
	
	/**
	 * ������� ��� ������������� ���������� �����
	 * @param args - ������ � ����������� ��������� ������
	 * @throws Exception 
	 */
	public static void initTest(String args[], String operationName, String testCaseName){
		try{
			DateFormat formatForResult = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT);
			Calendar calendar = Calendar.getInstance();
			CalculatedVariables.startTime = formatForResult.format(calendar.getTime());//����� ������ ������
			initConfigVariables(args);
					
			initCalculatedVariables(args, testCaseName);
			
			//���������� �������� � ���������		
			CommonLib.StartLog(Config.allProp.getProperty("resultsPath"));
			CommonLib.StartTable("1", Config.allProp.getProperty("resultsPath"));
			
			//���������, ���������� �� ���� � ����� ����������� ���������� ����������
			File ff = new File(Config.allProp.getProperty("resultsPathCommon"));
			if(ff.exists()){
				String fileContent = FileLib.readFromFile(Config.allProp.getProperty("resultsPathCommon"), "Windows-1251");
				//���� ���� � ����� ����������� ���������� ���������� ����������, �� ���������, ����� �� ��� ���� ��������� ����� �������� ������
				if(!fileContent.contains("Report of automation testing")){
					//���������� ���������� ���������/����� ��� ������
					CommonLib.StartLog(Config.allProp.getProperty("resultsPathCommon"));
					CommonLib.StartTableCommonResults("1", Config.allProp.getProperty("resultsPathCommon"));
				}
			}else{
				//���������� ���������� ���������/����� ��� ������
				CommonLib.StartLog(Config.allProp.getProperty("resultsPathCommon"));
				CommonLib.StartTableCommonResults("1", Config.allProp.getProperty("resultsPathCommon"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * ������� ��� ������������� ���������� �������
	 * @param args - ������ � ����������� ��������� ������
	 * @throws IOException 
	 */
	public static void initConfigVariables(String args[]) throws IOException{
		
		//�������� ��������� � ��������
		System.out.println("Config.runDirPath:" + Config.runDirPath);
		System.out.println("Config.configPath:" + Config.configPath);
		Config.runTestSuiteProp.load(new FileInputStream(Config.runDirPath + Config.configPath + "RunTestSettings.txt"));
		
		//���� ������ �� ������� ��� ����� JAZZ
		if(isRunFromConsole(args)){
			Config.runTestSuiteProp.setProperty("testStandName", args[3]);
		}
		//���� ������ ������������� ����� JENKINS, �������� ������ ��������� � ����� � ���������� �������
		if(Config.runTestSuiteProp.getProperty("isJenkinsRun").equals("true")){
			Config.runTestSuiteProp.setProperty("testStandName", getStandNameFromRemote().trim());
		}
		System.out.println("Selected stand:" + Config.runTestSuiteProp.getProperty("testStandName"));
		Config.stendProp.load(new FileInputStream(Config.runDirPath + Config.configPath + Config.runTestSuiteProp.getProperty("testStandName") + "_TestSettings.txt"));
		Config.businessProcessProp.load(new FileInputStream(Config.runDirPath + Config.configPath + Config.runTestSuiteProp.getProperty("testStandName") + "_BusinessProcessSettings.txt"));
		System.out.println("Set config settings: "+ Config.runDirPath + Config.configPath + Config.runTestSuiteProp.getProperty("testStandName") + "_BusinessProcessSettings.txt");	
		//�������� ��� �������� � ���� ������ 
		
		Config.allProp.putAll(Config.runTestSuiteProp);
		Config.allProp.putAll(Config.stendProp);
		Config.allProp.putAll(Config.businessProcessProp);
				
		//����������� ��������
		Config.allProp.setProperty("reportsPath", Config.runDirPath + Config.allProp.getProperty("reportsPath"));		
		Config.allProp.setProperty("configPath", Config.runDirPath + Config.allProp.getProperty("configPath"));
		Config.allProp.setProperty("xmlOutPath", Config.runDirPath + Config.allProp.getProperty("xmlOutPath"));
		Config.allProp.setProperty("xmlInPath", Config.runDirPath + Config.allProp.getProperty("xmlInPath"));
		Config.allProp.setProperty("logsPath", Config.runDirPath + Config.allProp.getProperty("logsPath"));
		Config.allProp.setProperty("wsdlPathCOD", Config.allProp.getProperty("hostToWebServicesCOD") + "DepoCOD.wsdl");
		Config.allProp.setProperty("xsdPath", Config.runDirPath + Config.allProp.getProperty("xsdPath"));
		Config.allProp.setProperty("pathToCopyResults", Config.runDirPath + Config.allProp.getProperty("pathToCopyResults"));
				
		System.out.println("Set config settings: " + Config.allProp);
		
		Config.reportsPath = Config.allProp.getProperty("reportsPath");
		
		Config.configPath = Config.allProp.getProperty("configPath");
		Config.xmlOutPath = Config.allProp.getProperty("xmlOutPath");
		Config.xmlInPath = Config.allProp.getProperty("xmlInPath");
		Config.logsPath = Config.allProp.getProperty("logsPath");
		
		setConfigParamsFromConfigFile();
		
	}
	
	/**
	 * ������� ��� ������������� ���������� ����������
	 * @throws IOException 
	 */
	public static void initCalculatedVariables(String[] args, String testCaseName) throws IOException{				
		//���� ������ �� �� �������
		if(!isRunFromConsole(args)){
			
			CalculatedVariables.pathToCopyResults = Config.allProp.getProperty("pathToCopyResults");

				
		}else{

			CalculatedVariables.pathToCopyResults = args[2];
			
			

		}
		
		if(Config.remoteServerLoginCOD.equals("dpctest")){
			CalculatedVariables.moduleFileToLoadCOD = "/osbtmp/dpcappl/WS_test/";
		}

		//������� ����� ��� ������������ ����� ����� � ������������ �����
		String currentTime = "";
		LocalDateTime curLocTime = LocalDateTime.now();
		DateTimeFormatter tmFrm = DateTimeFormatter.ofPattern("yyyy_MM_dd-hh_mm_ss");
		currentTime = curLocTime.format(tmFrm);
		
		//���������� � ������������ ������
		Config.resultsDir = Config.runDirPath + Config.allProp.getProperty("resultsPath");
		
		Config.allProp.setProperty("resultsPathCommon", Config.resultsDir + "hubblTestResults" + ".html");
		Config.allProp.setProperty("resultsPath", Config.resultsDir + testCaseName + "_" + currentTime + ".html");
	
		Config.resultsPath = Config.allProp.getProperty("resultsPath");
		Config.resultsPathCommon = Config.allProp.getProperty("resultsPathCommon");
		
	}
	
	/**
	 * �������, ������������, ��� �� ����� ����� �������
	 * @throws IOException
	 * 
	 * @param args - ��������� ��������� ������ 
	 */
	public static boolean isRunFromConsole(String[] args) throws IOException{
		boolean isRunFromConsole = false;
		String consoleArgs = "";
		try{
			consoleArgs = args[0];
			isRunFromConsole = true;
		}catch(Exception e){

		}
		CalculatedVariables.isRunFromConsole = isRunFromConsole;
		return isRunFromConsole;
		
	}
	
	/**
	 * ������� ��� ������������� ����������(������) �� ����� � �������� 
	 * @throws IOException 
	 */
	public static void setConfigParamsFromConfigFile() throws IOException{
		
		Config.testStandName = Config.allProp.getProperty("configPath");//�������� �����
		Config.smtpHost = Config.allProp.getProperty("smtpHost");
		Config.smtpPort = Config.allProp.getProperty("smtpPort");
		Config.smtpLogin = Config.allProp.getProperty("smtpLogin");
		Config.smtpPassword = Config.allProp.getProperty("smtpPassword");
		Config.mailTo = Config.allProp.getProperty("mailTo");
		Config.isSendResultsToMail = Config.allProp.getProperty("isSendResultsToMail");
		Config.testOperation = Config.allProp.getProperty("testOperation");
		Config.testCase = Config.allProp.getProperty("testCase");
		Config.webServiceMethodCOD = Config.allProp.getProperty("webServiceMethod");
		
		Config.hostToWebServicesCOD = Config.allProp.getProperty("hostToWebServices");
		Config.wsdlPathCOD = Config.allProp.getProperty("wsdlPath");
		Config.xsdPath = Config.allProp.getProperty("xsdPath");
		
		Config.BDHostCOD = Config.allProp.getProperty("BDHostCOD");
		Config.BDPortCOD = Config.allProp.getProperty("BDPortCOD");
		Config.BDServiceNameCOD = Config.allProp.getProperty("BDServiceNameCOD");
		Config.BDLoginCOD = Config.allProp.getProperty("BDLoginCOD");
		Config.BDPasswordCOD = Config.allProp.getProperty("BDPasswordCOD");
		
		Config.BDHostHUBBL = Config.allProp.getProperty("BDHostHUBBL");
		Config.BDPortHUBBL = Config.allProp.getProperty("BDPortHUBBL");
		Config.BDServiceNameHUBBL = Config.allProp.getProperty("BDServiceNameHUBBL");
		Config.BDLoginHUBBL = Config.allProp.getProperty("BDLoginHUBBL");
		Config.BDPasswordHUBBL = Config.allProp.getProperty("BDPasswordHUBBL");
		
		Config.isIssueFpp = Config.allProp.getProperty("isIssueFpp");
		Config.addminHubblPath = Config.allProp.getProperty("addminHubblPath");
		
		Config.addminHubblDomain = Config.allProp.getProperty("addminHubblDomain");
		Config.addminHubblLogin = Config.allProp.getProperty("addminHubblLogin");
		Config.addminHubblPassword = Config.allProp.getProperty("addminHubblPassword");
		
		Config.mqHost = Config.allProp.getProperty("mqHost");
		Config.mqPort =	Config.allProp.getProperty("mqPort");
		Config.mqQueueManager =	Config.allProp.getProperty("mqQueueManager");
		Config.mqChannel = Config.allProp.getProperty("mqChannel");
		
		Config.remoteServerLogsCOD = Config.allProp.getProperty("remoteServerLogsCOD");
		Config.remoteServerLoginCOD = Config.allProp.getProperty("remoteServerLoginCOD");
		Config.remoteServerPasswordCOD = Config.allProp.getProperty("remoteServerPasswordCOD");
		Config.remoteServerHostCOD = Config.allProp.getProperty("remoteServerHostCOD");
		
		Config.remoteModulePathCOD = Config.allProp.getProperty("remoteModulePathCOD");
		
		Config.logsHUBBL = Config.allProp.getProperty("logsHUBBL");
		
		Config.screenManagerPath = Config.allProp.getProperty("screenManagerPath");
		Config.screenManagerScreenId = Config.allProp.getProperty("screenManagerScreenId");
		Config.screenManagerPassword = Config.allProp.getProperty("screenManagerPassword");
		
		//businessProcessProps
		Config.OperDay = Config.allProp.getProperty("OperDay");
		Config.OperatorInfoOperatorLogin = Config.allProp.getProperty("OperatorInfoOperatorLogin");
		Config.OperatorInfoOperatorCode = Config.allProp.getProperty("OperatorInfoOperatorCode");
		Config.OperatorInfoOperatorName = Config.allProp.getProperty("OperatorInfoOperatorName");
		Config.IssueBankInfoBranchId = Config.allProp.getProperty("IssueBankInfoBranchId");
		Config.IssueBankInfoAgencyId = Config.allProp.getProperty("IssueBankInfoAgencyId");
		Config.IssueBankInfoRegionId = Config.allProp.getProperty("IssueBankInfoRegionId");
		Config.IssueBankInfoRbTbBrchId = Config.allProp.getProperty("IssueBankInfoRbTbBrchId");
		Config.DeliveryBankInfoBranchId = Config.allProp.getProperty("DeliveryBankInfoBranchId");
		Config.DeliveryBankInfoAgencyId = Config.allProp.getProperty("DeliveryBankInfoAgencyId");
		Config.DeliveryBankInfoRegionId = Config.allProp.getProperty("DeliveryBankInfoRegionId");
		Config.DeliveryBankInfoRbTbBrchId = Config.allProp.getProperty("DeliveryBankInfoRbTbBrchId");
		Config.isRunFromCSV = Config.allProp.getProperty("isRunFromCSV");
		Config.OperatorInfoOperatorLogin2 = Config.allProp.getProperty("OperatorInfoOperatorLogin2");
		Config.OperatorInfoOperatorCode2 = Config.allProp.getProperty("OperatorInfoOperatorCode2");
		Config.OperatorInfoOperatorName2 = Config.allProp.getProperty("OperatorInfoOperatorName2");
		
		
		//���������� ����� ������2
		Config.DeliveryBankInfoBranchId22 = Config.allProp.getProperty("DeliveryBankInfoBranchId22");
		Config.DeliveryBankInfoAgencyId22 = Config.allProp.getProperty("DeliveryBankInfoAgencyId22");
		Config.DeliveryBankInfoRegionId22 = Config.allProp.getProperty("DeliveryBankInfoRegionId22");
		Config.DeliveryBankInfoRbTbBrchId22 = Config.allProp.getProperty("DeliveryBankInfoRbTbBrchId22");
		Config.OperDay22 = Config.allProp.getProperty("OperDay22");
		Config.OperatorInfoOperatorLogin22 = Config.allProp.getProperty("OperatorInfoOperatorLogin22");
		Config.OperatorInfoOperatorCode22 = Config.allProp.getProperty("OperatorInfoOperatorCode22");
		Config.OperatorInfoOperatorName22 = Config.allProp.getProperty("OperatorInfoOperatorName22");
		
		
	}
	
	/**
	 * ������� ������������� �������� ��� ������ � ���������
	 */
	public static void initBrowser(){
		
		String browserName = "ie";
		if(browserName.equals("firefox")){
			CalculatedVariables.webDriver = new FirefoxDriver();
		}
		if(browserName.equals("ie")){//���� ���������� ���������� �� �������������(��-�� �������������� ������� ���� ��� �������), ��� ����� ������� ��� ������ � ��
			System.setProperty("webdriver.ie.driver", Config.runDirPath + "\\other\\libs\\exe\\IEDriverServer.exe");
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
	        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	        //caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://sbt-ot-125:9080/HubBLWeb/Controller/");
	        caps.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
	        caps.setCapability("ignoreProtectedModeSettings", true);
	        caps.setCapability("ignoreZoomSetting", true);
	        caps.setCapability("nativeEvents", false);
	        caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
	        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
	        caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

	        CalculatedVariables.webDriver = new InternetExplorerDriver(caps);

	        
		}
		
		CalculatedVariables.webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		
	}
	
	/**
	 * ������� ���������� �������� �������� ������ �� ����� � ���������� �������
	 * @throws IOException 
	 */
	public static String getStandNameFromRemote() throws IOException{
		String standName = "";
		RemoteServerMethods.downloadFileFromRemoteServer("/osbtmp/dpcappl/WS_test/standName.txt", Config.runDirPath + Config.runTestSuiteProp.getProperty("logsPath") + "\\standName.txt");
		standName = FileLib.readFromFile(Config.runDirPath + Config.runTestSuiteProp.getProperty("logsPath") + "\\standName.txt", "UTF-8");
		return standName;
		
	}

}
