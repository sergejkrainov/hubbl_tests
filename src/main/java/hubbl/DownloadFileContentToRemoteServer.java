package hubbl;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import libs.FileLib;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class DownloadFileContentToRemoteServer {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String fileContent = FileLib.readFromFile(args[0], "UTF-8");
		putFileToRemoteServer(fileContent, args[0], args[1], args[2], args[3], args[4]);

	}
	
	
	/**
	 * ������� �������� ���������� ����� �� ��������� ������
	 * 
	 * @param fileContent
	 *            - ���������� �����
	 * @param fileName
	 *            - ��� �����
	 * @param pathToFileRemoteServer
	 *            - ���� � ����� �� ��������� �������
	 */
	public static void putFileToRemoteServer(String fileContent, String fileName, String pathToFileRemoteServer, String user, String password, String host) {
		int port = 22;

		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			System.out.println("Establishing Connection...");
			session.connect();
			System.out.println("Connection established.");

			System.out.println("Crating SFTP Channel.");
			ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
			sftpChannel.connect();
			System.out.println("SFTP Channel created.");

			sftpChannel.put(new ByteArrayInputStream(fileContent.getBytes(Charset.forName("cp866"))),
					pathToFileRemoteServer + fileName, sftpChannel.OVERWRITE);
			System.out.println("File " + fileName + " was downloaded to " + pathToFileRemoteServer);
			System.exit(0);

		} catch (Exception e) {
			System.err.print(e);
		}
	}

}
