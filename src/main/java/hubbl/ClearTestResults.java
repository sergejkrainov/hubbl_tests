package hubbl;

import libs.FileLib;
import variables.Config;

import java.io.IOException;

public class ClearTestResults {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
				try {
					Init.initConfigVariables(args);
					
					Init.initCalculatedVariables(args, null);
					
					FileLib.clearFolder(Config.resultsDir);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	}

}
