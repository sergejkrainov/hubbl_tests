package hubbl;

import libs.FileLib;
import variables.Config;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class SendCommonResults {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			
			Init.initConfigVariables(args);
			
			Init.initCalculatedVariables(args, null);
			
			// ���� � ������ ��� ��������
			String pathToSendFileResult = Config.resultsPathCommon;

			System.out.println("���������� ���� � ������� " + pathToSendFileResult); 
			
			DateFormat formatForResult = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT);
			Calendar calendar = Calendar.getInstance();

			// ����� ���������� �����
			String endTime = formatForResult.format(calendar.getTime());

			String resultContent = FileLib.readFromFile(pathToSendFileResult, "Windows-1251");

			String statusResult = "";

			if (resultContent.contains(">FAIL")) {

				statusResult = "FAIL";

			}else{
				statusResult = "OK";
			}


			// ���� ������
			String subject = "[AUTO-HBL]test results: " + endTime + "_" + statusResult + "_" + Config.testStandName;

			
			// ���� ������
			String body = "[AUTO-HBL]test results: "    + Config.testStandName + "_" +  endTime + "_" + statusResult + " . \n Results see in attachment file.";

			Properties props = new Properties();

			props.put(
				"mail.smtp.host",
				Config.smtpHost);
			props.put(
					"mail.smtp.auth",
					"true");
			props.put(
				"mail.smtp.port",
				Config.smtpPort);
			props.put(
				"mail.from",
				"SBT-AVTOTEST@mail.ca.sbrf.ru");
			Session session = Session.getInstance(
				props,
				new javax.mail.Authenticator() {
		            protected PasswordAuthentication getPasswordAuthentication() {
		               return new PasswordAuthentication(
		                  Config.smtpLogin, Config.smtpPassword);
		            }
				}
					);
			System.out.println("mail.smtp.host:" + Config.smtpHost);
			System.out.println("mail.smtp.host:" + Config.smtpPort);
			System.out.println("mail.from:" + "SBT-AVTOTEST@mail.ca.sbrf.ru");
			System.out.println("mailTo:" + Config.mailTo);
			
			String mailTo = Config.mailTo;
			
			String filename = "C:\\SomeDir\\notes3.txt";
	         

			try {
				MimeMessage msg = new MimeMessage(session);
				MimeBodyPart mbp1 = new MimeBodyPart();

				String[] emails =  mailTo.split(","); // ������ �����������

				InternetAddress[] dests = new InternetAddress[emails.length];
				for (int i = 0; i < emails.length; i++) {
					dests[i] = new InternetAddress(emails[i].trim().toLowerCase());
				}
				mbp1.setText(body);
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				
				File dir = new File(Config.resultsDir);	    	   
			 	File[] files = dir.listFiles();
			 	ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(Config.resultsDir + "hubblTestResults.zip"));
			 	   
			 	if(files.length > 0){
			 		for(int i = 0; i < files.length; i++){
			 				   	
			 			FileInputStream fis = null;
				 		if(!files[i].getName().contains("hubblTestResults.html")){
				 				fis = new FileInputStream(files[i]);
				 				ZipEntry entry = new ZipEntry(files[i].getName());
						 	    zout.putNextEntry(entry);
						 	    // ��������� ���������� ����� � ������ byte
						 	    byte[] buffer = new byte[fis.available()];
						 	    fis.read(buffer);
						 	       
						 	    // ��������� ���������� � ������
						 	    zout.write(buffer);
						 	    // ��������� ������� ������ ��� ����� ������
						 	    zout.closeEntry();
						 	    fis.close();
						 	   System.out.println("Send file " + files[i].getName());

				 		}	 			
			 			   
			 		}
			 	}
			 	zout.close();
			 	MimeBodyPart mbp2 = new MimeBodyPart();
	 			mbp2.attachFile(Config.resultsDir + "hubblTestResults.zip");
	 			mp.addBodyPart(mbp2);
	 			
	 			MimeBodyPart mbp3 = new MimeBodyPart();
	 			mbp3.attachFile(Config.resultsDir + "hubblTestResults.html");
	 			mp.addBodyPart(mbp3);
	 			
				msg.setFrom();
				msg.setRecipients(
					Message.RecipientType.TO,
					dests);
				msg.setSubject(subject);
				msg.setSentDate(new Date());
	
				msg.setContent(mp);
				Transport.send(msg);
				System.out.println(subject);
				System.out.println("���� � ������� ������� ���������");
			} catch (MessagingException mex) {
				
					System.err.println("send failed, exception: " + mex);
			}
			

		} catch (Exception ex) {

			System.err.println("�������� ������ � �������� �������� �����������: " + ex.getMessage());

		}finally{
			//������� ���������� � ������������ ������
			try {
				FileLib.clearFolder(Config.resultsDir);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
