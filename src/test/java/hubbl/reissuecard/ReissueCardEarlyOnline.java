package hubbl.reissuecard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

// ������ �� ������.

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "RecreateCard\\RecreateCardEarlyFPP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class ReissueCardEarlyOnline {
	
	@Before
	public void initTest() throws Exception{
		
		if (System.getProperty("isError").equals("false")) throw new NullPointerException ("������ �� ������."); 
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));

		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("120", "timeOnline", "1");
		BrowserActions.setHUBBLMode("false", "isBlockFpp", "1");
		BrowserActions.setHUBBLMode("false", "isReissueFpp", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
	}
	
	@Test
	public void testRecreateCardEarlyFPP() {//@Param(name = "name") String name, @Param(name = "age") int age){

		if (System.getProperty("isError").equals("false")) throw new NullPointerException ("������ �� ������"); 
		
		try{
		
		//������������ ������  InitiateReissueCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("RecreateCard", "RecreateCardEarlyFPP", "InitiateReissueCardRq");
		xmlDoc = XMLLib.redefineXMLParamsInitiateReissueCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateReissueCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "RecreateCard" + "\\" 
				+ "RecreateCardEarlyFPP" + "\\" +  "InitiateReissueCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateReissueCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateReissueCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "srvInitiateReissueCardRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "srvInitiateReissueCardRequest");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateReissueCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateReissueCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateReissueCardRs.xml", "Windows-1251", "InitiateReissueCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateReissueCardRs
		String initiateReissueCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateReissueCardRs.xml", "Windows-1251");
		System.out.println("initiateReissueCardRs = " + initiateReissueCardRsStr);
		Document initiateReissueCardRs = XMLLib.convertStringToDom(initiateReissueCardRsStr, "UTF-8");
		String initiateReissueCardRsStatusCode = XMLLib.getElementValueFromDocument(initiateReissueCardRs, "StatusCode");
		String initiateReissueCardRsStatusDesc = XMLLib.getElementValueFromDocument(initiateReissueCardRs, "StatusDesc");
		String initiateReissueCardRsSeverity = XMLLib.getElementValueFromDocument(initiateReissueCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateReissueCardRsStatusCode", initiateReissueCardRsStatusCode);
		CalculatedVariables.actualValues.put("initiateReissueCardRsStatusDesc", initiateReissueCardRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateReissueCardRsSeverity", initiateReissueCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateReissueCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateReissueCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateReissueCardRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("RecreateCard", "RecreateCardEarlyFPP", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "RecreateCard" + "\\" 
				+ "RecreateCardEarlyFPP" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"���������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 64 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 64 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal64.xml", "Windows-1251", "JrnTotal", "�������� ���������� 64 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 64 � ���
		String jrnTotal64Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal64.xml", "Windows-1251");
		System.out.println("jrnTotal64 = " + jrnTotal64Str);
		Document jrnTotal64 = XMLLib.convertStringToDom(jrnTotal64Str, "UTF-8");
		String typeOperCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "TypeOperCode");
		String subSystemCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "SubSystemCode");
		String mailAccountJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal64", typeOperCodeJrnTotal64);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal64", subSystemCodeJrnTotal64);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal64", "64");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal64", "1");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(10000);
		
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal64 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateReissueCardRq
		String initiateBlockCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "RecreateCard" + "\\" + "RecreateCardEarlyFPP" + "\\" +  "InitiateReissueCardRq" + ".xml", "UTF-8");
		Document initiateReissueCardRq = XMLLib.convertStringToDom(initiateBlockCardRqStr, "UTF-8");
		String initiateReissueCardRqBlockCDCode = XMLLib.getElementValueFromDocument(initiateReissueCardRq, "BlockCDCode");
		String initiateReissueCardRqBlockDate = XMLLib.getElementValueFromDocument(initiateReissueCardRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateReissueCardRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("initiateReissueCardRqBlockDate", result.get("blockdate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateReissueCardRqBlockCDCode", initiateReissueCardRqBlockCDCode);
		CalculatedVariables.expectedValues.put("initiateReissueCardRqBlockDate", initiateReissueCardRqBlockDate);
		
		
		// �������� ��������� ������ CardStatusModASyncRq
		
		//������� 30 ������ ��������� ���������
				CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
						"CardStatusModASyncRq", "Windows-1251", 30);
				
				//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
				XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
				
				//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
				String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
				System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
				Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
				String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
				String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
				String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");
				CalculatedVariables.rqUID = cardStatusModASyncRqRqUID;
				CalculatedVariables.rQTm = cardStatusModASyncRqRqTm;
				CalculatedVariables.operUID = cardStatusModASyncRqOperUID;
			
				//������� 30 ������ ��������� ���������
				CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
						way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
				
				//��������� xml ��������� CardStatusModASyncRs
				xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "CardStatusModASyncRs");
				xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
				xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
				FileLib.writeToFile(Config.xmlOutPath  + "DestroyCard" + "\\" 
						+ "InitiateDestroyCard" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
				
				//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
				MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
				
				//������� 30 ������ ��������� ���������
				System.out.println("Wait for 1 seconds...");
				Thread.sleep(1000);
		
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"HUBBL operUID = " + CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)");
	/*	
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
	//			CalculatedVariables.testUUID, "�������� ���������� 68 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log",
				Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
		String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log",
				Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 60 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
		String subSystemCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "SubSystemCode");
		String mailAccountJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal60", subSystemCodeJrnTotal60);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal60", "1");
		
		//��������� �������� �� ��
		sQuery = "select ischange, reasonchange from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal68 + "'";
		result = DataBaseLib.getAnyRowAsHashMap(sQuery, "ischange,reasonchange".split(","), "COD");
		
		//��������� �������� ���� ReasonCode �� xml InitiateReissueCardRq
		String initiateReissueCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "RecreateCard" + "\\" + "RecreateCardEarlyFPP" + "\\" +  "InitiateReissueCardRq" + ".xml", "UTF-8");
		initiateReissueCardRq = XMLLib.convertStringToDom(initiateReissueCardRqStr, "UTF-8");
		String initiateReissueCardRqReasonCode = XMLLib.getElementValueFromDocument(initiateReissueCardRq, "ReasonCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateReissueCardRqReasonCode", result.get("reasonchange"));
		CalculatedVariables.actualValues.put("initiateReissueCardRqIsChange", result.get("ischange"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateReissueCardRqReasonCode", initiateReissueCardRqReasonCode);
		CalculatedVariables.expectedValues.put("initiateReissueCardRqIsChange", "1");
		*/
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 3 seconds...");
		Thread.sleep(3000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateReissueCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateReissueCardRsLast.xml", "Windows-1251", "InitiateReissueCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateReissueCardRsLast.xml", "Windows-1251");
		Document initiateReissueCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateReissueCardRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateReissueCardRsLast, "StatusCode");
		String initiateReissueCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateReissueCardRsLast, "StatusDesc");
		String initiateReissueCardRsLastSeverity = XMLLib.getElementValueFromDocument(initiateReissueCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateReissueCardRsLastStatusCode", initiateReissueCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateReissueCardRsLastStatusDesc", initiateReissueCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateReissueCardRsLastSeverity", initiateReissueCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateReissueCardRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateReissueCardRsLastStatusDesc", "������ ���, ����� ���������� ���, ��������� ����� �����");
		CalculatedVariables.expectedValues.put("initiateReissueCardRsLastSeverity", "Ok");
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(1000);
		
	}

}