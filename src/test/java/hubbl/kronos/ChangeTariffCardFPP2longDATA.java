package hubbl.kronos;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;

import libs.BrowserActions;
import libs.CommonLib;
import libs.DataBaseLib;
import libs.FileLib;
import libs.JunitMethods;
import libs.MQLib;
import libs.XMLLib;
import hubbl.Init;
import variables.CalculatedVariables;
import variables.Config;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "ChangeTariffCard\\ChangeTariffCardFPP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class ChangeTariffCardFPP2longDATA {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO2longDATA"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("true", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testChangeTariffCardFPP() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateChangeTariffCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("ChangeTariffCard", "ChangeTariffCardFPP", "InitiateChangeTariffCardRq");
		xmlDoc = redefineXMLParamsInitiateChangeTariffCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateChangeTariffCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeTariffCard" + "\\" 
				+ "ChangeTariffCardFPP" + "\\" +  "InitiateChangeTariffCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateChangeTariffCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateChangeTariffCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "srvInitiateChangeTariffCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "srvInitiateChangeTariffCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "InitiateChangeTariffCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateChangeTariffCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateChangeTariffCardRs.xml", "Windows-1251", "InitiateChangeTariffCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateChangeTariffCardRs
		String initiateChangeTariffCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateChangeTariffCardRs.xml", "Windows-1251");
		System.out.println("initiateChangeTariffCardRs = " + initiateChangeTariffCardRsStr);
		Document initiateChangeTariffCardRs = XMLLib.convertStringToDom(initiateChangeTariffCardRsStr, "UTF-8");
		String initiateChangeTariffCardRsStatusCode = XMLLib.getElementValueFromDocument(initiateChangeTariffCardRs, "StatusCode");
		String initiateChangeTariffCardRsStatusDesc = XMLLib.getElementValueFromDocument(initiateChangeTariffCardRs, "StatusDesc");
		String initiateChangeTariffCardRsSeverity = XMLLib.getElementValueFromDocument(initiateChangeTariffCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateChangeTariffCardRsStatusCode", initiateChangeTariffCardRsStatusCode);
		CalculatedVariables.actualValues.put("initiateChangeTariffCardRsStatusDesc", initiateChangeTariffCardRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateChangeTariffCardRsSeverity", initiateChangeTariffCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateChangeTariffCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateChangeTariffCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateChangeTariffCardRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
		//		"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
		//		" - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"��������� ������ ��������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������ ��������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 178 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 178 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal178.xml", "Windows-1251", "JrnTotal", "�������� ���������� 178 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 178 � ���
		String jrnTotal178Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal178.xml", "Windows-1251");
		System.out.println("jrnTotal178 = " + jrnTotal178Str);
		Document jrnTotal178 = XMLLib.convertStringToDom(jrnTotal178Str, "UTF-8");
		String typeOperCodeJrnTotal178 = XMLLib.getElementValueFromDocument(jrnTotal178, "TypeOperCode");
		String subSystemCodeJrnTotal178 = XMLLib.getElementValueFromDocument(jrnTotal178, "SubSystemCode");
		String mailAccountJrnTotal178 = XMLLib.getElementValueFromDocument(jrnTotal178, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal178", typeOperCodeJrnTotal178);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal178", subSystemCodeJrnTotal178);
		

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal178", "178");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal178", "1");
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 198 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal198.xml", "Windows-1251", "JrnTotal", "�������� ���������� 198 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 198 � ���
		String jrnTotal198Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal198.xml", "Windows-1251");
		System.out.println("jrnTotal198 = " + jrnTotal198Str);
		Document jrnTotal198 = XMLLib.convertStringToDom(jrnTotal198Str, "UTF-8");
		String typeOperCodeJrnTotal198 = XMLLib.getElementValueFromDocument(jrnTotal198, "TypeOperCode");
		String subSystemCodeJrnTotal198 = XMLLib.getElementValueFromDocument(jrnTotal198, "SubSystemCode");
		String mailAccountJrnTotal198 = XMLLib.getElementValueFromDocument(jrnTotal198, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal198", typeOperCodeJrnTotal198);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal198", subSystemCodeJrnTotal198);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal198", "198");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal198", "1");
		
		//������� 1 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(1000);
		
		String sQuery = "select tariffservcur, tariffservnext  from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal178 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "tariffservcur,tariffservnext".split(","), "COD");
		
		//��������� �������� ���� TarifFirst,TarifNext �� xml InitiateChangeTariffCardRq
		String initiateInitiateChangeTariffCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "ChangeTariffCard" + "\\" + "ChangeTariffCardFPP" + "\\" +  "InitiateChangeTariffCardRq" + ".xml", "UTF-8");
		Document initiateInitiateChangeTariffCardRq = XMLLib.convertStringToDom(initiateInitiateChangeTariffCardRqStr, "UTF-8");
		String initiateInitiateChangeTariffCardRqTarifFirst = XMLLib.getElementValueFromDocument(initiateInitiateChangeTariffCardRq, "TarifFirst");
		String initiateInitiateChangeTariffCardRqTarifNext = XMLLib.getElementValueFromDocument(initiateInitiateChangeTariffCardRq, "TarifNext");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateInitiateChangeTariffCardRqTarifFirst", result.get("tariffservcur"));
		CalculatedVariables.actualValues.put("initiateInitiateChangeTariffCardRqTarifNext", result.get("tariffservnext"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateInitiateChangeTariffCardRqTarifFirst", initiateInitiateChangeTariffCardRqTarifFirst);
		CalculatedVariables.expectedValues.put("initiateInitiateChangeTariffCardRqTarifNext", initiateInitiateChangeTariffCardRqTarifNext);
		
		//������� 3 ������ ��������� ���������
		System.out.println("Wait for 3 seconds...");
		Thread.sleep(3000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateChangeTariffCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateChangeTariffCardRsLast.xml", "Windows-1251", "InitiateChangeTariffCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateChangeTariffCardRsLast.xml", "Windows-1251");
		Document initiateInitiateChangeTariffCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateInitiateChangeTariffCardRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateInitiateChangeTariffCardRsLast, "StatusCode");
		String initiateInitiateChangeTariffCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateInitiateChangeTariffCardRsLast, "StatusDesc");
		String initiateInitiateChangeTariffCardRsLastSeverity = XMLLib.getElementValueFromDocument(initiateInitiateChangeTariffCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateInitiateChangeTariffCardRsLastStatusCode", initiateInitiateChangeTariffCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateInitiateChangeTariffCardRsLastStatusDesc", initiateInitiateChangeTariffCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateInitiateChangeTariffCardRsLastSeverity", initiateInitiateChangeTariffCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateInitiateChangeTariffCardRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateInitiateChangeTariffCardRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateInitiateChangeTariffCardRsLastSeverity", "Ok");
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsInitiateChangeTariffCardRq(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		 String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		CalculatedVariables.testUUID = RqUID;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
		//rowParams.put("OperDate", Config.OperDay);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		rowParams.put("OperDate", Config.OperDay);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		
		sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		CalculatedVariables.extDt = rowParams.get("ExpDt");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
			
			XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
			XMLLib.deleteTagInDocument(docXML, "Resident");
			XMLLib.deleteTagInDocument(docXML, "ControlWord");
			
		//������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		rowParams.clear();
		rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
			 						
		return docXML;
	}
	

	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		//Thread.sleep(1000);
		
	}

}