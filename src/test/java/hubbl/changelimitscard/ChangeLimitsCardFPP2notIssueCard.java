package hubbl.changelimitscard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "ChangeLimitsCard\\ChangeLimitsCardFPP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class ChangeLimitsCardFPP2notIssueCard {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("true", "isSetCardActivityLimitFPP", "1");
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
	//	prepareTestData();  �� ��������� ����� ������� �������������.
		
		//������� ���������� � ������ � ���������� �������
		////FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		////FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testChangeLimitsCardFPP() {//@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateChangeTariffCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("ChangeLimitsCard", "ChangeLimitsCardFPP", "InitiateSetCardActivityLimitRq");
		xmlDoc = redefineXMLParamsInitiateChangeLimitsCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR26Ver036", true, "InitiateSetCardActivityLimitRq");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeLimitsCard" + "\\" 
				+ "ChangeLimitsCardFPP" + "\\" +  "InitiateSetCardActivityLimitRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "<ns2:hblInitiateSetCardActivityLimitRq", "Windows-1251", 30);	
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:hblInitiateSetCardActivityLimitRq");
		
	//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "hblInitiateSetCardActivityLimitRq", "Windows-1251", 30);	
	
//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "hblInitiateSetCardActivityLimitRq");
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ setCardActivityLimit()", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ setCardActivityLimit()");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "InitiateSetCardActivityLimitRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateChangeTariffCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateSetCardActivityLimitRs.xml", "Windows-1251", "InitiateSetCardActivityLimitRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateChangeTariffCardRs
		String initiateSetCardActivityLimitRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateSetCardActivityLimitRs.xml", "Windows-1251");
		System.out.println("InitiateSetCardActivityLimitRs = " + initiateSetCardActivityLimitRsStr);
		Document initiateSetCardActivityLimitRs = XMLLib.convertStringToDom(initiateSetCardActivityLimitRsStr, "UTF-8");
		String initiateSetCardActivityLimitRsStatusCode = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitRs, "StatusCode");
		String initiateSetCardActivityLimitRsStatusDesc = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitRs, "StatusDesc");
		String initiateSetCardActivityLimitRsSeverity = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitdRsStatusCode", initiateSetCardActivityLimitRsStatusCode);
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitRsStatusDesc", initiateSetCardActivityLimitRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitRsSeverity", initiateSetCardActivityLimitRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateSetCardActivityLimitdRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateSetCardActivityLimitRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			" (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"���������/��������� ������ � ���", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������/��������� ������ � ���");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 70 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 70 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal70.xml", "Windows-1251", "JrnTotal", "�������� ���������� 70 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 70 � ���
		String jrnTotal70Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal70.xml", "Windows-1251");
		System.out.println("jrnTotal70 = " + jrnTotal70Str);
		Document jrnTotal70 = XMLLib.convertStringToDom(jrnTotal70Str, "UTF-8");
		String typeOperCodeJrnTotal70 = XMLLib.getElementValueFromDocument(jrnTotal70, "TypeOperCode");
		String subSystemCodeJrnTotal70 = XMLLib.getElementValueFromDocument(jrnTotal70, "SubSystemCode");
		String mailAccountJrnTotal70 = XMLLib.getElementValueFromDocument(jrnTotal70, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal70", typeOperCodeJrnTotal70);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal70", subSystemCodeJrnTotal70);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal70", "70");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal70", "1");
		
		
		//������� 1 ������ ��������� ���������
		System.out.println("Wait for 1 seconds... before SQL");
		Thread.sleep(1000);
		
/*
 * select SumBanCard as cashLimit, MonthLimCard as monthLimit, SumPayCard as payLimit from deposit.dcard d where d.numcard = 1312198619861220 /*�����_�����*/

		
		String sQuery = "select SumBanCard, MonthLimCard, SumPayCard from deposit.dcard d where d.numcard = '" + CalculatedVariables.cardNumber + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "CashLimit,MonthLimit,PayLimit".split(","), "COD");
		
		//��������� �������� ���� TarifFirst,TarifNext �� xml InitiateChangeTariffCardRq
		String initiateInitiateChangeTariffCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "ChangeLimitsCard" + "\\" 
					+ "ChangeLimitsCardFPP" + "\\" +  "InitiateSetCardActivityLimitRq" + ".xml", "UTF-8");
		Document InitiateSetCardActivityLimitRq = XMLLib.convertStringToDom(initiateInitiateChangeTariffCardRqStr, "UTF-8");
		String InitiateSetCardActivityLimitRqCashLimit = XMLLib.getElementValueFromDocument(InitiateSetCardActivityLimitRq, "CashLimit");
		String InitiateSetCardActivityLimitRqMonthLimit = XMLLib.getElementValueFromDocument(InitiateSetCardActivityLimitRq, "MonthLimit");
		String InitiateSetCardActivityLimitRqPayLimit = XMLLib.getElementValueFromDocument(InitiateSetCardActivityLimitRq, "PayLimit");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateSetCardActivityLimit_CashLimit", result.get("CashLimit"));
		CalculatedVariables.actualValues.put("InitiateSetCardActivityLimit_MonthLimit", result.get("MonthLimit"));
		CalculatedVariables.actualValues.put("InitiateSetCardActivityLimit_PayLimit", result.get("PayLimit"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateSetCardActivityLimit_CashLimit", InitiateSetCardActivityLimitRqCashLimit);
		CalculatedVariables.expectedValues.put("InitiateSetCardActivityLimit_MonthLimit", InitiateSetCardActivityLimitRqMonthLimit);
		CalculatedVariables.expectedValues.put("InitiateSetCardActivityLimit_PayLimit", InitiateSetCardActivityLimitRqPayLimit);
		
		
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateChangeTariffCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateSetCardActivityLimitdRsLast.xml", "Windows-1251", "InitiateSetCardActivityLimitRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateSetCardActivityLimitdRsLast.xml", "Windows-1251");
		Document initiateSetCardActivityLimitdRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateSetCardActivityLimitdRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitdRsLast, "StatusCode");
		String initiateSetCardActivityLimitdRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitdRsLast, "StatusDesc");
		String initiateSetCardActivityLimitdRsLastSeverity = XMLLib.getElementValueFromDocument(initiateSetCardActivityLimitdRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitdRsLastStatusCode", initiateSetCardActivityLimitdRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitdRsLastStatusDesc", initiateSetCardActivityLimitdRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateSetCardActivityLimitdRsLastSeverity", initiateSetCardActivityLimitdRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateSetCardActivityLimitdRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateSetCardActivityLimitdRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateSetCardActivityLimitdRsLastSeverity", "Ok");
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"�������� ���������� ������ setCardActivityLimit() #####", "Windows-1251", 10);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ setCardActivityLimit() #####");
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */	public static Document redefineXMLParamsInitiateChangeLimitsCardRq(Document docXML) throws Exception{
			HashMap<String, String> rowParams = new HashMap<String, String>(); 
			String RqUID = "";
			String newUUID = "";
			String OperUID = "";
			String sQuerry = "";
			newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			RqUID = newUUID;
			newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			OperUID = newUUID;
			CalculatedVariables.operUID = OperUID;
			System.out.println("OperUID = " + OperUID);
			CalculatedVariables.rqUID = RqUID;
			System.out.println("RqUID = " + RqUID);
			CalculatedVariables.testUUID = RqUID;
			
			rowParams.put("RqUID", RqUID);
			rowParams.put("OperUID", OperUID);
			rowParams.put("OperDate", Config.OperDay);
			rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
			rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
			rowParams.put("OperDate", Config.OperDay);
			rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
			rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
			
			
			sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
			rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
			
			CalculatedVariables.cardNumber = rowParams.get("CardNum");
			
			sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
			rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
														
			CalculatedVariables.extDt = rowParams.get("ExpDt");
		
			sQuerry = "SELECT  PERSON_MINOR  FROM deposit.dcard WHERE id_mega=38 and NumContrCard =  '" + CalculatedVariables.mailAccount + "'"; 
			String PERSON_MINOR = DataBaseLib.getDataAsString(sQuerry, "COD");
			
			sQuerry = "SELECT PERSON_MAJOR  FROM deposit.dcard WHERE id_mega=38 and NumContrCard =  '" + CalculatedVariables.mailAccount + "'"; 
			String PERSON_MAJOR = DataBaseLib.getDataAsString(sQuerry, "COD");
			
			CalculatedVariables.Id_client = "38" + "-" + PERSON_MAJOR + "-" + PERSON_MINOR;
			
			rowParams.put("Id_client", CalculatedVariables.Id_client);

			docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
				
			
			//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
				Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
				XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonName", "Gender" );
				XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "IdentityCard", "/PersonInfo");
				
				
				/*
				 * deleteTagInDocument(docXML, "gender");
				 * 
				deleteTagInDocument(docXML, "Birthplace");
				deleteTagInDocument(docXML, "Citizenship");
				deleteTagInDocument(docXML, "TaxId");
				deleteTagInDocument(docXML, "ClientCategory");
				deleteTagInDocument(docXML, "ClientStatus");
				deleteTagInDocument(docXML, "Verified");
				deleteTagInDocument(docXML, "Signed");
				deleteTagInDocument(docXML, "ContactInfo");
				deleteTagInDocument(docXML, "Resident");
				deleteTagInDocument(docXML, "ControlWord");
				*/
				
			//������ info 
		 	rowParams.clear();
			rowParams.put("BranchId", Config.IssueBankInfoBranchId);
			rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
			rowParams.put("RegionId", Config.IssueBankInfoRegionId);
			rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
				 						
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		/*	//������ info 
			rowParams.clear();
			rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
			rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
			rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
			rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
				 						
			 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
		*/ 		 				
			
			
			
			return docXML;
		}
		

	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		//Thread.sleep(1000);
		
	}

}