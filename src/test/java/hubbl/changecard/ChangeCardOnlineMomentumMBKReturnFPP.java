package hubbl.changecard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import hubbl.RunTestBase;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "ChangeCard\\ChangeCardOnlineMomentumMBKReturnFPP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class ChangeCardOnlineMomentumMBKReturnFPP {

	@Before
	public void initTest() throws Exception{
		String [] args = new String[4];
		args[0] = "BlockCard";
		args[1] = "BlockCardFPP";
		args[2] = Config.runDirPath + "\\target";
		args[3] = "st2";
		//	System.out.println("Run test args :" + args);

		String testOperation = args[0];
		String testCase = args[1];

		System.out.println("Run test CalculatedVariables.testOperation: " +  CalculatedVariables.testOperation);
		System.out.println("Run test CalculatedVariables.testCase: " + CalculatedVariables.testCase);


		try {
			if (System.getProperty ( "stand" ) != null) args[3] = System.getProperty ( "stand" );
		}
		catch (Exception e) {
			System.out.println ( "�� ����� ����� �� ���������: " + args[3] );
		}

		CommonLib.resetAllsettings();
		RunTestBase.startInitTests ( args, CalculatedVariables.testOperation, CalculatedVariables.testCase);


		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineMomentum"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL

		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		
		BrowserActions.setHUBBLMode("true", "isMomentumToMbkFpp", "1");
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	
	@Test
	public void testChangeCardOnlineMomentumMBKReturnFPP() {//@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateReplaceMomentumCardToCardRq �� �� ��. 
		Document xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMBKReturn", "InitiateReplaceMomentumCardToCardRq");
		xmlDoc = ChangeCardOnlineMomentumMBKReturnFPP.redefineXMLParamsInitiateReplaceMomentumCardToCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateReplaceMomentumCardToCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMBKReturn" + "\\" +  "InitiateReplaceMomentumCardToCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("���� 9 ��� 3 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateReplaceMomentumCardToCardRq", "Windows-1251", 30);	
		
		//String value = XMLLib.extractValueFromLogByTagName();
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "</InitiateReplaceMomentumCardToCardRq>");
		System.out.println("���� 9 ��� 4 ");
		
		
		//������� 30 ������ ��������� ���������
				CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "� ������ ������ momentum2mbk()", "Windows-1251", 30);
				JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + " - � ������ ������ momentum2mbk()");
		
/*		
        String sQuerry = "";
        
		//������ ������ � �� AC �����
				
		sQuerry = "select case when count(*) = 1 then 'OK' else 'BAD' end as Close_Open "
				+ " from ( select * from hubbl.parameters "
				+ " where PRPARAM like '%" + CalculatedVariables.rqUID + "%' )";
		String actualResultFromDB = DataBaseLib.getDataAsString(sQuerry, "HUBBL");
		System.out.println("actualResultFromDB = " + actualResultFromDB );

		
		//���������� ����������� �������� ����������

		CalculatedVariables.actualValues.put("ResultFromDB", actualResultFromDB);
						
		//��������� ����������� �������� ����������

		CalculatedVariables.expectedValues.put("ResultFromDB", "OK");
*/		
/*		if ( !actualResultFromDB.equals("OK")) {
			JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
            try {

                CommonLib.WriteLog("FAIL", "Fail due to checking ResultFromDB." , Config.resultsPath );

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}

            CalculatedVariables.isTestFailed = true;

            CalculatedVariables.isError = true;

            try {

                CommonLib.postProcessingTest();

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}       


		}
//	*/	
		System.out.println("���� 9 ��� 5 ");
		System.out.println("���� 5 ������...");
		Thread.sleep(5000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateReplaceMomentumCardToCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateReplaceMomentumCardToCardState99Rs.xml", "Windows-1251", "InitiateReplaceMomentumCardToCardRs");
		
		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateReplaceMomentumCardToCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateReplaceMomentumCardToCardState99Rs.xml", "Windows-1251");
		System.out.println("InitiateDeliveryCardMomentumRsStr = " + InitiateReplaceMomentumCardToCardRsStr);
		Document InitiateReplaceMomentumCardToCardRs = XMLLib.convertStringToDom(InitiateReplaceMomentumCardToCardRsStr, "UTF-8");
		String statusCodeInitiateReplaceMomentumCardToCardRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardRs, "StatusCode");
		String statusDescInitiateReplaceMomentumCardToCardRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardRs, "StatusDesc");
		String severityInitiateReplaceMomentumCardToCardRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateReplaceMomentumCardToCardRs", statusCodeInitiateReplaceMomentumCardToCardRs);
		CalculatedVariables.actualValues.put("statusDescInitiateReplaceMomentumCardToCardRs", statusDescInitiateReplaceMomentumCardToCardRs);
		CalculatedVariables.actualValues.put("severityInitiateReplaceMomentumCardToCardRs", severityInitiateReplaceMomentumCardToCardRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateReplaceMomentumCardToCardRs", "99");
		CalculatedVariables.expectedValues.put("statusDescInitiateReplaceMomentumCardToCardRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityInitiateReplaceMomentumCardToCardRs", "Ok");

		System.out.println("���� 9 ��� 6 ");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");

		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");

	
		System.out.println("���� 9 ��� 8 ");
		//������� 30 ������ ��������� ���������
//		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "�������� ���������� 63 � ���:", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 63 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 63 ���������� � ���!");
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");


		System.out.println("���� 9 ��� 9 ���������� � ��� ��� ");
		
		System.out.println("���� 9 ��� 10 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����) #####", "Windows-1251", 160);	

		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - �������� ���������� 68 � ���:", "Windows-1251", 160);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 68 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 68 ���������� � ���!");
/*
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
//		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
		String ukrBankCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "UKRBankCode");
//		String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
//		CalculatedVariables.mailAccount = mailAccountJrnTotal68;
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
//		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", ukrBankCodeJrnTotal68);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
//		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", "IGN");
		
*/
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - �������� ���������� 60 � ���:", "Windows-1251", 160);	

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 60 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 60 ���������� � ���!");

 /*		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
//		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
		String ukrBankCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "UKRBankCode");
//		String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
//		CalculatedVariables.mailAccount = mailAccountJrnTotal68;
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
//		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", ukrBankCodeJrnTotal60);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "60");
//		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", "IGN");
*/		
		System.out.println("test 9 ��� 11 � 12  ��������� �� ���");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "ReissueCardRq", "Windows-1251", 30);	
		
		//��������� �� ���� ���������� xml ��������� CustAddRq
	/*	XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\ReissueCardRq.xml", "Windows-1251", "ReissueCardRq");
	
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\ReissueCardRq.xml", "Windows-1251");
		Document reissueCardRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(reissueCardRqDoc, "RqUID");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(reissueCardRqDoc, "OperUID");
		System.out.println("rqUIDReissueCardRq = " + CalculatedVariables.rqUID);		*/
		
		//JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");

	/*	System.out.println("test 9 ��� 12 ");
		//��������� xml ��������� ReissueCardRs
		xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMBKReturn", "ReissueCardRs");
		xmlDoc = XMLLib.redefineXMLParamsReissueCardRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "ReissueCardRs");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMBKReturn" + "\\" +  "ReissueCardRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
						
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//System.out.println("���� 2 �������...");
		//Thread.sleep(2000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "ReissueCardRs", "Windows-1251", 30);	

		//��������� �� ���� ���������� xml ��������� ReissueCardRsRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\ReissueCardRs.xml", "Windows-1251", "ReissueCardRs");
					
		//��������� �������� ���� statusCode �� ������ ReissueCardRsRs
		String ReissueCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\ReissueCardRs.xml", "Windows-1251");
		System.out.println("ReissueCardRsStr = " + ReissueCardRsStr);
		Document ReissueCardRs = XMLLib.convertStringToDom(ReissueCardRsStr, "UTF-8");
		String rqUIDReissueCardRs = XMLLib.getElementValueFromDocument(ReissueCardRs, "RqUID");
		String operUIDReissueCardRs = XMLLib.getElementValueFromDocument(ReissueCardRs, "OperUID");
//		String statusCodeCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "StatusCode");
						
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDReissueCardRs", rqUIDReissueCardRs);
		CalculatedVariables.actualValues.put("operUIDReissueCardRs", operUIDReissueCardRs);
//		CalculatedVariables.actualValues.put("statusCodeCardStatusModASyncRs", statusCodeCardStatusModASyncRs);
										
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDReissueCardRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("operUIDReissueCardRs", CalculatedVariables.operUID);
//		CalculatedVariables.expectedValues.put("statusCodeCardStatusModASyncRs", "0");
	
		*/
		
		System.out.println("���� 9 ��� 13 ");
		System.out.println("���� 5 ������...");
		Thread.sleep(5000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "�������� ���������� ������ momentum2mbk()", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + " - �������� ���������� ������ momentum2mbk()");

		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateReplaceMomentumCardToCardLastStateRs.xml", "Windows-1251", "InitiateReplaceMomentumCardToCardRs");

		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateReplaceMomentumCardToCardLastStateRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateReplaceMomentumCardToCardLastStateRs.xml", "Windows-1251");
		//System.out.println("InitiateReplaceMomentumCardToCardLastStateRsStr = " + InitiateReplaceMomentumCardToCardLastStateRsStr);
		Document InitiateReplaceMomentumCardToCardLastStateRs = XMLLib.convertStringToDom(InitiateReplaceMomentumCardToCardLastStateRsStr, "UTF-8");
		String statusCodeInitiateReplaceMomentumCardToCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardLastStateRs, "StatusCode");
		String statusDescInitiateReplaceMomentumCardToCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardLastStateRs, "StatusDesc");
		String severityInitiateReplaceMomentumCardToCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateReplaceMomentumCardToCardLastStateRs, "Severity");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateReplaceMomentumCardToCardLastStateRs", statusCodeInitiateReplaceMomentumCardToCardLastStateRs);
		CalculatedVariables.actualValues.put("statusDescInitiateReplaceMomentumCardToCardLastStateRs", statusDescInitiateReplaceMomentumCardToCardLastStateRs);
		CalculatedVariables.actualValues.put("severityInitiateReplaceMomentumCardToCardLastStateRs", severityInitiateReplaceMomentumCardToCardLastStateRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateReplaceMomentumCardToCardLastStateRs", "0");
		CalculatedVariables.expectedValues.put("statusDescInitiateReplaceMomentumCardToCardLastStateRs", "������ ���, ����� ���������� ���, ��������� ����� �����");
		CalculatedVariables.expectedValues.put("severityInitiateReplaceMomentumCardToCardLastStateRs", "Ok");

		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		

}catch(Exception e){
	e.printStackTrace();
}

	}
	


public static Document redefineXMLParamsInitiateReplaceMomentumCardToCardRq(Document docXML) throws Exception {
	 HashMap<String, String> rowParams = new HashMap<String, String>(); 
	 String newUUID = "";
	 String RqUID = "";
	 String OperUID = "";
	 
	 LocalDateTime todayDateTime = LocalDateTime.now();
	 
	 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
	 RqUID = newUUID;
	 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
	 OperUID = newUUID;
	 CalculatedVariables.testUUID = RqUID;
	 CalculatedVariables.operUID = OperUID;
	 System.out.println("OperUID = " + OperUID);
	 CalculatedVariables.rqUID = RqUID;
	 System.out.println("RqUID = " + RqUID);
	 
		
	 //String lastName = "�������-" + CommonLib.getRandomLetters(15);
	 //String firstName = CommonLib.getRandomLetters(15);
	 //String middleName = CommonLib.getRandomLetters(15);
	 //System.out.println("��� 3 ");
		
	 //String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
	 //String idNum = CommonLib.getRandomNumericLetters(6);
	 //System.out.println("��� 4 ");
	 
	 //���������  � template.xml
	 //String cardCode = "111707";
	 //String productCode = "IRRDBCMF--";
	 //String eDBOContractFlag = "true";
	 //String contractProductCode = "IRRD--";
	 
	 						
	 rowParams.put("RqUID", RqUID);
	 rowParams.put("OperUID", OperUID);
	 rowParams.put("RqTm", todayDateTime.toString());
	 rowParams.put("IdSeries", CalculatedVariables.idSeries);
	 rowParams.put("IdNum", CalculatedVariables.idNum);
		
	 
			 
	//������ info 
	 rowParams.put("OperDay", Config.businessProcessProp.getProperty("OperDay"));
	 rowParams.put("OperatorLogin", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin"));
	 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode"));
	 rowParams.put("OperatorName", Config.businessProcessProp.getProperty("OperatorInfoOperatorName"));
	 
			
	 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
	 
		
	 //������ ��� 
	 rowParams.clear();
	 rowParams.put("LastName", CalculatedVariables.lastName);
	 rowParams.put("FirstName", CalculatedVariables.firstName);
	 rowParams.put("MiddleName", CalculatedVariables.middleName);
	 
	
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
	 
	
	 //������ info 
 	 rowParams.clear();
	 rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
	 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
	 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
	 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");
	 

	 //������ info 
	 rowParams.clear();
	 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId"));
	 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId"));
	 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId"));
	 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId"));
	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
	 

	 //������ info �� ����� ��������

	 rowParams.clear();
	 rowParams.put("CardNum", CalculatedVariables.cardNumber);
	 rowParams.put("IssueDate", CalculatedVariables.issueDate);
	 rowParams.put("ExpDt", CalculatedVariables.extDt);

	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdFrom");
//	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardRecMomentum");

	 if(CalculatedVariables.testCase.equals("ChangeCardOnlineMomentumMomentum")){
		 String cardNum = "";
		 String sQuerry = "";
			
			//���������� 16-�� ������� ����� �����, ������� ����������� � ��
			do{
				cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
				sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
			}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
				System.out.println("GeneratedCardNum = " + cardNum);

			//������ info 
		 	rowParams.clear();

			rowParams.put("CardNum", cardNum);
			
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdTo");
				 						
			}
	return docXML;
 
}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	



}
