package hubbl.changecard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import hubbl.RunTestBase;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "ChangeCard\\ChangeCardOnlineMomentumMomentum\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class ChangeCardOnlineMomentumMomentum2filial2DeliveryBankInfo22 {
//10.	������ ������ ����� �������� �� ��������.
	@Before
	public void initTest() throws Exception{
		String [] args = new String[4];
		args[0] = "BlockCard";
		args[1] = "BlockCardFPP";
		args[2] = Config.runDirPath + "\\target";
		args[3] = "st2";
		//	System.out.println("Run test args :" + args);

		String testOperation = args[0];
		String testCase = args[1];

		System.out.println("Run test CalculatedVariables.testOperation: " +  CalculatedVariables.testOperation);
		System.out.println("Run test CalculatedVariables.testCase: " + CalculatedVariables.testCase);


		try {
			if (System.getProperty ( "stand" ) != null) args[3] = System.getProperty ( "stand" );
		}
		catch (Exception e) {
			System.out.println ( "�� ����� ����� �� ���������: " + args[3] );
		}

		CommonLib.resetAllsettings();
		RunTestBase.startInitTests ( args, CalculatedVariables.testOperation, CalculatedVariables.testCase);


		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineMomentum"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL

		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		
		BrowserActions.setHUBBLMode("false", "isMomentumToMomentumFpp", "1");
		BrowserActions.setHUBBLMode("true", "IsReplaceMomentumCardToCardMultiOSB.tech", "1");
		BrowserActions.setHUBBLMode("180", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testChangeCardOnlineMomentumMomentum2filial2DeliveryBankInfo22() {//@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateReplaceMomentumCardToCardRq �� �� ��. 
		Document xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMomentum", "InitiateChangeMomentumCardToMomentumCardRq");
		xmlDoc = redefineXMLParamsInitiateReplaceMomentumCardToCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", false, "InitiateChangeMomentumCardToMomentumCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMomentum" + "\\" +  "InitiateChangeMomentumCardToMomentumCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("���� 10 ��� 3 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateChangeMomentumCardToMomentumCardRq", "Windows-1251", 30);	
		
		//String value = XMLLib.extractValueFromLogByTagName();
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "</InitiateChangeMomentumCardToMomentumCardRq>");

		
		System.out.println("���� 10 ��� 4 ");
		System.out.println("���� 2 ������...");
		Thread.sleep(2000);

	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateChangeMomentumCardToMomentumCardRs", "Windows-1251", 30);
		
		//������� 30 ������ ��������� ���������
		//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, " - � ������ ������ momentum2momentum() #####", "Windows-1251", 30);
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ momentum2momentum() #####");


		
		//��������� �� ���� ���������� xml ��������� InitiateChangeMomentumCardToMomentumCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateChangeMomentumCardToCardState99Rs.xml", "Windows-1251", "InitiateChangeMomentumCardToMomentumCardRs");
		
		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateChangeMomentumCardToMomentumCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateChangeMomentumCardToCardState99Rs.xml", "Windows-1251");
		System.out.println("InitiateChangeMomentumCardToMomentumCardRsStr = " + InitiateChangeMomentumCardToMomentumCardRsStr);
		Document InitiateChangeMomentumCardToMomentumCardRs = XMLLib.convertStringToDom(InitiateChangeMomentumCardToMomentumCardRsStr, "UTF-8");
		String statusCodeInitiateChangeMomentumCardToMomentumCardRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardRs, "StatusCode");
		String statusDescInitiateChangeMomentumCardToMomentumCardRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardRs, "StatusDesc");
		String severityInitiateChangeMomentumCardToMomentumCardRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateChangeMomentumCardToMomentumCardRs", statusCodeInitiateChangeMomentumCardToMomentumCardRs);
		CalculatedVariables.actualValues.put("statusDescInitiateChangeMomentumCardToMomentumCardRs", statusDescInitiateChangeMomentumCardToMomentumCardRs);
		CalculatedVariables.actualValues.put("severityInitiateChangeMomentumCardToMomentumCardRs", severityInitiateChangeMomentumCardToMomentumCardRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateChangeMomentumCardToMomentumCardRs", "99");
		CalculatedVariables.expectedValues.put("statusDescInitiateChangeMomentumCardToMomentumCardRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityInitiateChangeMomentumCardToMomentumCardRs", "Ok");

		System.out.println("���� 10 ��� 5 ");
		//������� 30 ������ ��������� ���������
//		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");

		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");

		System.out.println("test 10 ��� 6 ");
//		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, " - ����� ����� � WAY4 (�������� �����) #####", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����) #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);	
		
				
		//��������� �� ���� ���������� xml ��������� GetCardHolderRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		//��������� ����������� �������� ����������
		Document cardAcctDInqRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRqDoc, "RqUID");
		
		System.out.println("���� 1 �������...");
		Thread.sleep(1000);
		//��������� xml ��������� CardAcctDInqRs
		xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMBKReturn", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMBKReturn" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRs", "Windows-1251", 30);	

		//��������� �� ���� ���������� xml ��������� CardAcctDInqRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRs.xml", "Windows-1251", "CardAcctDInqRs");
		
		//��������� �������� ���� statusCode �� ������ CardAcctDInqRs
		String CardAcctDInqRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRs.xml", "Windows-1251");
		System.out.println("CardAcctDInqRsStr = " + CardAcctDInqRsStr);
		Document CardAcctDInqRs = XMLLib.convertStringToDom(CardAcctDInqRsStr, "UTF-8");
		String rqUIDCardAcctDInqRs = XMLLib.getElementValueFromDocument(CardAcctDInqRs, "RqUID");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDCardAcctDInqRs", rqUIDCardAcctDInqRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDCardAcctDInqRs", CalculatedVariables.rqUID);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, " - WAY4 ������ CRDWI ������: true (�������� �����)", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - WAY4 ������ CRDWI ������: true (�������� �����)");
		
		System.out.println("���� 10 ��� 7 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, " - ���������� ����� � ���� #####", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ����� � ���� #####");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 64 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 64 ���������� � ���!");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����) #####", "Windows-1251", 160);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 68 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 68 ���������� � ���!");

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
		String ukrBankCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "UKRBankCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", ukrBankCodeJrnTotal68);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", "IGN");
		

	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - �������� ���������� 60 � ���:", "Windows-1251", 160);	

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 60 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 60 ���������� � ���!");

 		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
		String ukrBankCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "UKRBankCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", ukrBankCodeJrnTotal60);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", "IGN");
	
		System.out.println("���� 10 ��� 10 ");
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		//JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + " - ��� ����� HBLCardService.getCardByInfo ������: true");

		System.out.println("���� 10 ��� 11 ");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - ������ ����� � ���� #####", "Windows-1251", 160);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 61 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 61 ���������� � ���!");

		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 61 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal61.xml", "Windows-1251", "JrnTotal", "�������� ���������� 61 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal61Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal61.xml", "Windows-1251");
		System.out.println("jrnTotal61 = " + jrnTotal61Str);
		Document jrnTotal61 = XMLLib.convertStringToDom(jrnTotal61Str, "UTF-8");
		String typeOperCodeJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "TypeOperCode");
		String ukrBankCodeJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "UKRBankCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal61", typeOperCodeJrnTotal61);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal61", ukrBankCodeJrnTotal61);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal61", "61");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal61", "IGN");

		System.out.println("test 10 ��� 12 ");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		System.out.println("test 10 ��� 13 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardStatusModASyncRq", "Windows-1251", 30);	
		
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		//��������� ����������� �������� ����������
		Document cardStatusModASyncRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String operUIDTotal = CalculatedVariables.operUID;
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRqDoc, "RqUID");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRqDoc, "OperUID");

		
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMomentum", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMomentum" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardStatusModASyncRs", "Windows-1251", 30);	
	
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRs.xml", "Windows-1251", "CardStatusModASyncRs");
		
		//��������� �������� ���� statusCode �� ������ CardStatusModASyncRs
		String CardStatusModASyncRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRs.xml", "Windows-1251");
//		System.out.println("CardStatusModASyncRsStr = " + CardStatusModASyncRsStr);
		Document CardStatusModASyncRs = XMLLib.convertStringToDom(CardStatusModASyncRsStr, "UTF-8");
		String rqUIDCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "RqUID");
		String operUIDCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "OperUID");
		String statusCodeCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "StatusCode");
		String statusDescCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "StatusDesc");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDCardStatusModASyncRs", rqUIDCardStatusModASyncRs);
		CalculatedVariables.actualValues.put("operUIDCardStatusModASyncRs", operUIDCardStatusModASyncRs);
		CalculatedVariables.actualValues.put("statusCodeCardStatusModASyncRs", statusCodeCardStatusModASyncRs);
		CalculatedVariables.actualValues.put("statusDescCardStatusModASyncRs", statusDescCardStatusModASyncRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDCardStatusModASyncRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("operUIDCardStatusModASyncRs", CalculatedVariables.operUID);
		CalculatedVariables.expectedValues.put("statusCodeCardStatusModASyncRs", "0");
		CalculatedVariables.expectedValues.put("statusDescCardStatusModASyncRs", "OK");

		
		System.out.println("test 10 ��� 14 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardReissueRq", "Windows-1251", 30);	
		
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardReissueRq.xml", "Windows-1251", "CardReissueRq");
	
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardReissueRq.xml", "Windows-1251");
		Document �ardReissueRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(�ardReissueRqDoc, "RqUID");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(�ardReissueRqDoc, "OperUID");
		System.out.println("rqUIDCardReissueRq = " + CalculatedVariables.rqUID);		
		
		//JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");

		System.out.println("test 10 ��� 15 ");
		//��������� xml ��������� ReissueCardRs
		xmlDoc = XMLLib.loadXMLTemplate("ChangeCard", "ChangeCardOnlineMomentumMomentum", "CardReissueRs");
		xmlDoc = XMLLib.redefineXMLParamsReissueCardRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardReissueRs");
		FileLib.writeToFile(Config.xmlOutPath  + "ChangeCard" + "\\" 
				+ "ChangeCardOnlineMomentumMomentum" + "\\" +  "CardReissuedRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
						
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//System.out.println("���� 2 �������...");
		//Thread.sleep(2000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardReissueRs", "Windows-1251", 30);	

		//��������� �� ���� ���������� xml ��������� CardReissueRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardReissueRs.xml", "Windows-1251", "CardReissueRs");
					
		//��������� �������� ���� statusCode �� ������ CardReissueRs
		String cardReissueRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardReissueRs.xml", "Windows-1251");
		System.out.println("CardReissueRsStr = " + cardReissueRsStr);
		Document cardReissueRs = XMLLib.convertStringToDom(cardReissueRsStr, "UTF-8");
		String rqUIDCardReissueRs = XMLLib.getElementValueFromDocument(cardReissueRs, "RqUID");
		String operUIDCardReissueRs = XMLLib.getElementValueFromDocument(cardReissueRs, "OperUID");
						
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDCardReissueRs", rqUIDCardReissueRs);
		CalculatedVariables.actualValues.put("operUIDCardReissueRs", operUIDCardReissueRs);
										
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDCardReissueRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("operUIDCardReissueRs", CalculatedVariables.operUID);
	

		CalculatedVariables.operUID = operUIDTotal;	
		
		System.out.println("���� 10 ��� 16-17 ");
		System.out.println("���� 5 ������...");
		Thread.sleep(5000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log","PlasticCardsI", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ momentum2momentum() #####", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ momentum2momentum() #####");

		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateChangeMomentumCardToMomentumCardLastStateRs.xml", "Windows-1251", "InitiateChangeMomentumCardToMomentumCardRs");

		//��������� �������� ���� statusCode �� ������ InitiateChangeMomentumCardToMomentumCardRs
		String InitiateChangeMomentumCardToMomentumCardLastStateRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateChangeMomentumCardToMomentumCardLastStateRs.xml", "Windows-1251");
		//System.out.println("InitiateReplaceMomentumCardToCardLastStateRsStr = " + InitiateReplaceMomentumCardToCardLastStateRsStr);
		Document InitiateChangeMomentumCardToMomentumCardLastStateRs = XMLLib.convertStringToDom(InitiateChangeMomentumCardToMomentumCardLastStateRsStr, "UTF-8");
		String statusCodeInitiateChangeMomentumCardToMomentumCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardLastStateRs, "StatusCode");
		String statusDescInitiateChangeMomentumCardToMomentumCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardLastStateRs, "StatusDesc");
		String severityInitiateChangeMomentumCardToMomentumCardLastStateRs = XMLLib.getElementValueFromDocument(InitiateChangeMomentumCardToMomentumCardLastStateRs, "Severity");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateChangeMomentumCardToMomentumCardLastStateRs", statusCodeInitiateChangeMomentumCardToMomentumCardLastStateRs);
		CalculatedVariables.actualValues.put("statusDescInitiateChangeMomentumCardToMomentumCardLastStateRs", statusDescInitiateChangeMomentumCardToMomentumCardLastStateRs);
		CalculatedVariables.actualValues.put("severityInitiateChangeMomentumCardToMomentumCardLastStateRs", severityInitiateChangeMomentumCardToMomentumCardLastStateRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateChangeMomentumCardToMomentumCardLastStateRs", "0");
		CalculatedVariables.expectedValues.put("statusDescInitiateChangeMomentumCardToMomentumCardLastStateRs", "������ ���");
		CalculatedVariables.expectedValues.put("severityInitiateChangeMomentumCardToMomentumCardLastStateRs", "Ok");
		
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	

public static Document redefineXMLParamsInitiateReplaceMomentumCardToCardRq(Document docXML) throws Exception {
	 HashMap<String, String> rowParams = new HashMap<String, String>(); 
	 String newUUID = "";
	 String RqUID = "";
	 String OperUID = "";
	 
	 LocalDateTime todayDateTime = LocalDateTime.now();
	 
	 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
	 RqUID = newUUID;
	 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
	 OperUID = newUUID;
	 CalculatedVariables.testUUID = RqUID;
	 CalculatedVariables.operUID = OperUID;
	 System.out.println("OperUID = " + OperUID);
	 CalculatedVariables.rqUID = RqUID;
	 System.out.println("RqUID = " + RqUID);
	 
		
	 //String lastName = "�������-" + CommonLib.getRandomLetters(15);
	 //String firstName = CommonLib.getRandomLetters(15);
	 //String middleName = CommonLib.getRandomLetters(15);
	 //System.out.println("��� 3 ");
		
	 //String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
	 //String idNum = CommonLib.getRandomNumericLetters(6);
	 //System.out.println("��� 4 ");
	 
	 //���������  � template.xml
	 //String cardCode = "111707";
	 //String productCode = "IRRDBCMF--";
	 //String eDBOContractFlag = "true";
	 //String contractProductCode = "IRRD--";
	 
	 						
	 rowParams.put("RqUID", RqUID);
	 rowParams.put("OperUID", OperUID);
	 rowParams.put("RqTm", todayDateTime.toString());
	 rowParams.put("IdSeries", CalculatedVariables.idSeries);
	 rowParams.put("IdNum", CalculatedVariables.idNum);
		
	 
			 
	//������ info 
	 rowParams.put("OperDay", Config.businessProcessProp.getProperty("OperDay"));
	 rowParams.put("OperatorLogin", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin22"));
	 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode22"));
	 rowParams.put("OperatorName", Config.businessProcessProp.getProperty("OperatorInfoOperatorName22"));
	 
			
	 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
	 
		
	 //������ ��� 
	 rowParams.clear();
	 rowParams.put("LastName", CalculatedVariables.lastName);
	 rowParams.put("FirstName", CalculatedVariables.firstName);
	 rowParams.put("MiddleName", CalculatedVariables.middleName);
	 
	
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
	 
	
	 //������ info 
 	 rowParams.clear();
	 rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
	 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
	 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
	 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");
	 

	 //������ info 
	 rowParams.clear();
	 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId22"));
	 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId22"));
	 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId22"));
	 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId22"));
	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
	 

	 //������ info �� ����� ��������

	 rowParams.clear();
	 rowParams.put("CardNum", CalculatedVariables.cardNumber);
	 rowParams.put("IssueDate", CalculatedVariables.issueDate);
	 rowParams.put("ExpDt", CalculatedVariables.extDt);

	 
		 						
	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdFrom");
//	 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardRecMomentum");

	
		 String cardNum = "";
		 String sQuerry = "";
			
			//���������� 16-�� ������� ����� �����, ������� ����������� � ��
			do{
				cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
				sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
			}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
				System.out.println("GeneratedCardNum = " + cardNum);

			//������ info 
		 	rowParams.clear();

			rowParams.put("CardNum", cardNum);
			
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardAcctIdTo");
				 						
			
	 
	return docXML;
 
}


	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	

}
