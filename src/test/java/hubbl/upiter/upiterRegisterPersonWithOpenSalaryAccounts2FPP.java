package hubbl.upiter;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineConnectUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class upiterRegisterPersonWithOpenSalaryAccounts2FPP {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("true", "isIssueFpp.jupiter", "1");
		BrowserActions.setHUBBLMode("45", "timeOnline", "1");
		//�������� ��������� ��� �������
		
		
		
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testRegisterPersonWithOpenSalaryAccounts() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  RegisterPersonWithOpenSalaryAccountsRq �� ������ . 
		Document xmlDoc = XMLLib.loadXMLTemplate("upiter", "RegisterPersonWithOpenSalaryAccounts", "RegisterPersonWithOpenSalaryAccountsRq");
			
		xmlDoc = redefineXMLParamsRegisterPersonWithOpenSalaryAccountsRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "RegisterPersonWithOpenSalaryAccountsRq");
		FileLib.writeToFile(Config.xmlOutPath  + "upiter" + "\\" 
				+ "RegisterPersonWithOpenSalaryAccounts" + "\\" +  "RegisterPersonWithOpenSalaryAccountsRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		//��������� � ������� HBL.REG.MQ.IN ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "HBL.REG.MQ.IN ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "RegisterPersonWithOpenSalaryAccountsRq", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<RegisterPersonWithOpenSalaryAccountsRq>");
		
		
				
		//������������ ������  StartProcessingRq �� ������ . 
		Document xmlDoc2 = XMLLib.loadXMLTemplate("upiter", "RegisterPersonWithOpenSalaryAccounts", "StartProcessingRq");
		xmlDoc2 = redefineXMLParamsStartProcessingRq(xmlDoc2);
		String xmlMessage2 = XMLLib.createXMLMessage(xmlDoc2, "Hubble_Emission_Provider_PIR28Ver011", true, "StartProcessingRq");
		FileLib.writeToFile(Config.xmlOutPath  + "upiter" + "\\" 
				+ "RegisterPersonWithOpenSalaryAccounts" + "\\" +  "StartProcessingRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		//��������� � ������� HBL.REG.MQ.IN ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage2, "HBL.REG.MQ.IN ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "StartProcessingRq", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<StartProcessingRq>");
		
		
		//������� 30 ������ ��������� ��������� StartProcessingRq � ������ TechnologicalReceiptRs
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "<StatusDesc>������ ������ � ���������</StatusDesc>", "Windows-1251", 35);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<StatusDesc>������ ������ � ���������</StatusDesc>");
		
		
		//������� 30 ������ ��������� ��������� StartProcessingRq � ������ TechnologicalReceiptRs
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "</TechnologicalReceiptRs>", "Windows-1251", 35);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "</TechnologicalReceiptRs>");
				
				
				
		//��������� �� ���� ���������� xml ��������� TechnologicalReceiptRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\TechnologicalReceiptRs.xml", "Windows-1251", "TechnologicalReceiptRs");
		
		
		
		//��������� �������� ���� statusCode �� ������ TechnologicalReceiptRs
		String TechnologicalReceiptRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\TechnologicalReceiptRs.xml", "Windows-1251");
		System.out.println("TechnologicalReceiptRs = " + TechnologicalReceiptRsStr);
		Document TechnologicalReceiptRs = XMLLib.convertStringToDom(TechnologicalReceiptRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(TechnologicalReceiptRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(TechnologicalReceiptRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(TechnologicalReceiptRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "1");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ ������ � ���������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "OK");
		

		
		
		// CalculatedVariables.testRqUID
		// CalculatedVariables.TechnoUID 
		
		//������ ������� (technoUID=CalculatedVariables.TechnoUID, rqUID=CalculatedVariables.testRqUID) #1 ����� �������� � ����� ��� ������ � ������ ����������������
	
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #1 ����� �������� � ����� ��� ������ � ������ ����������������", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #1 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		String strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int countPosSearch1 = strLocalLogs.indexOf("������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #1 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		System.out.println("countPosSearch1 = " + countPosSearch1 );
		
		String idrecord = strLocalLogs.substring(countPosSearch1, countPosSearch1+250);
		System.out.println("idrecord = " + idrecord );
//������ ������� (technoUID=EF270C7BE8FA4E2888BB4ABCB8DF8714, rqUID=A928D503D85D4249B80A2ECF0E78D453) #1 ����� �������� � ����� ��� ������ � ������ ���������������� (rqUid = 2f517af724584f9db366890963405ab8, operUid = b87dc73cfafb45a1b5bca1999a6e9eed):

		String idrecordRqUID = idrecord.substring(172, 204);
		String idrecordOperUID = idrecord.substring(216, 248);
		
		System.out.println("idrecordRqUID1 = " + idrecordRqUID );
		System.out.println("idrecordOperUID1 = " + idrecordOperUID );
		
		
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
		
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenRequest = " + cardOpenRequestStr);	
		Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
		CalculatedVariables.operUID1 = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
		
		System.out.println("CalculatedVariables.operUID1 = " + CalculatedVariables.operUID1);
		
		
		
		
		//������ ������� (technoUID=CalculatedVariables.TechnoUID, rqUID=CalculatedVariables.testRqUID) #1 ����� �������� � ����� ��� ������ � ������ ����������������
		
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #2 ����� �������� � ����� ��� ������ � ������ ����������������", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #2 ����� �������� � ����� ��� ������ � ������ ����������������");
				
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
		
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenRequest2 = " + cardOpenRequestStr2);	
		Document cardOpenRequest2 = XMLLib.convertStringToDom(cardOpenRequestStr2, "UTF-8");
		CalculatedVariables.operUID2 = XMLLib.getElementValueFromDocument(cardOpenRequest2, "operUID");
		
		System.out.println("CalculatedVariables.operUID2 = " + CalculatedVariables.operUID2);
		
		String strLocalLogs2 = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int countPosSearch2 = strLocalLogs2.indexOf("������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #2 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		System.out.println("countPosSearch2 = " + countPosSearch2 );
		
		String idrecord2 = strLocalLogs.substring(countPosSearch2, countPosSearch2+250);
		System.out.println("idrecord2 = " + idrecord2 );
//������ ������� (technoUID=EF270C7BE8FA4E2888BB4ABCB8DF8714, rqUID=A928D503D85D4249B80A2ECF0E78D453) #1 ����� �������� � ����� ��� ������ � ������ ���������������� (rqUid = 2f517af724584f9db366890963405ab8, operUid = b87dc73cfafb45a1b5bca1999a6e9eed):

		String idrecordRqUID2 = idrecord2.substring(172, 204);
		String idrecordOperUID2 = idrecord2.substring(216, 248);
		
		System.out.println("idrecordRqUID2 = " + idrecordRqUID2 );
		System.out.println("idrecordOperUID2 = " + idrecordOperUID2 );
		
		
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #3 ����� �������� � ����� ��� ������ � ������ ����������������", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #3 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		
		String strLocalLogs3 = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int countPosSearch3 = strLocalLogs3.indexOf("������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #3 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		System.out.println("countPosSearch3 = " + countPosSearch3 );
		
		String idrecord3 = strLocalLogs3.substring(countPosSearch3, countPosSearch3+250);
		System.out.println("idrecord2 = " + idrecord3 );
//������ ������� (technoUID=EF270C7BE8FA4E2888BB4ABCB8DF8714, rqUID=A928D503D85D4249B80A2ECF0E78D453) #1 ����� �������� � ����� ��� ������ � ������ ���������������� (rqUid = 2f517af724584f9db366890963405ab8, operUid = b87dc73cfafb45a1b5bca1999a6e9eed):

		String idrecordRqUID3 = idrecord3.substring(172, 204);
		String idrecordOperUID3 = idrecord3.substring(216, 248);
		
		System.out.println("idrecordRqUID3 = " + idrecordRqUID3 );
		System.out.println("idrecordOperUID3 = " + idrecordOperUID3 );
		
		
		
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #4 ����� �������� � ����� ��� ������ � ������ ����������������", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #4 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		
		String strLocalLogs4 = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int countPosSearch4 = strLocalLogs4.indexOf("������ ������� (technoUID=" + CalculatedVariables.TechnoUID + ", rqUID=" + CalculatedVariables.testRqUID + ") #4 ����� �������� � ����� ��� ������ � ������ ����������������");
		
		System.out.println("countPosSearch4 = " + countPosSearch4 );
		
		String idrecord4 = strLocalLogs.substring(countPosSearch4, countPosSearch4+250);
		System.out.println("idrecord4 = " + idrecord4 );
//������ ������� (technoUID=EF270C7BE8FA4E2888BB4ABCB8DF8714, rqUID=A928D503D85D4249B80A2ECF0E78D453) #1 ����� �������� � ����� ��� ������ � ������ ���������������� (rqUid = 2f517af724584f9db366890963405ab8, operUid = b87dc73cfafb45a1b5bca1999a6e9eed):

		String idrecordRqUID4 = idrecord4.substring(172, 204);
		String idrecordOperUID4 = idrecord4.substring(216, 248);
		
		System.out.println("idrecordRqUID4 = " + idrecordRqUID4 );
		System.out.println("idrecordOperUID4 = " + idrecordOperUID4 );
		

		
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	
	public static Document redefineXMLParamsRegisterPersonWithOpenSalaryAccountsRq(Document docXML, String... args) throws Exception{
 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
		 CalculatedVariables.testRqUID = RqUID;
		 
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 String TechnoUID = newUUID;
		 CalculatedVariables.TechnoUID = TechnoUID;
			
		//� ������� � �������� 296 �� ����� �����
		 String iRegisterNum = CommonLib.getRandomNumericLetters(3);
		 
		 
		 String StringdocXML = XMLLib.convertDomToString(docXML);

		 System.out.println("iRegisterNum: " + iRegisterNum);
		 
		 StringdocXML = StringdocXML.replace("296", iRegisterNum);
		 		
		 docXML = XMLLib.convertStringToDom(StringdocXML, "UTF-8");
		
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("TechnoUID", TechnoUID);
		 
		 System.out.println("rowParams = " + rowParams);	
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		 
			
		//	 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		 
		/* String lastName = "�������-" + CommonLib.getRandomLetters(15);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("OperDate", Config.OperDay);
		 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		 
		 rowParams.put("OpenAccount", "false");
		 
		 System.out.println("rowParams = " + rowParams);	
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
		//������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
*/
		return docXML;
	}
	
	public static Document redefineXMLParamsStartProcessingRq(Document docXML, String... args) throws Exception{
        HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 
		 String newUUID = "";
		 String RqUID2 = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID2 = newUUID;
		 
		 rowParams.put("RqUID", RqUID2);
		 rowParams.put("TechnoUID", CalculatedVariables.TechnoUID);
		 
		 System.out.println("rowParams = " + rowParams);	
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
	
	
	
		return docXML;
	}
	
	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
