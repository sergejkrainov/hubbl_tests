package hubbl.upiter;

import libs.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "OpenCardAccount\\OpenCardAccountOnline\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class OpenCardAccountOnline{
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO2noAccount"));


        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
	//	Init.initBrowser();
	//	BrowserActions.loginToHubblAdmin();
	//	BrowserActions.setHUBBLMode("false", "isBlockListFpp", "1");
	//	BrowserActions.setHUBBLMode("60", "timeOnline", "1");
	//	BrowserActions.logOutFromHubblAdmin();
		
	//	prepareTestData();
		
	}
	// �� ����� ��� 
	
	@Test
	public void testOpenCardAccountOnline(){ //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateOpenCardAccountRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("OpenCardAccount", "OpenCardAccountOnline", "InitiateOpenCardAccountRq");
		xmlDoc = redefineXMLParamsInitiateOpenCardAccountRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateOpenCardAccountRq");
		FileLib.writeToFile(Config.xmlOutPath  + "OpenCardAccount" + "\\" 
				+ "OpenCardAccountOnline" + "\\" +  "InitiateOpenCardAccountRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateOpenCardAccountRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateOpenCardAccountRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "<ns2:hblOpenCardAccountRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:hblOpenCardAccountRequest");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ openCardAccount() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ openCardAccount() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateOpenCardAccountRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateOpenCardAccountRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateOpenCardAccountRs.xml", "Windows-1251", "InitiateOpenCardAccountRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateOpenCardAccountRs
		String InitiateOpenCardAccountRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateOpenCardAccountRs.xml", "Windows-1251");
		System.out.println("InitiateOpenCardAccountRs = " + InitiateOpenCardAccountRsStr);
		Document InitiateOpenCardAccountRs = XMLLib.convertStringToDom(InitiateOpenCardAccountRsStr, "UTF-8");
		String InitiateOpenCardAccountRsStatusCode = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRs, "StatusCode");
		String InitiateOpenCardAccountRsStatusDesc = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRs, "StatusDesc");
		String InitiateOpenCardAccountRsSeverity = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsStatusCode", InitiateOpenCardAccountRsStatusCode);
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsStatusDesc", InitiateOpenCardAccountRsStatusDesc);
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsSeverity", InitiateOpenCardAccountRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsSeverity", "Ok");	
		
		/* /������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��� ����� HBLCardService.getCardByInfo ������: true");
		*/
		
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
				
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		

		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("OpenCardAccount", "OpenCardAccountOnline", "CardAcctDInqRs");
		xmlDoc = redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "OpenCardAccount" + "\\" 
				+ "OpenCardAccountOnline" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"���������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 64 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 64 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal64.xml", "Windows-1251", "JrnTotal", "�������� ���������� 64 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 64 � ���
		String jrnTotal64Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal64.xml", "Windows-1251");
		System.out.println("jrnTotal64 = " + jrnTotal64Str);
		Document jrnTotal64 = XMLLib.convertStringToDom(jrnTotal64Str, "UTF-8");
		String typeOperCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "TypeOperCode");
		String subSystemCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "SubSystemCode");
		String ukrBankCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "UKRBankCode");
		String mailAccountJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal64", typeOperCodeJrnTotal64);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal64", subSystemCodeJrnTotal64);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal64", ukrBankCodeJrnTotal64);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal64", "64");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal64", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal64", "IGN");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 1 seconds...");
		Thread.sleep(1000);
		
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal64 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateOpenCardAccountRq
		String InitiateOpenCardAccountRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "OpenCardAccount" + "\\" + "OpenCardAccountOnline" + "\\" +  "InitiateOpenCardAccountRq" + ".xml", "UTF-8");
		Document InitiateOpenCardAccountRq = XMLLib.convertStringToDom(InitiateOpenCardAccountRqStr, "UTF-8");
		String InitiateOpenCardAccountRqBlockCDCode = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRq, "BlockCDCode");
		String InitiateOpenCardAccountRqBlockDate = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRqBlockDate", result.get("blockdate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRqBlockCDCode", InitiateOpenCardAccountRqBlockCDCode);
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRqBlockDate", InitiateOpenCardAccountRqBlockDate);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"CardStatusModASyncRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
		String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
		Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
		String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
		String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
		String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");
		CalculatedVariables.rqUID = cardStatusModASyncRqRqUID;
		CalculatedVariables.rQTm = cardStatusModASyncRqRqTm;
		CalculatedVariables.way4operUID = CalculatedVariables.operUID;
		CalculatedVariables.operUID = cardStatusModASyncRqOperUID;
	
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("OpenCardAccount", "OpenCardAccountOnline", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "OpenCardAccount" + "\\" 
				+ "OpenCardAccountOnline" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 1 seconds...");
		Thread.sleep(1000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.way4operUID + " - �������� ���������� ������ OpenCardAccount() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.way4operUID + " - �������� ���������� ������ OpenCardAccount() #####");
		
		
		
		//��������� �� ���� ���������� xml ��������� InitiateOpenCardAccountRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateOpenCardAccountRsLast.xml", "Windows-1251", "InitiateOpenCardAccountRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateOpenCardAccountRsLast.xml", "Windows-1251");
		Document InitiateOpenCardAccountRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String InitiateOpenCardAccountRsLastStatusCode = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRsLast, "StatusCode");
		String InitiateOpenCardAccountRsLastStatusDesc = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRsLast, "StatusDesc");
		String InitiateOpenCardAccountRsLastSeverity = XMLLib.getElementValueFromDocument(InitiateOpenCardAccountRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsLastStatusCode", InitiateOpenCardAccountRsLastStatusCode);
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsLastStatusDesc", InitiateOpenCardAccountRsLastStatusDesc);
		CalculatedVariables.actualValues.put("InitiateOpenCardAccountRsLastSeverity", InitiateOpenCardAccountRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("InitiateOpenCardAccountRsLastSeverity", "Ok");
		

		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	
	public static Document redefineXMLParamsCardAcctDInqRs(Document docXML, String... args) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		
		rowParams.put("RqUID", CalculatedVariables.rqUID);
		rowParams.put("RqTm", CalculatedVariables.rQTm);
		
		/* /���� EndDt ,����� �� InitiateDeliveryCardRq + 1 ����
		LocalDate endDtDate = LocalDate.parse(CalculatedVariables.extDt, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		endDtDate = endDtDate.plusDays(1);
		String endDtDateStr = endDtDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")); 
		*/
		//rowParams.put("EndDt", endDtDateStr);
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
		return docXML;
	
	}
		public static Document redefineXMLParamsInitiateOpenCardAccountRq(Document docXML, String... args) throws Exception{
			HashMap<String, String> rowParams = new HashMap<String, String>(); 
			String RqUID = "";
			String newUUID = "";
			 String OperUID = "";
			String sQuerry = "";
			newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			RqUID = newUUID;
			newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			OperUID = newUUID;
			CalculatedVariables.operUID = OperUID;
			System.out.println("OperUID = " + OperUID);
			CalculatedVariables.rqUID = RqUID;
			System.out.println("RqUID = " + RqUID);
			
			rowParams.put("RqUID", RqUID);
			rowParams.put("OperUID", OperUID);
		//	rowParams.put("AcctId", CalculatedVariables.acctId);
			rowParams.put("OperDate", Config.OperDay);
			rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
			rowParams.put("UserFIO", "�������� ������������");
			
			/*
			 *         <CardCode>111703</CardCode>
        <ClientKind>0</ClientKind>
        <ProductCode>PRRCBMSP--</ProductCode>
			 */
			
			rowParams.put("CardCode", "111703");
			rowParams.put("ProductCode", "PRRCBMSP--");
			
			sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
			rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
			
			sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
			rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
			
			CalculatedVariables.extDt = rowParams.get("ExpDt");
			
			docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
				
			//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
				String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO2noAccount" + "\\" +
						"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
				Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
				XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
				
				XMLLib.deleteTagInDocument(docXML, "Birthplace");
				XMLLib.deleteTagInDocument(docXML, "Citizenship");
				XMLLib.deleteTagInDocument(docXML, "TaxId");
				XMLLib.deleteTagInDocument(docXML, "ClientCategory");
				XMLLib.deleteTagInDocument(docXML, "ClientStatus");
				XMLLib.deleteTagInDocument(docXML, "Verified");
				XMLLib.deleteTagInDocument(docXML, "Signed");
				XMLLib.deleteTagInDocument(docXML, "ContactInfo");
				XMLLib.deleteTagInDocument(docXML, "Resident");
				XMLLib.deleteTagInDocument(docXML, "ControlWord");
				
			 //������ info 
		 	rowParams.clear();
			rowParams.put("BranchId", Config.IssueBankInfoBranchId);
			rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
			rowParams.put("RegionId", Config.IssueBankInfoRegionId);
			rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
				 						
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo"); 
			
			 //������ info 
		 	rowParams.clear();
			rowParams.put("BranchId", Config.IssueBankInfoBranchId);
			rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
			rowParams.put("RegionId", Config.IssueBankInfoRegionId);
			rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
				 						
			 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "BankInfo"); 
			
			return docXML;
		}

	
	
	
	
	
	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(1000);
		
	}

}
