package hubbl.repeatablecardissue;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//������������� �� �������.
// @ RunWith(DataDrivenTestRunner.class)
// @ DataLoader(filePaths = {Config.XLS_PATH + "RepeatableCardIssue\\RepeatableCardIssueFPPrecreatedCard\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class RepeatableCardIssueFPPrecreatedCard {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "reissuecard" + "." + "ReissueCardEarlyFPP"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("true", "isBlockFpp", "1");
		BrowserActions.setHUBBLMode("true", "isReissueFpp", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
	}
	
	@Test
	public void testRepeatableCardIssueFPPrecreatedCard() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
	//��������� �������� � ���������  
	//������������ ������  InitiateRepeatableCardIssueRq �� �� �� �� �� 
	
		
		Document xmlDoc = XMLLib.loadXMLTemplate("RepeatableCardIssue", "RepeatableCardIssue", "InitiateRepeatableCardIssueRq");
		xmlDoc = redefineXMLParamsInitiateRepeatableCardIssueRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateRepeatableCardIssueRq");
		FileLib.writeToFile(Config.xmlOutPath  + "RepeatableCardIssue" + "\\" 
				+ "RepeatableCardIssue" + "\\" +  "InitiateRepeatableCardIssueRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateRepeatableCardIssueRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateRepeatableCardIssueRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "srvInitiateRepeatableCardIssueRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "srvInitiateRepeatableCardIssueRequest");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateRepeatableCardIssueRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateRepeatableCardIssueRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateRepeatableCardIssueRs.xml", "Windows-1251", "InitiateRepeatableCardIssueRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateRepeatableCardIssueRs
		String InitiateRepeatableCardIssueRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateRepeatableCardIssueRs.xml", "Windows-1251");
		System.out.println("InitiateRepeatableCardIssueRs = " + InitiateRepeatableCardIssueRsStr);
		Document InitiateRepeatableCardIssueRs = XMLLib.convertStringToDom(InitiateRepeatableCardIssueRsStr, "UTF-8");
		String InitiateRepeatableCardIssueRsStatusCode = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRs, "StatusCode");
		String InitiateRepeatableCardIssueRsStatusDesc = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRs, "StatusDesc");
		String InitiateRepeatableCardIssueRsSeverity = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsStatusCode", InitiateRepeatableCardIssueRsStatusCode);
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsStatusDesc", InitiateRepeatableCardIssueRsStatusDesc);
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsSeverity", InitiateRepeatableCardIssueRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��� ����� HBLCardService.getCardByInfo ������: true");
		
		/*//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("RecreateCard", "RecreateCardEarlyFPP", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "RecreateCard" + "\\" 
				+ "RecreateCardEarlyFPP" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);*/
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"���������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 64 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 64 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal64.xml", "Windows-1251", "JrnTotal", "�������� ���������� 64 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 64 � ���
		String jrnTotal64Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal64.xml", "Windows-1251");
		System.out.println("jrnTotal64 = " + jrnTotal64Str);
		Document jrnTotal64 = XMLLib.convertStringToDom(jrnTotal64Str, "UTF-8");
		String typeOperCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "TypeOperCode");
		String subSystemCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "SubSystemCode");
		String mailAccountJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal64", typeOperCodeJrnTotal64);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal64", subSystemCodeJrnTotal64);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal64", "64");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal64", "1");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(10000);
		
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal64 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateRepeatableCardIssueRq
		String initiateBlockCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "RecreateCard" + "\\" + "RecreateCardEarlyFPP" + "\\" +  "InitiateRepeatableCardIssueRq" + ".xml", "UTF-8");
		Document InitiateRepeatableCardIssueRq = XMLLib.convertStringToDom(initiateBlockCardRqStr, "UTF-8");
		String InitiateRepeatableCardIssueRqBlockCDCode = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRq, "BlockCDCode");
		String InitiateRepeatableCardIssueRqBlockDate = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRqBlockDate", result.get("blockdate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRqBlockCDCode", InitiateRepeatableCardIssueRqBlockCDCode);
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRqBlockDate", InitiateRepeatableCardIssueRqBlockDate);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 68 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log",
				Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
		String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log",
				Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 60 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
		String subSystemCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "SubSystemCode");
		String mailAccountJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal60", subSystemCodeJrnTotal60);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal60", "1");
		
		//��������� �������� �� ��
		sQuery = "select ischange, reasonchange from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal68 + "'";
		result = DataBaseLib.getAnyRowAsHashMap(sQuery, "ischange,reasonchange".split(","), "COD");
		
		//��������� �������� ���� ReasonCode �� xml InitiateRepeatableCardIssueRq
		String InitiateRepeatableCardIssueRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "RecreateCard" + "\\" + "RecreateCardEarlyFPP" + "\\" +  "InitiateRepeatableCardIssueRq" + ".xml", "UTF-8");
		InitiateRepeatableCardIssueRq = XMLLib.convertStringToDom(InitiateRepeatableCardIssueRqStr, "UTF-8");
		String InitiateRepeatableCardIssueRqReasonCode = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRq, "ReasonCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRqReasonCode", result.get("reasonchange"));
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRqIsChange", result.get("ischange"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRqReasonCode", InitiateRepeatableCardIssueRqReasonCode);
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRqIsChange", "1");
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 30 seconds...");
		Thread.sleep(30000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateRepeatableCardIssueRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateRepeatableCardIssueRsLast.xml", "Windows-1251", "InitiateRepeatableCardIssueRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateRepeatableCardIssueRsLast.xml", "Windows-1251");
		Document InitiateRepeatableCardIssueRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String InitiateRepeatableCardIssueRsLastStatusCode = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRsLast, "StatusCode");
		String InitiateRepeatableCardIssueRsLastStatusDesc = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRsLast, "StatusDesc");
		String InitiateRepeatableCardIssueRsLastSeverity = XMLLib.getElementValueFromDocument(InitiateRepeatableCardIssueRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsLastStatusCode", InitiateRepeatableCardIssueRsLastStatusCode);
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsLastStatusDesc", InitiateRepeatableCardIssueRsLastStatusDesc);
		CalculatedVariables.actualValues.put("InitiateRepeatableCardIssueRsLastSeverity", InitiateRepeatableCardIssueRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRsLastStatusDesc", "������ ���, ����� ���������� ���, ��������� ����� �����");
		CalculatedVariables.expectedValues.put("InitiateRepeatableCardIssueRsLastSeverity", "Ok");
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsInitiateRepeatableCardIssueRq(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		CalculatedVariables.testUUID = RqUID;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
		rowParams.put("OperDate", Config.OperDay);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		String createProductPackageRs = FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageLastRs.xml", "UTF-8");
		//		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageLastRs.xml", "Windows-1251");
				Document createProductPackageRsDoc = XMLLib.convertStringToDom(createProductPackageRs, "UTF-8");
				String numContrCardCreateProductPackageLastRs = XMLLib.getElementValueFromDocument(createProductPackageRsDoc, "NumContrCard");
				
				
		rowParams.put("NumContrCard", numContrCardCreateProductPackageLastRs);
		
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOfflineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "/CustomerRec");
				
     		XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
			XMLLib.deleteTagInDocument(docXML, "Resident");
			XMLLib.deleteTagInDocument(docXML, "ControlWord");
			
			
		//������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		rowParams.clear();
		rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
			
		
		return docXML;
	}
	

	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		

		
	}

}