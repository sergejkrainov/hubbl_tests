package hubbl.getcardinfo;

import libs.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "GetCardInfo\\GetCardInfo4IssuedCard\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class GetCardInfo4IssuedCard {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		/* /������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "initiateGetCardInfoFPP", "1");
		BrowserActions.logOutFromHubblAdmin();
		*/
		prepareTestData();
		
	}
	
	@Test
	public void testGetCardInfo4IssuedCard() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  initiateGetCardInfoRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("GetCardInfo", "GetCardInfo", "InitiateGetCardInfoRq");
		xmlDoc = redefineXMLParamsGetCardInfoRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR26Ver036", true, "InitiateGetCardInfoRq");
		FileLib.writeToFile(Config.xmlOutPath  + "GetCardInfo" + "\\" 
				+ "GetCardInfo" + "\\" +  "InitiateGetCardInfoRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateGetCardInfoRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateGetCardInfoRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "ns2:cardInfoDetailRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "ns2:cardInfoDetailRequest");
		
		/* /��������� �� ���� ���������� xml ��������� ns2:cardInfoDetailRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\hblCloseCardDepositRequest.xml", "Windows-1251", "ns2:cardInfoDetailRequest");
		
		//��������� �������� ���� operUID �� xml hblCloseCardDepositRequest
		String hblCloseCardDepositRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + 
				FileLib.readFromFile(Config.logsPath + "\\hblCloseCardDepositRequest.xml", "Windows-1251");
		System.out.println("hblCloseCardDepositRequest = " + hblCloseCardDepositRequestStr);
		Document hblCloseCardDepositRequest = XMLLib.convertStringToDom(hblCloseCardDepositRequestStr, "UTF-8");
		String hblCloseCardDepositRequestoperUID = XMLLib.getElementValueFromDocument(hblCloseCardDepositRequest, "operUID");
		CalculatedVariables.operUID = hblCloseCardDepositRequestoperUID;
		*/
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ cardInfoDetail() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ cardInfoDetail() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateGetCardInfoRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� initiateGetCardInfoRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateGetCardInfoRs.xml", "Windows-1251", "InitiateGetCardInfoRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml initiateGetCardInfoRs
		String initiateGetCardInfoRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\initiateGetCardInfoRs.xml", "Windows-1251");
		System.out.println("initiateGetCardInfoRs = " + initiateGetCardInfoRsStr);
		Document initiateGetCardInfoRs = XMLLib.convertStringToDom(initiateGetCardInfoRsStr, "UTF-8");
		String initiateGetCardInfoRsStatusCode = XMLLib.getElementValueFromDocument(initiateGetCardInfoRs, "StatusCode");
		String initiateGetCardInfoRsStatusDesc = XMLLib.getElementValueFromDocument(initiateGetCardInfoRs, "StatusDesc");
		String initiateGetCardInfoRsSeverity = XMLLib.getElementValueFromDocument(initiateGetCardInfoRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsStatusCode", initiateGetCardInfoRsStatusCode);
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsStatusDesc", initiateGetCardInfoRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsSeverity", initiateGetCardInfoRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateGetCardInfoRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateGetCardInfoRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
//		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
//				"��� ����� HBLCardService.findCard ������: true (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��� ����� HBLCardService.findCard ������: true (�������� �����)");
		
		
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("GetCardInfo", "GetCardInfo", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "GetCardInfo" + "\\" 
				+ "GetCardInfo" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		
		//������� 30 ������ ��������� ���������
		 CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				" - �������� ���������� ������ cardInfoDetail #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ cardInfoDetail #####");
		
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		
		
		//��������� �� ���� ���������� xml ��������� initiateGetCardInfoRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\initiateGetCardInfoRsLast.xml", "Windows-1251", "InitiateGetCardInfoRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\initiateGetCardInfoRsLast.xml", "Windows-1251");
		Document initiateGetCardInfoRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateGetCardInfoRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateGetCardInfoRsLast, "StatusCode");
		String initiateGetCardInfoRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateGetCardInfoRsLast, "StatusDesc");
		String initiateGetCardInfoRsLastSeverity = XMLLib.getElementValueFromDocument(initiateGetCardInfoRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsLastStatusCode", initiateGetCardInfoRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsLastStatusDesc", initiateGetCardInfoRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateGetCardInfoRsLastSeverity", initiateGetCardInfoRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateGetCardInfoRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateGetCardInfoRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateGetCardInfoRsLastSeverity", "Ok");
		

		
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsGetCardInfoRq(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		CalculatedVariables.testUUID = RqUID;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
		//rowParams.put("OperDate", Config.OperDay);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		rowParams.put("OperDate", Config.OperDay);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		
		rowParams.put("NumContrCard", CalculatedVariables.mailAccount);
		
		sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		CalculatedVariables.extDt = rowParams.get("ExpDt");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "/CustomerRec");
			
			XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
			XMLLib.deleteTagInDocument(docXML, "Resident");
			XMLLib.deleteTagInDocument(docXML, "ControlWord");
			
		//������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		rowParams.clear();
		rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
			 						
		return docXML;
	}
	
	
	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
	/*	String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD"); 
		//Thread.sleep(1000);
		
		*/
	}

}
