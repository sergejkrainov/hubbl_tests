package hubbl.issuenewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "IssueNewCard\\IssueNewCardOnlineWithoutAccountOpen\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class IssueNewCardOnlineWithoutAccountOpen2storno {

	@Before
	public void initTest() throws Exception{

		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isDeliveryFpp", "1");
		BrowserActions.setHUBBLMode("143", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
	}
	
	@Test
	public void testIssueNewCardOnlineWithoutAccountOpen() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateDeliveryCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("IssueNewCard", "IssueNewCardOnlineWithoutAccountOpen", "InitiateDeliveryCardRq");
		xmlDoc = redefineXMLParamsInitiateDeliveryCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateDeliveryCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "IssueNewCard" + "\\" 
				+ "IssueNewCardOnlineWithoutAccountOpen" + "\\" +  "InitiateDeliveryCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateDeliveryCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "hblDeliveryCardRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:hblDeliveryCardRequest");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDeliveryCardRs.xml", "Windows-1251", "InitiateDeliveryCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateDeliveryCardRs
		String initiateDeliveryCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDeliveryCardRs.xml", "Windows-1251");
		System.out.println("initiateDeliveryCardRs = " + initiateDeliveryCardRsStr);
		Document initiateDeliveryCardRs = XMLLib.convertStringToDom(initiateDeliveryCardRsStr, "UTF-8");
		String initiateDeliveryCardRsStatusCode = XMLLib.getElementValueFromDocument(initiateDeliveryCardRs, "StatusCode");
		String initiateDeliveryCardRsStatusDesc = XMLLib.getElementValueFromDocument(initiateDeliveryCardRs, "StatusDesc");
		String initiateDeliveryCardRsSeverity = XMLLib.getElementValueFromDocument(initiateDeliveryCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsStatusCode", initiateDeliveryCardRsStatusCode);
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsStatusDesc", initiateDeliveryCardRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsSeverity", initiateDeliveryCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,  "��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� ����������. ������ ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("IssueNewCard", "IssueNewCardOnlineWithoutAccountOpen", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "IssueNewCard" + "\\" 
				+ "IssueNewCardOnlineWithoutAccountOpen" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"������ ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ������ ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 61 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 61 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal61.xml", "Windows-1251", "JrnTotal", "�������� ���������� 61 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 61 � ���
		String jrnTotal61Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal61.xml", "Windows-1251");
		System.out.println("jrnTotal61 = " + jrnTotal61Str);
		Document jrnTotal61 = XMLLib.convertStringToDom(jrnTotal61Str, "UTF-8");
		String typeOperCodeJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "TypeOperCode");
		String subSystemCodeJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "SubSystemCode");
		String ukrBankCodeJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "UKRBankCode");
		String mailAccountJrnTotal61 = XMLLib.getElementValueFromDocument(jrnTotal61, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal61", typeOperCodeJrnTotal61);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal61", subSystemCodeJrnTotal61);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal61", ukrBankCodeJrnTotal61);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal61", "61");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal61", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal61", "IGN");
		
		String sQuery = "select numcard, to_char(trunc(enddate), 'yyyy-mm-dd') as enddate from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal61 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "numcard,enddate".split(","), "COD");
		
		//��������� �������� ���� CardNum,ExpDt �� xml InitiateDeliveryCardRq
		String initiateDeliveryCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "IssueNewCard" + "\\" + "IssueNewCardOnlineWithoutAccountOpen" + "\\" +  "InitiateDeliveryCardRq" + ".xml", "UTF-8");
		Document initiateDeliveryCardRq = XMLLib.convertStringToDom(initiateDeliveryCardRqStr, "UTF-8");
	//	String initiateDeliveryCardRqCardNum = XMLLib.getElementValueFromDocument(initiateDeliveryCardRq, "CardNum");
		String initiateDeliveryCardRqExpDt = XMLLib.getElementValueFromDocument(initiateDeliveryCardRq, "ExpDt");
		
		
		//���������� ����������� �������� ����������
	//	CalculatedVariables.actualValues.put("initiateDeliveryCardRqCardNum", result.get("numcard"));
		CalculatedVariables.actualValues.put("initiateDeliveryCardRqExpDt", result.get("enddate"));

	
	//	String numcard1= null;
				
		//��������� ����������� �������� ����������
	//	CalculatedVariables.expectedValues.put("initiateDeliveryCardRqCardNum", numcard1);
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRqExpDt", initiateDeliveryCardRqExpDt);
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������������� �������� � ��� c sid", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������������� �������� � ��� c sid");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������� �������������� �������� � ��� c sid", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������� �������������� �������� � ��� c sid");
		
		
	/*
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"CardStatusModASyncRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
		String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
		Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
		String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
		String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
		String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");

		CalculatedVariables.way4rqUID = cardStatusModASyncRqRqUID;
		CalculatedVariables.way4rQTm = cardStatusModASyncRqRqTm;
		CalculatedVariables.way4operUID = cardStatusModASyncRqOperUID;
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("IssueNewCard", "IssueNewCardOnlineWithoutAccountOpen", "CardStatusModASyncRs");
		xmlDoc = redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "IssueNewCard" + "\\" 
				+ "IssueNewCardOnlineWithoutAccountOpen" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		*/
		
		//������� 15 ������ ��������� ���������
	//	System.out.println("Wait for 1 seconds...");
	//	Thread.sleep(1000);
		
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ deliveryCard() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ deliveryCard() #####");
		

		/*
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDeliveryCardRsLast.xml", "Windows-1251", "InitiateDeliveryCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDeliveryCardRsLast.xml", "Windows-1251");
		Document initiateDeliveryCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateDeliveryCardRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateDeliveryCardRsLast, "StatusCode");
		String initiateDeliveryCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateDeliveryCardRsLast, "StatusDesc");
		String initiateDeliveryCardRsLastSeverity = XMLLib.getElementValueFromDocument(initiateDeliveryCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsLastStatusCode", initiateDeliveryCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsLastStatusDesc", initiateDeliveryCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateDeliveryCardRsLastSeverity", initiateDeliveryCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateDeliveryCardRsLastSeverity", "Ok");
	*/
		
		
		String sQuery2 = "SELECT NUMCARD FROM  deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		String result2 = DataBaseLib.getDataAsString(sQuery2, "COD");
		
		
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateStornoCard_NUMCARD2", result2);
		
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateStornoCard_NUMCARD2", "null");
	//	CalculatedVariables.expectedValues.put("InitiateSetCardActivityLimit_MonthLimit", "");
	//	CalculatedVariables.expectedValues.put("InitiateSetCardActivityLimit_PayLimit",  "");
		
		
		
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
}catch(Exception e){
	e.printStackTrace();
}
		
	}

	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */
	public static Document redefineXMLParamsCardStatusModASyncRs(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		
		rowParams.put("RqUID", CalculatedVariables.way4rqUID);
		rowParams.put("RqTm", CalculatedVariables.way4rQTm);
		rowParams.put("OperUID", CalculatedVariables.way4operUID);
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
		return docXML;
	}

	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */
	public static Document redefineXMLParamsInitiateDeliveryCardRq(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		 String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		CalculatedVariables.testUUID = RqUID;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
		rowParams.put("NumContrCard", CalculatedVariables.mailAccount);
		rowParams.put("OperDate", Config.OperDay);
	//	rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);

		rowParams.put("Channel", "ZP");
		rowParams.put("UserName", "oper136666");
		rowParams.put("SPName", "BR_ZP");
		rowParams.put("OperatorCode", "16666");
		
		sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		CalculatedVariables.extDt = rowParams.get("ExpDt");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
			
			XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
		
			
		//������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		rowParams.clear();
		rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
		 
		return docXML;
	}
	
	

	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}


}
