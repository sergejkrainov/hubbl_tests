package hubbl.createnewcard;
/*
import lib.ManagerMQ;
import src.main.java.libs.BrowserActions;
import src.main.java.libs.CommonLib;
import hubbl.Init;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.sbt.qa.bdd.Props;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardSmoke\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardSmoke2 {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
	}
	
	@Test
	public void testCreateNewCardSmoke() { //@Param(name = "name") String name, @Param(name = "age") int age){
		// Init.getStash().put("fpRus", "�����");
	     //   Init.getStash().put("fpEng", "cards");

	        try {
	            System.out.println("��������� MQ ������� � ���������� ���� ���������.");
	            file_to_MQ();
	            Thread.sleep(60000);
	            System.out.println("��������� ���� ����: " + Props.get("cards.log.path"));
	            FileInputStream input;
	            String result1 = null, result2 = null;
	            // ��������� ���� ���� � ���� ��� ��������� ����� ��� ��������
	            input = new FileInputStream(new File(Props.get("cards.log.path")));
	            CharsetDecoder decoder = Charset.forName("windows-1251").newDecoder();
	            decoder.onMalformedInput(CodingErrorAction.IGNORE);
	            InputStreamReader reader = new InputStreamReader(input, decoder);
	            BufferedReader bufferedReader = new BufferedReader(reader);
	            StringBuilder sb1 = new StringBuilder(), sb2 = new StringBuilder();
	            String line = bufferedReader.readLine();
	            while (line != null) {
	                if (line.contains("MANUL_IN_MESSAGE: <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><")) {
	                    line = bufferedReader.readLine();
	                }
	                if (line.contains("MANUL_IN_MESSAGE: <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>")) {
	                    sb1.append(line + "\n");
	                }
	                if (line.contains("���������� � ���: ")) {
	                    sb2.append(line + "\n");
	                }
	                line = bufferedReader.readLine();
	            }
	            bufferedReader.close();
	            result1 = sb1.toString();
	            result2 = sb2.toString();
	            // ��������� ������� ���� ����� ���������
	            Assert.assertTrue("� ���� ��� ��������� 'MANUL_IN_MESSAGE' ��� '���������� � ���'", result1.length() != 0 || result2.length() != 0);
	            //System.out.println("pos1: "+ pos1+" Data in 1: "+result1.substring(pos1, pos1 + 14));
	            //System.out.println("pos2: "+ pos2+" Data in 2: "+result2.substring(pos2, pos2 + 14));
	            Date act_date = new Date();
	            int pos1, pos2;
	            SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yy HH:mm");
	            // �������� �� ����� ���� + �����
	            System.out.println("Hours now : " + act_date.getHours());
	            if (act_date.getHours() >= 10) {
	                pos1 = (result1.length() - 128);
	                pos2 = (result2.length() - 73);
	            } else {
	                pos1 = (result1.length() - 127);
	                pos2 = (result2.length() - 72);
	            }
	            Date log_date1 = format1.parse(result1.substring(pos1, pos1 + 14));
	            Date log_date2 = format1.parse(result2.substring(pos2, pos2 + 14));
	            int d1 = log_date1.getHours() + log_date1.getMinutes();
	            int d2 = log_date2.getHours() + log_date2.getMinutes();
	            int ad = act_date.getHours() + act_date.getMinutes();
	            System.out.println("Last MANUL_IN_MESSAGE in Logs: " + log_date1 + " Duration: " + d1);
	            System.out.println("Last ���������� � ��� in Logs: " + log_date2 + " Duration: " + d2);
	            System.out.println("Date Now: " + format1.format(act_date) + " Duration: " + ad);
	            Assert.assertTrue("� ���� ��� ������� 'MANUL_IN_MESSAGE' ��� ������� ������� � �������!", d1 + 2 >= ad);
	            System.out.println("��������� 'MANUL_IN_MESSAGE' ��� ������� ������� ������� � ����.");
	            Assert.assertTrue(" � ���� ��� ������� '���������� � ���:' ��� ������� ������� � �������!", d2 + 2 >= ad);
	            System.out.println("��������� '���������� � ���:' ��� ������� ������� ������� � ����.");

	        }   	catch(Exception e){
	        		e.printStackTrace();
	        		try {
	        			System.setProperty("isError", "true");
	        			System.setProperty("isFail", "true");
	        			CommonLib.postProcessingTest();
	        		} catch (Exception e1) {
	        			// TODO Auto-generated catch block
	        			e1.printStackTrace();
	        		}
	        	}
	    }

	    public void file_to_MQ() throws IOException {
	        String encoding = System.getProperty("console.encoding", "UTF-8");
	        ManagerMQ managerMQ = new ManagerMQ();
	        try {
	            String uid = ManagerMQ.generateID();
	            String line = "";
	            Scanner file1 = new Scanner(new File("src/test/resources/xml/message.xml"), encoding);
	            while (file1.hasNext())
	                line += file1.nextLine() + "\r\n";
	            file1.close();
	            line = line.replace("e4d9f62134004d54bb38045fdcecf7d9", uid);
	            managerMQ.sendXMLMessageToMQ(line);

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	
	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}

}
*/