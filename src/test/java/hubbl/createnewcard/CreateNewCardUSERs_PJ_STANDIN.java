package hubbl.createnewcard;

import libs.CommonLib;
import libs.MQLib;
import libs.XMLLib;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;


public class CreateNewCardUSERs_PJ_STANDIN {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL
//		Init.initBrowser();
//		BrowserActions.loginToHubblAdmin();
//		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
//		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
//		BrowserActions.logOutFromHubblAdmin();
	}
	
	@Test
	public void testCreateNewCardSmoke() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
		
		//������������ ������  CreateIndividualRq �� ��� . 
		
	
		for (int  i=0; i<1000; i++)  {
	
		//��������� xml ��������� CreateProductPackageRq
		Document xmlDoc = XMLLib.loadXMLTemplate("PPRB", "PJ", "CreateIndividualRq");
		xmlDoc = redefineXMLParamsCreateProductPackageRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "individual_v2.0", false, "CreateIndividual");
	//	FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
	//			+ "CreateNewCardSmoke" + "\\" +  "CreateProductPackageRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
				//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.PPRB.SRVCREATEUPDPRCLIENT.REQUEST", "10.116.146.135", "1417" , "IM.CI.WAS", "SRV.CHANNEL");
	
		Config.ODay=i; 
		
		System.out.println("sended:" + i);
		//������� 1 ������ ��������� ���������
		System.out.println("Wait for 1.2 seconds...");
		Thread.sleep(1000);
		
		}
	
		//������� ���������� � ��������� �������� ������
	//	JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
		
}catch(Exception e){
	e.printStackTrace();
	try {
		System.setProperty("isError", "true");
		System.setProperty("isFail", "true");
		CommonLib.postProcessingTest();
		
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
}
		
	}
	
	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */
	public static Document redefineXMLParamsCreateProductPackageRq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
			
		 String lastName = "���-"+ Config.ODay + "--" + CommonLib.getRandomLetters(5);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
		
		 String externalSystemClientId10 = CommonLib.getRandomNumericLetters(10);
		 
		 
		 rowParams.put("env:MessageID", RqUID);
		 rowParams.put("env:ConversationID", OperUID);
		 
		 //<env:CreateTime>2017-10-26T11:38:00.962</env:CreateTime> 
		 rowParams.put("env:CreateTime", todayDateTime.toString());
				
		 rowParams.put("documentSeries", idSeries);
		 rowParams.put("documentNumber", idNum);
		// rowParams.put("OperDate", Config.OperDay);
		// rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		// rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		 
		 rowParams.put("nameInDocument", lastName);
		 rowParams.put("patronymicInDocument", lastName);
		 rowParams.put("surnameInDocument", lastName);
		 
		 rowParams.put("externalSystemClientId", externalSystemClientId10);
		 
		 String fullName = lastName + " " + lastName + " " + lastName;
		 
		 
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
	
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("surname", lastName);
		 rowParams.put("name", lastName);
		 rowParams.put("patronymic", lastName);
		 rowParams.put("nonStandartizedName", lastName);
		 rowParams.put("nonStandartizedPatronymic", lastName);
		 rowParams.put("nonStandartizedSurname", lastName);
		 rowParams.put("fullName", fullName);
		 rowParams.put("nameTransliteration", lastName);
		 rowParams.put("patronymicTransliteration", firstName);
		 rowParams.put("surnameTransliteration", middleName);
		
			
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "names");
		
		return docXML;
	}
	

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}

}
