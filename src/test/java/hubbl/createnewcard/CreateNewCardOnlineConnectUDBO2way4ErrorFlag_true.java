package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineConnectUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineConnectUDBO2way4ErrorFlag_true {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("190", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardConnectUDBO() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CreateProductPackageConnectUDBORq");
		xmlDoc = redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageConnectUDBORq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "CreateProductPackageConnectUDBORq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 35);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
		
		//��������� �������� ���� statusCode �� ������ createProductPackageRs
		String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "99");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Ok");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "cardOpenRequest", "Windows-1251", 35);	
				
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:cardOpenRequest");
		
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
		
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenRequest = " + cardOpenRequestStr);	
		Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
		
		System.out.println("CalculatedVariables.operUID = " + CalculatedVariables.operUID);
		
		/*//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "����� ������� � ���� (�������� �����)", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � ���� (�������� �����)");
		*/
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRq", "Windows-1251", 35);	
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)", "Windows-1251", 35);
		
		
//		String strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
//		int countPosSearchWay4 = strLocalLogs.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");
//		String idrecordWay4Queue = strLocalLogs.substring(countPosSearchWay4 - 33, countPosSearchWay4 - 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");

		
		//��������� �� ���� ���������� xml ��������� GetCardHolderRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251", "GetCardHolderRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251");
		//�������� xml ��������� �� xsd �����
        String isXMLValid;	
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "GetCardHolderRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "GetCardHolderRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "GetCardHolderRq", "true");
		Document getCardHolderRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUIDgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqUID");
		CalculatedVariables.operUIDgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "OperUID");
		CalculatedVariables.rQTmgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
		
		System.out.println("���� 1 ������...");
		Thread.sleep(1000);
//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log",
//				"Windows-1251",  idrecordWay4Queue + " GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log",
				"Windows-1251",   "GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");

		
		//��������� xml ��������� GetCardHolderRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "GetCardHolderRs");
		xmlDoc = redefineXMLParamsGetCardHolderRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "GetCardHolderRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "GetCardHolderRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
	
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRq", "Windows-1251", 45);	
		/*
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRq.xml", "Windows-1251", "CustAddRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRq.xml", "Windows-1251");
		*//* //�������� xml ��������� �� xsd �����
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "CustAddRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "CustAddRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "CustAddRq", "true");*/ /*
		Document custAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
		
//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");
		
		//��������� xml ��������� CustAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CustAddRs");
		xmlDoc = redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CustAddRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "CustAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "IssueCardRq", "Windows-1251", 35);	
		
		//��������� �� ���� ���������� xml ��������� IssueCardRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRq.xml", "Windows-1251", "IssueCardRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRq.xml", "Windows-1251");
		// /�������� xml ��������� �� xsd �����
		// isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "IssueCardRq");				 			 	
		// CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "IssueCardRq", isXMLValid);					
		// ��������� ����������� �������� ����������
		// CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "IssueCardRq", "true");
		Document issueCardRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(issueCardRqDoc, "RqUID");
		CalculatedVariables.mainApplRegNumber = XMLLib.getElementValueFromDocument(issueCardRqDoc, "MainApplRegNumber");
		CalculatedVariables.acctId = XMLLib.getElementValueFromDocument(issueCardRqDoc, "AcctId");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ��������� �� ������������ ����� � WAY4 (�������� �����)", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ��������� �� ������������ ����� � WAY4 (�������� �����)");
		
		//��������� xml ��������� IssueCardRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "IssueCardRs");
		xmlDoc = XMLLib.redefineXMLParamsIssueCardRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "IssueCardRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "IssueCardRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 30 seconds... for end time online");
		Thread.sleep(30000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� IssueCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRs.xml", "Windows-1251", "IssueCardRs");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRs.xml", "Windows-1251");
		Document issueCardRsDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		
		String mainApplRegNumberIssueCardRs = XMLLib.getElementValueFromDocument(issueCardRsDoc, "MainApplRegNumber");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("mainApplRegNumberIssueCardRs", mainApplRegNumberIssueCardRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("mainApplRegNumberIssueCardRs", CalculatedVariables.mainApplRegNumber);
		
//		strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
//		String regex = "���� isNeedToRestart �� ��������� � 1 ��� ������ �� WAY4 \\(�������� ��� ����������� ������ �� ���������\\) "
//				+ "ISSUE_CARD/ISSUE_CARD\\(\\[rqUID=[\\w\\d]{32}, " + "mainApplRegNumber=" + CalculatedVariables.mainApplRegNumber;

		//���������� ����������� �������� ����������
//		CalculatedVariables.actualValues.put("mainApplRegNumberWay4Queue", String.valueOf(CommonLib.isStrContainsSubStr(strLocalLogs, regex)));
						
		//��������� ����������� �������� ����������
//		CalculatedVariables.expectedValues.put("mainApplRegNumberWay4Queue", "true");
	
	*/
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ hblIssueCard() #####", "Windows-1251", 35);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ hblIssueCard() #####");
		
		//������� 30 ������ �������� ��������� � ���������� ������� 
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"MqSender      I   ��������� ����� ���������� � ������� jms/ESB.HUBBLE.EMISSION.RESPONSE � �������������� ������� jms/ESB.HUBBLE.EMISSION.RESPONSE.QCF", "Windows-1251", 35);
		
	 	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);	
				
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251", "CreateProductPackageRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251");
		Document createProductPackageState11Rs = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusCode");
		String statusDescCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusDesc");
		String severityCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageState11Rs", statusCodeCreateProductPackageState11Rs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageState11Rs", statusDescCreateProductPackageState11Rs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageState11Rs", severityCreateProductPackageState11Rs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageState11Rs", "17");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageState11Rs", "���������� � ���������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageState11Rs", "InProcess");
		
		/*
		
		//��������� xml ��������� NotifyIssueCardResultNfRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "NotifyIssueCardResultNfRs");
		xmlDoc = XMLLib.redefineXMLParamsNotifyIssueCardResultNfRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "NotifyIssueCardResultNfRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "NotifyIssueCardResultNf", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� NotifyIssueCardResultNf
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251", "NotifyIssueCardResultNf");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251");
		Document notifyIssueCardResultNfDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusCode");
		String severityNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "Severity");
		String statusDescNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusDesc");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeNotifyIssueCardResultNf", statusCodeNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("severityNotifyIssueCardResultNf", severityNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("statusDescNotifyIssueCardResultNf", statusDescNotifyIssueCardResultNf);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeNotifyIssueCardResultNf", "0");
		CalculatedVariables.expectedValues.put("severityNotifyIssueCardResultNf", "Posted");
		CalculatedVariables.expectedValues.put("statusDescNotifyIssueCardResultNf", "Successfully processed");
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 35);
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
		String jrnTotal0LastStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251");
		System.out.println("jrnTotal0Last = " + jrnTotal0LastStr);
		Document jrnTotal0Last = XMLLib.convertStringToDom(jrnTotal0LastStr, "UTF-8");
		String typeOperCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "TypeOperCode");
//		String subSystemCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0Last", typeOperCodeJrnTotal0Last);
//		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0Last", subSystemCodeJrnTotal0Last);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0Last", "0");
	//	CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0Last", "6");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ hblIssueCard() #####", "Windows-1251", 35);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ hblIssueCard() #####");
		*/
		
		xmlMessage = "���������� � ������ ���������!";
		//��������� � ������� Q.HBL.TIMER.REPLICATION ��� ������� ���������� � ������
		MQLib.sendMessageToMQ(xmlMessage, "Q.HBL.TIMER.REPLICATION", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
	
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsCustAddRs(Document docXML, String... args) throws Exception{
		
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String RqTm = "";
		 
		RqUID = CalculatedVariables.rqUID;
		RqTm = CalculatedVariables.rQTm;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("RqTm", RqTm);
		
		
		if (args.length > 0) {rowParams.put("StatusCode", args[0]);}
						
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
		return docXML;
	}

	public static Document redefineXMLParamsGetCardHolderRs(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String RqUID = "";
		 String OperUID = "";
		 String RqTm = "";
		 
		 RqUID = CalculatedVariables.rqUIDgetCardHolderRq;
		 OperUID = CalculatedVariables.operUIDgetCardHolderRq;
		 RqTm = CalculatedVariables.rQTmgetCardHolderRq;
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", RqTm);
		 
		 rowParams.put("StatusCode", "1770");
		 
		
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);

		return docXML;
	}

	public static Document redefineXMLParamsCreateProductPackageConnectUDBORq(Document docXML, String... args) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
		 CalculatedVariables.testRqUID = RqUID;
			
		 String lastName = "�������-" + CommonLib.getRandomLetters(10);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("OperDate", Config.OperDay);
		 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		// rowParams.put("Channel", "UFS");
		 
		 System.out.println("rowParams = " + rowParams);	
		 
		 
		 
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		 System.out.println("rowParams  PersonName = " + rowParams);	
		 
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
		//������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
		
		 System.out.println("rowParams IssueBankInfo = " + rowParams);	
		 
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 System.out.println("rowParams  DeliveryBankInfo = " + rowParams);	
		 
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");
		 
			//������ info 
		 rowParams.clear();
		 rowParams.put("ProcessClientWayErr", "true");
		
		 System.out.println("rowParams ServiceParams = " + rowParams);	
		 
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "ServiceParams");

		return docXML;
	}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
