package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineConnectUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineConnectUDBO2longEMBOSTEXT {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("70", "timeOnline", "1");
		BrowserActions.setHUBBLMode("21", "embosed.maxlength", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardConnectUDBO() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CreateProductPackageConnectUDBORq");
		xmlDoc = redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageConnectUDBORq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "CreateProductPackageConnectUDBORq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 35);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
		
		//��������� �������� ���� statusCode �� ������ createProductPackageRs
		String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "1");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "�������������� ����� � ������ ������������ ��������� 21 �������� (����� � CardRecNum = 0)");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Error");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "cardOpenRequest", "Windows-1251", 35);	
				
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:cardOpenRequest");
		
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
		
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenRequest = " + cardOpenRequestStr);	
		Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
		
		System.out.println("CalculatedVariables.operUID = " + CalculatedVariables.operUID);
		
		/*//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "����� ������� � ���� (�������� �����)", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � ���� (�������� �����)");
		*/
		
		
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251", "CreateProductPackageRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251");
		Document createProductPackageState11Rs = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusCode");
		String statusDescCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusDesc");
		String severityCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageState11Rs", statusCodeCreateProductPackageState11Rs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageState11Rs", statusDescCreateProductPackageState11Rs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageState11Rs", severityCreateProductPackageState11Rs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageState11Rs", "1");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageState11Rs", "�������������� ����� � ������ ������������ ��������� 21 �������� (����� � CardRecNum = 0)");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageState11Rs", "Error");
		
		
		
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
		//		"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ hblIssueCard() #####", "Windows-1251", 35);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ hblIssueCard() #####");
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
		public static Document redefineXMLParamsCreateProductPackageConnectUDBORq(Document docXML, String... args) throws Exception{
			 HashMap<String, String> rowParams = new HashMap<String, String>(); 
			 
			 String newUUID = "";
			 String RqUID = "";
			 String OperUID = "";
			 
			 LocalDateTime todayDateTime = LocalDateTime.now();
		
			 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			 RqUID = newUUID;
			 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
			 OperUID = newUUID;
			 CalculatedVariables.testUUID = RqUID;
			 CalculatedVariables.testRqUID = RqUID;
				
			 String lastName = "�������-" + CommonLib.getRandomLetters(15);
			 String firstName = CommonLib.getRandomLetters(15);
			 String middleName = CommonLib.getRandomLetters(15);
				
			 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
			 String idNum = CommonLib.getRandomNumericLetters(6);
				
			 rowParams.put("RqUID", RqUID);
			 rowParams.put("OperUID", OperUID);
			 rowParams.put("RqTm", todayDateTime.toString());
			 rowParams.put("IdSeries", idSeries);
			 rowParams.put("IdNum", idNum);
			 rowParams.put("OperDate", Config.OperDay);
			 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
			 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
			// rowParams.put("IssuedBy", "������� �������� �� �������������� �������� �� ��������������� �������� � ������� �������� �� �������������� �������� �� ��������������� �������� � ");
			 System.out.println("rowParams = " + rowParams);	
			 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
				
			 //������ ��� 
			 rowParams.clear();
			 rowParams.put("LastName", lastName);
			 rowParams.put("FirstName", firstName);
			 rowParams.put("MiddleName", middleName);
			 	 
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
			
			 //������ Emboss in CardRecs long then 21 
			 rowParams.clear();
			 rowParams.put("FirstName", "FirstNameLONGDATAaaa");
			 rowParams.put("LastName", "LastNameeLONGDATAaaa");
			 rowParams.put("PersonTitle", "Mr");
			 	 
			docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "CardRecs");
			
			//������ info 
		 	 rowParams.clear();
			 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
			 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
			 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
			 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
				 						
			 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

			//������ info 
			 rowParams.clear();
			 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
			 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
			 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
			 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
				 						
			 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

			return docXML;
		}
		

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
