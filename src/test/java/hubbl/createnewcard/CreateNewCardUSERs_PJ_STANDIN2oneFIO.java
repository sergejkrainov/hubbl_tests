package hubbl.createnewcard;

import libs.CommonLib;
import libs.MQLib;
import libs.XMLLib;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;

import java.time.LocalDateTime;
import java.util.HashMap;


public class CreateNewCardUSERs_PJ_STANDIN2oneFIO {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL
//		Init.initBrowser();
//		BrowserActions.loginToHubblAdmin();
//		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
//		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
//		BrowserActions.logOutFromHubblAdmin();
	}
	
	@Test
	public void testCreateNewCardSmoke() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
		
		//������������ ������  CreateIndividualRq �� ��� . 
		
	
		for (int  i=0; i<1; i++)  {
	
		//��������� xml ��������� CreateProductPackageRq
		Document xmlDoc = XMLLib.loadXMLTemplate("PPRB", "PJ", "CreateIndividualRq");
		xmlDoc = redefineXMLParamsCreateProductPackageRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "individual_v2.0", false, "CreateIndividual");
	//	FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
	//			+ "CreateNewCardSmoke" + "\\" +  "CreateProductPackageRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
				//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.PPRB.SRVCREATEUPDPRCLIENT.REQUEST", "10.116.146.135", "1417" , "IM.CI.WAS", "SRV.CHANNEL");
	
		
		System.out.println("sended:" + i);
		//������� 1 ������ ��������� ���������
		System.out.println("Wait for 1/2 seconds...");
		Thread.sleep(500);
		
		}
	
		//������� ���������� � ��������� �������� ������
	//	JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
		
}catch(Exception e){
	e.printStackTrace();
	try {
		System.setProperty("isError", "true");
		System.setProperty("isFail", "true");
		CommonLib.postProcessingTest();
		
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
}
		
	}
	
	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */
	public static Document redefineXMLParamsCreateProductPackageRq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
			
		 String lastName = "�������-" + CommonLib.getRandomLetters(15);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
		 String externalSystemClientId10 = CommonLib.getRandomNumericLetters(10);
		
		 rowParams.put("env:MessageID", RqUID);
		 rowParams.put("env:ConversationID", OperUID);
		// rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("documentSeries", "11 11");
		 rowParams.put("documentNumber", "123459");
		// rowParams.put("OperDate", Config.OperDay);
		// rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		// rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		 rowParams.put("externalSystemClientId", externalSystemClientId10);
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
	
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("surname", "���56");
		 rowParams.put("name", "���56");
		 rowParams.put("patronymic", "���56");
		 rowParams.put("nonStandartizedName", "���56");
		 rowParams.put("nonStandartizedPatronymic", "���56");
		 rowParams.put("nonStandartizedSurname", "���56");
		 rowParams.put("fullName", "���56 ���56 ���56");
		 rowParams.put("nameTransliteration", "���56");
		 rowParams.put("patronymicTransliteration", "���56");
		 rowParams.put("surnameTransliteration", "���56");
		
			
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "names");
		
		return docXML;
	}
	

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}

}
