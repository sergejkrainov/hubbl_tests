package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardSmoke\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardSmoke {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
	}
	
	@Test
	public void testCreateNewCardSmoke() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
		
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		
		//��������� xml ��������� CreateProductPackageRq
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardSmoke", "CreateProductPackageRq");
		xmlDoc = XMLLib.redefineXMLParamsCreateProductPackageRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardSmoke" + "\\" +  "CreateProductPackageRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
		
		//��������� �������� ���� statusCode �� ������ createProductPackageRs
		String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "99");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "cardOpenRequest", "Windows-1251", 30);	
				
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:cardOpenRequest");
		
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
	
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenReques = " + cardOpenRequestStr);	
		Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
		
		System.out.println("CalculatedVariables.operUID = " + CalculatedVariables.operUID);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "���������� ������� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "���������� ������� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
		String jrnTotal0Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0.xml", "Windows-1251");
		System.out.println("jrnTotal0 = " + jrnTotal0Str);
		Document jrnTotal0 = XMLLib.convertStringToDom(jrnTotal0Str, "UTF-8");
		String typeOperCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "TypeOperCode");
		String subSystemCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0", typeOperCodeJrnTotal0);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0", subSystemCodeJrnTotal0);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0", "0");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0", "6");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "�������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 25 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
		String jrnTotal25Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25.xml", "Windows-1251");
		System.out.println("jrnTotal25 = " + jrnTotal25Str);
		Document jrnTotal25 = XMLLib.convertStringToDom(jrnTotal25Str, "UTF-8");
		String typeOperCodeJrnTotal25 = XMLLib.getElementValueFromDocument(jrnTotal25, "TypeOperCode");
		String subSystemCodeJrnTotal25 = XMLLib.getElementValueFromDocument(jrnTotal25, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25", typeOperCodeJrnTotal25);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25", subSystemCodeJrnTotal25);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25", "25");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25", "1");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ �� ������ ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "������ �� ������ ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 68 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
		String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
		System.out.println("jrnTotal68 = " + jrnTotal68Str);
		Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
		String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
		String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 60 � ���
		String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
		System.out.println("jrnTotal60 = " + jrnTotal60Str);
		Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
		String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
		String subSystemCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal60", subSystemCodeJrnTotal60);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal60", "1");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRq", "Windows-1251", 30);	
		
		//��������� �� ���� ���������� xml ��������� GetCardHolderRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251", "GetCardHolderRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251");
		//�������� xml ��������� �� xsd �����
        String isXMLValid;	
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "GetCardHolderRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "GetCardHolderRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "GetCardHolderRq", "true");
		Document getCardHolderRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqUID");
		CalculatedVariables.operUIDgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "OperUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
			
		//��������� xml ��������� GetCardHolderRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardSmoke", "GetCardHolderRs");
		xmlDoc = XMLLib.redefineXMLParamsGetCardHolderRs(xmlDoc, "2200");
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "GetCardHolderRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardSmoke" + "\\" +  "GetCardHolderRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRq", "Windows-1251", 30);	
		
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRq.xml", "Windows-1251", "CustAddRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRq.xml", "Windows-1251");

		Document custAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
		
		//��������� xml ��������� CustAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardSmoke", "CustAddRs");
		xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CustAddRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardSmoke" + "\\" +  "CustAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "IssueCardRq", "Windows-1251", 30);	
		
		//��������� �� ���� ���������� xml ��������� IssueCardRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRq.xml", "Windows-1251", "IssueCardRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRq.xml", "Windows-1251");

		Document issueCardRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(issueCardRqDoc, "RqUID");
		CalculatedVariables.mainApplRegNumber = XMLLib.getElementValueFromDocument(issueCardRqDoc, "MainApplRegNumber");
		CalculatedVariables.acctId = XMLLib.getElementValueFromDocument(issueCardRqDoc, "AcctId");
		
		//��������� xml ��������� IssueCardRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardSmoke", "IssueCardRs");
		xmlDoc = XMLLib.redefineXMLParamsIssueCardRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "IssueCardRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardSmoke" + "\\" +  "IssueCardRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 30 seconds...");
		Thread.sleep(30000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);	
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251", "CreateProductPackageRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251");
		Document createProductPackageState11Rs = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageState11Rs", statusCodeCreateProductPackageState11Rs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageState11Rs", "11");
		
		//��������� xml ��������� NotifyIssueCardResultNfRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardSmoke", "NotifyIssueCardResultNfRs");
		xmlDoc = XMLLib.redefineXMLParamsNotifyIssueCardResultNfRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "NotifyIssueCardResultNfRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardSmoke" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "NotifyIssueCardResultNf", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� NotifyIssueCardResultNf
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251", "NotifyIssueCardResultNf");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251");
		Document notifyIssueCardResultNfDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String statusCodeNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusCode");
		String severityNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "Severity");
		String statusDescNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusDesc");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeNotifyIssueCardResultNf", statusCodeNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("severityNotifyIssueCardResultNf", severityNotifyIssueCardResultNf);
		CalculatedVariables.actualValues.put("statusDescNotifyIssueCardResultNf", statusDescNotifyIssueCardResultNf);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeNotifyIssueCardResultNf", "0");
		CalculatedVariables.expectedValues.put("severityNotifyIssueCardResultNf", "Posted");
		CalculatedVariables.expectedValues.put("statusDescNotifyIssueCardResultNf", "Successfully processed");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "���������� ����� � ���", "Windows-1251", 30);

		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "���������� ����� � ���");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
		String jrnTotal0LastStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251");
		System.out.println("jrnTotal0Last = " + jrnTotal0LastStr);
		Document jrnTotal0Last = XMLLib.convertStringToDom(jrnTotal0LastStr, "UTF-8");
		String typeOperCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "TypeOperCode");
		String subSystemCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0Last", typeOperCodeJrnTotal0Last);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0Last", subSystemCodeJrnTotal0Last);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0Last", "0");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0Last", "1");
		
	CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0Last", "1");
					
	//��������� ����������� �������� ����������
	CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0Last", "1");
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
	try {
		System.setProperty("isError", "true");
		System.setProperty("isFail", "true");
		CommonLib.postProcessingTest();
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
}
		
	}
	
	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}

}
