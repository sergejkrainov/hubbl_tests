package hubbl.createnewcard;


import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineMomentum\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineMomentum2filial2DeliveryBankInfo22 {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL

		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isMomentumFpp", "1");
		//BrowserActions.setHUBBLMode("true", "isChangeTariffMultiOSB.tech", "1");
		BrowserActions.setHUBBLMode("180", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardOnlineMomentum() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateDeliveryCardMomentumRq �� �� ��. 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "InitiateDeliveryCardMomentumRq");
		xmlDoc = redefineXMLParamsInitiateDeliveryCardMomentumRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateDeliveryCardMomentumRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "InitiateDeliveryCardMomentumRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("��� 20 ");
		System.out.println("xmlMessage = " + xmlMessage);
		Document xmlMessageStr = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.cardNumber = XMLLib.getElementValueFromDocument(xmlMessageStr, "CardNum");
		CalculatedVariables.issueDate = XMLLib.getElementValueFromDocument(xmlMessageStr, "IssueDate");
		CalculatedVariables.extDt = XMLLib.getElementValueFromDocument(xmlMessageStr, "ExpDt");
		System.out.println("cardNumber = " + CalculatedVariables.cardNumber);
		System.out.println("issueDate = " + CalculatedVariables.issueDate);
		System.out.println("extDt = " + CalculatedVariables.extDt);

//		System.out.println("CardNumGetCardHolderRs = " + CardNumGetCardHolderRs);
		//���������� �������� ���������� ��� ���������� �����
		//CalculatedVariables.cardNumber = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "CardNum");
		//CalculatedVariables.issueDate = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "IssueDate");
		//CalculatedVariables.extDt = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "ExpDt");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardMomentumRq", "Windows-1251", 30);	
		
		//String value = XMLLib.extractValueFromLogByTagName();
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateDeliveryCardMomentumRq>");
//		System.out.println("��� 4 ");
/*		
        String sQuerry = "";
        
		//������ ������ � �� AC �����
				
		sQuerry = "select case when count(*) = 1 then 'OK' else 'BAD' end as Close_Open "
				+ " from ( select * from hubbl.parameters "
				+ " where PRPARAM like '%" + CalculatedVariables.rqUID + "%' )";
		String actualResultFromDB = DataBaseLib.getDataAsString(sQuerry, "HUBBL");
		System.out.println("actualResultFromDB = " + actualResultFromDB );

		
		//���������� ����������� �������� ����������

		CalculatedVariables.actualValues.put("ResultFromDB", actualResultFromDB);
						
		//��������� ����������� �������� ����������

		CalculatedVariables.expectedValues.put("ResultFromDB", "OK");
*/		
/*		if ( !actualResultFromDB.equals("OK")) {
			JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
            try {

                CommonLib.WriteLog("FAIL", "Fail due to checking ResultFromDB." , Config.resultsPath );

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}

            CalculatedVariables.isTestFailed = true;

            CalculatedVariables.isError = true;

            try {

                CommonLib.postProcessingTest();

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}       


		}
	*/	
		System.out.println("��� 5 ");
	//	System.out.println("���� 2 ������...");
	//	Thread.sleep(2000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardMomentumRs", "Windows-1251", 30);
		System.out.println("���� 1 ������...");
		Thread.sleep(1000);
		
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDeliveryCardMomentumState99Rs.xml", "Windows-1251", "InitiateDeliveryCardMomentumRs");
		
		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateDeliveryCardMomentumRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDeliveryCardMomentumState99Rs.xml", "Windows-1251");
		System.out.println("InitiateDeliveryCardMomentumRsStr = " + InitiateDeliveryCardMomentumRsStr);
		Document InitiateDeliveryCardMomentumRs = XMLLib.convertStringToDom(InitiateDeliveryCardMomentumRsStr, "UTF-8");
		String statusCodeInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "StatusCode");
		String statusDescInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "StatusDesc");
		String severityInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateDeliveryCardMomentumRs", statusCodeInitiateDeliveryCardMomentumRs);
		CalculatedVariables.actualValues.put("statusDescInitiateDeliveryCardMomentumRs", statusDescInitiateDeliveryCardMomentumRs);
		CalculatedVariables.actualValues.put("severityInitiateDeliveryCardMomentumRs", severityInitiateDeliveryCardMomentumRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateDeliveryCardMomentumRs", "99");
		CalculatedVariables.expectedValues.put("statusDescInitiateDeliveryCardMomentumRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityInitiateDeliveryCardMomentumRs", "Ok");

	
		System.out.println("��� 11 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRq", "Windows-1251", 30);	
		
		
		
		System.out.println("��� 6 ");
		//������� 30 ������ ��������� ���������
		//CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.operUID, "] ��� ����� HBLPersonService.findPerson ������: false", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: false");
	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ������� � ���� (�������� �����)");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 0 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 0 ���������� � ���!");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���� (�������� �����)");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateEdboUSV", CalculatedVariables.operUID + " - �������� ���������� 25 � ���:", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 25 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 25 ���������� � ���!");

		System.out.println("��� 7 ");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "OpenDepositUS", CalculatedVariables.operUID + " - �������� ����� � ���� (�������� �����)", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 25 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 25 ���������� � ���!");

		System.out.println("��� 8 ");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "CreateClaimUS", CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 68 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 68 ���������� � ���!");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 60 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 60 ���������� � ���!");

		System.out.println("��� 9 ");
		//������� 30 ������ ��������� ���������
//		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "GetCardInfoUS", CalculatedVariables.operUID + " - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true (�������� �����)", "Windows-1251", 30);
//		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true (�������� �����)");

		System.out.println("��� 10 ");
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "OneCardIssuan", CalculatedVariables.operUID + " - ������ ����� � ����", "Windows-1251", 30);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� 61 � ���:");
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� 61 ���������� � ���!");
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");
		
		String strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");

		int countPosSearchWay4 = strLocalLogs.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");
		System.out.println("operUID = " + CalculatedVariables.operUID);

		String idrecordWay4Queue = strLocalLogs.substring(countPosSearchWay4 - 33, countPosSearchWay4 - 25);
		
		//��������� �� ���� ���������� xml ��������� GetCardHolderRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251", "GetCardHolderRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251");
		//�������� xml ��������� �� xsd �����
        String isXMLValid;	
		isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "GetCardHolderRq");				 			 	
		CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "GetCardHolderRq", isXMLValid);					
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "GetCardHolderRq", "true");
		Document getCardHolderRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqUID");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "OperUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
//		System.out.println("operUID = " + CalculatedVariables.operUID);
		
		System.out.println("���� 1 �������...");
		Thread.sleep(1000);
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log",
				"Windows-1251",  idrecordWay4Queue + " GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");
			
		System.out.println("��� 12 ");
		//��������� xml ��������� GetCardHolderRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "GetCardHolderRs");
		xmlDoc = XMLLib.redefineXMLParamsGetCardHolderRs(xmlDoc, "2200");
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "GetCardHolderRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "GetCardHolderRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRs", "Windows-1251", 30);	

		//��������� �� ���� ���������� xml ��������� GetCardHolderRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRs.xml", "Windows-1251", "GetCardHolderRs");
		
		//��������� �������� ���� statusCode �� ������ GetCardHolderRs
		String GetCardHolderRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\GetCardHolderRs.xml", "Windows-1251");
		System.out.println("GetCardHolderRsStr = " + GetCardHolderRsStr);
		Document GetCardHolderRs = XMLLib.convertStringToDom(GetCardHolderRsStr, "UTF-8");
		String rqUIDGetCardHolderRs = XMLLib.getElementValueFromDocument(GetCardHolderRs, "RqUID");
		String rqTmGetCardHolderRs = XMLLib.getElementValueFromDocument(GetCardHolderRs, "RqTm");
		String operUIDGetCardHolderRs = XMLLib.getElementValueFromDocument(GetCardHolderRs, "OperUID");
		String statusCodeGetCardHolderRs = XMLLib.getElementValueFromDocument(GetCardHolderRs, "StatusCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDGetCardHolderRs", rqUIDGetCardHolderRs);
		CalculatedVariables.actualValues.put("rqTmGetCardHolderRs", rqTmGetCardHolderRs);
		CalculatedVariables.actualValues.put("operUIDGetCardHolderRs", operUIDGetCardHolderRs);
		CalculatedVariables.actualValues.put("statusCodeGetCardHolderRs", statusCodeGetCardHolderRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDGetCardHolderRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("rqTmGetCardHolderRs", CalculatedVariables.rQTm);
		CalculatedVariables.expectedValues.put("operUIDGetCardHolderRs", CalculatedVariables.operUID);
		CalculatedVariables.expectedValues.put("statusCodeGetCardHolderRs", "2200");
		
		System.out.println("��� 13 ");
		System.out.println("���� 5 ������...");
		Thread.sleep(5000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRq", "Windows-1251", 60);	
		
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRq.xml", "Windows-1251", "CustAddRq");
		
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRq.xml", "Windows-1251");
		Document custAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqTm");
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");
		
		System.out.println("��� 14 ");
		//��������� xml ��������� CustAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "CustAddRs");
		xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CustAddRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "CustAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRs", "Windows-1251", 30);	

		//��������� �� ���� ���������� xml ��������� CustAddRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRs.xml", "Windows-1251", "CustAddRs");
		
		//��������� �������� ���� statusCode �� ������ CustAddRs
		String CustAddRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRs.xml", "Windows-1251");
		System.out.println("CustAddRsStr = " + CustAddRsStr);
		Document CustAddRs = XMLLib.convertStringToDom(CustAddRsStr, "UTF-8");
		String rqUIDCustAddRs = XMLLib.getElementValueFromDocument(CustAddRs, "RqUID");
		String rqTmCustAddRs = XMLLib.getElementValueFromDocument(CustAddRs, "RqTm");
		String statusCodeCustAddRs = XMLLib.getElementValueFromDocument(CustAddRs, "StatusCode");
		System.out.println("rqUIDCustAddRs = " + rqUIDCustAddRs);		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDCustAddRs", rqUIDCustAddRs);
		CalculatedVariables.actualValues.put("rqTmCustAddRs", rqTmCustAddRs);
		CalculatedVariables.actualValues.put("statusCodeCustAddRs", statusCodeCustAddRs);
		
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDCustAddRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("rqTmCustAddRs", CalculatedVariables.rQTm);
		CalculatedVariables.expectedValues.put("statusCodeCustAddRs", "0");
		
		System.out.println("��� 15 ");
		System.out.println("���� 3 �������...");
		Thread.sleep(3000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "BankAcctAddRq", "Windows-1251", 45);	
				
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\BankAcctAddRq.xml", "Windows-1251", "BankAcctAddRq");
			
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\BankAcctAddRq.xml", "Windows-1251");
		Document bankAcctAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(bankAcctAddRqDoc, "RqUID");
		CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(bankAcctAddRqDoc, "RqTm");
		System.out.println("rqUIDbankAcctAddRq = " + CalculatedVariables.rqUID);		
				
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");
		
		System.out.println("��� 16 ");
		//��������� xml ��������� BankAcctAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "BankAcctAddRs");
		xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "BankAcctAddRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "BankAcctAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
				
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("���� 2 �������...");
		Thread.sleep(2000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "BankAcctAddRs", "Windows-1251", 30);	

		
		//��������� �� ���� ���������� xml ��������� BankAcctAddRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\BankAcctAddRs.xml", "Windows-1251", "BankAcctAddRs");

			
		//��������� �������� ���� statusCode �� ������ BankAcctAddRs
		String BankAcctAddRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\BankAcctAddRs.xml", "Windows-1251");
		System.out.println("BankAcctAddRsStr = " + BankAcctAddRsStr);
		Document bankAcctAddRs = XMLLib.convertStringToDom(BankAcctAddRsStr, "UTF-8");
		String rqUIDBankAcctAddRs = XMLLib.getElementValueFromDocument(bankAcctAddRs, "RqUID");
		String rqTmBankAcctAddRs = XMLLib.getElementValueFromDocument(bankAcctAddRs, "RqTm");
		String statusCodeBankAcctAddRs = XMLLib.getElementValueFromDocument(bankAcctAddRs, "StatusCode");
		System.out.println("rqUIDBankAcctAddRs = " + rqUIDBankAcctAddRs);		
				
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDBankAcctAddRs", rqUIDBankAcctAddRs);
		CalculatedVariables.actualValues.put("rqTmBankAcctAddRs", rqTmBankAcctAddRs);
		CalculatedVariables.actualValues.put("statusCodeBankAcctAddRs", statusCodeBankAcctAddRs);
								
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDBankAcctAddRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("rqTmBankAcctAddRs", CalculatedVariables.rQTm);
		CalculatedVariables.expectedValues.put("statusCodeBankAcctAddRs", "0");

//step17	������������� ������� WAY CardStatusModASync ���������
/*	
		System.out.println("��� 17 ");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardStatusModASyncRq", "Windows-1251", 160);	
						
		//��������� �� ���� ���������� xml ��������� CustAddRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
					
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		Document cardStatusModASyncRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRqDoc, "RqUID");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRqDoc, "OperUID");
						
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");
//step18		
		System.out.println("��� 18 ");
		//��������� xml ��������� BankAcctAddRs
		xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
						
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		System.out.println("��� 18 1 ");

		//��������� �� ���� ���������� xml ��������� BankAcctAddRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRs.xml", "Windows-1251", "CardStatusModASyncRs");
		System.out.println("��� 18 2");
					
		//��������� �������� ���� statusCode �� ������ BankAcctAddRs
		String CardStatusModASyncRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRs.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRsStr = " + CardStatusModASyncRsStr);
		Document CardStatusModASyncRs = XMLLib.convertStringToDom(CardStatusModASyncRsStr, "UTF-8");
		String rqUIDCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "RqUID");
		String operUIDCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "OperUID");
		String statusCodeCardStatusModASyncRs = XMLLib.getElementValueFromDocument(CardStatusModASyncRs, "StatusCode");
						
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("rqUIDCardStatusModASyncRs", rqUIDCardStatusModASyncRs);
		CalculatedVariables.actualValues.put("rqTmCardStatusModASyncRs", operUIDCardStatusModASyncRs);
		CalculatedVariables.actualValues.put("statusCodeCardStatusModASyncRs", statusCodeCardStatusModASyncRs);
										
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("rqUIDCardStatusModASyncRs", CalculatedVariables.rqUID);
		CalculatedVariables.expectedValues.put("operUIDCardStatusModASyncRs", CalculatedVariables.operUID);
		CalculatedVariables.expectedValues.put("statusCodeCardStatusModASyncRs", "0");

*/
		
	
		System.out.println("���� 3 ������...");
		Thread.sleep(3000);
		
		System.out.println("��� 19 ");
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "ns2:srvInitiateDeliveryCardMomentumRs", "Windows-1251", 30);
//		System.out.println("��� 19 2");	
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDeliveryCardMomentumLastStateRs.xml", "Windows-1251", "ns2:srvInitiateDeliveryCardMomentumRs");
//		System.out.println("��� 19 3");	
		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateDeliveryCardMomentumLastStateRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDeliveryCardMomentumLastStateRs.xml", "Windows-1251");
//		System.out.println("InitiateDeliveryCardMomentumLastStateRsStr = " + InitiateDeliveryCardMomentumLastStateRsStr);
		Document InitiateDeliveryCardMomentumLastStateRs = XMLLib.convertStringToDom(InitiateDeliveryCardMomentumLastStateRsStr, "UTF-8");
		String statusCodeInitiateDeliveryCardMomentumLastStateRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumLastStateRs, "statusCode");
		String statusDescInitiateDeliveryCardMomentumLastStateRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumLastStateRs, "statusDesc");
		String severityInitiateDeliveryCardMomentumLastStateRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumLastStateRs, "severity");

		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateDeliveryCardMomentumLastStateRs", statusCodeInitiateDeliveryCardMomentumLastStateRs);
		CalculatedVariables.actualValues.put("statusDescInitiateDeliveryCardMomentumLastStateRs", statusDescInitiateDeliveryCardMomentumLastStateRs);
		CalculatedVariables.actualValues.put("severityInitiateDeliveryCardMomentumLastStateRs", severityInitiateDeliveryCardMomentumLastStateRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateDeliveryCardMomentumLastStateRs", "0");
		CalculatedVariables.expectedValues.put("statusDescInitiateDeliveryCardMomentumLastStateRs", "������ ���, ����� �������");
		CalculatedVariables.expectedValues.put("severityInitiateDeliveryCardMomentumLastStateRs", "OK");

		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsInitiateDeliveryCardMomentumRq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 String sQuerry = "";

		 String cardNum = "";
			
		//���������� 16-�� ������� ����� �����, ������� ����������� � ��
		do{
			cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
			sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
		}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
			System.out.println("GeneratedCardNum = " + cardNum);
			CalculatedVariables.cardNumber = cardNum;
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
		 
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
		 CalculatedVariables.operUID = OperUID;
		 System.out.println("OperUID = " + OperUID);
		 CalculatedVariables.rqUID = RqUID;
		 System.out.println("RqUID = " + RqUID);
			
		 String lastName = "�������-" + CommonLib.getRandomLetters(15);
		 CalculatedVariables.lastName = lastName;
		 String firstName = CommonLib.getRandomLetters(15);
		 CalculatedVariables.firstName = firstName;
		 String middleName = CommonLib.getRandomLetters(15);
		 CalculatedVariables.middleName = middleName;
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 CalculatedVariables.idSeries = idSeries;
		 String idNum = CommonLib.getRandomNumericLetters(6);
		 CalculatedVariables.idNum = idNum;
		 
		 //���������  � template.xml
		 //String cardCode = "111707";
		 //String productCode = "IRRDBCMF--";
		 //String eDBOContractFlag = "true";
		 //String contractProductCode = "IRRD--";
		 
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("CardNum", cardNum);

				 
		//������ info 
		 rowParams.put("OperDate", Config.businessProcessProp.getProperty("OperDay22"));
		 rowParams.put("UserName", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin22"));
		 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode22"));
		 rowParams.put("UserFIO", Config.businessProcessProp.getProperty("OperatorInfoOperatorName22"));
				
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
			 //������ info 
	 	 rowParams.clear();
		/* rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
		*/
	 	 
		 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId22"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId22"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId22"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId22"));
		
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

			 //������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId22"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId22"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId22"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId22"));
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

		return docXML;
	}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
