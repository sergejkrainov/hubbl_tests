package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineWithoutUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineWithoutUDBO {

		//14. ������. ���������� ������ �� ����� � ��������� �����, ��� ����������� ����
		@Before
		public void initTest() throws Exception{
			//������������� ����� ������ ������� HUBBL
			Init.initBrowser();
			BrowserActions.loginToHubblAdmin();
			BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
			BrowserActions.setHUBBLMode("60", "timeOnline", "1");
			BrowserActions.logOutFromHubblAdmin();
			
			//������� ���������� � ������ � ���������� �������
			//FileLib.clearFolder(Config.logsPath);
			
			//������� ���������� � ������� ��� Jazz
			//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
			
		}
		
		@Test
		public void testCreateNewCardOnlineWithoutUDBO() { //@Param(name = "name") String name, @Param(name = "age") int age){
	try{
		
			//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
			Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineWithoutUDBO", "CreateProductPackageWithoutUDBORq");
			xmlDoc = XMLLib.redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
			String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_v_1.38", true, "CreateProductPackageWithoutUDBORq");
			FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineWithoutUDBO" + "\\" +  "CreateProductPackageWithoutUDBORq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
			//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
			MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 30);	

			System.out.println("���� 14 ��� 5 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
			
			//��������� �������� ���� statusCode �� ������ createProductPackageRs
			String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
			System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
			Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
			String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
			String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
			String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
			CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
			CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "99");
			CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ �������");
			CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Ok");

			System.out.println("���� 14 ��� 6 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "cardOpenRequest", "Windows-1251", 30);	
					
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:cardOpenRequest");
			
			//��������� �� ���� ���������� xml ��������� cardOpenRequest
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
			
			//��������� �������� ���� operUID �� ��������� cardOpenRequest
			String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
			System.out.println("cardOpenRequest = " + cardOpenRequestStr);	
			Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
			CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
			
			System.out.println("CalculatedVariables.operUID = " + CalculatedVariables.operUID);

			
			/*//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "����� ������� � ���� (�������� �����)", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � ���� (�������� �����)");
			*/
			System.out.println("���� 14 ��� 7 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "���������� ������� � ����", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "���������� ������� � ����");
			
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "first");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
			String jrnTotal0Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0.xml", "Windows-1251");
			System.out.println("jrnTotal0 = " + jrnTotal0Str);
			Document jrnTotal0 = XMLLib.convertStringToDom(jrnTotal0Str, "UTF-8");
			String typeOperCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "TypeOperCode");
			String subSystemCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "SubSystemCode");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0", typeOperCodeJrnTotal0);
			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0", subSystemCodeJrnTotal0);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0", "0");
			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0", "6");

			System.out.println("���� 14 ��� 9 ");
			//������� 30 ������ ��������� ���������
	/*		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���� (�������� �����)", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���� (�������� �����)");

			System.out.println("���� 14 ��� 9 ");
			//������� 30 ������ ��������� ���������
	*/
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 25 � ���", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "first");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
			String jrnTotal25StrFirst = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251");
			System.out.println("jrnTotal25First = " + jrnTotal25StrFirst);
			Document jrnTotal25First = XMLLib.convertStringToDom(jrnTotal25StrFirst, "UTF-8");
			String typeOperCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "TypeOperCode");
			String subSystemCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "SubSystemCode");
			String ReceiverJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "Receiver");
			
			System.out.println("���� 14 ��� 10 ");
			String sQuerry = "select state from client.edbo where id_mega = '38' and edbo_no = '" + ReceiverJrnTotal25First + "'";
			String stateJrnTotal25First = DataBaseLib.getDataAsString(sQuerry, "COD");
			//��� � ��� ��� ����� 

			
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25First", typeOperCodeJrnTotal25First);
			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25First", subSystemCodeJrnTotal25First);
			CalculatedVariables.actualValues.put("stateJrnTotal25First", stateJrnTotal25First);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25First", "25");
			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25First", "6");
			CalculatedVariables.expectedValues.put("stateJrnTotal25First", "null");
			
			System.out.println("���� 14 ��� 11 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ����� � ����", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ����� � ���� (�������� �����)");
			
			System.out.println("���� 14 ��� 12 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "������ �� ������ ����� � ���� (�������� �����)", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25Last.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "last");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
			String jrnTotal25StrLast = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25Last.xml", "Windows-1251");
			System.out.println("jrnTotal25Last = " + jrnTotal25StrLast);
			Document jrnTotal25Last = XMLLib.convertStringToDom(jrnTotal25StrLast, "UTF-8");
			String typeOperCodeJrnTotal25Last = XMLLib.getElementValueFromDocument(jrnTotal25Last, "TypeOperCode");
			String subSystemCodeJrnTotal25Last = XMLLib.getElementValueFromDocument(jrnTotal25Last, "SubSystemCode");
			String accountNumber = XMLLib.getElementValueFromDocumentNode(jrnTotal25Last, "Comment", "Way");
			String account = accountNumber.substring(0, 5);
			String currency = accountNumber.substring(5, 8);
			String key = accountNumber.substring(8, 9);
			String branch = accountNumber.substring(9, 13);
			String no = accountNumber.substring(13, 20);
			
			System.out.println("���� 14 ��� 13 ");		
			sQuerry = "SELECT DEPOSIT.DEPOSIT.STATE " +
					  " FROM DEPOSIT.DEPOSIT, DEPOSIT.DEPOKEY98 " +
					  " WHERE DEPOKEY98.ACCOUNT=" + account +
					  "      AND DEPOKEY98.CURRENCY=" + currency +
					  "      AND DEPOKEY98.KEY=" + key +
					  "      AND DEPOKEY98.BRANCH=" + branch +
					  "      AND DEPOKEY98.NO=" + no +
					  "      AND DEPOKEY98.DEPOSIT_MAJOR=DEPOSIT.ID_MAJOR " +
					  "      and DEPOKEY98.DEPOSIT_MINOR=DEPOSIT.ID_MINOR ";

			String stateJrnTotal25Last = DataBaseLib.getDataAsString(sQuerry, "COD");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25Last", typeOperCodeJrnTotal25Last);
			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25Last", subSystemCodeJrnTotal25Last);
			CalculatedVariables.actualValues.put("stateJrnTotal25Last", stateJrnTotal25Last);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25Last", "25");
			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25Last", "6");
			CalculatedVariables.expectedValues.put("stateJrnTotal25Last", "0");

			System.out.println("���� 14 ��� 14 ");
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ������ �� ������ ����� � ���� (�������� �����)");
			
			System.out.println("���� 14 ��� 15 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 68 � ���", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 68 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal68.xml", "Windows-1251", "JrnTotal", "�������� ���������� 68 � ���", "first");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 68 � ���
			String jrnTotal68Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal68.xml", "Windows-1251");
			System.out.println("jrnTotal68 = " + jrnTotal68Str);
			Document jrnTotal68 = XMLLib.convertStringToDom(jrnTotal68Str, "UTF-8");
			String typeOperCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "TypeOperCode");
			String subSystemCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "SubSystemCode");
			String ukrBankCodeJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "UKRBankCode");
			String mailAccountJrnTotal68 = XMLLib.getElementValueFromDocument(jrnTotal68, "MailAccount");
			CalculatedVariables.mailAccount = mailAccountJrnTotal68;
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal68", typeOperCodeJrnTotal68);
			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal68", subSystemCodeJrnTotal68);
			CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", ukrBankCodeJrnTotal68);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal68", "68");
			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal68", "1");
			CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal68", "IGN");
			
			System.out.println("���� 14 ��� 16 ");
			sQuerry = "select CODECARD from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal68 + "'";

			String codeCardJrnTotal68 = DataBaseLib.getDataAsString(sQuerry, "COD");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("codeCardJrnTotal68", codeCardJrnTotal68);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("codeCardJrnTotal68", "111703");
			
			System.out.println("���� 14 ��� 17 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 60 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal60.xml", "Windows-1251", "JrnTotal", "�������� ���������� 60 � ���", "first");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 60 � ���
			String jrnTotal60Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal60.xml", "Windows-1251");
			System.out.println("jrnTotal60 = " + jrnTotal60Str);
			Document jrnTotal60 = XMLLib.convertStringToDom(jrnTotal60Str, "UTF-8");
			String typeOperCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "TypeOperCode");
			String subSystemCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "SubSystemCode");
			String ukrBankCodeJrnTotal60 = XMLLib.getElementValueFromDocument(jrnTotal60, "UKRBankCode");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal60", typeOperCodeJrnTotal60);
			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal60", subSystemCodeJrnTotal60);
			CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", ukrBankCodeJrnTotal60);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal60", "60");
			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal60", "1");
			CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal60", "IGN");

			System.out.println("���� 14 ��� 18 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "GetCardHolderRq", "Windows-1251", 30);	
			
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
					"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");
			
			String strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");

			int countPosSearchWay4 = strLocalLogs.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � WAY4 (�������� �����)");

			String idrecordWay4Queue = strLocalLogs.substring(countPosSearchWay4 - 33, countPosSearchWay4 - 25);
			
			System.out.println("���� 14 ��� 19 ");
			//��������� �� ���� ���������� xml ��������� GetCardHolderRq
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251", "GetCardHolderRq");

			
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\GetCardHolderRq.xml", "Windows-1251");
	/*		//�������� xml ��������� �� xsd �����
	        String isXMLValid;	
			isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "GetCardHolderRq");				 			 	
			CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "GetCardHolderRq", isXMLValid);					
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "GetCardHolderRq", "true");
	*/
			Document getCardHolderRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqUID");
			CalculatedVariables.operUIDgetCardHolderRq = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "OperUID");
			CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
			

			System.out.println("���� 5 ������...");
			Thread.sleep(3000);
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log","Windows-1251",  idrecordWay4Queue + " GetCardHolder I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID:");
			
			System.out.println("���� 14 ��� 20 ");
			//��������� xml ��������� GetCardHolderRs
			xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineWithoutUDBO", "GetCardHolderRs");
			xmlDoc = XMLLib.redefineXMLParamsGetCardHolderRs(xmlDoc, "2200");
			xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "GetCardHolderRs");

			FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
					+ "CreateNewCardOnlineWithoutUDBO" + "\\" +  "GetCardHolderRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 

			//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
			MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

			System.out.println("���� 14 ��� 21 ");
			System.out.println("���� 2 ������...");
			Thread.sleep(2000);			
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CustAddRq", "Windows-1251", 30);	
			
			//��������� �� ���� ���������� xml ��������� CustAddRq
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CustAddRq.xml", "Windows-1251", "CustAddRq");
			
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CustAddRq.xml", "Windows-1251");
			/*//�������� xml ��������� �� xsd �����
			isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "CustAddRq");				 			 	
			CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "CustAddRq", isXMLValid);					
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "CustAddRq", "true");*/
			Document custAddRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(custAddRqDoc, "RqUID");
			CalculatedVariables.rQTm = XMLLib.getElementValueFromDocument(getCardHolderRqDoc, "RqTm");
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",  idrecordWay4Queue + " CustAddWay4Se I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID");
			
			System.out.println("���� 14 ��� 22 ");
			//��������� xml ��������� CustAddRs
			xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineWithoutUDBO", "CustAddRs");
			xmlDoc = XMLLib.redefineXMLParamsCustAddRs(xmlDoc);
			xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CustAddRs");
			FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
					+ "CreateNewCardOnlineWithoutUDBO" + "\\" +  "CustAddRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
			
			//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
			MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

			System.out.println("���� 14 ��� 23 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "IssueCardRq", "Windows-1251", 30);	
			
			//��������� �� ���� ���������� xml ��������� IssueCardRq
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRq.xml", "Windows-1251", "IssueCardRq");
			
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRq.xml", "Windows-1251");
			/*//�������� xml ��������� �� xsd �����
			isXMLValid = XMLLib.validationXML(xmlMessage, Config.allProp.getProperty("xsdPath") + "Hubble_Emission_Customer_v_1.25" + ".xsd", "IssueCardRq");				 			 	
			CalculatedVariables.actualValues.put("XMLRequestValid_for_" + "IssueCardRq", isXMLValid);					
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("XMLRequestValid_for_" + "IssueCardRq", "true");*/
			Document issueCardRqDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			CalculatedVariables.rqUID = XMLLib.getElementValueFromDocument(issueCardRqDoc, "RqUID");
			CalculatedVariables.mainApplRegNumber = XMLLib.getElementValueFromDocument(issueCardRqDoc, "MainApplRegNumber");
			CalculatedVariables.acctId = XMLLib.getElementValueFromDocument(issueCardRqDoc, "AcctId");
			
			System.out.println("���� 14 ��� 24 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ��������� �� ������������ ����� � WAY4 (�������� �����)", "Windows-1251", 30);
			
			JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ��������� �� ������������ ����� � WAY4 (�������� �����)");
			
			System.out.println("���� 14 ��� 25 ");
			//��������� xml ��������� IssueCardRs
			xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineWithoutUDBO", "IssueCardRs");
			xmlDoc = XMLLib.redefineXMLParamsIssueCardRs(xmlDoc);
			xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "IssueCardRs");
			FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
					+ "CreateNewCardOnlineWithoutUDBO" + "\\" +  "IssueCardRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
			
			//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
			MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
			System.out.println("���� 14 ��� 26 ");
			//������� 30 ������ ��������� ���������
			System.out.println("Wait for 30 seconds...");
			Thread.sleep(30000);
			
			//��������� ���� � ���������� ������� ��� ��������
			FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
			//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
			FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
							"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
			
			//��������� �� ���� ���������� xml ��������� IssueCardRs
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\IssueCardRs.xml", "Windows-1251", "IssueCardRs");
			
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\IssueCardRs.xml", "Windows-1251");
			Document issueCardRsDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			
			String mainApplRegNumberIssueCardRs = XMLLib.getElementValueFromDocument(issueCardRsDoc, "MainApplRegNumber");
			
			System.out.println("���� 14 ��� 27 ");
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("mainApplRegNumberIssueCardRs", mainApplRegNumberIssueCardRs);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("mainApplRegNumberIssueCardRs", CalculatedVariables.mainApplRegNumber);
			
	/*		strLocalLogs = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
			String regex = "���� isNeedToRestart �� ��������� � 1 ��� ������ �� WAY4 \\(�������� ��� ����������� ������ �� ���������\\) "
					+ "ISSUE_CARD/ISSUE_CARD\\(\\[rqUID=[\\w\\d]{32}, " + "mainApplRegNumber=" + CalculatedVariables.mainApplRegNumber;

			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("mainApplRegNumberWay4Queue", String.valueOf(CommonLib.isStrContainsSubStr(strLocalLogs, regex)));
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("mainApplRegNumberWay4Queue", "true");
	*/
			System.out.println("���� 14 ��� 28 ");
			//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
			XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251", "CreateProductPackageRs");
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251");
			Document createProductPackageState11Rs = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			String statusCodeCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusCode");
			String statusDescCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusDesc");
			String severityCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "Severity");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("statusCodeCreateProductPackageState11Rs", statusCodeCreateProductPackageState11Rs);
			CalculatedVariables.actualValues.put("statusDescCreateProductPackageState11Rs", statusDescCreateProductPackageState11Rs);
			CalculatedVariables.actualValues.put("severityCreateProductPackageState11Rs", severityCreateProductPackageState11Rs);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageState11Rs", "17");
			CalculatedVariables.expectedValues.put("statusDescCreateProductPackageState11Rs", "���������� � ���������");
			CalculatedVariables.expectedValues.put("severityCreateProductPackageState11Rs", "InProcess");
			
			System.out.println("���� 14 ��� 29 ");
			//��������� xml ��������� NotifyIssueCardResultNfRs
			xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineWithoutUDBO", "NotifyIssueCardResultNfRs");
			xmlDoc = XMLLib.redefineXMLParamsNotifyIssueCardResultNfRs(xmlDoc);
			xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "NotifyIssueCardResultNfRs");
			FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
					+ "CreateNewCardOnlineWithoutUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
			
			//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 		
			MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
			System.out.println("���� 14 ��� 30 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "NotifyIssueCardResultNf", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� NotifyIssueCardResultNf
			XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251", "NotifyIssueCardResultNf");
			
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\NotifyIssueCardResultNf.xml", "Windows-1251");
			Document notifyIssueCardResultNfDoc = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
			String statusCodeNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusCode");
			String severityNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "Severity");
			String statusDescNotifyIssueCardResultNf = XMLLib.getElementValueFromDocument(notifyIssueCardResultNfDoc, "StatusDesc");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("statusCodeNotifyIssueCardResultNf", statusCodeNotifyIssueCardResultNf);
			CalculatedVariables.actualValues.put("severityNotifyIssueCardResultNf", severityNotifyIssueCardResultNf);
			CalculatedVariables.actualValues.put("statusDescNotifyIssueCardResultNf", statusDescNotifyIssueCardResultNf);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("statusCodeNotifyIssueCardResultNf", "0");
			CalculatedVariables.expectedValues.put("severityNotifyIssueCardResultNf", "Posted");
			CalculatedVariables.expectedValues.put("statusDescNotifyIssueCardResultNf", "Successfully processed");
			
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 60 � ���", "Windows-1251", 30);
			
			System.out.println("���� 14 ��� 31 ");
			//������� 30 ������ ��������� ���������
			CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 30);
			
			//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
			XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "last");
			
			//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
			String jrnTotal0LastStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0Last.xml", "Windows-1251");
			System.out.println("jrnTotal0Last = " + jrnTotal0LastStr);
			Document jrnTotal0Last = XMLLib.convertStringToDom(jrnTotal0LastStr, "UTF-8");
			String typeOperCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "TypeOperCode");
//			String subSystemCodeJrnTotal0Last = XMLLib.getElementValueFromDocument(jrnTotal0Last, "SubSystemCode");
			
			//���������� ����������� �������� ����������
			CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0Last", typeOperCodeJrnTotal0Last);
//			CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0Last", subSystemCodeJrnTotal0Last);
							
			//��������� ����������� �������� ����������
			CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0Last", "0");
//			CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0Last", "6");
			

			
			//������� ���������� � ��������� �������� ������
			JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		
		
		
		
	}catch(Exception e){
		e.printStackTrace();
	}
		
	}
	
	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}

}
