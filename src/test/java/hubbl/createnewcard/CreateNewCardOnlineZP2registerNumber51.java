package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineZP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineZP2registerNumber51 {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("60", "timeOnline", "1");
		BrowserActions.setHUBBLMode("30", "way4.response.timeout", "1");
		
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardConnectUDBO() {//@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineZP", "CreateProductPackageRq");
		xmlDoc = redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", false, "CreateProductPackageRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineZP" + "\\" +  "CreateProductPackageRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 35);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
		
		//��������� �������� ���� statusCode �� ������ createProductPackageRs
		String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
	//	CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "1");
	//	CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Error");
		
	
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
		
		
	
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsNotifyIssueCardResultNfRs(Document docXML) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String sQuerry = "";
		RqUID = CalculatedVariables.rqUID;
		String cardNumNotifyIssueCardResultNf = "";
		
		//���������� 16-�� ������� ����� �����, ������� ����������� � ��
		do{
			cardNumNotifyIssueCardResultNf = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
			sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNumNotifyIssueCardResultNf + "'";
		}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
		System.out.println("GeneratedCardNum = " + cardNumNotifyIssueCardResultNf);
		
		
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("MainApplRegNumber", CalculatedVariables.mainApplRegNumber);
		rowParams.put("AcctId", CalculatedVariables.acctId);
		rowParams.put("CardNum", cardNumNotifyIssueCardResultNf);
		
		rowParams.put("StatusCode", "315");
		rowParams.put("Severity", "Error");
		rowParams.put("StatusDesc", "StatusDescStatusDesc");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
		return docXML;
	}
	

	public static Document redefineXMLParamsCreateProductPackageConnectUDBORq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
			
		 String lastName = "�������-" + CommonLib.getRandomLetters(15);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
		
		 String idRegisterNumber = CommonLib.getRandomNumericLetters(51);
		 rowParams.put("RegisterNumber", idRegisterNumber);
		 
		 
		 
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("OperDate", Config.OperDay);
		 rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		 rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		 
		// rowParams.put("Channel", "VSP");
		 
		 System.out.println("rowParams = " + rowParams);	
		 
		 
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
		//������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

		return docXML;
	}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
