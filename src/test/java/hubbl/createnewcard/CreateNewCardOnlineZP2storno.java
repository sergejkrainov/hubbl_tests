package hubbl.createnewcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineZP\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineZP2storno {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("60", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardConnectUDBO() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  CreateProductPackageRq �� �� �� �� �� . 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineZP", "CreateProductPackageRq");
		xmlDoc = redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", false, "CreateProductPackageRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineZP" + "\\" +  "CreateProductPackageRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
			
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRq", "Windows-1251", 35);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<CreateProductPackageRq>");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CreateProductPackageRs", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251", "CreateProductPackageRs");
		
		//��������� �������� ���� statusCode �� ������ createProductPackageRs
		String createProductPackageRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState99Rs.xml", "Windows-1251");
		System.out.println("createProductPackageRsStr = " + createProductPackageRsStr);
		Document createProductPackageRs = XMLLib.convertStringToDom(createProductPackageRsStr, "UTF-8");
		String statusCodeCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusCode");
		String statusDescCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "StatusDesc");
		String severityCreateProductPackageRs = XMLLib.getElementValueFromDocument(createProductPackageRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeCreateProductPackageRs", statusCodeCreateProductPackageRs);
		CalculatedVariables.actualValues.put("statusDescCreateProductPackageRs", statusDescCreateProductPackageRs);
		CalculatedVariables.actualValues.put("severityCreateProductPackageRs", severityCreateProductPackageRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageRs", "99");
		CalculatedVariables.expectedValues.put("statusDescCreateProductPackageRs", "������ �������");
		CalculatedVariables.expectedValues.put("severityCreateProductPackageRs", "Ok");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "cardOpenRequest", "Windows-1251", 35);	
				
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:cardOpenRequest");
		
		//��������� �� ���� ���������� xml ��������� cardOpenRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251", "ns2:cardOpenRequest");
		
		//��������� �������� ���� operUID �� ��������� cardOpenRequest
		String cardOpenRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardOpenRequest.xml", "Windows-1251");
		System.out.println("cardOpenRequest = " + cardOpenRequestStr);	
		Document cardOpenRequest = XMLLib.convertStringToDom(cardOpenRequestStr, "UTF-8");
		CalculatedVariables.operUID = XMLLib.getElementValueFromDocument(cardOpenRequest, "operUID");
		
		System.out.println("CalculatedVariables.operUID = " + CalculatedVariables.operUID);
		
		/*//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "����� ������� � ���� (�������� �����)", "Windows-1251", 35);	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ������� � ���� (�������� �����)");
		*/
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "���������� ������� � ����", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "���������� ������� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 0 � ���", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 0 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal0.xml", "Windows-1251", "JrnTotal", "�������� ���������� 0 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 0 � ���
		String jrnTotal0Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal0.xml", "Windows-1251");
		System.out.println("jrnTotal0 = " + jrnTotal0Str);
		Document jrnTotal0 = XMLLib.convertStringToDom(jrnTotal0Str, "UTF-8");
		String typeOperCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "TypeOperCode");
		String subSystemCodeJrnTotal0 = XMLLib.getElementValueFromDocument(jrnTotal0, "SubSystemCode");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal0", typeOperCodeJrnTotal0);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal0", subSystemCodeJrnTotal0);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal0", "0");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal0", "6");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���� (�������� �����)", "Windows-1251", 35);
		
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���� (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 25 � ���", "Windows-1251", 35);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 25 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251", "JrnTotal", "�������� ���������� 25 � ���", "first");
		
		//��������� �������� ���� typeOperCode,subSystemCode �� xml JrnTotal, �������� ���������� 25 � ���
		String jrnTotal25StrFirst = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal25First.xml", "Windows-1251");
		System.out.println("jrnTotal25First = " + jrnTotal25StrFirst);
		Document jrnTotal25First = XMLLib.convertStringToDom(jrnTotal25StrFirst, "UTF-8");
		String typeOperCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "TypeOperCode");
		String subSystemCodeJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "SubSystemCode");
		String ReceiverJrnTotal25First = XMLLib.getElementValueFromDocument(jrnTotal25First, "Receiver");
		
		String sQuerry = "select state from client.edbo where id_mega = '38' and edbo_no = '" + ReceiverJrnTotal25First + "'";
		String stateJrnTotal25First = DataBaseLib.getDataAsString(sQuerry, "COD");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal25First", typeOperCodeJrnTotal25First);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal25First", subSystemCodeJrnTotal25First);
	//	CalculatedVariables.actualValues.put("stateJrnTotal25First", stateJrnTotal25First);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal25First", "25");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal25First", "6");
	//	CalculatedVariables.expectedValues.put("stateJrnTotal25First", "0");
		
		
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������������� �������� � ��� c sid =", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������������� �������� � ��� c sid =");
		
/*/				������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"/" + CalculatedVariables.operUID + ": ��� ������� ����������. ONLINE-��� 50 ��������� ��� 0", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"/" + CalculatedVariables.operUID + ": ��� ������� ����������. ONLINE-��� 50 ��������� ��� 0");		

//		������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"/" + CalculatedVariables.operUID + ": ��� ������� ����������. ONLINE-��� 50 ��������� ��� 0", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"/" + CalculatedVariables.operUID + ": ��� ������� ����������. ONLINE-��� 50 ��������� ��� 0");
		*/
//		������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������� �������������� �������� � ��� c sid =", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������� �������������� �������� � ��� c sid =");
		
//������� 30 ������ ��������� ���������
CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
		"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ hblIssueCard()", "Windows-1251", 45);
JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ���������� ������ hblIssueCard()");


//������� 30 ������ ��������� ���������
//System.out.println("Wait for 3 seconds...");
//Thread.sleep(3000);

//��������� �� ���� ���������� xml ��������� CreateProductPackageRs
XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251", "CreateProductPackageRs");
xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\CreateProductPackageState11Rs.xml", "Windows-1251");
Document createProductPackageState11Rs = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
String statusCodeCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusCode");
//String statusDescCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "StatusDesc");
String severityCreateProductPackageState11Rs = XMLLib.getElementValueFromDocument(createProductPackageState11Rs, "Severity");

//���������� ����������� �������� ����������
CalculatedVariables.actualValues.put("statusCodeCreateProductPackageState11Rs", statusCodeCreateProductPackageState11Rs);
//CalculatedVariables.actualValues.put("statusDescCreateProductPackageState11Rs", statusDescCreateProductPackageState11Rs);
CalculatedVariables.actualValues.put("severityCreateProductPackageState11Rs", severityCreateProductPackageState11Rs);
				
//��������� ����������� �������� ����������
CalculatedVariables.expectedValues.put("statusCodeCreateProductPackageState11Rs", "13");
//CalculatedVariables.expectedValues.put("statusDescCreateProductPackageState11Rs", "���������� � ���������");
CalculatedVariables.expectedValues.put("severityCreateProductPackageState11Rs", "Error");


//������� ���������� � ��������� �������� ������
JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	



}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsCreateProductPackageConnectUDBORq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
	
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
			
		 String lastName = "�������-������" + CommonLib.getRandomLetters(9);
		 String firstName = CommonLib.getRandomLetters(15);
		 String middleName = CommonLib.getRandomLetters(15);
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 String idNum = CommonLib.getRandomNumericLetters(6);
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("OperDate", Config.OperDay);
		 rowParams.put("UserName", Config.OperatorInfoOperatorLogin2);
		// rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode2);
		 rowParams.put("OperatorCode", "1313");

		 
		 
		// rowParams.put("Channel", "VSP");
		 
		 System.out.println("rowParams = " + rowParams);	
		 
		 
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
		//������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		 rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		 rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

		//������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.DeliveryBankInfoBranchId);
		 rowParams.put("AgencyId", Config.DeliveryBankInfoAgencyId);
		 rowParams.put("RegionId", Config.DeliveryBankInfoRegionId);
		 rowParams.put("RbTbBrchId", Config.DeliveryBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

		return docXML;
	}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
