package hubbl.createnewcard;


import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.time.LocalDateTime;
import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineMomentum\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineMomentum2SBOLerror666 {

	@Before
	public void initTest() throws Exception{
		//������������� ����� ������ ������� HUBBL

		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("true", "isMomentumFpp", "1");
		BrowserActions.setHUBBLMode("true", "isMomentumFpp.virtual.decline", "1");
		
		BrowserActions.setHUBBLMode("43", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardOnlineMomentum(){ //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateDeliveryCardMomentumRq �� �� ��. 
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineMomentum", "InitiateDeliveryCardMomentumRq");
		xmlDoc = redefineXMLParamsInitiateDeliveryCardMomentumRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateDeliveryCardMomentumRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineMomentum" + "\\" +  "InitiateDeliveryCardMomentumRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);

		System.out.println("��� 20 ");
		System.out.println("xmlMessage = " + xmlMessage);
		Document xmlMessageStr = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		CalculatedVariables.cardNumber = XMLLib.getElementValueFromDocument(xmlMessageStr, "CardNum");
		CalculatedVariables.issueDate = XMLLib.getElementValueFromDocument(xmlMessageStr, "IssueDate");
		CalculatedVariables.extDt = XMLLib.getElementValueFromDocument(xmlMessageStr, "ExpDt");
		System.out.println("cardNumber = " + CalculatedVariables.cardNumber);
		System.out.println("issueDate = " + CalculatedVariables.issueDate);
		System.out.println("extDt = " + CalculatedVariables.extDt);

//		System.out.println("CardNumGetCardHolderRs = " + CardNumGetCardHolderRs);
		//���������� �������� ���������� ��� ���������� �����
		//CalculatedVariables.cardNumber = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "CardNum");
		//CalculatedVariables.issueDate = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "IssueDate");
		//CalculatedVariables.extDt = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "ExpDt");

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardMomentumRq", "Windows-1251", 30);	
		
		//String value = XMLLib.extractValueFromLogByTagName();
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateDeliveryCardMomentumRq>");
//		System.out.println("��� 4 ");
/*		
        String sQuerry = "";
        
		//������ ������ � �� AC �����
				
		sQuerry = "select case when count(*) = 1 then 'OK' else 'BAD' end as Close_Open "
				+ " from ( select * from hubbl.parameters "
				+ " where PRPARAM like '%" + CalculatedVariables.rqUID + "%' )";
		String actualResultFromDB = DataBaseLib.getDataAsString(sQuerry, "HUBBL");
		System.out.println("actualResultFromDB = " + actualResultFromDB );

		
		//���������� ����������� �������� ����������

		CalculatedVariables.actualValues.put("ResultFromDB", actualResultFromDB);
						
		//��������� ����������� �������� ����������

		CalculatedVariables.expectedValues.put("ResultFromDB", "OK");
*/		
/*		if ( !actualResultFromDB.equals("OK")) {
			JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	
            try {

                CommonLib.WriteLog("FAIL", "Fail due to checking ResultFromDB." , Config.resultsPath );

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}

            CalculatedVariables.isTestFailed = true;

            CalculatedVariables.isError = true;

            try {

                CommonLib.postProcessingTest();

            	} catch (Exception e1) {

                // TODO Auto-generated catch block

                e1.printStackTrace();

            	}       


		}
	*/	
		System.out.println("��� 5 ");
	//	System.out.println("���� 2 ������...");
	//	Thread.sleep(2000);

		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDeliveryCardMomentumRs", "Windows-1251", 30);
		System.out.println("���� 1 ������...");
		Thread.sleep(1000);
		
		//��������� �� ���� ���������� xml ��������� InitiateDeliveryCardMomentumRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDeliveryCardMomentumState99Rs.xml", "Windows-1251", "InitiateDeliveryCardMomentumRs");
		
		//��������� �������� ���� statusCode �� ������ InitiateDeliveryCardMomentumRs
		String InitiateDeliveryCardMomentumRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDeliveryCardMomentumState99Rs.xml", "Windows-1251");
		System.out.println("InitiateDeliveryCardMomentumRsStr = " + InitiateDeliveryCardMomentumRsStr);
		Document InitiateDeliveryCardMomentumRs = XMLLib.convertStringToDom(InitiateDeliveryCardMomentumRsStr, "UTF-8");
		String statusCodeInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "StatusCode");
	//	String statusDescInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "StatusDesc");
		String severityInitiateDeliveryCardMomentumRs = XMLLib.getElementValueFromDocument(InitiateDeliveryCardMomentumRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("statusCodeInitiateDeliveryCardMomentumRs", statusCodeInitiateDeliveryCardMomentumRs);
	//	CalculatedVariables.actualValues.put("statusDescInitiateDeliveryCardMomentumRs", statusDescInitiateDeliveryCardMomentumRs);
		CalculatedVariables.actualValues.put("severityInitiateDeliveryCardMomentumRs", severityInitiateDeliveryCardMomentumRs);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("statusCodeInitiateDeliveryCardMomentumRs", "666");
	//	CalculatedVariables.expectedValues.put("statusDescInitiateDeliveryCardMomentumRs", "������ ����������� ����� �� ������ SBOL ����� ��� 349F8030652540459D167CDD0235B831 �������� �������� ����� isMomentumFpp.virtual.decline. ��� ��������� ������ �� ��� ���������� ������� �������� ����� isMomentumFpp.virtual.decline �� false");
		CalculatedVariables.expectedValues.put("severityInitiateDeliveryCardMomentumRs", "Error");

	
		
		
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	

}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsCustAddRs(Document docXML) throws Exception{
		
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String RqTm = "";
		 
		RqUID = CalculatedVariables.rqUID;
		RqTm = CalculatedVariables.rQTm;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("RqTm", RqTm);
		
		rowParams.put("StatusCode", "11");
	//	rowParams.put("Severity", "info");
	//	rowParams.put("StatusDesc", "Successfully");


		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
		
		return docXML;
	}

	public static Document redefineXMLParamsInitiateDeliveryCardMomentumRq(Document docXML) throws Exception{
		 HashMap<String, String> rowParams = new HashMap<String, String>(); 
		 String newUUID = "";
		 String RqUID = "";
		 String OperUID = "";
		 String sQuerry = "";

		 String cardNum = "";
			
		//���������� 16-�� ������� ����� �����, ������� ����������� � ��
		do{
			cardNum = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
			sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNum + "'";
		}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
			System.out.println("GeneratedCardNum = " + cardNum);
			CalculatedVariables.cardNumber = cardNum;
		 
		 LocalDateTime todayDateTime = LocalDateTime.now();
		 
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 RqUID = newUUID;
		 newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		 OperUID = newUUID;
		 CalculatedVariables.testUUID = RqUID;
		 CalculatedVariables.operUID = OperUID;
		 System.out.println("OperUID = " + OperUID);
		 CalculatedVariables.rqUID = RqUID;
		 System.out.println("RqUID = " + RqUID);
			
		 String lastName = "�������-SBOL666-" + CommonLib.getRandomLetters(5);
		 CalculatedVariables.lastName = lastName;
		 String firstName = CommonLib.getRandomLetters(15);
		 CalculatedVariables.firstName = firstName;
		 String middleName = CommonLib.getRandomLetters(15);
		 CalculatedVariables.middleName = middleName;
			
		 String idSeries = CommonLib.getRandomNumericLetters(2) + " " + CommonLib.getRandomNumericLetters(2);
		 CalculatedVariables.idSeries = idSeries;
		 String idNum = CommonLib.getRandomNumericLetters(6);
		 CalculatedVariables.idNum = idNum;
		 
		 //���������  � template.xml
		 //String cardCode = "111707";
		 //String productCode = "IRRDBCMF--";
		 //String eDBOContractFlag = "true";
		 //String contractProductCode = "IRRD--";
		 
			
		 rowParams.put("RqUID", RqUID);
		 rowParams.put("OperUID", OperUID);
		 rowParams.put("RqTm", todayDateTime.toString());
		 rowParams.put("IdSeries", idSeries);
		 rowParams.put("IdNum", idNum);
		 rowParams.put("CardNum", cardNum);
		 
		 rowParams.put("Channel", "SBOL");
		 
		 
		 
				 
		//������ info 
		 rowParams.put("OperDate", Config.businessProcessProp.getProperty("OperDay"));
		 rowParams.put("UserName", Config.businessProcessProp.getProperty("OperatorInfoOperatorLogin"));
		 rowParams.put("OperatorCode", Config.businessProcessProp.getProperty("OperatorInfoOperatorCode"));
		 rowParams.put("UserFIO", Config.businessProcessProp.getProperty("OperatorInfoOperatorName"));
				
		 docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		 //������ ��� 
		 rowParams.clear();
		 rowParams.put("LastName", lastName);
		 rowParams.put("FirstName", firstName);
		 rowParams.put("MiddleName", middleName);
			
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "PersonName");
		
			 //������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.businessProcessProp.getProperty("IssueBankInfoBranchId"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("IssueBankInfoAgencyId"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("IssueBankInfoRegionId"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("IssueBankInfoRbTbBrchId"));
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");

			 //������ info 
		 rowParams.clear();
		 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId"));
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo");

		return docXML;
	}

	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
