package hubbl.createnewcard;

import hubbl.Init;
import hubbl.RunTestBase;
import libs.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.io.FileInputStream;


//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineConnectUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineConnectUDBO2_2018 {

	String operation = "";
	String testCase = "";
	String standName = "";
	String targetDir = "";
	String[] args = new String[4];
	@Before
	public void initTest() throws Exception{

		Config.runTestSuiteProp.load(new FileInputStream(Config.runDirPath + Config.configPath + "RunTestSettings.txt"));

		operation = System.getProperty("testOperation", Config.runTestSuiteProp.getProperty("testOperation"));
		testCase = System.getProperty("testCase", Config.runTestSuiteProp.getProperty("testCase"));
		targetDir = Config.runDirPath + "\\target";
		standName = System.getProperty("testStandName", Config.runTestSuiteProp.getProperty("testStandName"));

		args[0] = operation;
		args[1] = testCase;
		args[2] = targetDir;
		args[3] = standName;

		CalculatedVariables.testOperation = args[0];
		CalculatedVariables.testCase = args[1];

		System.out.println("Run test CalculatedVariables.testOperation: " +  CalculatedVariables.testOperation);
		System.out.println("Run test CalculatedVariables.testCase: " + CalculatedVariables.testCase);


		try {
			if (System.getProperty ( "stand" ) != null) args[3] = System.getProperty ( "stand" );
		}
		catch (Exception e) {
			System.out.println ( "�� ����� ����� �� ���������: " + args[3] );
		}

		CommonLib.resetAllsettings();
		RunTestBase.startInitTests ( args, CalculatedVariables.testOperation, CalculatedVariables.testCase);

		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("60", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
	}
	
	@Test
	public void testCreateNewCardOnlineConnectUDBO2_2018() { //@Param(name = "name") String name, @Param(name = "age") int age){
		try {
			// ����� ����������� ���������� ����-������
			// ������� ��� �������� � ��������� class HabblLib
			// ��������� ����� ��� ������ �������������������� ������� � �� ����� ��� ������
	
	/* ��������� ��� �����:
	 * ������������ xml (��� ��������, ���.�������,  ) 
	 * ��������� �� XSD (�����)
	 * �������� � MQ 
	 * ��������� ��������� 99 � MQ 
	 * ��� ������ �������� ������� �� MQ �������� � ������� way4 � �������� ������ ������� ������
	 * ��������� ���������� ������ � MQ (��� ������, ������)
	 * �������������� �������� �������� ()
	 * ������ ���������� � ������ ()
	 * �������� ���������� (��������� ��� ���)
	 * 	 *  * 
	 */

			//����� ����������������� ������� ����� ������������ �� � ������ ��������.


			//������������ ������  CreateProductPackageRq �� �� �� �� ��

			Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CreateProductPackageConnectUDBORq");
			xmlDoc = XMLLib.redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
			String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageRq");

			HabblLib.sendRQtoMQ(xmlMessage);

			HabblLib.response99test();

			HabblLib.checkWay4getCardHolderRqRs();

			HabblLib.checkWay4CustAddRqRs();

			HabblLib.checkWay4issueCardRqRs();

			HabblLib.NotifyIssueCardResultNfRs();

			System.out.println("������ ������ � �� AC ����� �� ��������� ���������� ������ � ���������� �� �� ������ �� ������� RqUID ");

			HabblLib.GetFinalResponseFromBDhubble();

			HabblLib.CheckjrnTotal0();

			//HabblLib.CheckUDBO(); // �������� � 25 ����������

			HabblLib.CheckjrnTotal25();

			HabblLib.CheckJrnTotal68();

			HabblLib.CheckJrnTotal60();

			//������� ���������� � ��������� �������� ������
			JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);

		}catch(Exception e){
			Assert.fail(e.getMessage());
		}
	}


	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
