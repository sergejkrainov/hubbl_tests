package hubbl.new_no_logs;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;




//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CreateNewCard\\CreateNewCardOnlineConnectUDBO\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CreateNewCardOnlineConnectUDBO2_NEW2018 {

	@Before
	public void initTest() throws Exception{
		
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isIssueFpp", "1");
		BrowserActions.setHUBBLMode("60", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		//������� ���������� � ������ � ���������� �������
		//FileLib.clearFolder(Config.logsPath);
		
		//������� ���������� � ������� ��� Jazz
		//FileLib.clearFolder(CalculatedVariables.pathToCopyResults);
		
	}
	
	@Test
	public void testCreateNewCardConnectUDBO() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
	// ����� ����������� ���������� ����-������
	// ������� ��� �������� � ��������� class HabblLib
	// ��������� ����� ��� ������ �������������������� ������� � �� ����� ��� ������
	
	/* ��������� ��� �����:
	 * ������������ xml (��� ��������, ���.�������,  ) 
	 * ��������� �� XSD (�����)
	 * �������� � MQ 
	 * ��������� ��������� 99 � MQ 
	 * ��� ������ �������� ������� �� MQ �������� � ������� way4 � �������� ������ ������� ������
	 * ��������� ���������� ������ � MQ (��� ������, ������)
	 * �������������� �������� �������� ()
	 * ������ ���������� � ������ ()
	 * �������� ���������� (��������� ��� ���)
	 * 	 *  * 
	 */
		
	//����� ����������������� ������� ����� ������������ �� � ������ ��������.
	
		
	//������������ ������  CreateProductPackageRq �� �� �� �� �� 
	
		Document xmlDoc = XMLLib.loadXMLTemplate("CreateNewCard", "CreateNewCardOnlineConnectUDBO", "CreateProductPackageConnectUDBORq");
		xmlDoc = XMLLib.redefineXMLParamsCreateProductPackageConnectUDBORq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "CreateProductPackageRq");
				
		HabblLib.sendRQtoMQ(xmlMessage);
		
		HabblLib.response99test(); 
				
		HabblLib.checkWay4getCardHolderRqRs();
		
		//HabblLib.checkWay4CustAddRqRs();
		
		HabblLib.checkWay4issueCardRqRs();
		
		HabblLib.NotifyIssueCardResultNfRs();
		
		System.out.println("������ ������ � �� AC ����� �� ��������� ���������� ������ � ���������� �� �� ������ �� ������� RqUID ");
		
		HabblLib.GetFinalResponseFromBDhubble();
		
		HabblLib.CheckjrnTotal0();
		
		//HabblLib.CheckUDBO(); // �������� � 25 ����������
		
		HabblLib.CheckjrnTotal25();
				
		HabblLib.CheckJrnTotal68();
		
		HabblLib.CheckJrnTotal60();
		


		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);	

		

		
		
		
		

}catch(Exception e){
	e.printStackTrace();
}
		
	}


	


	@After
	public void afterTest() throws InterruptedException{
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	


}
