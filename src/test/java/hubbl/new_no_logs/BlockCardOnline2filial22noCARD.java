package hubbl.new_no_logs;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "BlockCard\\BlockCardOnline2notIssuedCard\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class BlockCardOnline2filial22noCARD {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));


        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isBlockListFpp", "1");
		BrowserActions.setHUBBLMode("120", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
	//  �� ������ �����	
		prepareTestData();
		
	}
	
	@Test
	public void testBlockCardOnline2notIssuedCard() { // @Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateBlockCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("BlockCard", "BlockCardOnline", "InitiateBlockCardRq");
		xmlDoc = redefineXMLParamsInitiateBlockCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateBlockCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "BlockCard" + "\\" 
				+ "BlockCardOnline" + "\\" +  "InitiateBlockCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateBlockCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateBlockCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "<ns2:initiateBlockCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:initiateBlockCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ blockCard() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ blockCard() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateBlockCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateBlockCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateBlockCardRs.xml", "Windows-1251", "InitiateBlockCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateBlockCardRs
		String initiateBlockCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateBlockCardRs.xml", "Windows-1251");
		System.out.println("initiateBlockCardRs = " + initiateBlockCardRsStr);
		Document initiateBlockCardRs = XMLLib.convertStringToDom(initiateBlockCardRsStr, "UTF-8");
		String initiateBlockCardRsStatusCode = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "StatusCode");
		String initiateBlockCardRsStatusDesc = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "StatusDesc");
		String initiateBlockCardRsSeverity = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateBlockCardRsStatusCode", initiateBlockCardRsStatusCode);
		CalculatedVariables.actualValues.put("initiateBlockCardRsStatusDesc", initiateBlockCardRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateBlockCardRsSeverity", initiateBlockCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateBlockCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateBlockCardRsSeverity", "Ok");	
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"[����� ������� � ���/��������/" + CalculatedVariables.operUID + " ����� getCardByInfo ���� � ���  ����� �������� ����. ����� �� ������� �������� ��������. ������� ������ ��� ������ ������ ��������. (�������� �����) #####", "Windows-1251", 30);
		
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
	//			"[����� ������� � ���/��������/" + CalculatedVariables.operUID + "] ��� ����� HBLPersonService.findPerson ������: true");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
	//			"HUBBL operUID = " + CalculatedVariables.operUID + " - ��� ����� HBLCardService.getCardByInfo ������: true");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("BlockCard", "BlockCardOnline", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "BlockCard" + "\\" 
				+ "BlockCardOnline" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				" - ���������� ����� (64-�� ����������) #####", "Windows-1251", 30);
		
		JunitMethods.checkLogsNOT(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� ����� � ����");
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ���������� (64) ������� ���������� � ���������� � ���� (JRN_64)");
		
		
		
		
	/*	//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "�������� ���������� 64 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 64 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal64.xml", "Windows-1251", "JrnTotal", "�������� ���������� 64 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 64 � ���
		String jrnTotal64Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal64.xml", "Windows-1251");
		System.out.println("jrnTotal64 = " + jrnTotal64Str);
		Document jrnTotal64 = XMLLib.convertStringToDom(jrnTotal64Str, "UTF-8");
		String typeOperCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "TypeOperCode");
		String subSystemCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "SubSystemCode");
		String ukrBankCodeJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "UKRBankCode");
		String mailAccountJrnTotal64 = XMLLib.getElementValueFromDocument(jrnTotal64, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal64", typeOperCodeJrnTotal64);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal64", subSystemCodeJrnTotal64);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal64", ukrBankCodeJrnTotal64);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal64", "64");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal64", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal64", "IGN");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 1 seconds...");
		Thread.sleep(1000);
		
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal64 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateBlockCardRq
		String initiateBlockCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "BlockCard" + "\\" + "BlockCardOnline" + "\\" +  "InitiateBlockCardRq" + ".xml", "UTF-8");
		Document initiateBlockCardRq = XMLLib.convertStringToDom(initiateBlockCardRqStr, "UTF-8");
		String initiateBlockCardRqBlockCDCode = XMLLib.getElementValueFromDocument(initiateBlockCardRq, "BlockCDCode");
		String initiateBlockCardRqBlockDate = XMLLib.getElementValueFromDocument(initiateBlockCardRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateBlockCardRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("initiateBlockCardRqBlockDate", result.get("blockdate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateBlockCardRqBlockCDCode", initiateBlockCardRqBlockCDCode);
		CalculatedVariables.expectedValues.put("initiateBlockCardRqBlockDate", initiateBlockCardRqBlockDate);
		
		 */
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)", "Windows-1251", 35);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"CardStatusModASyncRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
		String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
		Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
		String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
		String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
		String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");
		CalculatedVariables.rqUID = cardStatusModASyncRqRqUID;
		CalculatedVariables.rQTm = cardStatusModASyncRqRqTm;
		CalculatedVariables.way4operUID = CalculatedVariables.operUID;
		CalculatedVariables.operUID = cardStatusModASyncRqOperUID;
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("BlockCard", "BlockCardOnline", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "BlockCard" + "\\" 
				+ "BlockCardOnline" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 1 seconds...");
		Thread.sleep(1000);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.way4operUID + " - �������� ���������� ������ blockCard() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.way4operUID + " - �������� ���������� ������ blockCard() #####");
		
		
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		
		
		//��������� �� ���� ���������� xml ��������� InitiateBlockCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateBlockCardRsLast.xml", "Windows-1251", "InitiateBlockCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateBlockCardRsLast.xml", "Windows-1251");
		Document initiateBlockCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateBlockCardRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "StatusCode");
		String initiateBlockCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "StatusDesc");
		String initiateBlockCardRsLastSeverity = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastStatusCode", initiateBlockCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastStatusDesc", initiateBlockCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastSeverity", initiateBlockCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastSeverity", "Ok");
		
		

		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	
	/**
	 * 
	 * @param docXML - ������ XML � Document �������������
	 * @return
	 * @throws Exception 
	 */
	public static Document redefineXMLParamsInitiateBlockCardRq(Document docXML, String... args) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		 String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		CalculatedVariables.testUUID = RqUID;
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
		rowParams.put("AcctId", CalculatedVariables.acctId);
		rowParams.put("OperDate", Config.OperDay22);
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin22);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode22);
		
	//	sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
	//	rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		
		String cardNumNotifyIssueCardResultNf;
		//���������� 16-�� ������� ����� �����, ������� ����������� � ��
		do{
			cardNumNotifyIssueCardResultNf = LuhnAlgoritm.correctNumberByLuhn(Config.MasterCardStandart + CommonLib.getRandomNumericLetters(12));
			sQuerry = "select * from deposit.dcard where id_mega=38 and cardmadenumber='" + cardNumNotifyIssueCardResultNf + "'";
		}while(!DataBaseLib.getDataAsString(sQuerry, "COD").equals("null"));						
		System.out.println("GeneratedCardNum = " + cardNumNotifyIssueCardResultNf);
		
				rowParams.put("CardNum", cardNumNotifyIssueCardResultNf);
				
				
		sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		CalculatedVariables.extDt = rowParams.get("ExpDt");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
			
			XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
			XMLLib.deleteTagInDocument(docXML, "Resident");
		//	deleteTagInDocument(docXML, "ControlWord");
		
		//������ info 
	 	 rowParams.clear();
		 rowParams.put("BranchId", Config.businessProcessProp.getProperty("DeliveryBankInfoBranchId22"));
		 rowParams.put("AgencyId", Config.businessProcessProp.getProperty("DeliveryBankInfoAgencyId22"));
		 rowParams.put("RegionId", Config.businessProcessProp.getProperty("DeliveryBankInfoRegionId22"));
		 rowParams.put("RbTbBrchId", Config.businessProcessProp.getProperty("DeliveryBankInfoRbTbBrchId22"));
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "IssueBankInfo");
		 
		return docXML;
	}

	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(1000);
		
	}

}
