package hubbl.upiter2;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import variables.CalculatedVariables;
import variables.Config;

import java.util.HashMap;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CloseCard\\CloseCardOnlineReturn\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class upiterDestroyCardOnline2ZP {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "upiter" + "." + "upiterCreateNewCardOnlineConnectUDBO2ZP"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isDestroyFpp", "1");
		BrowserActions.setHUBBLMode("70", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
	}
	
	@Test
	public void testCloseCardOnlineReturn() { // @Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateDestroyCardRq �� �� �� �� �� 
	
	Document xmlDoc = XMLLib.loadXMLTemplate("DestroyCard", "InitiateDestroyCard", "InitiateDestroyCardRq");
	xmlDoc = redefineXMLParamsInitiateDestroyCardRq(xmlDoc);
	String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateDestroyCardRq");
	FileLib.writeToFile(Config.xmlOutPath  + "DestroyCard" + "\\" 
			+ "InitiateDestroyCard" + "\\" +  "InitiateDestroyCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 

		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDestroyCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateDestroyCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "srvInitiateDestroyCardRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "srvInitiateDestroyCardRequest");
		
		//��������� �� ���� ���������� xml ��������� ns2:hblCloseCardDepositRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\srvInitiateDestroyCardRequest.xml", "Windows-1251", "ns2:srvInitiateDestroyCardRequest");
		
		//��������� �������� ���� operUID �� xml hblCloseCardDepositRequest
		String hblCloseCardDepositRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + 
				FileLib.readFromFile(Config.logsPath + "\\srvInitiateDestroyCardRequest.xml", "Windows-1251");
		System.out.println("srvInitiateDestroyCardRequest = " + hblCloseCardDepositRequestStr);
		Document hblCloseCardDepositRequest = XMLLib.convertStringToDom(hblCloseCardDepositRequestStr, "UTF-8");
		String hblCloseCardDepositRequestoperUID = XMLLib.getElementValueFromDocument(hblCloseCardDepositRequest, "operUID");
		CalculatedVariables.operUID = hblCloseCardDepositRequestoperUID;
		
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
	//			"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ closeDeposit() #####", "Windows-1251", 30);
		
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ closeDeposit() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateDestroyCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateDestroyCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDestroyCardRs.xml", "Windows-1251", "InitiateDestroyCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateDestroyCardRs
		String InitiateDestroyCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDestroyCardRs.xml", "Windows-1251");
		System.out.println("InitiateDestroyCardRs = " + InitiateDestroyCardRsStr);
		Document InitiateDestroyCardRs = XMLLib.convertStringToDom(InitiateDestroyCardRsStr, "UTF-8");
		String InitiateDestroyCardRsStatusCode = XMLLib.getElementValueFromDocument(InitiateDestroyCardRs, "StatusCode");
		String InitiateDestroyCardRsStatusDesc = XMLLib.getElementValueFromDocument(InitiateDestroyCardRs, "StatusDesc");
		String InitiateDestroyCardRsSeverity = XMLLib.getElementValueFromDocument(InitiateDestroyCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsStatusCode", InitiateDestroyCardRsStatusCode);
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsStatusDesc", InitiateDestroyCardRsStatusDesc);
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsSeverity", InitiateDestroyCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,	"��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");	
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "DestroyCard" + "\\" 
				+ "InitiateDestroyCard" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"����������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 189 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 63 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal63.xml", "Windows-1251", "JrnTotal", "�������� ���������� 189 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 63 � ���
		String jrnTotal63Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal63.xml", "Windows-1251");
		System.out.println("jrnTotal63 = " + jrnTotal63Str);
		Document jrnTotal63 = XMLLib.convertStringToDom(jrnTotal63Str, "UTF-8");
		String typeOperCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "TypeOperCode");
		String subSystemCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "SubSystemCode");
		String ukrBankCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "UKRBankCode");
		String mailAccountJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal63", typeOperCodeJrnTotal63);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal63", subSystemCodeJrnTotal63);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal63", ukrBankCodeJrnTotal63);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal63", "189");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal63", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal63", "IGN");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(10000);
	/*	
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate, to_char(trunc(returndate), 'yyyy-mm-dd') as returndate "
				+ "from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal63 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate,returndate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateDestroyCardRq
		String InitiateDestroyCardRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "CloseCard" + "\\" + "CloseCardOnlineReturn" + "\\" +  "InitiateDestroyCardRq" + ".xml", "UTF-8");
		Document InitiateDestroyCardRq = XMLLib.convertStringToDom(InitiateDestroyCardRqStr, "UTF-8");
		String InitiateDestroyCardRqBlockCDCode = XMLLib.getElementValueFromDocument(InitiateDestroyCardRq, "BlockCDCode");
		String InitiateDestroyCardRqBlockDate = XMLLib.getElementValueFromDocument(InitiateDestroyCardRq, "OperDate");
		String InitiateDestroyCardRqReturnDate = XMLLib.getElementValueFromDocument(InitiateDestroyCardRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateDestroyCardRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("InitiateDestroyCardRqBlockDate", result.get("blockdate"));
		CalculatedVariables.actualValues.put("InitiateDestroyCardRqReturnDate", result.get("returndate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRqBlockCDCode", InitiateDestroyCardRqBlockCDCode);
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRqBlockDate", InitiateDestroyCardRqBlockDate);
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRqReturnDate", InitiateDestroyCardRqReturnDate);
		*/
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"CardStatusModASyncRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
		String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
		Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
		String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
		String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
		String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");
		CalculatedVariables.rqUID = cardStatusModASyncRqRqUID;
		CalculatedVariables.rQTm = cardStatusModASyncRqRqTm;
		CalculatedVariables.operUID = cardStatusModASyncRqOperUID;
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "DestroyCard" + "\\" 
				+ "InitiateDestroyCard" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(10000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateDestroyCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateDestroyCardRsLast.xml", "Windows-1251", "InitiateDestroyCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateDestroyCardRsLast.xml", "Windows-1251");
		Document InitiateDestroyCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String InitiateDestroyCardRsLastStatusCode = XMLLib.getElementValueFromDocument(InitiateDestroyCardRsLast, "StatusCode");
		String InitiateDestroyCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(InitiateDestroyCardRsLast, "StatusDesc");
		String InitiateDestroyCardRsLastSeverity = XMLLib.getElementValueFromDocument(InitiateDestroyCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsLastStatusCode", InitiateDestroyCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsLastStatusDesc", InitiateDestroyCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("InitiateDestroyCardRsLastSeverity", InitiateDestroyCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("InitiateDestroyCardRsLastSeverity", "Ok");
		
		/* /������� 30 ������ ��������� ���������
		 * Check contains filelog for HUBBL operUID = f29c65a1f486422d87ef5c2235402ac1 - �������� ���������� ������ closeDeposit() ##### is not ok!The value must be true, but it is false
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				" - �������� ���������� ������ closeDeposit() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ closeDeposit() #####");
		*/
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	public static Document redefineXMLParamsInitiateDestroyCardRq(Document docXML, String... args) throws Exception{
		HashMap<String, String> rowParams = new HashMap<String, String>(); 
		String RqUID = "";
		String newUUID = "";
		 String OperUID = "";
		String sQuerry = "";
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		RqUID = newUUID;
		newUUID = java.util.UUID.randomUUID().toString().replaceAll("-","").toUpperCase(); 
		OperUID = newUUID;
		CalculatedVariables.operUID = OperUID;
		System.out.println("OperUID = " + OperUID);
		CalculatedVariables.rqUID = RqUID;
		System.out.println("RqUID = " + RqUID);
		
		rowParams.put("RqUID", RqUID);
		rowParams.put("OperUID", OperUID);
	//	rowParams.put("AcctId", CalculatedVariables.acctId);
		rowParams.put("OperDate", Config.OperDay);
		
		rowParams.put("UserName", Config.OperatorInfoOperatorLogin);
		rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		
		rowParams.put("UserFIO", "�������� ������������");
		
		rowParams.put("Channel", "ZP");	
		rowParams.put("SPName", "urn:sbrfsystems:99-jupiter");
		
		
	//	XMLLib.deleteTagInDocument(docXML, "UserName");
	//	XMLLib.deleteTagInDocument(docXML, "UserFIO");
	//	XMLLib.deleteTagInDocument(docXML, "OperatorCode");
	//	XMLLib.deleteTagInDocument(docXML, "GroupId");
		
		//rowParams.put("OperatorCode", Config.OperatorInfoOperatorCode);
		
		sQuerry = "SELECT cardmadenumber FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("CardNum", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		sQuerry = "SELECT  to_char(trunc(cardmadeenddate), 'yyyy-mm-dd') as cardmadeenddate FROM deposit.dcard WHERE id_mega=38 and NumContrCard = '" + CalculatedVariables.mailAccount + "'"; 
		rowParams.put("ExpDt", DataBaseLib.getDataAsString(sQuerry, "COD"));
		
		CalculatedVariables.extDt = rowParams.get("ExpDt");
		
		docXML = XMLLib.setXMLValuesFromHashMap(rowParams, docXML, false, null);
			
		//���� PersonInfo ��������� �� CreateProductPackageConnectUDBORq � ������� ������ ����
			String createProductPackageRq = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" + "CreateNewCardOnlineConnectUDBO" + "\\" +
					"CreateProductPackageConnectUDBORq" + ".xml", "UTF-8");
			Document createProductPackageRqDoc = XMLLib.convertStringToDom(createProductPackageRq, "UTF-8");
			XMLLib.replaceTagInDocument(createProductPackageRqDoc, docXML, "PersonInfo", "EDBOContract");
			
			XMLLib.deleteTagInDocument(docXML, "Birthplace");
			XMLLib.deleteTagInDocument(docXML, "Citizenship");
			XMLLib.deleteTagInDocument(docXML, "TaxId");
			XMLLib.deleteTagInDocument(docXML, "ClientCategory");
			XMLLib.deleteTagInDocument(docXML, "ClientStatus");
			XMLLib.deleteTagInDocument(docXML, "Verified");
			XMLLib.deleteTagInDocument(docXML, "Signed");
			XMLLib.deleteTagInDocument(docXML, "ContactInfo");
			XMLLib.deleteTagInDocument(docXML, "Resident");
			XMLLib.deleteTagInDocument(docXML, "ControlWord");
			
		 //������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "DeliveryBankInfo"); 
		
		 //������ info 
	 	rowParams.clear();
		rowParams.put("BranchId", Config.IssueBankInfoBranchId);
		rowParams.put("AgencyId", Config.IssueBankInfoAgencyId);
		rowParams.put("RegionId", Config.IssueBankInfoRegionId);
		rowParams.put("RbTbBrchId", Config.IssueBankInfoRbTbBrchId);
			 						
		 docXML = XMLLib.setXMLValuesIntoTagNameFromHashMap(rowParams, docXML, "BankInfo"); 
		
		return docXML;
	}



	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(5000);
		
	}

}
