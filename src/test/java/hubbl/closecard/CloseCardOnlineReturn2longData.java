package hubbl.closecard;

import java.util.HashMap;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;

import libs.BrowserActions;
import libs.CommonLib;
import libs.DataBaseLib;
import libs.FileLib;
import libs.JunitMethods;
import libs.MQLib;
import libs.XMLLib;
import hubbl.Init;
import variables.CalculatedVariables;
import variables.Config;

//@RunWith(DataDrivenTestRunner.class)
//@DataLoader(filePaths = {Config.XLS_PATH + "CloseCard\\CloseCardOnlineReturn\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class CloseCardOnlineReturn2longData {
	
	@Before
	public void initTest() throws Exception{
	
		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO2longDATA"));

        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "InitiateCloseCardDepositFPP", "1");
		BrowserActions.setHUBBLMode("70", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
	}
	
	@Test
	public void testCloseCardOnlineReturn() { // @Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateCloseCardDepositRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "InitiateCloseCardDepositRq");
		xmlDoc = XMLLib.redefineXMLParamsInitiateCloseCardDepositRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateCloseCardDepositRq");
		FileLib.writeToFile(Config.xmlOutPath  + "CloseCard" + "\\" 
				+ "CloseCardOnlineReturn" + "\\" +  "InitiateCloseCardDepositRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateCloseCardDepositRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateCloseCardDepositRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "hblCloseCardDepositRequest", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "hblCloseCardDepositRequest");
		
		//��������� �� ���� ���������� xml ��������� ns2:hblCloseCardDepositRequest
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\hblCloseCardDepositRequest.xml", "Windows-1251", "ns2:hblCloseCardDepositRequest");
		
		//��������� �������� ���� operUID �� xml hblCloseCardDepositRequest
		String hblCloseCardDepositRequestStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + 
				FileLib.readFromFile(Config.logsPath + "\\hblCloseCardDepositRequest.xml", "Windows-1251");
		System.out.println("hblCloseCardDepositRequest = " + hblCloseCardDepositRequestStr);
		Document hblCloseCardDepositRequest = XMLLib.convertStringToDom(hblCloseCardDepositRequestStr, "UTF-8");
		String hblCloseCardDepositRequestoperUID = XMLLib.getElementValueFromDocument(hblCloseCardDepositRequest, "operUID");
		CalculatedVariables.operUID = hblCloseCardDepositRequestoperUID;
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ closeDeposit() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ closeDeposit() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateCloseCardDepositRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateCloseCardDepositRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateCloseCardDepositRs.xml", "Windows-1251", "InitiateCloseCardDepositRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateCloseCardDepositRs
		String initiateCloseCardDepositRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateCloseCardDepositRs.xml", "Windows-1251");
		System.out.println("initiateCloseCardDepositRs = " + initiateCloseCardDepositRsStr);
		Document initiateCloseCardDepositRs = XMLLib.convertStringToDom(initiateCloseCardDepositRsStr, "UTF-8");
		String initiateCloseCardDepositRsStatusCode = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRs, "StatusCode");
		String initiateCloseCardDepositRsStatusDesc = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRs, "StatusDesc");
		String initiateCloseCardDepositRsSeverity = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsStatusCode", initiateCloseCardDepositRsStatusCode);
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsStatusDesc", initiateCloseCardDepositRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsSeverity", initiateCloseCardDepositRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsSeverity", "Ok");
		
		//������� 30 ������ ��������� ���������
	//	CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,	"��� ����� HBLCardService.getCardByInfo ������: true", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " (�������� �����) - ����� getCardByInfo ���� � ���  ����� �������� ����. ������ ������: true");	
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "CardAcctDInqRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardAcctDInqRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251", "CardAcctDInqRq");
		
		//��������� �������� ���� RqUID,RqTm �� xml CardAcctDInqRq
		String cardAcctDInqRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardAcctDInqRq.xml", "Windows-1251");
		System.out.println("cardAcctDInqRqStr = " + cardAcctDInqRqStr);
		Document cardAcctDInqRq = XMLLib.convertStringToDom(cardAcctDInqRqStr, "UTF-8");
		String cardAcctDInqRqRqUID = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqUID");
		String cardAcctDInqRqRqTm = XMLLib.getElementValueFromDocument(cardAcctDInqRq, "RqTm");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		String localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		int indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		String way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ����� ����� � WAY4 (�������� �����)");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardAcctDInqW I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardAcctDInqRs
		CalculatedVariables.rqUID = cardAcctDInqRqRqUID;
		CalculatedVariables.rQTm = cardAcctDInqRqRqTm;
		xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "CardAcctDInqRs");
		xmlDoc = XMLLib.redefineXMLParamsCardAcctDInqRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", true, "CardAcctDInqRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CloseCard" + "\\" 
				+ "CloseCardOnlineReturn" + "\\" +  "CardAcctDInqRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"������� ����� � ����", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ������� ����� � ����");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log",
				CalculatedVariables.testUUID, "�������� ���������� 63 � ���", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� JrnTotal, �������� ���������� 63 � ���
		XMLLib.extractXMLFromLogByLeftBorder(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\JrnTotal63.xml", "Windows-1251", "JrnTotal", "�������� ���������� 63 � ���", "last");
		
		//��������� �������� ���� typeOperCode,subSystemCode,UKRBankCode �� xml JrnTotal, �������� ���������� 63 � ���
		String jrnTotal63Str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\JrnTotal63.xml", "Windows-1251");
		System.out.println("jrnTotal63 = " + jrnTotal63Str);
		Document jrnTotal63 = XMLLib.convertStringToDom(jrnTotal63Str, "UTF-8");
		String typeOperCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "TypeOperCode");
		String subSystemCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "SubSystemCode");
		String ukrBankCodeJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "UKRBankCode");
		String mailAccountJrnTotal63 = XMLLib.getElementValueFromDocument(jrnTotal63, "MailAccount");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("typeOperCodeJrnTotal63", typeOperCodeJrnTotal63);
		CalculatedVariables.actualValues.put("subSystemCodeJrnTotal63", subSystemCodeJrnTotal63);
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal63", ukrBankCodeJrnTotal63);

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("typeOperCodeJrnTotal63", "63");
		CalculatedVariables.expectedValues.put("subSystemCodeJrnTotal63", "1");
		CalculatedVariables.actualValues.put("ukrBankCodeJrnTotal63", "IGN");
		
		//������� 10 ������ ��������� ���������
		System.out.println("Wait for 10 seconds...");
		Thread.sleep(10000);
		
		String sQuery = "select blockcode, to_char(trunc(blockdate), 'yyyy-mm-dd') as blockdate, to_char(trunc(returndate), 'yyyy-mm-dd') as returndate "
				+ "from DEPOSIT.DCARD where NUMCONTRCARD = '" + mailAccountJrnTotal63 + "'";
		HashMap<String,String> result = DataBaseLib.getAnyRowAsHashMap(sQuery, "blockcode,blockdate,returndate".split(","), "COD");
		
		//��������� �������� ���� BlockCDCode,OperDate �� xml InitiateCloseCardDepositRq
		String initiateCloseCardDepositRqStr = 
			FileLib.readFromFile(Config.xmlOutPath  + "CloseCard" + "\\" + "CloseCardOnlineReturn" + "\\" +  "InitiateCloseCardDepositRq" + ".xml", "UTF-8");
		Document initiateCloseCardDepositRq = XMLLib.convertStringToDom(initiateCloseCardDepositRqStr, "UTF-8");
		String initiateCloseCardDepositRqBlockCDCode = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRq, "BlockCDCode");
		String initiateCloseCardDepositRqBlockDate = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRq, "OperDate");
		String initiateCloseCardDepositRqReturnDate = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRq, "OperDate");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRqBlockCDCode", result.get("blockcode"));
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRqBlockDate", result.get("blockdate"));
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRqReturnDate", result.get("returndate"));

						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRqBlockCDCode", initiateCloseCardDepositRqBlockCDCode);
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRqBlockDate", initiateCloseCardDepositRqBlockDate);
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRqReturnDate", initiateCloseCardDepositRqReturnDate);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		
		localLogStr = FileLib.readFromFile(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251");
		indexSubStr = localLogStr.indexOf("HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ������� ����� � WAY4 (�������� �����)");
		way4UniqueNumber = localLogStr.substring(indexSubStr - 33, indexSubStr - 25);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"CardStatusModASyncRq", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� CardStatusModASyncRq
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251", "CardStatusModASyncRq");
		
		//��������� �������� ���� RqUID,RqTm,OperUID �� xml CardStatusModASyncRq
		String cardStatusModASyncRqStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\CardStatusModASyncRq.xml", "Windows-1251");
		System.out.println("CardStatusModASyncRqStr = " + cardStatusModASyncRqStr);
		Document cardStatusModASyncRq = XMLLib.convertStringToDom(cardStatusModASyncRqStr, "UTF-8");
		String cardStatusModASyncRqRqUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqUID");
		String cardStatusModASyncRqRqTm = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "RqTm");
		String cardStatusModASyncRqOperUID = XMLLib.getElementValueFromDocument(cardStatusModASyncRq, "OperUID");
		CalculatedVariables.rqUID = cardStatusModASyncRqRqUID;
		CalculatedVariables.rQTm = cardStatusModASyncRqRqTm;
		CalculatedVariables.operUID = cardStatusModASyncRqOperUID;
	
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				way4UniqueNumber + " CardStatusMod I   � ������� WAY4 (queue:///ESB.HUBBLE.ASYNC.IN) ���������� ��������� � JMSMessageID: ", "Windows-1251", 30);
		
		//��������� xml ��������� CardStatusModASyncRs
		xmlDoc = XMLLib.loadXMLTemplate("CloseCard", "CloseCardOnlineReturn", "CardStatusModASyncRs");
		xmlDoc = XMLLib.redefineXMLParamsCardStatusModASyncRs(xmlDoc);
		xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Customer_v_1.25", false, "CardStatusModASyncRs");
		FileLib.writeToFile(Config.xmlOutPath  + "CloseCard" + "\\" 
				+ "CloseCardOnlineReturn" + "\\" +  "CardStatusModASyncRs" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
		
		//��������� � ������� ESB.HUBBLE.ASYNC.OUT ��� ������ �� �������  XML ���������. 
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.ASYNC.OUT", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		System.out.println("Wait for 30 seconds...");
		Thread.sleep(30000);
		
		//��������� ���� � ���������� ������� ��� ��������
		FileLib.copyFiles(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", "Windows-1251");
		//��������� ���� � ���������� ������� ��� ��������, ���������� ������ ������ �� �����		
		FileLib.extractLocalLog(Config.logsPath + "\\SystemOut.log", Config.logsPath + "\\SystemOutLocal.log",
						"Windows-1251", CalculatedVariables.testUUID, "RqUID", 207);
		
		//��������� �� ���� ���������� xml ��������� InitiateCloseCardDepositRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateCloseCardDepositRsLast.xml", "Windows-1251", "InitiateCloseCardDepositRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateCloseCardDepositRsLast.xml", "Windows-1251");
		Document initiateCloseCardDepositRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateCloseCardDepositRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRsLast, "StatusCode");
		String initiateCloseCardDepositRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRsLast, "StatusDesc");
		String initiateCloseCardDepositRsLastSeverity = XMLLib.getElementValueFromDocument(initiateCloseCardDepositRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsLastStatusCode", initiateCloseCardDepositRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsLastStatusDesc", initiateCloseCardDepositRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateCloseCardDepositRsLastSeverity", initiateCloseCardDepositRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRsLastStatusCode", "0");
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRsLastStatusDesc", "������ ���");
		CalculatedVariables.expectedValues.put("initiateCloseCardDepositRsLastSeverity", "Ok");
		
		/* /������� 30 ������ ��������� ���������
		 * Check contains filelog for HUBBL operUID = f29c65a1f486422d87ef5c2235402ac1 - �������� ���������� ������ closeDeposit() ##### is not ok!The value must be true, but it is false
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				" - �������� ���������� ������ closeDeposit() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251",
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ closeDeposit() #####");
		*/
		
		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
}catch(Exception e){
	e.printStackTrace();
}
		
	}
	
	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(5000);
		
	}

}
