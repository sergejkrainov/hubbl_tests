package hubbl.blockcard;

import libs.*;
import hubbl.Init;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.w3c.dom.Document;
import hubbl.RunTestBase;
import variables.CalculatedVariables;
import variables.Config;

//@RunWith(DataDrivenTestRunner.class)
// @DataLoader(filePaths = {Config.XLS_PATH + "BlockCard\\BlockCardOnline\\testData.xls"} , loaderType = LoaderType.EXCEL)
public class BlockCardOnline2error50 {
	
	@Before
	public void initTest() throws Exception{

		String [] args = new String[4];
		args[0] = "BlockCard";
		args[1] = "BlockCardOnline2error50";
		args[2] = Config.runDirPath + "\\target";
		args[3] = "st2";
		//	System.out.println("Run test args :" + args);

		CalculatedVariables.testOperation = args[0];
		CalculatedVariables.testCase = args[1];

		System.out.println("Run test CalculatedVariables.testOperation: " +  CalculatedVariables.testOperation);
		System.out.println("Run test CalculatedVariables.testCase: " + CalculatedVariables.testCase);


		try {
			if (System.getProperty ( "stand" ) != null) args[3] = System.getProperty ( "stand" );
		}
		catch (Exception e) {
			System.out.println ( "�� ����� ����� �� ���������: " + args[3] );
		}

		CommonLib.resetAllsettings();
		RunTestBase.startInitTests ( args, CalculatedVariables.testOperation, CalculatedVariables.testCase);


		//������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();

        JUnitCore.runClasses(Class.forName("hubbl." + "createnewcard" + "." + "CreateNewCardOnlineConnectUDBO"));


        //������� ���������� ��� �������� ������ ��� ��������
		CalculatedVariables.actualValues.clear();
		CalculatedVariables.expectedValues.clear();
		
		
		//������������� ����� ������ ������� HUBBL
		Init.initBrowser();
		BrowserActions.loginToHubblAdmin();
		BrowserActions.setHUBBLMode("false", "isBlockListFpp", "1");
		BrowserActions.setHUBBLMode("8", "timeOnline", "1");
		BrowserActions.logOutFromHubblAdmin();
		
		prepareTestData();
		
	}
	
	@Test
	public void testBlockCardOnline() { //@Param(name = "name") String name, @Param(name = "age") int age){
try{
	
		//������������ ������  InitiateBlockCardRq �� �� �� �� �� 
		
		Document xmlDoc = XMLLib.loadXMLTemplate("BlockCard", "BlockCardOnline", "InitiateBlockCardRq");
		xmlDoc = XMLLib.redefineXMLParamsInitiateBlockCardRq(xmlDoc);
		String xmlMessage = XMLLib.createXMLMessage(xmlDoc, "Hubble_Emission_Provider_PIR28Ver011", true, "InitiateBlockCardRq");
		FileLib.writeToFile(Config.xmlOutPath  + "BlockCard" + "\\" 
				+ "BlockCardOnline" + "\\" +  "InitiateBlockCardRq" + ".xml", xmlMessage, "UTF-8");//���������� ��������� xml 
	
		//��������� � ������� ESB.HUBBLE.EMISSION.REQUEST ��� ������ �� �������  XML ���������. 		
		MQLib.sendMessageToMQ(xmlMessage, "ESB.HUBBLE.EMISSION.REQUEST ", Config.mqHost, Config.mqPort, Config.mqQueueManager, Config.mqChannel);
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateBlockCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<InitiateBlockCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "<ns2:initiateBlockCardRq", "Windows-1251", 30);	
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "<ns2:initiateBlockCardRq");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ blockCard() #####", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - � ������ ������ blockCard() #####");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID,
				"HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������", "Windows-1251", 30);
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ������� ����������");
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateBlockCardRs", "Windows-1251", 30);
		
		//��������� �� ���� ���������� xml ��������� InitiateBlockCardRs
		XMLLib.extractXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateBlockCardRs.xml", "Windows-1251", "InitiateBlockCardRs");
		
		//��������� �������� ���� StatusCode,StatusDesc,Severity �� xml InitiateBlockCardRs
		String initiateBlockCardRsStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateBlockCardRs.xml", "Windows-1251");
		System.out.println("initiateBlockCardRs = " + initiateBlockCardRsStr);
		Document initiateBlockCardRs = XMLLib.convertStringToDom(initiateBlockCardRsStr, "UTF-8");
		String initiateBlockCardRsStatusCode = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "StatusCode");
		String initiateBlockCardRsStatusDesc = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "StatusDesc");
		String initiateBlockCardRsSeverity = XMLLib.getElementValueFromDocument(initiateBlockCardRs, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateBlockCardRsStatusCode", initiateBlockCardRsStatusCode);
		CalculatedVariables.actualValues.put("initiateBlockCardRsStatusDesc", initiateBlockCardRsStatusDesc);
		CalculatedVariables.actualValues.put("initiateBlockCardRsSeverity", initiateBlockCardRsSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateBlockCardRsStatusCode", "99");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsStatusDesc", "������ �������");
		CalculatedVariables.actualValues.put("initiateBlockCardRsSeverity", "Ok");	
		
		
		//������� 30 ������ ��������� ���������
		CommonLib.waitForLogContainsString(Config.logsHUBBL + "\\SystemOut.log", Config.logsPath + "\\SystemOut.log", CalculatedVariables.testUUID, "InitiateBlockCardRs", "Windows-1251", 30);
	
	
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", CalculatedVariables.operUID + ": ��� ������� ����������. ONLINE-��� 50 ��������� ��� 0");
		
		JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - ��������� ONLINE-����� #####");
		
		
		
	//	JunitMethods.checkLogs(Config.logsPath + "\\SystemOutLocal.log", "Windows-1251", "HUBBL operUID = " + CalculatedVariables.operUID + " - �������� ���������� ������ blockCard() #####");
		
	
		
		//��������� �� ���� ���������� xml ��������� InitiateBlockCardRs
		XMLLib.extractLastXMLFromLog(Config.logsPath + "\\SystemOutLocal.log", Config.logsPath + "\\InitiateBlockCardRsLast.xml", "Windows-1251", "InitiateBlockCardRs");
		xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileLib.readFromFile(Config.logsPath + "\\InitiateBlockCardRsLast.xml", "Windows-1251");
		Document initiateBlockCardRsLast = XMLLib.convertStringToDom(xmlMessage, "UTF-8");
		String initiateBlockCardRsLastStatusCode = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "StatusCode");
		String initiateBlockCardRsLastStatusDesc = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "StatusDesc");
		String initiateBlockCardRsLastSeverity = XMLLib.getElementValueFromDocument(initiateBlockCardRsLast, "Severity");
		
		//���������� ����������� �������� ����������
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastStatusCode", initiateBlockCardRsLastStatusCode);
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastStatusDesc", initiateBlockCardRsLastStatusDesc);
		CalculatedVariables.actualValues.put("initiateBlockCardRsLastSeverity", initiateBlockCardRsLastSeverity);
						
		//��������� ����������� �������� ����������
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastStatusCode", "50");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastStatusDesc", "50; ������� �������� � ����, ��� �������� ����������");
		CalculatedVariables.expectedValues.put("initiateBlockCardRsLastSeverity", "Error");
		

		//������� ���������� � ��������� �������� ������
		JunitMethods.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		
}catch(Exception e){
	e.printStackTrace();
}
		
	}

	public void way4CardAcctDInqRq() throws Exception {
		Document xmlDoc;
		String xmlMessage;
		}
	
	@After
	public void afterTest(){
		//����� ���������� ����� ��������� �������
		//CalculatedVariables.webDriver.quit();
	}
	
	/**
	 * ������� ���������� �������� ������ ����� �������� ������
	 * @throws Exception
	 */
	private void prepareTestData() throws Exception{
		
		String xmlRs = FileLib.readFromFile(Config.xmlOutPath  + "CreateNewCard" + "\\" 
				+ "CreateNewCardOnlineConnectUDBO" + "\\" +  "NotifyIssueCardResultNfRs" + ".xml", "UTF-8");//���������� ��������� xml
		Document xmlRsDoc = XMLLib.convertStringToDom(xmlRs, "UTF-8");
		String cardNumber = XMLLib.getElementValueFromDocument(xmlRsDoc, "CardNum");
		String sQuerry = "";
		sQuerry = "update deposit.dcard t \n" +
					"set t.numcard = t.cardmadenumber, \n" +
					"t.enddate = t.cardmadeenddate, \n" +
					"t.issuedate = to_date (sysdate, 'dd.mm.yyyy') \n" +
					"where       t.id_mega = 38 \n" +
					"and         t.blockcode  = 0 \n" +
					"and         t.numcard is null \n" +
					"and         t.cardmadenumber in ( \n" +
					"'" + cardNumber + "' \n" +
					")";
		DataBaseLib.ExecuteQueryUpdate(sQuerry, "COD");
		Thread.sleep(1000);
		
	}

}
