#include <MsgBoxConstants.au3>
#include <StringConstants.au3>
#include <AutoItConstants.au3>

; Run("PuTTY.exe putty -P 22 10.68.16.78 -l dpcappl -pw dpc123");
MsgBox($MB_SYSTEMMODAL, "Exec proc", "CmdLine = " & $CmdLine[1] , 1);
;Local $iPID = Run("putty -P 22 10.68.17.92 -l dpctest -pw dpct123");
Local $iPID = Run($CmdLine[1]);
; Wait 10 seconds for the PuTTY window to appear.
Local $hWnd = WinWait("[CLASS:PuTTY]", "", 10);
; Activate the Notepad window using the handle returned by WinWait.
WinActivate($hWnd);
Sleep(5000);

;if double password input
Local $cmdLineArray = StringSplit($CmdLine[2], ",");
If $cmdLineArray[0] > 1 Then
   ;input password
   ControlSend($hWnd, "", "", $cmdLineArray[1]);
   ControlSend($hWnd, "", "", "{Enter}");
   Sleep(15000);Wait for login

   Local $cmdLineArrayAfterlogin = StringSplit($cmdLineArray[2], "&&", 2);

   ;Send("cd /u1/dpcappl/subsys/load/bin
   ;ControlSend($hWnd, "", "", $cmdLineArrayAfterlogin[1] & "{Enter}")
   For $strArr In $cmdLineArrayAfterlogin

	  For $str In StringToASCIIArray($strArr)
		 Local $char = Chr($str);
		 ControlSend($hWnd, "", "", $char, 1);
		 Sleep(100);Wait for input char
	  Next
	  ControlSend($hWnd, "", "", "{Enter}");

   Next

   Sleep(3000);

   ;Send("cd /u1/dpcappl/subsys/load/bin && ./cardBlock.out -file CardBlockTest.txt{Enter}");
   ;ControlSend($hWnd, "", "", $cmdLineArray[2])

Else
   Sleep(10000);Wait for login
   ;Send("cd /u1/dpcappl/subsys/load/bin && ./cardBlock.out -file CardBlockTest.txt{Enter}");
   Local $cmdLineArray = StringSplit($CmdLine[2], "&&", 2);

   ;Send("cd /u1/dpcappl/subsys/load/bin
   ;ControlSend($hWnd, "", "", $cmdLineArray[1] & "{Enter}")
   For $strArr In $cmdLineArray

	  For $str In StringToASCIIArray($strArr)
		 Local $char = Chr($str);
		 ControlSend($hWnd, "", "", $char, 1);
		 Sleep(100);Wait for input char
	  Next
	  ControlSend($hWnd, "", "", "{Enter}");

   Next
   Sleep(3000);Wait for cd

   ;Send("cd /u1/dpcappl/subsys/load/bin && ./cardBlock.out -file CardBlockTest.txt{Enter}");
   ;ControlSend($hWnd, "", "", $CmdLine[2])

EndIf


Sleep($CmdLine[3]);

ProcessClose($iPID);

